/********************************************************************************
* Convert text via Google to voice using Jquery and PHP 
* Written by Vasplus Programming Blog
* Website: www.vasplus.info
* Email: vasplusblog@gmail.com or info@vasplus.info

*********************************Copyright Info***********************************
* This script has been released with the hope that it will be useful
* You are not allowed to sell the script
* Please do not remove this copyright information from the top of this page
* All Copy Rights Reserved by Vasplus Programming Blog
***********************************************************************************/

//This function converts the text to mp3 audio file
function vpb_convert_text_to_voice()
{
	var vpb_text_to_convert = $("#vpb_text_to_convert").val();
	
	if( vpb_text_to_convert == "")
	{
		$("#conversion_status").html('Please enter the text that you intend to convert in the required field to proceed.<br>Thank you!');
		return false;
	}
	else
	{
		var dataString = {'vpb_text_to_convert':vpb_text_to_convert};
		
		$("#conversion_status").html('<center><div align="center" style="font-family: helvetica, arial, \'lucida grande\', sans-serif; font-size:12px;">Please wait <img src="images/loadings.gif" alt="Loading...." align="absmiddle" title="Loading...."/></div></center><br />');
		
		$.post('vpb_converter.php', dataString, function(response) 
		{
			var response_brought = response.indexOf('failed to open stream');
			if(response_brought != -1)
			{
				$("#conversion_status").html('Sorry, the process has been terminated because there was an error.<br>Please try again or contact us to report this issue if the problem persist.<br />Thank You!'); 
				return false;
			}
			else
			{
				var response_brought_b = response.indexOf('Maximum execution time');
				if(response_brought_b != -1)
				{
					$("#conversion_status").html('Sorry, the process has been terminated because there was an error.<br>Please try again or contact us to report this issue if the problem persist.<br />Thank You!'); 
					return false;
				}
				else
				{
					$("#conversion_status").html(response);
					return false;
				}
			}
				
		}).fail(function(error_response) 
		{
			$("#conversion_status").html('Sorry, the process has been terminated because there was an error.<br>Please try again or contact us to report this issue if the problem persist.<br />Thank You!'); 
			return false;
		});		
	}
}