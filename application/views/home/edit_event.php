<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Koala | Embrace your needs</title>
<?php 
	$ctype=$caregiver[0]->care_type_id;
	$pid=$caregiver[0]->pat_id;
	$cid=$caregiver[0]->id;
	$photo=$caregiver[0]->photo;
	$name=$caregiver[0]->name;
?>
<!-- Bootstrap -->
<link href="<?php echo base_url() ?>css/bootstrap.css" rel="stylesheet">
	 <!--<link href="<?php echo base_url() ?>css/bootstrap-theme.css" rel="stylesheet">-->
	 <link href="<?php echo base_url() ?>style.css" rel="stylesheet">
	 <link href="<?php echo base_url() ?>css/res.css" rel="stylesheet">
	 <link href="<?php echo base_url() ?>css/component.css" rel="stylesheet">
	 <link href="<?php echo base_url() ?>css/bootstrap-select.css" rel="stylesheet">
	 <link href="<?php echo base_url() ?>css/font-awesome.css" rel="stylesheet">
	 <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,300,600,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css" />
<link rel="stylesheet" media="all" type="text/css" href="<?php echo base_url('css/jquery-ui-timepicker-addon.css');?>" />
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
    <script src="<?php echo base_url('js/jquery.min.js')?>"></script>
<script type="text/javascript">
function changeact(pass)
{   
var xmlhttp;
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
	var res=xmlhttp.responseText;
	//document.getElementById("owner_lc").value=res;
	document.getElementById("actdel").innerHTML=res;
    }
  }
 
xmlhttp.open("GET","<?php echo  base_url();?>caregiver/getmoreevents/"+pass+"/<?php echo $pid; ?>",true);
xmlhttp.send();
}

</script>
  <script type="text/javascript" src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
		<script type="text/javascript" src="http://code.jquery.com/ui/1.11.0/jquery-ui.min.js"></script>

		<script type="text/javascript" src="<?php echo base_url('js/jquery-ui-timepicker-addon.js');?>"></script>
		<script type="text/javascript" src="<?php echo base_url('js/i18n/jquery-ui-timepicker-addon-i18n.min.js');?>"></script>
		<script type="text/javascript" src="<?php echo base_url('js/jquery-ui-sliderAccess.js');?>"></script>

  </head>
 
<body class="cbp-spmenu-push" onload=dis()>
  
     
      <script type="text/javascript">  

		$(function(){
//$('#tabs').tabs();
		$('#rest_example_1').timepicker({
	hourMin: 0,
	hourMax: 23
});

				
			});
			
		</script>
  
  <style type="text/css">
  .fieldset select{width: 70%;margin: 0px auto; padding:5px; border:solid 1px #ccc; float:right; border-radius:5px;
 }
  
  </style>
  
<!--___________________________________________________Header____________________________________________________________---> 
	<?php $this->load->view("includes/careheader.php") ?>
	
	<!--___________________________________________________Header____________________________________________________________--->
<div class="clearfix"></div>
<div class="container">
		
		<!--<div class="inner-cont top">
		
			<label>Register Yourself as</label><select class="selectpicker">
											<option>Select</option>
											<option>Owner</option>
											<option>Professional</option>
										</select>
		</div>-->
<!--<div class="upgrd-lic">
			<button type="button" class="btn btn-success" aria-label="Left Align" >Acquista un'altra licenza</button>
			<button type="button" class="btn btn-success" aria-label="Left Align" >Aggiorna la licenza</button>
		
		</div>-->
		
		<div class="clearfix"></div>
    
        
        
		<div class="login-hm-cont caregiver-routine">
			<?php  $this->load->view("includes/careleft.php");?>
			<div class="col-md-7 col-lg-7 midsd">
				<?php $this->load->view("includes/caremenu.php");   ?>
<button id="showLeft" class="btn btn-success menu-btn">Menu</button>
				<div class="clearfix"></div>
<form  method="post" id="form-register" name="form-register" action="<?php echo base_url(); ?>caregiver/edit_notification/<?php echo $editnoti[0]->note_id;  ?>" enctype="multipart/form-data">
				<div class="content-cont nw-evnt">
				<h3> <?php echo $this->lang->line('update'); ?> <?php echo $this->lang->line('event'); ?> </h3>
					<div class="new-evnt-cont">	
					<input type="hidden" name="patient_id" value="<?php  echo $caregiver[0]->pat_id; ?>"/>
<input type="hidden" name="caregiver_id" value="<?php echo $cid; ?>" />
<input type="hidden" name="date" value="<?php  echo  $date=date("Y-m-d");  ?>" />

<div class="fieldset"><label> <?php echo $this->lang->line('event'); ?> <?php echo $this->lang->line('type') ?></label>
						<?php foreach($events as $key=>$temp ) { 
						if($temp->evn_id==$editnoti[0]->event_type_id)
						{
						?>
                        <?php  echo $temp->evn_name; ?>
							<?php }}
							$etype=$editnoti[0]->event_type_id;
						 		 ?>
                            			</div>
                                        
                      <?php 
					    if($etype==1)
					    { 
						   $subid=$editnoti[0]->sub_event_id;
						   $ps="select * from parameter_name where id='$subid'";					 
						   $pe=mysql_query($ps) or die(mysql_error());
						   $pr=mysql_fetch_array($pe);
					  ?>          
                         
						<div class="clearfix"></div>
						<div class="fieldset"><label><?php echo $this->lang->line('parameters'); ?> Name</label>
                        <span><?php echo $pr['pname']; ?></span> 	</div>
                        <?php } ?>
                       <?php 
					    if($etype==2)
					    { 
						   $subid=$editnoti[0]->sub_event_id;
						   $ps="select * from medicine where med_id='$subid'";					 
						   $pe=mysql_query($ps) or die(mysql_error());
						   $pr=mysql_fetch_array($pe);
					  ?>          
						<div class="clearfix"></div>
						<div class="fieldset"><label> Name</label>
                        <span><?php echo $pr['med_name']; ?></span> 	</div>
                      <div class="fieldset"><label> Note </label>
                        <span><?php echo $pr['med_note']; ?>&nbsp;</span> 	</div>
                   
                     <div class="fieldset"><label> Meal <?php echo $this->lang->line('time'); ?> </label>
                        <span><?php echo $pr['med_meal_time']; ?></span> 	</div>
                        <?php } ?>
                        

<?php 
					    if($etype==3)
					    { 
						  $subid=$editnoti[0]->sub_event_id;
						  $ps="select * from diets where id='$subid'";					 
						  $pe=mysql_query($ps) or die(mysql_error());
						  $pr=mysql_fetch_array($pe);
						  $ms="select * from foodlike where foodid='$pr[id]'";
						  $me=mysql_query($ms) or die(mysql_error());
						  $mr=mysql_fetch_array($me);
					  ?>          
						<div class="clearfix"></div>
						<div class="fieldset"><label> Name</label>
                        <span><?php echo $pr['food_name']; ?></span> 	</div>
                      <div class="fieldset"><label> <?php echo $this->lang->line('details'); ?> </label>
                        <span><?php echo $pr['food_desc']; ?></span> 	</div>
                     <div class="fieldset"><label> Like </label>
                        <span><?php echo $mr['foodlike']; ?></span> 	</div>
                        <?php } ?>
                        
                        <?php 
					    if($etype==4)
					    { 
						   $subid=$editnoti[0]->sub_event_id;
						   $ps="select * from activities where act_id='$subid'";					 
						   $pe=mysql_query($ps) or die(mysql_error());
						   $pr=mysql_fetch_array($pe);
					  ?>          
						<div class="clearfix"></div>
						<div class="fieldset"><label> Name</label>
                        <span><?php echo $pr['act_name']; ?></span> 	</div>
                      <div class="fieldset"><label> <?php echo $this->lang->line('details'); ?> </label>
                        <span><?php echo $pr['act_details']; ?></span> 	</div>
                   
                     <div class="fieldset"><label> <?php echo $this->lang->line('attachment'); ?> </label>
                        <span><a href="<?php echo base_url() ?>activity/<?php echo $pr['act_attached']; ?>" target="_blank"><?php echo $pr['act_attached']; ?></a></span> 	</div>
                        <?php } ?>
                   
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
						<div class="clearfix"></div>
                        <div class="fieldset"><label><?php echo $this->lang->line('time');   ?></label>
                        <input type="text" value="<?php echo substr($editnoti[0]->note_time,0,5);   ?>"  name="time" required id="datetimepicker_1231" class="selectpicker" style="width:30%; height:1%; text-align:center; font-weight:bold; border:solid 1px #ccc;"/>
					<!-- Time picker code start -->			
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>css/jquery.datetimepicker.css"/>
<script src="<?php echo base_url() ?>js/jquery.datetimepicker.full.js"></script>
<script>
$(function () {
$('#datetimepicker_1231').datetimepicker({
	datepicker:false,
	format:'H:i',
	step:5
});
});
</script>	
<!-- Time picker code end -->		
                        
                        
                        
                        	</div>	
                        
                       <div class="fieldset" id="actdel"><label> <?php echo $this->lang->line('status'); ?> </label>
                        
                        <select class="selectpicker"  name="status" required id="act">
											<option value="1" <?php if($editnoti[0]->status==1){ ?> selected<?php } ?>>Done</option>
											<option value="0" <?php if($editnoti[0]->status==0 or $editnoti[0]->status==-1){ ?> selected<?php } ?>>Not Done</option>
										</select>
                                     
						</div> 
                        
                <?php
				$pid=$this->session->userdata('pid');
				
				?>        		
					</div>
					<div class="btn-cont">
						<div class="col-md-5 pull-left btn no-padding btns3">
<!--<button type="button" class="btn btn-success back-btn" aria-label="Left Align"></button>--><a href="<?php echo base_url(); ?>caregiver/caregiver_home/<?php echo $pid; ?>" class="btn btn-success back-btn"> <?php echo $this->lang->line('cancle');  ?></a> 
<button type="submit" class="btn btn-success back-btn" aria-label="Left Align" style="margin-right:10px;"><?php echo $this->lang->line('confirm'); ?></button>
						</div>
					</div>
				</div>
				</form>
                <div class="clearfix"></div>
			</div>
			<?php $this->load->view("includes/careright.php");   ?>
		</div>		
	</div>
<footer>
	<div class="container">
<p><?php echo $this->lang->line('copy_rights'); ?></p>
		</div>
  </footer>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo base_url() ?>js/bootstrap.min.js"></script>
	<script src="<?php echo base_url() ?>js/bootstrap-select.js"></script>
	
	<script src="<?php echo base_url() ?>js/modernizr.custom.js"></script>
	<script src="<?php echo base_url() ?>js/classie.js"></script>
	<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				menuRight = document.getElementById( 'cbp-spmenu-s2' ),
				menuTop = document.getElementById( 'cbp-spmenu-s3' ),
				menuBottom = document.getElementById( 'cbp-spmenu-s4' ),
				showLeft = document.getElementById( 'showLeft' ),
				showRight = document.getElementById( 'showRight' ),
				showTop = document.getElementById( 'showTop' ),
				showBottom = document.getElementById( 'showBottom' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				showRightPush = document.getElementById( 'showRightPush' ),
				body = document.body;

			showLeft.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeft' );
			};
			

			function disableOther( button ) {
				if( button !== 'showLeft' ) {
					classie.toggle( showLeft, 'disabled' );
				}
				
			}
			$(function(){
  $(document).click(function(e){
if($(e.target).is('#showLeft') || $(e.target).closest('.cbp-spmenu').length>0){
 
} else{
  if($('.cbp-spmenu.cbp-spmenu-open').length>0){
$('#showLeft').trigger('click');
  }
}
  });
});
		</script>
  </body>
</html>