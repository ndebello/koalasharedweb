<!DOCTYPE html>
<html lang="en">
  <head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Koala | Embrace your needs</title>
    <?php 
		$ctype=$caregiver[0]->care_type_id;
		$pid=$caregiver[0]->pat_id;
		$cid=$caregiver[0]->id;
		$photo=$caregiver[0]->photo;
		$name=$caregiver[0]->name;
		
		?>
<!-- Bootstrap -->
<link href="<?php echo base_url() ?>css/bootstrap.css" rel="stylesheet">
	 <!--<link href="<?php echo base_url() ?>css/bootstrap-theme.css" rel="stylesheet">-->
	 <link href="<?php echo base_url() ?>style.css" rel="stylesheet">
	 <link href="<?php echo base_url() ?>css/res.css" rel="stylesheet">
	 <link href="<?php echo base_url() ?>css/component.css" rel="stylesheet">
	 <link href="<?php echo base_url() ?>css/bootstrap-select.css" rel="stylesheet">
	 <link href="<?php echo base_url() ?>css/font-awesome.css" rel="stylesheet">
	<!-- <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,300,600,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css" />-->
<link rel="stylesheet" media="all" type="text/css" href="<?php echo base_url('css/jquery-ui-timepicker-addon.css');?>" />
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

    
<script src="<?php echo base_url('js/jquery.min.js')?>"></script>



<script type="text/javascript">
function changeact(pass)
{   
  
	
	//alert(pass);

var xmlhttp;
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    //document.form.password.focus();
	var res=xmlhttp.responseText;
	//alert(res);
	
	document.getElementById("paralist").innerHTML=res;
	
	
    }
  }
 
xmlhttp.open("GET","<?php echo  base_url();?>caregiver/getparameterlist/"+pass,true);
xmlhttp.send();
}

</script>
<script>
function addinput(a)
{   
  
	text = "";
	//Create an input type dynamically.
    //var element = document.createElement("input");
	var i;
	//alert(a);
  for (i = 0; i <a; i++) {
	 
    text +=  "<p><input type='text' name='column[]' id='l"+i+"'size='15' placeholder='Enter Name '/></p>";
}


document.getElementById("numcal").value=a;

    //Assign different attributes to the element.
    //element.setAttribute("type", "text");
    //element.setAttribute("value", "");
    //element.setAttribute("name", "licence[]");
    document.getElementById("fooBar").innerHTML="<p>"+text+"</p>";
    //Append the element in page (in span).
    //foo.appendChild(element);
	
}

function validate()
{
	var pname=document.getElementById("pname").value;
	
	var vno=document.getElementById("vno").value;
	
	if(pname=='' || pname==null)
	{
		alert("Enter The Parameter Name");
		document.getElementById("pname").focus();
		return false;
		
	}
	
	if(vno=='' || vno==null)
	{
		alert("Select Number Of Columns");
		document.getElementById("vno").focus();
		return false;
		
	}
		var lnum=document.getElementById("numcal").value;
	
	 for (var i = 0; i <lnum; i++)
	{
		if(document.getElementById("l"+i).value=='')
		{
			alert('Enter The  Name ');
			document.getElementById("l"+i).focus();
			return false;
		}
	}
}
	</script>
  </head>
  <body class="cbp-spmenu-push" onload=dis()>
  <style type="text/css">
  .fieldset select{width: 60%;margin: 0px auto; float:right; padding:5px; border:solid 1px #ccc; float:right; border-radius:5px;
 }
 .fieldset input[type="text"]{ width:70%; float:right;}
 #fooBar{ width:42%; margin-right:130px; float:right;}
 #fooBar  input[type="text"]{ width:100%; }
  </style>
  
<!--___________________________________________________Header____________________________________________________________---> 
	<?php $this->load->view("includes/careheader.php") ?>
	
	<!--___________________________________________________Header____________________________________________________________--->
<div class="clearfix"></div>
<div class="container">
		
		<!--<div class="inner-cont top">
		
			<label>Register Yourself as</label><select class="selectpicker">
											<option>Select</option>
											<option>Owner</option>
											<option>Professional</option>
										</select>
		</div>-->
<!--<div class="upgrd-lic">
			<button type="button" class="btn btn-success" aria-label="Left Align" >Acquista un'altra licenza</button>
			<button type="button" class="btn btn-success" aria-label="Left Align" >Aggiorna la licenza</button>
		
		</div>-->
		
		<div class="clearfix"></div>
    
        
        
		<div class="login-hm-cont caregiver-routine">
        <?php //echo $parameters[0]->id; ?>
        
        
			<?php  $this->load->view("includes/careleft.php");?>
			<div class="col-md-7 col-lg-7 midsd">
				<?php $this->load->view("includes/caremenu.php");   ?>
<button id="showLeft" class="btn btn-success menu-btn">Menu</button>
				<div class="clearfix"></div>
<form  method="post" id="form-register" name="form-register" action="<?php echo base_url(); ?>caregiver/edit_parameters/<?php echo $parameters[0]->id; ?>" enctype="multipart/form-data" onsubmit="return validate()">
				<div class="content-cont nw-evnt">
				<h3> <?php echo $this->lang->line('edit'); ?> <?php echo $this->lang->line('parameters'); ?> </h3>
					<div class="new-evnt-cont">	
						 <input type="hidden" name="numclu" id="numcal" /> 
					<input type="hidden" name="patient_id" value="<?php  echo $caregiver[0]->pat_id; ?>"/>
<input type="hidden" name="caregiver_id" value="<?php echo $cid; ?>" />
<input type="hidden" name="date" value="<?php  echo  $date=date("Y-m-d");  ?>" />

						<div class="fieldset"><label> <?php echo $this->lang->line('parameters'); ?> Name </label>
                       <input type="text" value="<?php echo $parameters[0]->pname; ?>" name="pname" id="pname" required />
                         <?php 
								$pid=$parameters[0]->id;
								$ms="select * from  parameter_columns  where para_id='$pid'"; 
								$me=mysql_query($ms) or die(mysql_error());
								$j=0;
								while($mr=mysql_fetch_array($me))
								{
									$j++;
								 ?>
                                 <input type="text" name="column1[]"  value="<?php echo $mr['paraname']; ?>"/> 
                                <?php }
								 $mn= 5-$j;
								 ?>               
                                        
                                        </div>
										<div class="clearfix"></div>
							
						<div class="clearfix"></div>
                    <?php if($j<5) { ?>       
                  <div class="fieldset"><label> Number Measurement </label>
                      <select name="lno" onchange="addinput(this.value)" class="selectpicker" id="vno">
                                    <option value="">-<?php echo $this->lang->line('select'); ?>-</option>
                                    <?php  for($i=1;$i<=$mn;$i++) {   ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                    <?php } ?>
                                    </select>
                                        </div>
					
                    <?php  }  ?>
                    
                    <div class="clearfix"></div>
							
						<div class="clearfix"></div>
		                <div id="fooBar">
                       	</div>	
                                
           <?php $pid=$this->session->userdata('pid'); ?>     		
					</div>
					<div class="btn-cont">
						<div class="col-md-5 pull-left btn no-padding btns3">
                       <a href="<?php echo base_url() ?>caregiver/showparameter/<?php  echo $pid;?>">	<button type="button" class="btn btn-success back-btn" aria-label="Left Align" > <?php echo $this->lang->line('cancle'); ?> </button></a>
<button type="submit" name="Confirm" class="btn btn-success back-btn" aria-label="Left Align" style="margin-right:10px;"><?php echo $this->lang->line('confirm'); ?></button>										
						</div>
					</div>
				</div>
				</form>
                <div class="clearfix"></div>
				
				
			</div>
			<?php $this->load->view("includes/careright.php");   ?>
			
		</div>		
		
	</div>

<footer>
	<div class="container">
<p><?php echo $this->lang->line('copy_rights'); ?></p>
		</div>
  </footer>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo base_url() ?>js/bootstrap.min.js"></script>
	<script src="<?php echo base_url() ?>js/bootstrap-select.js"></script>
	
	<script src="<?php echo base_url() ?>js/modernizr.custom.js"></script>
	<script src="<?php echo base_url() ?>js/classie.js"></script>
	<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				menuRight = document.getElementById( 'cbp-spmenu-s2' ),
				menuTop = document.getElementById( 'cbp-spmenu-s3' ),
				menuBottom = document.getElementById( 'cbp-spmenu-s4' ),
				showLeft = document.getElementById( 'showLeft' ),
				showRight = document.getElementById( 'showRight' ),
				showTop = document.getElementById( 'showTop' ),
				showBottom = document.getElementById( 'showBottom' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				showRightPush = document.getElementById( 'showRightPush' ),
				body = document.body;

			showLeft.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeft' );
			};
			

			function disableOther( button ) {
				if( button !== 'showLeft' ) {
					classie.toggle( showLeft, 'disabled' );
				}
				
			}
			$(function(){
  $(document).click(function(e){
if($(e.target).is('#showLeft') || $(e.target).closest('.cbp-spmenu').length>0){
 
} else{
  if($('.cbp-spmenu.cbp-spmenu-open').length>0){
$('#showLeft').trigger('click');
  }
}
  });
});
		</script>
  </body>
</html>