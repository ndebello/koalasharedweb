<div class="login-hm-cont caregiver-routine">
 

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!--<a class="navbar-brand" href="#">Koala</a>-->
            </div>
            <!-- Top Menu Items -->
           <link href="<?php echo base_url('css/admin/custom.css');?>" rel="stylesheet">
           
            <ul class="nav navbar-right top-nav">
                
                
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>
					<?php echo 'Admin'; ?>
					<?php //echo $logged_users['username']; ?>
					<b class="caret"></b></a>
                    
					<ul class="dropdown-menu">
<!--<li>
    <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
</li>
<li>
    <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
</li>   <?php //echo base_url(); ?>admin/edit_admin  
<li>
    <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
</li> -->
<li class="divider"></li>
<li>
    <a href="<?php echo base_url(); ?>auth/logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
</li>
                    </ul>
					
					
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
               

			   <ul class="nav navbar-nav side-nav">
                   <!-- <li class="active">
<a href="<?php echo base_url(); ?>admin/index"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>-->
					
					
					<li>
<a href="<?php echo base_url(); ?>admin/addowner_rsa" data-toggle="collapse" data-target="#users"><i class="fa fa-fw fa-arrows-v"></i> Add Owner  </a>
<!--<ul id="users" class="collapse">
    <li>
         <a href="<?php echo base_url(); ?>admin/addowner_rsa"><i class="fa fa-fw fa-file"></i> Add Owner </a>
    </li>
    <li>
        <a href="<?php echo base_url(); ?>admin/addowner_consortuim"><i class="fa fa-fw fa-file"></i> Consortuim </a>
    </li>
							<li>
    <a href="<?php //echo base_url(); ?>admin/users_caregivers"><i class="fa fa-fw fa-file"></i> Caregivers </a>
    </li>     
    </ul>-->
                    </li>
                    
                    <li <?php if($menu=="owa"){ ?> class="active" <?php  } ?>>
<a href="<?php echo base_url(); ?>admin/list_owners" data-toggle="collapse" data-target="#users1"><i class="fa fa-fw fa-arrows-v"></i> Show Owner </a>
                    </li>

					<li <?php if($menu=="med"){ ?> class="active" <?php  } ?>>
<a href="<?php echo base_url(); ?>admin/list_medicine" data-toggle="collapse" data-target="#medicine"><i class="fa fa-fw fa-arrows-v"></i> Medicine <!--<i class="fa fa-fw fa-caret-down"></i>--></a>
<!--<ul id="medicine" class="collapse">
     <li>
      <a href="<?php echo base_url(); ?>admin/add_medicine"> <i class="fa fa-fw fa-file"></i>Add Medicine </a>
    </li>
							<li>
      <a href="<?php echo base_url(); ?>admin/list_medicine"> <i class="fa fa-fw fa-file"></i>Medicine List</a>
    </li>
							
</ul>-->
                    </li>
					<li <?php if($menu=="dit"){ ?> class="active" <?php  } ?>>
<a href="<?php echo base_url(); ?>admin/list_diets" data-toggle="collapse" data-target="#diet"><i class="fa fa-fw fa-arrows-v"></i>Diet <!--<i class="fa fa-fw fa-caret-down"></i>--></a>
<!--<ul id="diet" class="collapse">
    <li>
<a href="<?php echo base_url(); ?>home/add_diets"> <i class="fa fa-fw fa-file"></i>Add diet</a>
    </li>
<li>
<a href="<?php echo base_url(); ?>home/list_diets"><i class="fa fa-fw fa-file"></i> Diet List</a>
</li>
</ul>-->

</li>
					<li <?php if($menu=="pra"){ ?> class="active" <?php  } ?>>
<a href="<?php echo base_url(); ?>admin/list_parameters" data-toggle="collapse" data-target="#parameter"><i class="fa fa-fw fa-arrows-v"></i>Parameter <!--<i class="fa fa-fw fa-caret-down"></i>--></a>
<!--<ul id="parameter" class="collapse">
    <li>
      <a href="<?php echo base_url(); ?>home/add_parameters"> <i class="fa fa-fw fa-file"></i>Add Parameter</a>
    </li>
							<li>
							 <a href="<?php echo base_url(); ?>home/list_parameters"><i class="fa fa-fw fa-file"></i> Parameter List</a>
							</li>
</ul>
     -->               </li>
                    
        <li <?php if($menu=="act"){ ?> class="active" <?php  } ?>>
<a href="<?php echo base_url(); ?>admin/activity_list" data-toggle="collapse" data-target="#activity"><i class="fa fa-fw fa-arrows-v"></i> Activity <!--<i class="fa fa-fw fa-caret-down"></i>--></a>
<!--<ul id="activity" class="collapse">
    <li>
      <a href="<?php echo base_url(); ?>admin/add_activity"> <i class="fa fa-fw fa-file"></i>Add Activity </a>
    </li>
							<li>
							 <a href="<?php echo base_url(); ?>admin/activity_list "><i class="fa fa-fw fa-file"></i> Activity List</a>
							</li>
</ul>-->
                    </li>  
                    
                    
 <li <?php if($menu=="prf"){ ?> class="active" <?php  } ?>>
<a href="<?php echo base_url(); ?>admin/list_profetionals" data-toggle="collapse" data-target="#parameter"><i class="fa fa-fw fa-arrows-v"></i>   Professional  <!--<i class="fa fa-fw fa-caret-down"></i>--></a>
<!--<ul id="parameter" class="collapse">
    <li>
      <a href="<?php echo base_url(); ?>home/add_parameters"> <i class="fa fa-fw fa-file"></i>Add Parameter</a>
    </li>
							<li>
							 <a href="<?php echo base_url(); ?>home/list_parameters"><i class="fa fa-fw fa-file"></i> Parameter List</a>
							</li>
</ul>
     -->               </li>                   
                    
                    
<li <?php if($menu=="care"){ ?> class="active" <?php  } ?>>
<a href="<?php echo base_url(); ?>admin/list_caregivers" data-toggle="collapse" data-target="#parameter"><i class="fa fa-fw fa-arrows-v"></i>   Caregivers  <!--<i class="fa fa-fw fa-caret-down"></i>--></a>
<!--<ul id="parameter" class="collapse">
    <li>
      <a href="<?php echo base_url(); ?>home/add_parameters"> <i class="fa fa-fw fa-file"></i>Add Parameter</a>
    </li>
							<li>
							 <a href="<?php echo base_url(); ?>home/list_parameters"><i class="fa fa-fw fa-file"></i> Parameter List</a>
							</li>
</ul>
     -->               </li>                  
                    
                    
                              
                   <!-- <li>
<a href="javascript:;" data-toggle="collapse" data-target="#periods"><i class="fa fa-fw fa-arrows-v"></i>Periods <i class="fa fa-fw fa-caret-down"></i></a>
<ul id="periods" class="collapse">
    <li>
      <a href="<?php //echo base_url(); ?>home/add_periods"> <i class="fa fa-fw fa-file"></i>Add Period</a>
    </li>
							<li>
							 <a href="<?php //echo base_url(); ?>home/list_periods"><i class="fa fa-fw fa-file"></i> Period List</a>
							</li>
</ul>
                    </li>
					<li>
<a href="javascript:;" data-toggle="collapse" data-target="#foods"><i class="fa fa-fw fa-arrows-v"></i> Foods <i class="fa fa-fw fa-caret-down"></i></a>
<ul id="foods" class="collapse">
     <li>
      <a href="<?php //echo base_url(); ?>home/add_foods"> <i class="fa fa-fw fa-file"></i>Add Foods </a>
    </li>
							<li>
      <a href="<?php //echo base_url(); ?>home/list_foods"> <i class="fa fa-fw fa-file"></i>Food List</a>
    </li>
							
</ul>
                    </li>
                    -->
       <!--             <li>
<a href="javascript:;" data-toggle="collapse" data-target="#account"><i class="fa fa-fw fa-arrows-v"></i> Account setting <i class="fa fa-fw fa-caret-down"></i></a>
<ul id="account" class="collapse">
    <li>
      <a href="#"> <i class="fa fa-fw fa-file"></i>Profile Setting</a>
    </li>
							<li>
							 <a href="#"><i class="fa fa-fw fa-file"></i> Change Password </a>
							</li>
</ul>
                    </li>-->
							
					
					
					
					
                  <!--  <li>
<a href="charts.html"><i class="fa fa-fw fa-bar-chart-o"></i> Charts</a>
                    </li>
                    <li>
<a href="tables.html"><i class="fa fa-fw fa-table"></i> Tables</a>
                    </li>
                    <li>
<a href="forms.html"><i class="fa fa-fw fa-edit"></i> Forms</a>
                    </li>
                    <li>
<a href="bootstrap-elements.html"><i class="fa fa-fw fa-desktop"></i> Bootstrap Elements</a>
                    </li>
                    <li>
<a href="bootstrap-grid.html"><i class="fa fa-fw fa-wrench"></i> Bootstrap Grid</a>
                    </li>
                    <li>
<a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i> Dropdown <i class="fa fa-fw fa-caret-down"></i></a>
<ul id="demo" class="collapse">
    <li>
        <a href="#">Dropdown Item</a>
    </li>
    <li>
        <a href="#">Dropdown Item</a>
    </li>
</ul>
                    </li>
                    
                    <li>
<a href="blank-page.html"><i class="fa fa-fw fa-file"></i> Blank Page</a>
                    </li>-->
					
					
                </ul>
				
				
				
				
				
				
            </div>
            <!-- /.navbar-collapse -->
        </nav>
        </div>