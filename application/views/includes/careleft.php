<div class="col-md-2 col-lg-2 leftsd" >
				<div class="pat-dp">
                <?php 
				 $pimg=$patient[0]->photo;
				if(empty($pimg))
				{
				 $src= base_url()."img/img-holder2.jpg";
				?>
					<!-- <img src="<?php echo base_url(); ?>img/img-holder2.jpg" alt="patient image" /> -->
				<?php } else { 
				 $src= base_url()."uploads/".$pimg;?>
               <!-- <img src="<?php echo base_url(); ?>uploads/<?php echo $pimg; ?>" alt="patient image" />-->
                <?php } ?>
				<div class="fileinput-new thumbnail" style="background-image:url(<?php echo $src; ?>); background-position:center center; background-repeat:no-repeat; background-size:cover; width:176px; height:200px;">
				</div>
				
                	<div class="clearfix"></div>
					<span class="pat-name"> <?php echo $patient[0]->name;  ?> </span>
				</div>
				<div class="clearfix"></div>
				<div class="pat-desc" style="background:#EEFCED; width:100%; padding-left:5%; padding-top:3%; margin-top:0%;">
					<ul>
		<li><strong><?php echo $this->lang->line('age'); ?></strong><br><?php echo $patient[0]->age; ?>yrs</li>
		<li><strong><?php echo $this->lang->line('contact'); ?></strong><br><?php echo $patient[0]->phone; ?></li>
		<li><strong><?php echo $this->lang->line('gender'); ?></strong><br> <?php if($patient[0]->gender=="male"){
			 echo $this->lang->line('male');
		} else {
			 echo $this->lang->line('female');
		}?></li>
		<li><strong><?php echo $this->lang->line('pathology'); ?></strong><br><?php echo $patient[0]->pathology; ?></li>
		<li><strong><?php echo $this->lang->line('allergies'); ?></strong><br><?php echo $patient[0]->allergies; ?></li>
		<li><strong><?php echo $this->lang->line('intolerances'); ?></strong><br><?php echo $patient[0]->intolerances; ?></li>
					</ul>
				</div>
			</div>