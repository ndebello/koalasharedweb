<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Koala | Embrace your needs</title>

    <!-- Bootstrap -->
   

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <!--___________________________________________________Header____________________________________________________________---> 
	<?php $this->load->view('includes/header.php'); ?>
	<!--___________________________________________________Header____________________________________________________________--->
<div class="clearfix"></div>
    <div class="container">
	<?php echo form_open('auth/register_own_rsa_step3', array('name'=>'form-register', 'id'=>'form-register', 'novalidate'=>'novalidate'));?>
		<!--<div class="inner-cont top">
		
			<label>Register Yourself as</label><select class="selectpicker">
											<option>Select</option>
											<option>Owner</option>
											<option>Professional</option>
										</select>
		</div>-->
		<div class="inner-cont btm">
        
 <?php if(!empty($flash)) { ?>
<div class="error" align="center">
<p>
<b><font color="#FF0000"><?php echo $flash; ?></font></b>
</p>
</div>
<?php } ?>
        
        
		<!--<div class="nmber"><p><span class="badge">1</span><span class="badge">2</span><span class="txt">Licence Type</span></p> </div>-->
		<div class="clearfix"></div>
			<div class="form-inn-cont2 cons">
				<div class="reg2-select">
					<div class="fieldset">
						<label>Enter License Number<span class="rqrd">*</span></label>
                        <input type="text" id="licence" name="licence" value="<?php echo @$this->session->userdata('cnumber'); ?>" required/>
                        
                        
                        
					</div>
				<div class="clearfix"></div>
					
				</div>
										<div class="clearfix"></div>
				<div class="col-md-5 pull-left btn no-padding">
							<a href="<?php echo base_url("/"); ?>"><input type="button" value="cancel" class="btn btn-default"></a>
							<!--<input type="submit" value="Forward" class="btn btn-success">-->
							<button type="submit" class="btn btn-success" aria-label="Left Align">
										  Submit <span class="glyphicon glyphicon-upload nxt-arrow" aria-hidden="true" ></span>
										</button>
							
						</div>
						<div class="clearfix"></div>
			</div>		
		</div>
	<?php echo form_close(); ?>
	</div>

<?php $this->load->view('includes/footer.php'); ?>
  </body>
</html>