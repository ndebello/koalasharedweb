<?php $this->load->view('includes/adminheader.php');?>

	<!-- Bootstrap Core CSS -->
 <style type="text/css">
.hidden1 { position:fixed; visibility:hidden;background: #fff; border: 1px solid maroon; padding: 10px;width:60%; height:500px;z-index:999; overflow:auto; overflow-style:marquee-line !important;  border-radius:5px; box-shadow: 5px 5px 5px 5px #888; font-family:Calibri; font:Arial, Helvetica, sans-serif;   opacity:1; margin-left:1%; margin-top:-229px;} 

table tr td{ color:#000;}
table tr td select{ width:60%; height:40px;}
table tr td input[type="text"],input[type="password"]{ width:60%; }
</style>
	
<?php
$segs = $this->uri->segment_array();
//print_r($segs);
//$ownerid = $segs[3];
?>
		<script src="<?php echo base_url('js/datetimepicker.js');?>"></script>	
	
  
    
    
    
    
    
    
    	
		<?php error_reporting(0);    ?>
        <div id="page-wrapper">
<?php  $this->load->view("includes/admin_left.php");?>
            <div class="container" style="color:#333;">
		
<script src="<?php echo base_url('js/datetimepicker.js');?>"></script>
		<!--<div class="inner-cont top">
		
			<label>Register Yourself as</label><select class="selectpicker">
											<option>Select</option>
											<option>Owner</option>
											<option>Professional</option>
										</select>
		</div>-->
<!--<div class="upgrd-lic">
			<button type="button" class="btn btn-success" aria-label="Left Align" >Buy another licence</button>
			<button type="button" class="btn btn-success" aria-label="Left Align" >Upgrade licence</button>
		
		</div>-->
		
		<div class="clearfix"></div>
		  <!-- Page Heading -->
   <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           Medicine <small></small>
                           
                         
                           
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Add medicine
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
				 
					<div class="col-lg-12">
                    <font color="#FF0000"> <b> <?php if(!empty($flash)) { echo $flash;   }   ?> </b> </font>
                    
                    
						<!--<h2>User table</h2>-->
						<div class="table-responsive">
                        
						   <form  name="add-forms" id="add-forms"  method="post" action="<?php echo base_url(); ?>admin/add_medicine">
								
							<table class="table table-bordered table-hover">
							   
								<tr>
								<th>Medicine Name<em class="required">*</em></th>
								<td><input type="text" name="medicine_name" id="medicine_name" required/><span class="error_message" ><?php echo form_error('medicine_name'); ?></span></td>
								<tr>
								<th>Medicine Format <em class="required">*</em></th>
								<td>
                               <!-- <input type="text"  name="medicine_format" id="medicine_format" required/>-->
                         <select name="medicine_format" id="medicine_format">
                                <option value="">- select type-</option>
                                <option>injection</option>
                                <option>capsule</option>
                                <option>tablet</option>
                                <option>syrup</option>
                                <option>aerosol</option>
                                <option>suppository</option>
                                <option>ointment</option>
                                <option>eye drops</option>
                                <option>transdermal patch</option>
                                <option>drops</option>
                         </select>
                                
                                
                                
                                <span class="error_message" >
								
								
								
								<?php echo form_error('medicine_format'); ?></span></td>
								<tr valign="middle">
								<th valign="middle">Note <em class="required">*</em> </th>
								<td><textarea  name="note" id="note" required style="width:400px;"/></textarea><span class="error_message" ><?php echo form_error('note'); ?></span></td>
								</tr>
								<tr>
								<th>Quantity </th>
								<td><input type="text" name="quantity" id="quantity" /><span class="error_message" ><?php echo form_error('quantity'); ?></span></td>
								</tr>
								<tr>
								<th>Purchase Date </th>
								<td><input id="demo1"  type="text" name="purchase_date" />
                                <a href="javascript:NewCal('demo1','YYYYMMDD',false,24)"><img src="<?php echo base_url(); ?>img/cal.png" width="16" height="16" border="0" alt="Pick a date" style="margin-left:5px;"></a>
                                
                                <span class="error_message" ><?php echo form_error('purchase_date'); ?></span></td>
								</tr>
								<tr>
								<th>Expiration Date </th>
								<td><input id="demo2" type="text" name="expiration_date" />
                                <a href="javascript:NewCal('demo2','YYYYMMDD',false,24)"><img src="<?php echo base_url(); ?>img/cal.png" width="16" height="16" border="0" alt="Pick a date"></a>
                                
                                <span class="error_message" ><?php echo form_error('expiration_date'); ?></span></td>
								</tr>
								<tr>
								<th>Dosage </th>
								<td><input  type="text" name="dosage" id="dosage" /> <small> Per Day </small><span class="error_message" ><?php echo form_error('dosage'); ?></span></td>
								</tr>
								<tr>
								<th>Meal Time </th>
								<td>
                                <select name="meal_time" id="meal_time" >
                                            <option value="">-select-</option>
                                            <option>Before Meal</option>
                                            <option>Afer Meal</option>
                                            <option>Both</option>
                                 </select>
                                <span class="error_message" ><?php echo form_error('meal_time'); ?></span></td>
								</tr>
								<tr>
								<th>Price </th>
								<td><input type="text" name="price" id="price" /><span class="error_message" ><?php echo form_error('price'); ?></span></td>
								</tr>
								<tr>
                                <td></td>
								<td>
                             
                                  <a href="<?php echo base_url(); ?>admin/list_medicine"><input type="button" value="cancel" class="btn btn-default"></a> 
                                        &nbsp;&nbsp;
                                        <button type="submit" class="btn btn-success" aria-label="Left Align">
										  Add 
										</button>
                                
                                
                                </td>

								</tr>
							</table>
						  </form>
						</div>
					</div>

	
                </div>
                <!-- /.row -->

            </div>
                <!-- /.row -->
             <!-- /.row -->
                <!-- /.row -->
			
			
			
		
	</div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

		
<?php $this->load->view('includes/footer.php');?>