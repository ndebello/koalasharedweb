<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Koala | Embrace your needs</title>
    <!-- Bootstrap -->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
      <link href="<?php echo base_url(); ?>css/bootstrap.css" rel="stylesheet">
	 <!--<link href="css/bootstrap-theme.css" rel="stylesheet">-->
	 <link href="<?php echo base_url(); ?>style.css" rel="stylesheet">
	 <link href="<?php echo base_url(); ?>css/res.css" rel="stylesheet">
	 <link href="<?php echo base_url(); ?>css/jasny-bootstrap.min.css" rel="stylesheet">
	 <link href="<?php echo base_url(); ?>css/bootstrap-select.css" rel="stylesheet">
  </head>
<body onLoad="initialize()">
    <!--___________________________________________________Header____________________________________________________________---> 
	<?php $this->load->view('includes/header.php');?>
<!--___________________________________________________Header____________________________________________________________--->
<div class="clearfix"></div>

 <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places"></script>
    <script>
	// This example displays an address form, using the autocomplete feature
	// of the Google Places API to help users fill in the information.
		var placeSearch, autocomplete;
		var componentForm = {
		   postal_code: 'short_name'
		};

function initialize() {

autocomplete = new google.maps.places.Autocomplete(
 
  /** @type {HTMLInputElement} */(document.getElementById('autocomplete')),
      { types: ['geocode'] });
  // When the user selects an address from the dropdown,
  // populate the address fields in the form.
  google.maps.event.addListener(autocomplete, 'place_changed', function() {
    fillInAddress();
  });
}

function initialize2()
{ 
   
	//alert('a');
	var address=document.getElementById("autocomplete").value;
	//alert(address);
	var locationDetails=address;
            var  value=locationDetails.split(",");
            
            count=value.length;
            
             country=value[count-1];
             state=value[count-2];
             city=value[count-3];
		
		//alert(city);
		 document.getElementById("city").value=city;
	document.getElementById("state").value=country;
	
	
}



        function initialize1(a) {
        var address = a;
            geocoder = new google.maps.Geocoder();
            geocoder.geocode({
            'address': address
            }, function(results, status) {      
                var lat=document.getElementById("lat").value=results[0].geometry.location.lat();    
                var lng=document.getElementById("lng").value=results[0].geometry.location.lng();        
            });
        }
        google.maps.event.addDomListener(window, 'load', initialize1);
   








// [START region_fillform]
function fillInAddress() {
  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();

  for (var component in componentForm) {
    document.getElementById(component).value = '';
    document.getElementById(component).disabled = false;
  }

  // Get each component of the address from the place details
  // and fill the corresponding field on the form.
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
      document.getElementById(addressType).value = val;
    }
  }
}
// [END region_fillform]

// [START region_geolocation]
// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = new google.maps.LatLng(
          position.coords.latitude, position.coords.longitude);
	  
	  
	// alert(geolocation);
	  
	  
      autocomplete.setBounds(new google.maps.LatLngBounds(geolocation,
          geolocation));
	  //alert(geolocation);
	  
	  
    });
  }
}
// [END region_geolocation]

    </script>


	<script type="text/javascript">
	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
			//$('#blah').attr('src', e.target.result);
	         document.getElementById("uploadphoto").src=e.target.result;
			}
			reader.readAsDataURL(input.files[0]);
		}
	}
</script>


<?php
	$segs = $this->uri->segment_array();
//print_r($segs);
$cid = $segs[3];
	
	 ?>

    <div class="container">
		<form action="<?php echo base_url() ?>auth/caregiver_register_step2/<?php echo $cid; ?>" method="post" enctype="multipart/form-data">
		<!--<div class="inner-cont top">
		
			<label>Register Yourself as</label><select class="selectpicker">
											<option>Select</option>
											<option>Owner</option>
											<option>Professional</option>
										</select>
		</div>-->
		<div class="inner-cont btm prof-cont">
		<div class="nmber"><p><span class="badge">1</span><span class="badge">2</span><span class="txt">Caregiver Details</span></p> </div>
		<div class="clearfix"></div>
			<div class="form-inn-cont2 prof-reg">
				<div class="top-div">
					<div class="lft">
					<div class="fileinput fileinput-new" data-provides="fileinput">
					  <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
						<img data-src="holder.js/100%x100%" alt="..." src="<?php echo base_url(); ?>img/img-holder2.jpg"  id="uploadphoto" width="200" height="150"/>
					  </div>
						<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
						<div>
						<span class="btn btn-success btn-file"><span class="glyphicon glyphicon-upload upld" aria-hidden="true"></span><span class="fileinput-new"><?php echo $this->lang->line('uploadphoto'); ?> </span><span class="fileinput-exists">Change</span> <input type="file" name="photo" id="logo" style="opacity:0" onchange='readURL(this)' accept="image/png,image/jpeg,image/gif," /></span>
						<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput"><span class="glyphicon glyphicon-remove remove" aria-hidden="true"></span>Remove</a>
						</div>
</div>
					</div>
					<div class="ryt">
						<div class="fieldset span2">
							<div class="lft">
								<label>Name<span class="rqrd">*</span></label><div class="clearfix"></div>
								<input type="text" name="cname"  id="cname" value="<?php echo $this->session->userdata('cname'); ?>" required/>
							</div>
							<div class="ryt">
								<label><?php echo $this->lang->line('surname'); ?><span class="rqrd">*</span></label><div class="clearfix"></div>
								<input type="text" name="surname" id="surname" required/>
							</div>
						</div>
						<div class="fieldset span2">
							<div class="lft select-sm">
								<label><?php echo $this->lang->line('age'); ?><span class="rqrd">*</span></label>
								<select class="selectpicker" name="age" id="age" required>
											<option value="">--select--</option>
                                <?php for($i=20;$i<121;$i++){ ?>
                                <?php if(@$this->session->userdata('age')==$i){ ?>
                                <option value="<?php echo $i; ?>" selected><?php echo $i; ?></option>
                                <?php }else{ ?>
                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php } ?>
                                <?php } ?>
										</select>
                                        
                                        <input type="hidden" name="lat"  id="lat" />
<input type="hidden" name="lng"  id="lng" />
                                        
                                        
							</div>
							<div class="ryt select-sm">
								<label><?php echo $this->lang->line('gender'); ?><span class="rqrd">*</span></label>
								<select class="selectpicker" name="gender" required>
											<option value=""><?php echo $this->lang->line('select'); ?></option>
											<option>Maschio</option>
											<option>Femmina</option>
											
										</select>
							</div>
						</div>
						<div class="fieldset">
							<label><?php echo $this->lang->line('address'); ?><span class="rqrd">*</span></label><textarea name="address" id="autocomplete" onFocus="geolocate()" onblur="initialize1(this.value)" onfocusout="initialize2()" required></textarea>
						</div>
						<div class="fieldset span2">
							<div class="lft">
								<label><?php echo $this->lang->line('city'); ?><span class="rqrd">*</span></label><div class="clearfix"></div>
								<input type="text" name="city" id="city" required onFocus="initialize2()"/>
							</div>
							<div class="ryt">
								<label><?php echo $this->lang->line('zipcode'); ?><span class="rqrd">*</span></label><div class="clearfix"></div>
								<input type="text" id="postal_code" name="zipcode" />
							</div>
						</div>
						<div class="fieldset span2">
							<div class="lft select-lg">
								<label> <?php echo $this->lang->line('country'); ?><span class="rqrd">*</span></label><div class="clearfix"></div>
								<input type="text" name="state" id="state"> 
							</div>
							<div class="ryt">
								<label><?php echo $this->lang->line('contact'); ?><span class="rqrd">*</span></label><div class="clearfix"></div>
								<input type="text" name="contact" required/>
							</div>
						</div>
						
					</div>
				</div>
				
										<div class="clearfix"></div>
				<div class="col-md-5 pull-left btn no-padding">
							<input type="reset" value="Reset" class="btn btn-default">
							<!--<input type="submit" value="Forward" class="btn btn-success">-->
							<button type="submit" class="btn btn-success" aria-label="Left Align" style="width:100px;">
										  Submit 
										</button>
							
						</div>
						<div class="clearfix"></div>
			</div>		
		</div>
		</form>
	</div>

<?php $this->load->view('includes/footer.php');?>
  </body>
</html>