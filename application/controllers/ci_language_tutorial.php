<?php
ob_start();
session_start();
class CI_Language_Tutorial extends CI_Controller {

    public function __construct() {
        parent::__construct();
		$this->load->helper('url');

	//$this->load->library('user_agent');

    $this->load->library('form_validation');

	$this->load->library('email');

	//$this->load->model("sap");
$this->load->helper('language');
	$this->load->library('session');

		
		
		
    }

    // Create sessions for default view page
    public function index() {
        // Destroy session if already set
       //$this->session->sess_destroy();
        // Load default language
        $this->lang->load('italian', 'italian');
        $lan="italian";
        // Create session array and store default language values in array
        $this->load->helper('language');
		//$this->lang->load($lan, $lan);
		//$this->lang->load('$lang', '$lang');
		$sess_data = array();
        $sess_data['default'] = 'italian';
        $sess_data['title'] = $this->lang->line('title');
		$sess_data['associatedprofessionals']=$this->lang->line('associatedprofessionals');
        $sess_data['associatedpatients']=$this->lang->line('associatedpatients');
		$sess_data['registeryourself']   =$this->lang->line('registeryourself');
		$sess_data['select']  =$this->lang->line('select');
		$sess_data['private']  =$this->lang->line('private');
		$sess_data['business']  =$this->lang->line('business');
		$sess_data['type']=$this->lang->line('type');
		$sess_data['registration']=$this->lang->line('registration');
		$sess_data['name']=$this->lang->line('name');
		$sess_data['email']=$this->lang->line('email');
		$sess_data['reenteremail']=$this->lang->line('reenteremail');
		$sess_data['password']=$this->lang->line('password');
		$sess_data['cancle']=$this->lang->line('cancle');
		$sess_data['forward']=$this->lang->line('forward');
		$sess_data['licencetype']=$this->lang->line('licencetype');
		$sess_data['chooseyourlicence']=$this->lang->line('chooseyourlicence');
		$sess_data['chooseyourbusinessmodel']=$this->lang->line('chooseyourbusinessmodel');
		$sess_data['payment']=$this->lang->line('payment');
		$sess_data['amounttopay']=$this->lang->line('amounttopay');
		$sess_data['search']=$this->lang->line('search');
		$sess_data['editprofile']=$this->lang->line('editprofile');
		$sess_data['routine']=$this->lang->line('routine');
		$sess_data['parameters']=$this->lang->line('parameters');
		$sess_data['medicines']=$this->lang->line('medicines');
		$sess_data['diets']=$this->lang->line('diets');
		$sess_data['activities']=$this->lang->line('activities');
		$sess_data['report']=$this->lang->line('report');
		$sess_data['notificationsettings']=$this->lang->line('notificationsettings');
		$sess_data['newevent']=$this->lang->line('newevent');
		$sess_data['details']=$this->lang->line('details');
		$sess_data['time']=$this->lang->line('time');
		$sess_data['frequency']=$this->lang->line('frequency');
		$sess_data['selectdays']=$this->lang->line('selectdays');
		$sess_data['monday']=$this->lang->line('monday');
		$sess_data['tuesday']=$this->lang->line('tuesday');
		$sess_data['wednesday']=$this->lang->line('wednesday');
		$sess_data['thursday']=$this->lang->line('thursday');
		$sess_data['friday']=$this->lang->line('friday');
		$sess_data['saturday']=$this->lang->line('saturday');
		$sess_data['sunday']=$this->lang->line('sunday');
		$sess_data['confirm']=$this->lang->line('confirm');
		$sess_data['intolerance']=$this->lang->line('intolerance');
		$sess_data['favourites']=$this->lang->line('favourites');
		$sess_data['date']=$this->lang->line('date');
		$sess_data['action']=$this->lang->line('action');
		$sess_data['status']=$this->lang->line('status');
		$sess_data['details']=$this->lang->line('details');
		$sess_data['like']=$this->lang->line('like');
		$sess_data['attachment']=$this->lang->line('attachment');
		$sess_data['patient']=$this->lang->line('patient');
		$sess_data['gender']=$this->lang->line('gender');
		$sess_data['surname']=$this->lang->line('surname');
		$sess_data['age']=$this->lang->line('age');
		$sess_data['address']=$this->lang->line('address');
		$sess_data['city']=$this->lang->line('city');
		$sess_data['zipcode']=$this->lang->line('zipcode');
		$sess_data['country']=$this->lang->line('country');
		$sess_data['contact']=$this->lang->line('contact');
		$sess_data['uploadphoto']=$this->lang->line('uploadphoto');
		$sess_data['back']=$this->lang->line('back');
		$sess_data['update']=$this->lang->line('update');
		$sess_data['logout']=$this->lang->line('logout');
		$sess_data['cregiverdetails']=$this->lang->line('cregiverdetails');
		$sess_data['notifications']=$this->lang->line('notifications');
		$sess_data['pending']=$this->lang->line('pending');
		$sess_data['requestsend']=$this->lang->line('requestsend');
		$sess_data['accept']=$this->lang->line('accept');
		$sess_data['onlyprimarycaregiveraddthenewevent']=$this->lang->line('onlyprimarycaregiveraddthenewevent');
		$sess_data['norecordsfound']=$this->lang->line('norecordsfound');
		$sess_data['addpatients']=$this->lang->line('addpatients');
		$sess_data['addcaregiver']=$this->lang->line('addcaregiver');
		$sess_data['buyanotherlicence']=$this->lang->line('buyanotherlicence');
		$sess_data['upgradelicence']=$this->lang->line('upgradelicence');
		$sess_data['doyouwantalsotobeacaregiver']=$this->lang->line('doyouwantalsotobeacaregiver');
		$sess_data['yes']=$this->lang->line('yes');
		$sess_data['no']=$this->lang->line('no');
		$sess_data['save']=$this->lang->line('save');
		$sess_data['enterlicencenumber']=$this->lang->line('enterlicencenumber');
		$sess_data['contacts']=$this->lang->line('contacts');
		$sess_data['map']=$this->lang->line('map');
		$sess_data['agenda']=$this->lang->line('agenda');
		$sess_data['consultationdetails']=$this->lang->line('consultationdetails');
		$sess_data['noassociatedprofetionalsadded']=$this->lang->line('noassociatedprofetionalsadded');
		$sess_data['quantitytopurchase']=$this->lang->line('quantitytopurchase');
		$sess_data['medicinedose']=$this->lang->line('medicinedose');
		$sess_data['price']=$this->lang->line('price');
		$sess_data['dateofpurchase']=$this->lang->line('dateofpurchase');
		$sess_data['dateofexpiry']=$this->lang->line('dateofexpiry');
		$sess_data['note']=$this->lang->line('note');
		$sess_data['injection']=$this->lang->line('injection');
		$sess_data['suppository']=$this->lang->line('suppository');
		$sess_data['ointment']=$this->lang->line('ointment');
		$sess_data['eyedrops']=$this->lang->line('eyedrops');
		$sess_data['transdermalpatch']=$this->lang->line('transdermalpatch');
		$sess_data['syrup']=$this->lang->line('syrup');
		$sess_data['tablet']=$this->lang->line('tablet');
		$sess_data['forgotpassword']=$this->lang->line('forgotpassword');
		$sess_data['logintoyouraccount']=$this->lang->line('logintoyouraccount');
		$sess_data['rememberme']=$this->lang->line('rememberme');
		$sess_data['donhaveanaccountyet']=$this->lang->line('donhaveanaccountyet');
		$sess_data['createanaccount']=$this->lang->line('createanaccount');
		$sess_data['howitworks']=$this->lang->line('howitworks');
		$sess_data['requestinformation']=$this->lang->line('requestinformation');
		$sess_data['admittedtodoctorsboardof']=$this->lang->line('admittedtodoctorsboardof');
		$sess_data['sendchatmessage']=$this->lang->line('sendchatmessage');
		$sess_data['videocall']=$this->lang->line('videocall');
		$sess_data['sendvideocall']=$this->lang->line('sendvideocall');
		$sess_data['sleepwake']=$this->lang->line('sleepwake');
		$sess_data['event']=$this->lang->line('event');
		$sess_data['success']=$this->lang->line('success');
		$sess_data['consultationrequest']=$this->lang->line('consultationrequest');
		$sess_data['hometitle']=$this->lang->line('hometitle');
		$sess_data['registercomplete']=$this->lang->line('registercomplete');
		$sess_data['clicktologin']=$this->lang->line('clicktologin');
		$sess_data['nocaregiversadded']=$this->lang->line('nocaregiversadded');
		$sess_data['nopatientadded']=$this->lang->line('nopatientadded');
		$sess_data['correctlicence']=$this->lang->line('correctlicence');
          $this->session->set_userdata('session_data', $sess_data);
        // Retrieve session values
		
        $set_data = $this->session->all_userdata();
		$session_data=$this->session->userdata('session_data');
		echo "<pre>";
		//print_r($session_data);
		echo "</pre>";
		echo $session_data['forgotpassword'];
		// Send retrieved data to view page
        //$this->load->view('contact_form', $set_data);
		//$curl=current_url();
		//echo $curl;
		redirect('auth/owner_register_step1');
		//redirect($curl);
    }
        // Create sessions for default view page
		public function changelang() 
		{
		 
		     
        $this->lang->load('italian', 'italian');
        $lan="italian";
        // Create session array and store default language values in array
        
		//$this->lang->load($lan, $lan);
		//$this->lang->load('$lang', '$lang');
		$sess_data = array();
        $sess_data['default'] = 'italian';
        $sess_data['title'] = $this->lang->line('title');
		$sess_data['associatedprofessionals']=$this->lang->line('associatedprofessionals');
        $sess_data['associatedpatients']=$this->lang->line('associatedpatients');
		$sess_data['registeryourself']   =$this->lang->line('registeryourself');
		$sess_data['select']  =$this->lang->line('select');
		$sess_data['private']  =$this->lang->line('private');
		$sess_data['business']  =$this->lang->line('business');
		$sess_data['type']=$this->lang->line('type');
		$sess_data['registration']=$this->lang->line('registration');
		$sess_data['name']=$this->lang->line('name');
		$sess_data['email']=$this->lang->line('email');
		$sess_data['reenteremail']=$this->lang->line('reenteremail');
		$sess_data['password']=$this->lang->line('password');
		$sess_data['cancle']=$this->lang->line('cancle');
		$sess_data['forward']=$this->lang->line('forward');
		$sess_data['licencetype']=$this->lang->line('licencetype');
		$sess_data['chooseyourlicence']=$this->lang->line('chooseyourlicence');
		$sess_data['chooseyourbusinessmodel']=$this->lang->line('chooseyourbusinessmodel');
		$sess_data['payment']=$this->lang->line('payment');
		$sess_data['amounttopay']=$this->lang->line('amounttopay');
		$sess_data['search']=$this->lang->line('search');
		$sess_data['editprofile']=$this->lang->line('editprofile');
		$sess_data['routine']=$this->lang->line('routine');
		$sess_data['parameters']=$this->lang->line('parameters');
		$sess_data['medicines']=$this->lang->line('medicines');
		$sess_data['diets']=$this->lang->line('diets');
		$sess_data['activities']=$this->lang->line('activities');
		$sess_data['report']=$this->lang->line('report');
		$sess_data['notificationsettings']=$this->lang->line('notificationsettings');
		$sess_data['newevent']=$this->lang->line('newevent');
		$sess_data['details']=$this->lang->line('details');
		$sess_data['time']=$this->lang->line('time');
		$sess_data['frequency']=$this->lang->line('frequency');
		$sess_data['selectdays']=$this->lang->line('selectdays');
		$sess_data['monday']=$this->lang->line('monday');
		$sess_data['tuesday']=$this->lang->line('tuesday');
		$sess_data['wednesday']=$this->lang->line('wednesday');
		$sess_data['thursday']=$this->lang->line('thursday');
		$sess_data['friday']=$this->lang->line('friday');
		$sess_data['saturday']=$this->lang->line('saturday');
		$sess_data['sunday']=$this->lang->line('sunday');
		$sess_data['confirm']=$this->lang->line('confirm');
		$sess_data['intolerance']=$this->lang->line('intolerance');
		$sess_data['favourites']=$this->lang->line('favourites');
		$sess_data['date']=$this->lang->line('date');
		$sess_data['action']=$this->lang->line('action');
		$sess_data['status']=$this->lang->line('status');
		$sess_data['details']=$this->lang->line('details');
		$sess_data['like']=$this->lang->line('like');
		$sess_data['attachment']=$this->lang->line('attachment');
		$sess_data['patient']=$this->lang->line('patient');
		$sess_data['gender']=$this->lang->line('gender');
		$sess_data['surname']=$this->lang->line('surname');
		$sess_data['age']=$this->lang->line('age');
		$sess_data['address']=$this->lang->line('address');
		$sess_data['city']=$this->lang->line('city');
		$sess_data['zipcode']=$this->lang->line('zipcode');
		$sess_data['country']=$this->lang->line('country');
		$sess_data['contact']=$this->lang->line('contact');
		$sess_data['uploadphoto']=$this->lang->line('uploadphoto');
		$sess_data['back']=$this->lang->line('back');
		$sess_data['update']=$this->lang->line('update');
		$sess_data['logout']=$this->lang->line('logout');
		$sess_data['cregiverdetails']=$this->lang->line('cregiverdetails');
		$sess_data['notifications']=$this->lang->line('notifications');
		$sess_data['pending']=$this->lang->line('pending');
		$sess_data['requestsend']=$this->lang->line('requestsend');
		$sess_data['accept']=$this->lang->line('accept');
		$sess_data['onlyprimarycaregiveraddthenewevent']=$this->lang->line('onlyprimarycaregiveraddthenewevent');
		$sess_data['norecordsfound']=$this->lang->line('norecordsfound');
		$sess_data['addpatients']=$this->lang->line('addpatients');
		$sess_data['addcaregiver']=$this->lang->line('addcaregiver');
		$sess_data['buyanotherlicence']=$this->lang->line('buyanotherlicence');
		$sess_data['upgradelicence']=$this->lang->line('upgradelicence');
		$sess_data['doyouwantalsotobeacaregiver']=$this->lang->line('doyouwantalsotobeacaregiver');
		$sess_data['yes']=$this->lang->line('yes');
		$sess_data['no']=$this->lang->line('no');
		$sess_data['save']=$this->lang->line('save');
		$sess_data['enterlicencenumber']=$this->lang->line('enterlicencenumber');
		$sess_data['contacts']=$this->lang->line('contacts');
		$sess_data['map']=$this->lang->line('map');
		$sess_data['agenda']=$this->lang->line('agenda');
		$sess_data['consultationdetails']=$this->lang->line('consultationdetails');
		$sess_data['noassociatedprofetionalsadded']=$this->lang->line('noassociatedprofetionalsadded');
		$sess_data['quantitytopurchase']=$this->lang->line('quantitytopurchase');
		$sess_data['medicinedose']=$this->lang->line('medicinedose');
		$sess_data['price']=$this->lang->line('price');
		$sess_data['dateofpurchase']=$this->lang->line('dateofpurchase');
		$sess_data['dateofexpiry']=$this->lang->line('dateofexpiry');
		$sess_data['note']=$this->lang->line('note');
		$sess_data['injection']=$this->lang->line('injection');
		$sess_data['suppository']=$this->lang->line('suppository');
		$sess_data['ointment']=$this->lang->line('ointment');
		$sess_data['eyedrops']=$this->lang->line('eyedrops');
		$sess_data['transdermalpatch']=$this->lang->line('transdermalpatch');
		$sess_data['syrup']=$this->lang->line('syrup');
		$sess_data['tablet']=$this->lang->line('tablet');
		$sess_data['forgotpassword']=$this->lang->line('forgotpassword');
		$sess_data['logintoyouraccount']=$this->lang->line('logintoyouraccount');
		$sess_data['rememberme']=$this->lang->line('rememberme');
		$sess_data['donhaveanaccountyet']=$this->lang->line('donhaveanaccountyet');
		$sess_data['createanaccount']=$this->lang->line('createanaccount');
		$sess_data['howitworks']=$this->lang->line('howitworks');
		$sess_data['requestinformation']=$this->lang->line('requestinformation');
		$sess_data['admittedtodoctorsboardof']=$this->lang->line('admittedtodoctorsboardof');
		$sess_data['sendchatmessage']=$this->lang->line('sendchatmessage');
		$sess_data['videocall']=$this->lang->line('videocall');
		$sess_data['sendvideocall']=$this->lang->line('sendvideocall');
		$sess_data['sleepwake']=$this->lang->line('sleepwake');
		$sess_data['event']=$this->lang->line('event');
		$sess_data['success']=$this->lang->line('success');
		$sess_data['consultationrequest']=$this->lang->line('consultationrequest');
		$sess_data['hometitle']=$this->lang->line('hometitle');
		$sess_data['registercomplete']=$this->lang->line('registercomplete');
		$sess_data['clicktologin']=$this->lang->line('clicktologin');
		$sess_data['nocaregiversadded']=$this->lang->line('nocaregiversadded');
		$sess_data['nopatientadded']=$this->lang->line('nopatientadded');
		$sess_data['correctlicence']=$this->lang->line('correctlicence');
        $this->session->set_userdata('session_data_session',$sess_data);
		
        // Retrieve session values
         $set_data = $this->session->all_userdata();
		$session_data=$this->session->userdata('session_data_session');
		echo "<pre>";
		print_r($session_data);
		echo "</pre>";
		echo $session_data['forgotpassword'];
		// Send retrieved data to view page
        //$this->load->view('contact_form', $set_data);
		//$curl=current_url();
		//echo $curl;
		$session_data = $this->session->all_userdata();
		redirect('auth/owner_register_step1');
		//$this->load->view('auth/owner_register_step1');
		//redirect($curl);
          
		}
    // Select language and manage view page 
    public function select_language() {
        // Destroy session data
        $this->session->sess_destroy();
        $language = $this->input->post('language');
        if ($language == 'english') {
            $data['selected'] = 'english';
        } else {
            $data['selected'] = 'french';
        }
        
        // Load language according to language selected
        $this->lang->load('contact_form_' . $language, $language);
        $sess_data['select_lang'] = $this->lang->line('select_lang');
        $sess_data['title'] = $this->lang->line('title');
        $sess_data['default'] = $language;
        $sess_data['name'] = $this->lang->line('name');
        $sess_data['message'] = $this->lang->line('message');
        $sess_data['name_placeholder_value'] = $this->lang->line('name_placeholder_value');
        $sess_data['message_placeholder_value'] = $this->lang->line('message_placeholder_value');
        $sess_data['send'] = $this->lang->line('send');
        $sess_data['success_message'] = $this->lang->line('message_sent');
        
        // Set session values according to selected language 
        $this->session->set_userdata('session_data', $sess_data);
        $set_data = $this->session->all_userdata();
        $this->load->view('contact_form', $set_data);
    }

}

?>
