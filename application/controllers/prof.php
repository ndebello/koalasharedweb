<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Prof extends CI_Controller
{
	function __construct()
	{
	  parent::__construct();
	  $this->load->library('form_validation');
	  //$this->load->library('security');
	  //$this->load->library('tank_auth');
	  //$this->lang->load('tank_auth');
	  $this->load->helper('url');
	  $this->load->model('user');
	  $this->load->model('admin');
	  $this->load->model('patient');
	  //$this->load->library('user_agent');
	  $this->load->helper('language');
	$country=$this->session->userdata('lan');
		if($country=="it")
		{
		$this->lang->load('italian', 'italian');
		}
		else
		{
			$this->lang->load('english', 'english');
		}
     //$this->data['patient'] = $this->patient->get_patient_by_id($this->session->userdata('user_id'));
	  $this->data='';
	  $this->data['flash']='';
	  //if(!$this->session->userdata('user_id')){
	   	//redirect('/');
	  //}
	$username = $this->session->userdata('owner_prof');
	if(empty($username))
		{
	   redirect('/');
		}
		
		if(!empty($username))
		{
		$username = $this->session->userdata('owner_prof');
		$pro= $this->user->getprof($username);
		$this->data['profetional']=$pro;
		$pfid=$this->data['profetional'][0]->id;
        $photo=$this->data['profetional'][0]->photo;
        $name=$this->data['profetional'][0]->name;
		$this->data['profnoti1']= $this->user->getprofnoti_new_pop($pfid);
		//$this->data['profnotic11']= $this->user->getprofnoti_new_count($pfid);
		$this->data['profnotic1']= $this->user->getprofnoti_new_count($pfid);
		
		$this->data['profnoti2']= $this->user->getprofnotinet_new($pfid);
		$this->data['profnoti3']= $this->user->getprofnoticon_new($pfid);
		$this->data['profnoti4']= $this->user->getprofnoticon_msg($pfid);
		$this->data['profnoti4c']= $this->user->getprofnoticon_prfmsgcount($pfid);
		$this->data['profnoti5']= $this->user->getprofnoticon_msg1($pfid);
		$this->data['profnotic6']= $this->user->getprofnoticon_caremsgcount($pfid);
		$this->data['consortiamnoti1']=$this->user->getconsultaion_msg1($pfid);
		
		//count($this->data['consortiamnoti1'][0]);
		
		}
		
		
		
		
		
	}
	
  function getnotification()
  {
	    $username = $this->session->userdata('owner_prof');
		$pro= $this->user->getprof($username);
		$this->data['profetional']=$pro;
		$pfid=$this->data['profetional'][0]->id;
        $photo=$this->data['profetional'][0]->photo;
        $name=$this->data['profetional'][0]->name;
		$this->data['profnoti1']= $this->user->getprofnoti_new_pop($pfid);
		$this->data['profnotic11']= $this->user->getprofnoti_new_count($pfid);
		
		/*$this->data['profnoti2']= $this->user->getprofnotinet_new($pfid);
		$this->data['profnoti3']= $this->user->getprofnoticon_new($pfid);
		$this->data['profnoti4']= $this->user->getprofnoticon_msg($pfid);
		$this->data['profnoti4c']= $this->user->getprofnoticon_prfmsgcount($pfid);
		$this->data['profnoti5']= $this->user->getprofnoticon_msg1($pfid);
		$this->data['profnotic6']= $this->user->getprofnoticon_caremsgcount($pfid);
		$this->data['consortiamnoti1']=$this->user->getconsultaion_msg1($pfid);  */
		
		$this->data['photo']=$photo;
		$this->data['pid']=$pfid;
		$this->data['name']=$name;
		$this->load->view('home/prof_auto_notification', $this->data);
		
		
  }
  
  
   function getnotification_video()
  {
	    $username = $this->session->userdata('owner_prof');
		$pro= $this->user->getprof($username);
		$this->data['profetional']=$pro;
		$pfid=$this->data['profetional'][0]->id;
        $photo=$this->data['profetional'][0]->photo;
        $name=$this->data['profetional'][0]->name;
		$this->data['profnoti1']= $this->user->getprofnoti_new_pop($pfid);
		$this->data['profnotic11']= $this->user->getprofnoti_new_count($pfid);
		
		/*$this->data['profnoti2']= $this->user->getprofnotinet_new($pfid);
		$this->data['profnoti3']= $this->user->getprofnoticon_new($pfid);
		$this->data['profnoti4']= $this->user->getprofnoticon_msg($pfid);
		$this->data['profnoti4c']= $this->user->getprofnoticon_prfmsgcount($pfid);
		$this->data['profnoti5']= $this->user->getprofnoticon_msg1($pfid);
		$this->data['profnotic6']= $this->user->getprofnoticon_caremsgcount($pfid);
		$this->data['consortiamnoti1']=$this->user->getconsultaion_msg1($pfid);  */
		
		$this->data['photo']=$photo;
		$this->data['pid']=$pfid;
		$this->data['name']=$name;
		$this->load->view('home/prof_auto_notification_video', $this->data);
		
		
  }
  
  
  
	function videocall_set($get_token,$get_sessionid)
{
	
	$this->data['get_token']=$get_token;
	$this->data['get_sessionid']=$get_sessionid;
	
	
	$this->load->view('home/testvideocall1', $this->data);
	
	
	
	
}
	
	function videocall($get_token,$get_sessionid,$usertype,$rid)
  {
	    
 
        $username = $this->session->userdata('owner_prof');
		$pro= $this->user->getprof($username);
		$this->data['profetional']=$pro;
		$pfid=$this->data['profetional'][0]->id;
        //$photo=$this->data['profetional'][0]->photo;
        $name=$this->data['profetional'][0]->name;
		$this->data['get_token']=$get_token;
	    $this->data['get_sessionid']=$get_sessionid;
	     $this->data['name']=$name;
	
	//$get_token=$_POST['get_token'];
	//$get_sessionid=$_POST['get_sessionid'];
	//$get_status=$_POST['get_status'];
	$rid;
	//$this->data['get_token']=$get_token;
	//$this->data['get_sessionid']=$get_sessionid;
	//$this->data['get_status']=$get_status;
	//$this->session->set_userdata('get_token', $_POST['get_token']);
	//$this->session->set_userdata('get_sessionid', $_POST['get_sessionid']);
	if($usertype=="video_cp")
		{
		$res="caregiver";
		}
		if($usertype=="video_pp")
		{
		$res="professional";
		}
	
	$note_type=$usertype;
	$ip=$_SERVER['REMOTE_ADDR'];
	$date=date("Y-m-d");
	$message="you  have a Video call request from ".$name;
	$cstatus=1;
	$ps="insert into popup_notifications (n_sid,n_rid,status,note_type,n_ip,n_date,token_id,session_id,note_message,is_oncall,ltype) values('$pfid','$rid','0','$note_type','$ip','$date','$get_token','$get_sessionid','$message','$cstatus','$res')";
	$pe=mysql_query($ps) or die(mysql_error());

	
		define('API_ACCESS_KEY', 'AIzaSyBHV-ghmAtzWCzpL7ySyXjztoW7Eo5RBuw');
			$device=" ";
			$devices = " ";
			$device_array = array();	
		
		
		
$ds="select * from device_tocken where userid='$rid' and user_type='$res'";
	$se=mysql_query($ds) or die(mysql_error());
	$sn=mysql_num_rows($se);
	
	if($sn>0)
	{
			while($sr=mysql_fetch_array($se))
			{
				if(!empty($sr['tockenid']))
				{
						/*$device=$sr['tockenid'];
						$msg = array(
						'message'       => $message,
						'title'         => $name.'Send The Video Call Request',
						'subtitle'      => 'Chat Mssage',
						'vibrate'       => 1,
						'sound'         => 1
						 );
						$res = $this->sendPushNotificationToGCM($device,$msg);*/
						
						  $device_platform=$sr['platform'];
				//$device=$sr['tockenid'];
				
				
				
				$device_array[] = $sr['tockenid'];
					//$device_platform = $sr['platform'];
					
					
					if($device_platform=="Android"){
				 $device_platform=$sr['platform'];
				 $usertype;
						$registrationIds = $device_array;
			
						$msg = array(
						  'message'       => $message,//"Testing Video Call Message",
						  'title'         => "Koala- Video Call",
						  'video_session' => $get_sessionid,
						  'video_token' => $get_token,
						  'call_type' => "video",
						  'video_call_type' => $usertype,
						  'sender_id' => $careid,
						  'sender_name' => $name,
						  'type'      => '2',
						   'vibrate'   => 1,
						   'sound'     => 1
							);
			
			

		
						$fields = array('registration_ids' => $registrationIds, 'data' => $msg);


						$headers = array
						(
							'Authorization: key=' . API_ACCESS_KEY,
							'Content-Type: application/json'
						);

						$ch = curl_init();
						curl_setopt($ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
						curl_setopt($ch,CURLOPT_POST, true );
						curl_setopt($ch,CURLOPT_HTTPHEADER, $headers );
						curl_setopt($ch,CURLOPT_RETURNTRANSFER, true );
						curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false );
						curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
						$result = curl_exec($ch );
						curl_close( $ch );

						//echo $result;
		
						 json_encode(array("result" =>"success","message" =>"Connection created"), 200); 
					} elseif($device_platform=="iOS"){
		 
						$deviceToken =$sr['tockenid'];
						$passphrase = 'bravemount';
			
						$message=$sender_name." had Send a video vall request ";
			
						$path = base_url().'assets/KoalaProPushCert.pem';
			
						//echo $path; exit;
						$ctx = stream_context_create();
			
						$entrustCert =  '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/entrust_2048_ca.cer';
						stream_context_set_option($ctx, 'ssl', 'cafile', $entrustCert);
			
						stream_context_set_option($ctx, 'ssl', 'local_cert', '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/KoalaProPushCert.pem');
						stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
			
			
						// Open a connection to the APNS server
						$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

			
						$body['aps'] = array(
							'alert' => $message,
							'sound' => 'beep.caf',
							'video_session' => $get_sessionid,
						  'video_token' => $get_token,
						  'call_type' => "video",
						  'video_call_type' => $user_type,
						  'sender_id' => $careid,
						  'sender_name' => $name,
						  'type'      => '2'
							
						);

						// Encode the payload as JSON
						$payload = json_encode($body);

						// Build the binary notification
				
						$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
			

						// Send it to the server
						$result = fwrite($fp, $msg, strlen($msg));

						//if (!$result)
						//	echo 'Message not delivered' . PHP_EOL;
						//else
						//	echo 'Message successfully delivered' . PHP_EOL;

						// Close the connection to the server
						fclose($fp);
			
				 
					}
						
						
						
						
						
						
						
				}
			}
	}
	
	
	
	 
	 
	 
	
	$this->load->view('home/testvideocall', $this->data);
	
	//redirect('/caregiver/videocall_set/');	
	
	



}

	
	
	
	
	
	
	
	
	function getnotificationcount()
	{
		$username = $this->session->userdata('owner_prof');
		$pro= $this->user->getprof($username);
		$this->data['profetional']=$pro;
		$pfid=$this->data['profetional'][0]->id;
        $photo=$this->data['profetional'][0]->photo;
        $name=$this->data['profetional'][0]->name;
		$this->data['profnotic1']= $this->user->getprofnoti_new_count($pfid);
		
		echo count($this->data['profnotic1']);
		
		
		/*$this->data['profnoti2']= $this->user->getprofnotinet_new($pfid);
		$this->data['profnoti3']= $this->user->getprofnoticon_new($pfid);
		$this->data['profnoti4']= $this->user->getprofnoticon_msg($pfid);
		$this->data['profnoti4c']= $this->user->getprofnoticon_prfmsgcount($pfid);
		$this->data['profnoti5']= $this->user->getprofnoticon_msg1($pfid);
		$this->data['profnotic6']= $this->user->getprofnoticon_caremsgcount($pfid);*/
		
		
		
		
		//$this->data['consortiamnoti1']=$this->user->getconsultaion_msg1($pfid);  
		
		
		//$this->load->view('home/prof_auto_notificationcount', $this->data);
	}
	
	function readallnotifications()
	{
		$username = $this->session->userdata('owner_prof');
		$pro= $this->user->getprof($username);
		$this->data['profetional']=$pro;
		 $pfid=$this->data['profetional'][0]->id;
        $photo=$this->data['profetional'][0]->photo;
         $name=$this->data['profetional'][0]->name;
		
		 // $this->user->getprofnoti_new_update($pfid);
		 //$this->user->getprofnotinet_new_update($pfid);
		 $this->user->getprofnoticon_new_update($pfid);
		 $this->user->getprofnoticon_msg_update($pfid);
		 $this->user->get_carenoti_care_all_popup($pfid);
		 
		 
		// $this->user->getprofnoticon_msg1_update($pfid);/**/
	}
	
	
	
	
	
	
	
	
   function index()
	{
	}
	function professional_home()
	{
		$this->load->view('home/professional_index', $this->data);
	}
	function search_caregiver()
	{ 
	  if($_POST){
	    $this->data['caregivers']= $this->user->caregiverSearch();
	  }
	  else
	  {
	   $this->data['caregivers']= $this->user->caregiverList();
	  }
	  $this->load->view('home/caregiver_list', $this->data);
	}
	function search_professional()
	{ 
	  if($_POST)
	  {
	  $this->data['professionals']= $this->user->professionalSearch();
	  }else
	  {
	   $this->data['professionals']= $this->user->professionalList();
	  }
	  $this->load->view('home/professional', $this->data);
	}


	function professional_details($id=null)
	{
	 if (!($this->session->userdata('user_name')!="")) {
			redirect('/auth/login/');
		} else 
		{
		  $this->data['user'] = $this->user->getProfessionalsAll($id);
	      $this->load->view('home/professional_details', $this->data);
		 }
	}
	function caregivers_details($id=null)
	{
		if (!($this->session->userdata('user_name')!="")) {
		redirect('/auth/login/');
		} else {
		$this->data['user'] = $this->user->getCaregiversAll($id);
		$this->load->view('home/caregiver_details', $this->data);
		}
	}
	
/* get poup in the consultation requests   */

	function getcarepopup($sid)
	{
 $this->data['id']=$sid;
 
$this->load->view('home/finalcare1',$this->data);
	}
	
/* View Consultation Details */	
function view_consultaion($sid)
{
      $username = $this->session->userdata('owner_prof');
		$pro= $this->user->getprof($username);
		// $mid=$pro['patid'];
		//$pro= $this->user->getprof1($username); 
	    $us="update popup_notifications set status='1' where n_sid='$sid' and note_type='caregiver_consultation'";	
		$ue=mysql_query($us) or die(mysql_error());
		
		$this->data['profetional']=$pro;
		$pid= $this->data['profetional'][0]->id;
		$this->session->set_userdata('usertypeid', '2');
		
		$this->session->set_userdata('userid', $pid);
		
		
		if($_POST)
		{  
		  
			if($_POST['form_type']== 'patient_search')
			{    
			    $this->session->set_userdata('search_keyword1',$_POST['keyword']);
			     
				$this->data['patients']= $this->patient->searchPatients();
				$this->data['professionals']= $this->user->getProfessionals();
				
			
			  }
			elseif($_POST['form_type']== 'professional_search')
			{
			    $this->session->set_userdata('search_keyword2',$_POST['keyword']);
				
				//$this->data['patients']= $this->patient->getPatients();
				//$this->data['professionals']= $this->user->searchProfessionals();
				$this->data['patients']= $this->patient->searchPatients();
				$this->data['professionals']= $this->user->getProfessionals();
				
				
				
			}
	      
	     
		}
		else
		{
			$pid= $this->data['profetional'][0]->id;
			$this->data['patients'] = $this->patient->getPatients22($pid);
			$this->data['professionals1'] = $this->user->getnewsProfessionals();
			
			$this->data['professionals'] = $this->user->getProfessionals($pid);
			
			
			
			$this->data['caregivers'] = $this->user->getprofetionals123($pid);
		}
		
		
	
		$pfid=$this->data['profetional'][0]->id;
        $photo=$this->data['profetional'][0]->photo;
        $name=$this->data['profetional'][0]->name;
		$this->data['profnoti1']= $this->user->getprofnoti_new_pop($pfid);
		$this->data['profnoti2']= $this->user->getprofnotinet_new($pfid);
		$this->data['profnoti3']= $this->user->getprofnoticon_new($pfid);
		
					
	    $pfid=$this->data['profetional'][0]->id;
        $photo=$this->data['profetional'][0]->photo;
        $name=$this->data['profetional'][0]->name;
		$this->data['profnoti1']= $this->user->getprofnoti_new_pop($pfid);
		$this->data['profnoti2']= $this->user->getprofnotinet_new($pfid);
		$this->data['profnoti3']= $this->user->getprofnoticon_new($pfid);
		
		//$this->data['caregivers']= $this->user->getcaregivers_new1($pid,$pfid);
	   
	   $ps1="select * from cons_request_messages as cons,caregiver as cs  where (recipient='$pid' and subject!='') and        (cons.sender=cs.id)"; 
       $pe1=mysql_query($ps1) or die(mysql_error());
       $pn1=mysql_num_rows($pe1);
	
	$this->data['pn1']=$pn1;
	
	
	$pid= $this->data['profetional'][0]->id;
	
	$session_id=$pid;
//$segs[3]
 $iRecipient = $sid;
$iPid = (int)$session_id;
	
$upms="update cons_request_messages set `read`=1 where sender='$iRecipient' and recipient='$iPid'";
$upme=mysql_query($upms) or die(mysql_error());

//$sRecipientSQL = "WHERE ((`sender` = '{$iRecipient}' && stype=1) && (`recipient` = '{$iPid}' && rtype=1) || (`sender` = '{$iRecipient}' && stype=0) && (`recipient` = '{$iPid}' && rtype=1) ) || ((`recipient` = '{$iRecipient}' && stype=1) && (`sender` = '{$iPid}' && rtype=1) || (`recipient` = '{$iRecipient}' && stype=1) && (`sender` = '{$iPid}' && rtype=0) ) ";
	

$this->data['consutaiondel']=$this->user->getconsultaion_new($iRecipient,$iPid);

	
	
	
	
	
	
	
	
	
$this->load->view('home/caregiver_consult_replay',$this->data);	



}
	
	
function consultation_replay()
{
	
	
	$username = $this->session->userdata('owner_prof');
		$pro= $this->user->getprof($username);
		// $mid=$pro['patid'];
		//$pro= $this->user->getprof1($username); 
		$this->data['profetional']=$pro;
		$pid= $this->data['profetional'][0]->id;
		$name=$this->data['profetional'][0]->name;
	
		$rid=$_POST['reciver'];
		$sid=$_POST['sender'];
		$msg=$_POST['send'];
		$when = time();
		$date = date('Y-m-d H:i:s');
		$uid=$session_id;
		$stype=1;
		$read=0;
$subject="RE :".$_POST['subject'];
		$d="insert into cons_request_messages(sender,recipient,message,subject,`when`,date,stype,`read`) values('$sid','$rid','$msg','$subject',UNIX_TIMESTAMP( ),'$date','$stype','$read')";
		
		$e=mysql_query($d) or die(mysql_error());

		$lid=mysql_insert_id();

		$nmsg=$name."  Had Replay To The Your Consultation Request ";
		$ntype="caregiver_consultation";
		$nip=$_SERVER['REMOTE_ADDR'];
		$res="caregiver";
		
		$ds="select * from device_tocken where userid='$rid' and user_type='$res'";
  $se=mysql_query($ds) or die(mysql_error());
  
  
  $ntype="caregiver_consultation";
  while($sr=mysql_fetch_array($se))
  {
   $device_platform = $sr['platform'];
   $device=$sr['tockenid'];
   $msg = array(
   'message'       => $nmsg,
   'title'         => "Consultation request from ".$name,
   //'subtitle'      => 'Chat Mssage',
   'lid'			=> $lid,
   'call_type'     =>"caregiver_consultation",
   'vibrate'       => 1,
   'sound'         => 1
    );
   $res1 = $this->sendPushNotificationToGCM_sendconsultation($device,$msg,$device_platform,$name,$sid,$rid,$ntype,$lid,$nmsg);
   
  }
		
		
		
		
		
		
		$nts="insert into popup_notifications(note_message,n_sid,n_rid,nm_id,status,note_type,n_ip,ltype)          values('$nmsg','$sid','$rid','$lid','0','$ntype','$ip','$res')";
		$nte=mysql_query($nts) or die(mysql_error());






if($e)
{
	redirect('/prof/view_consultaion/'.$rid);
}

	
}
	
	function sendPushNotificationToGCM_sendconsultation($deviceid,$msg1,$device_platform,$name,$from,$to,$loginusertye,$lid,$nmsg) {

		$API_ACCESS_KEY1 = 'AIzaSyBHV-ghmAtzWCzpL7ySyXjztoW7Eo5RBuw';
		$registrationIds = array($deviceid);
		// prep the bundle
		$msg = $msg1;

			//echo $deviceid; exit;
			//print_r()
			
			//echo $device_platform; exit;
		if($device_platform=="Android"){
		$fields = array
		(
			'registration_ids'  => $registrationIds,
			'data'              => $msg
		);

		$headers = array
		(
			'Authorization: key=' . $API_ACCESS_KEY1,
			'Content-Type: application/json'
		);

		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
		curl_setopt($ch,CURLOPT_POST, true );
		curl_setopt($ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );
//echo $result;  exit;
	//	echo json_encode(array("result" =>"success"), 200); //exit;
		} elseif($device_platform=="iOS"){
		 
			$deviceToken =$deviceid;
			$passphrase = 'bravemount';
			$message = $nmsg; //"Consultation request from ".$name;
			
			$path = base_url().'assets/KoalaProPushCert.pem';
			
			//echo $path; exit;
			$ctx = stream_context_create();
			
			$entrustCert =  '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/entrust_2048_ca.cer';
			stream_context_set_option($ctx, 'ssl', 'cafile', $entrustCert);
			
			stream_context_set_option($ctx, 'ssl', 'local_cert', '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/KoalaProPushCert.pem');
			stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
			
			
			// Open a connection to the APNS server
			$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

			//if (!$fp)
				//exit("Failed to connect: $err $errstr" . PHP_EOL);

			//echo 'Connected to APNS' . PHP_EOL;

			// Create the payload body
			$body['aps'] = array(
				'alert' => $message,
				'sound' => 'default',
				'sid'           =>$from,
				'rid'           =>$to,
				'sname'         =>$name,
				'recivertype'   =>$loginusertye,
				'lid' => $lid,
				
				'call_type'     =>"caregiver_consultation"
				
			);

			// Encode the payload as JSON
			$payload = json_encode($body);

			// Build the binary notification
				
				$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
			//$msg = chr (0) . chr (0) . chr (32) . pack ('H*', $deviceToken) . pack ('n', strlen ($payload)) . $payload;
				
				//chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $deviceToken)) . chr(0) . chr(strlen($payload)) . $payload;
				//chr (0) . chr (0) . chr (32) . pack ('H*', $tToken) . pack ('n', strlen ($tBody)) . $tBody;
				//chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

			// Send it to the server
			$result = fwrite($fp, $msg, strlen($msg));

			//if (!$result)
			//	echo 'Message not delivered' . PHP_EOL;
			//else
			//	echo 'Message successfully delivered' . PHP_EOL;

			// Close the connection to the server
			fclose($fp);
			
				// echo json_encode(array("result" =>"success"), 200); exit;
		}
		
		//echo $result;
		echo json_encode(array("result" =>"success"), 200); 
    }
	
	
	
	public function addnetwotk()
	{
		$this->load->view('home/professional_care_fallow_req',$this->data);
	}
	
	function acceptrec($lid,$rid,$mnid)
      {
   	    $lid;
		$username = $this->session->userdata('owner_prof');
		$pro= $this->user->getprof($username);
		// $mid=$pro['patid'];
		//$pro= $this->user->getprof1($username); 
		$this->data['profetional']=$pro;
		$name= $this->data['profetional'][0]->name;
        $sid=	$this->data['profetional'][0]->id;
		//$date=date("Y-m-d");
		$us="update professional_networks set netstatus='1',cread='0' where id='$lid'";
		$ue=mysql_query($us) or die(mysql_error());
		$ms="update popup_notifications set status='1' where n_id='$mnid'";
		$me=mysql_query($ms) or die(mysql_error());
		$message=$name."  Had Accept The Request ";
		$ntype="caregivernetwork";
		$nip=$_SERVER['REMOTE_ADDR'];
		 $res = "caregiver";
		$nts="insert into popup_notifications(note_message,n_sid,n_rid,nm_id,status,note_type,n_ip,ltype) values('$message','$sid','$rid','$lid','0','$ntype','$nip','$res')";
		$nte=mysql_query($nts) or die(mysql_error());
		
		  
		   $message = $name." ha accettato la tua richiesta di collegamento";
		   $receivertype=1;

			$ds="select * from device_tocken where userid='$rid' and user_type='$res'";
			$se=mysql_query($ds) or die(mysql_error());
			$sn=mysql_num_rows($se);
			if($sn>0)
			{
			while($sr=mysql_fetch_array($se))
			{
			$device=$sr['tockenid'];
			$device_platform = $sr['platform'];
			$msg = array(
						'message'       => $message,
						'title'         => 'Koala add request',
						 'sid'           =>$rid,
						'rid'           =>$sid,
						'sname'         =>$name,
						'recivertype'   =>$receivertype,
						'lid' => $lid,
						'add_req_state'	=> 'yes',
						'call_type'     =>"add_request"
						
			);
			
		
		 // echo $device; exit;
		   
		  //$res = $this->sendPushNotificationToGCM_addreq_succ($device,$msg,$device_platform,$name,1);
		  //echo json_encode(array("result" =>"success"), 200);
		// $res = $this->sendPushNotificationToGCM_addreq_succ($device,$msg,$device_platform,$name,1);

				$API_ACCESS_KEY1 = 'AIzaSyBHV-ghmAtzWCzpL7ySyXjztoW7Eo5RBuw';
				if($device_platform=="Android"){
					
					$registrationIds = array($device);
					
					$fields = array
					(
						'registration_ids'  => $registrationIds,
						'data'              => $msg
					);
					
					$headers = array
					(
						'Authorization: key=' . $API_ACCESS_KEY1,
						'Content-Type: application/json'
					);
					
					$ch = curl_init();
					curl_setopt($ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
					curl_setopt($ch,CURLOPT_POST, true );
					curl_setopt($ch,CURLOPT_HTTPHEADER, $headers );
					curl_setopt($ch,CURLOPT_RETURNTRANSFER, true );
					curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false );
					curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
					$result = curl_exec($ch );
					curl_close( $ch );
					
					
				} elseif($device_platform=="iOS"){
		 
					$deviceToken =$device;
					$passphrase = 'bravemount';
					
					$message = $name." ha accettato la tua richiesta di collegamento";
					
			
					$path = base_url().'assets/koalaDevpush1.pem';
			
					//echo $path; exit;
					$ctx = stream_context_create();
			
					$entrustCert =  '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/entrust_2048_ca.cer';
					stream_context_set_option($ctx, 'ssl', 'cafile', $entrustCert);
			
					//stream_context_set_option($ctx, 'ssl', 'local_cert', '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/koalaDevpush1.pem');
				 stream_context_set_option($ctx, 'ssl', 'local_cert', '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/KoalaProPushCert.pem');	
					
					stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
			
			
					// Open a connection to the APNS server
					//$fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);


					//if (!$fp)
						//exit("Failed to connect: $err $errstr" . PHP_EOL);

					//echo 'Connected to APNS' . PHP_EOL;

					// Create the payload body
					$body['aps'] = array(
						'alert' => $message,
						'sound' => 'default',
						 'sid'           =>$rid,
						'rid'           =>$sid,
						'sname'         =>$name,
						'recivertype'   =>$receivertype,
						'lid' => $lid,
						'add_req_state'	=> 'yes',
						'call_type'     =>"add_request"
						
						
					);

					// Encode the payload as JSON
					$payload = json_encode($body);

					// Build the binary notification
						
						$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
					//$msg = chr (0) . chr (0) . chr (32) . pack ('H*', $deviceToken) . pack ('n', strlen ($payload)) . $payload;
						
						//chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $deviceToken)) . chr(0) . chr(strlen($payload)) . $payload;
						//chr (0) . chr (0) . chr (32) . pack ('H*', $tToken) . pack ('n', strlen ($tBody)) . $tBody;
						//chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

					// Send it to the server
					$result = fwrite($fp, $msg, strlen($msg));

					//if (!$result)
					//	echo 'Message not delivered' . PHP_EOL;
					//else
					//	echo 'Message successfully delivered' . PHP_EOL;

					// Close the connection to the server
					fclose($fp);
			
				 
				}
			
			
				echo $result;
			
			}
			}
		
		
		
		
		
       	
	
   }

/* 
consultaiondetail Details   


*/
function consultaiondetail()
{
	$username = $this->session->userdata('owner_prof');
		
		
		
		$pro= $this->user->getprof($username);
		echo  $mid=$pro['id'];
		/*echo "<script>alert('$mid')</script>";*/
		$this->data['const']= $this->user->consultation_request($mid);
		//echo $this->$const[0]->sender;
		//print_r($this->data['const']);
		/* 
		
		
		
		*/
   //echo "consultaiondetail is Colled ";	
	$this->load->view('home/caregiver_consult_requests',$this->data);
}





function acceptrec1($cid,$lid,$mnid)
{ 
 
	/*$this->data['events'] = $this->caregiver_model->get_moreevent($id);
	$this->data['flash']=$pid;
	$this->load->view('home/final1', $this->data,$this->data);*/
	//$this->data['cid']=$cid;
	//$this->data['pid']=$pid;
	//$this->data['nid']=$nid;
	
        $username = $this->session->userdata('owner_prof');
		$pro= $this->user->getprof($username);
		// $mid=$pro['patid'];
		//$pro= $this->user->getprof1($username); 
		$this->data['profetional']=$pro;
		$name= $this->data['profetional'][0]->name;
        $sid=	$this->data['profetional'][0]->id;	
	
	
	
	
$rid=$cid;
$pid;

//$date=date("Y-m-d");
	
	$us="update professional_networks_prf set netstatus='1',pread='0' where id='$lid' ";
	$ue=mysql_query($us) or die(mysql_error());
			
	$ms="update popup_notifications set status=1 where n_id='$mnid'";
	$me=mysql_query($ms);
		
	$message=$name."  Had Accept The Request ";
	$ntype="professionalnetwork";
	$nip=$_SERVER['REMOTE_ADDR'];
	 $res = "professional";
	$nts="insert into popup_notifications(note_message,n_sid,n_rid,nm_id,status,note_type,n_ip,ltype) values('$message','$sid','$rid','$lid','0','$ntype','$nip','$res')";
	$nte=mysql_query($nts) or die(mysql_error());
	
	
	$receivertype=1;

			$ds="select * from device_tocken where userid='$rid' and user_type='$res'";
			$se=mysql_query($ds) or die(mysql_error());
			while($sr=mysql_fetch_array($se))
			{
			$device=$sr['tockenid'];
			$device_platform = $sr['platform'];
			$msg = array(
						'message'       => $message,
						'title'         => 'Koala add request',
						 'sid'           =>$rid,
						'rid'           =>$sid,
						'sname'         =>$name,
						'recivertype'   =>$receivertype,
						'lid' => $lid,
						'add_req_state'	=> 'yes',
						'call_type'     =>"add_request"
						
			);
			
		
		 // echo $device; exit;
		   
		  //$res = $this->sendPushNotificationToGCM_addreq_succ($device,$msg,$device_platform,$name,1);
		  //echo json_encode(array("result" =>"success"), 200);
		// $res = $this->sendPushNotificationToGCM_addreq_succ($device,$msg,$device_platform,$name,1);

				$API_ACCESS_KEY1 = 'AIzaSyBHV-ghmAtzWCzpL7ySyXjztoW7Eo5RBuw';
				if($device_platform=="Android"){
					
					$registrationIds = array($device);
					
					$fields = array
					(
						'registration_ids'  => $registrationIds,
						'data'              => $msg
					);
					
					$headers = array
					(
						'Authorization: key=' . $API_ACCESS_KEY1,
						'Content-Type: application/json'
					);
					
					$ch = curl_init();
					curl_setopt($ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
					curl_setopt($ch,CURLOPT_POST, true );
					curl_setopt($ch,CURLOPT_HTTPHEADER, $headers );
					curl_setopt($ch,CURLOPT_RETURNTRANSFER, true );
					curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false );
					curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
					$result = curl_exec($ch );
					curl_close( $ch );
					
					
				} elseif($device_platform=="iOS"){
		 
					$deviceToken =$device;
					$passphrase = 'bravemount';
					
					$message = $name." ha accettato la tua richiesta di collegamento";
					
			
					$path = base_url().'assets/koalaDevpush1.pem';
			
					//echo $path; exit;
					$ctx = stream_context_create();
			
					$entrustCert =  '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/entrust_2048_ca.cer';
					stream_context_set_option($ctx, 'ssl', 'cafile', $entrustCert);
			
					//stream_context_set_option($ctx, 'ssl', 'local_cert', '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/koalaDevpush1.pem');
				 stream_context_set_option($ctx, 'ssl', 'local_cert', '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/KoalaProPushCert.pem');	
					
					stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
			
			
					// Open a connection to the APNS server
					//$fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
					
					 $fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
					

					//if (!$fp)
						//exit("Failed to connect: $err $errstr" . PHP_EOL);

					//echo 'Connected to APNS' . PHP_EOL;

					// Create the payload body
					$body['aps'] = array(
						'alert' => $message,
						'sound' => 'default',
						 'sid'           =>$rid,
						'rid'           =>$sid,
						'sname'         =>$name,
						'recivertype'   =>$receivertype,
						'lid' => $lid,
						'add_req_state'	=> 'yes',
						'call_type'     =>"add_request"
						
						
					);

					// Encode the payload as JSON
					$payload = json_encode($body);

					// Build the binary notification
						
						$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
					//$msg = chr (0) . chr (0) . chr (32) . pack ('H*', $deviceToken) . pack ('n', strlen ($payload)) . $payload;
						
						//chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $deviceToken)) . chr(0) . chr(strlen($payload)) . $payload;
						//chr (0) . chr (0) . chr (32) . pack ('H*', $tToken) . pack ('n', strlen ($tBody)) . $tBody;
						//chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

					// Send it to the server
					$result = fwrite($fp, $msg, strlen($msg));

					//if (!$result)
					//	echo 'Message not delivered' . PHP_EOL;
					//else
					//	echo 'Message successfully delivered' . PHP_EOL;

					// Close the connection to the server
					fclose($fp);
			
				 
				}
			
			
				echo $result;
			
			}
	
}




public  function addnetwotk_prof()
{

$this->load->view('home/professional_prof_fallow_req',$this->data);

}

function sendfallowrequest($sid,$rid)
{
	
	//$this->data['sid']=$sid;
	//$this->data['rid']=$rid;
	//$this->data['nid']=$nid;
//$ptid=$patid;

     $username = $this->session->userdata('owner_prof');
		$pro= $this->user->getprof($username);
		
		$this->data['profetional']=$pro;
		$name= $this->data['profetional'][0]->name;
        $senid=	$this->data['profetional'][0]->id;
		$date=date("Y-m-d");
		$status=0;
		$ip=$_SERVER['REMOTE_ADDR'];
		$is="insert into professional_networks_prf (sid,rid,netstatus,sdate,netip) values('$sid','$rid','$status','$date','$ip')";
		$ie=mysql_query($is) or die(mysql_error());
		$lid=mysql_insert_id();

if($ie)
{
echo "1";	
}
	$receivertype=2;
	$message=$name." ha inviato una richiesta";
	$ntype="professionalnetwork";
	$nip=$_SERVER['REMOTE_ADDR'];
	$res="professional";
	$nts="insert into popup_notifications(note_message,n_sid,n_rid,nm_id,status,note_type,n_ip,ltype) values('$message','$sid','$rid','$lid','0','$ntype','$ip','$res')";
	$nte=mysql_query($nts) or die(mysql_error());

    
    $ds="select * from device_tocken where userid='$rid' and user_type='$res'";
	$se=mysql_query($ds) or die(mysql_error());
	while($sr=mysql_fetch_array($se))
			{
						$device=$sr['tockenid'];
						$device_platform = $sr['platform'];
						$msg = array(
						'message'       => $message,
						'title'         => 'Koala add request',
						 'sid'           =>$rid,
						'rid'           =>$sid,
						'sname'         =>$name,
						'recivertype'   =>$receivertype,
						'lid' => $lid,
						'add_req_state'	=> 'yes',
						'call_type'     =>"add_request"
						
									);

				$API_ACCESS_KEY1 = 'AIzaSyBHV-ghmAtzWCzpL7ySyXjztoW7Eo5RBuw';
				if($device_platform=="Android"){
					
					$registrationIds = array($device);
					
					$fields = array
					(
						'registration_ids'  => $registrationIds,
						'data'              => $msg
					);
					
					$headers = array
					(
						'Authorization: key=' . $API_ACCESS_KEY1,
						'Content-Type: application/json'
					);
					
					$ch = curl_init();
					curl_setopt($ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
					curl_setopt($ch,CURLOPT_POST, true );
					curl_setopt($ch,CURLOPT_HTTPHEADER, $headers );
					curl_setopt($ch,CURLOPT_RETURNTRANSFER, true );
					curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false );
					curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
					$result = curl_exec($ch );
					curl_close( $ch );
					
					
				} elseif($device_platform=="iOS"){
		 
					$deviceToken =$device;
					$passphrase = 'bravemount';
					
					$message = $name." ha accettato la tua richiesta di collegamento";
					
			
					$path = base_url().'assets/koalaDevpush1.pem';
			
					//echo $path; exit;
					$ctx = stream_context_create();
			
					$entrustCert =  '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/entrust_2048_ca.cer';
					stream_context_set_option($ctx, 'ssl', 'cafile', $entrustCert);
			
					//stream_context_set_option($ctx, 'ssl', 'local_cert', '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/koalaDevpush1.pem');
					stream_context_set_option($ctx, 'ssl', 'local_cert', '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/KoalaProPushCert.pem');
					stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
			
			
					// Open a connection to the APNS server
					//$fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
 $fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
					//if (!$fp)
						//exit("Failed to connect: $err $errstr" . PHP_EOL);

					//echo 'Connected to APNS' . PHP_EOL;

					// Create the payload body
					$body['aps'] = array(
						'alert' => $message,
						'sound' => 'default',
						 'sid'           =>$rid,
						'rid'           =>$sid,
						'sname'         =>$name,
						'recivertype'   =>$receivertype,
						'lid' => $lid,
						'add_req_state'	=> 'yes',
						'call_type'     =>"add_request"
						
						
					);

					// Encode the payload as JSON
					$payload = json_encode($body);

					// Build the binary notification
						
						$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
					//$msg = chr (0) . chr (0) . chr (32) . pack ('H*', $deviceToken) . pack ('n', strlen ($payload)) . $payload;
						
						//chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $deviceToken)) . chr(0) . chr(strlen($payload)) . $payload;
						//chr (0) . chr (0) . chr (32) . pack ('H*', $tToken) . pack ('n', strlen ($tBody)) . $tBody;
						//chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

					// Send it to the server
					$result = fwrite($fp, $msg, strlen($msg));

					//if (!$result)
					//	echo 'Message not delivered' . PHP_EOL;
					//else
					//	echo 'Message successfully delivered' . PHP_EOL;

					// Close the connection to the server
					fclose($fp);
			
				 
				}
				echo $result;
		
			}






	
}

function rejectrec($rid,$lid,$nid,$receivertype)
{
		  /*$sid = $_POST['sid'];
		  $rid = $_POST['rid'];
		  $lid = $_POST['lid'];
		  $receivertype = $_POST['receivertype'];*/
		  //$name = $_POST['user_disp_name'];
		 $username = $this->session->userdata('owner_prof');
		$pro= $this->user->getprof($username);
		$this->data['profetional']=$pro;
		$name= $this->data['profetional'][0]->name;
		$sid= $this->data['profetional'][0]->id; 
		  
		  
		  if($receivertype==1) {
		   $us="delete from professional_networks  where id='$lid'";
		   } else {
			     $us="delete from professional_networks_prf  where id='$lid'";
			   
			   
		   }
		   
		   $ue=mysql_query($us) or die(mysql_error());
		   $ms="update popup_notifications set status='1' where n_id='$nid'";
		   $me=mysql_query($ms) or die(mysql_error());
		   
		   if($receivertype==1) {
			   $res = "caregiver";
		   } else if($receivertype==2){
			    $res = "professional";
		   }
		   
		   $message = $name." rejected your add request";
		   
		   
		   if($receivertype==1) {
			
			$ntype="caregivernetwork";
			} else {
				$ntype="professionalnetwork";
			}
			$nip=$_SERVER['REMOTE_ADDR'];
		   
		   $nts="insert into popup_notifications(note_message,n_sid,n_rid,nm_id,status,note_type,n_ip,ltype) values('$message','$sid','$rid','$lid','0','$ntype','$nip','$res')";
			$nte=mysql_query($nts) or die(mysql_error());
		  
		  
		  
		  $ds="select * from device_tocken where userid='$rid' and user_type='$res'";
			
				
			$se=mysql_query($ds) or die(mysql_error());
			while($sr=mysql_fetch_array($se))
			{
			$device=$sr['tockenid'];
			$device_platform = $sr['platform'];
			$msg = array(
						'message'       => $message,
						'title'         => 'Koala add request',
						 'sid'           =>$rid,
						'rid'           =>$sid,
						'sname'         =>$name,
						'recivertype'   =>$receivertype,
						'lid' => $lid,
						'add_req_state'	=> 'no',
						'call_type'     =>"add_request"
						
			);
		  
		  
		   //$res = $this->sendPushNotificationToGCM_addreq_succ($device,$msg,$device_platform,$name,0);
			
			
			$API_ACCESS_KEY1 = 'AIzaSyBHV-ghmAtzWCzpL7ySyXjztoW7Eo5RBuw';
				if($device_platform=="Android"){
					
					$registrationIds = array($device);
					
					$fields = array
					(
						'registration_ids'  => $registrationIds,
						'data'              => $msg
					);
					
					$headers = array
					(
						'Authorization: key=' . $API_ACCESS_KEY1,
						'Content-Type: application/json'
					);
					
					$ch = curl_init();
					curl_setopt($ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
					curl_setopt($ch,CURLOPT_POST, true );
					curl_setopt($ch,CURLOPT_HTTPHEADER, $headers );
					curl_setopt($ch,CURLOPT_RETURNTRANSFER, true );
					curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false );
					curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
					$result = curl_exec($ch );
					curl_close( $ch );
					
					
				} elseif($device_platform=="iOS"){
		 
					$deviceToken =$device;
					$passphrase = 'bravemount';
					
					$message = $name." ha accettato la tua richiesta di collegamento";
					
			
					$path = base_url().'assets/koalaDevpush1.pem';
			
					//echo $path; exit;
					$ctx = stream_context_create();
			
					$entrustCert =  '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/entrust_2048_ca.cer';
					stream_context_set_option($ctx, 'ssl', 'cafile', $entrustCert);
			
					//stream_context_set_option($ctx, 'ssl', 'local_cert', '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/koalaDevpush1.pem');
				stream_context_set_option($ctx, 'ssl', 'local_cert', '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/KoalaProPushCert.pem');	
					
					stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
			
			
					// Open a connection to the APNS server
					//$fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

					//if (!$fp)
						//exit("Failed to connect: $err $errstr" . PHP_EOL);

					//echo 'Connected to APNS' . PHP_EOL;

					// Create the payload body
					$body['aps'] = array(
						'alert' => $message,
						'sound' => 'default',
						 'sid'           =>$rid,
						'rid'           =>$sid,
						'sname'         =>$name,
						'recivertype'   =>$receivertype,
						'lid' => $lid,
						'add_req_state'	=> 'no',
						'call_type'     =>"add_request"
						
						
					);

					// Encode the payload as JSON
					$payload = json_encode($body);

					// Build the binary notification
						
						$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
					//$msg = chr (0) . chr (0) . chr (32) . pack ('H*', $deviceToken) . pack ('n', strlen ($payload)) . $payload;
						
						//chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $deviceToken)) . chr(0) . chr(strlen($payload)) . $payload;
						//chr (0) . chr (0) . chr (32) . pack ('H*', $tToken) . pack ('n', strlen ($tBody)) . $tBody;
						//chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

					// Send it to the server
					$result = fwrite($fp, $msg, strlen($msg));

					//if (!$result)
					//	echo 'Message not delivered' . PHP_EOL;
					//else
					//	echo 'Message successfully delivered' . PHP_EOL;

					// Close the connection to the server
					fclose($fp);
			
				 
				}
			
			
				echo $result;
			
			
			
			
			}
	
}


	
	
	
	
	
	public function add_diets()
	{
	if($_POST){
			$this->form_validation->set_rules('food_type', 'Food Type ', 'trim|required|xss_clean');
			$this->form_validation->set_rules('food_name', 'Food Name ', 'trim|required|xss_clean');
			$this->form_validation->set_rules('diet_day', 'Diet Day ', 'trim|required|xss_clean');
			$temp=$_POST['diet_day'];
			$data=explode('/',$temp);
			$result=$data[2].'-'.$data[0].'-'.$data[1];
			$_POST['diet_day']=$result;
			if($this->form_validation->run() == TRUE)
			{	
				$this->patient->adddiets();
				$this->data['flash']="Diet has been added !";
			}
			else
			{
				$this->data['flash']="Unsuccesfull, Try again !";
			}	
		}
			$this->data['caregivers']= $this->user->getCaregiverslist();
			$this->data['patient'] = $this->patient->get_patient_by_id($this->session->userdata('user_id'));
			
		
		if($this->session->userdata('user_role')=='admin')
		{
			$this->load->view('admin/add_diets', $this->data);
		}
		else
		{
			$this->load->view('home/add_diets', $this->data);
		}
	}
	function list_diets()
	{	   
		$this->data['diets']=$this->patient->getdiets();
		$this->load->view('admin/list_diets', $this->data);
	}
	function edit_diets($id=null)
	{
	if($_POST)
		{
		$this->patient->updatediets($id);
			redirect('/home/list_diets/');
		}
			$this->data['diets']=$this->patient->dietsDetail($id);
			$this->load->view('home/edit_diets', $this->data);

	}
	function details_diets($id=null)
	{
		$this->data['diets']= $this->patient->dietsDetail($id);
		$this->load->view('home/diet_details',$this->data );
	}

	function delete_diets($id)
	{
		$this->patient->deletediets($id);
		redirect('/home/list_diets/');
	}
	
	
/*==============================//////// add Owner,rsa,consortium ////////////================================  */	
	
	function add_rsa_owner()
	{
	if($_POST){

		    $this->form_validation->set_rules('owner_name', 'Owner Name', 'trim|required|xss_clean');
			$this->form_validation->set_rules('owner_type', 'Owner Type', 'trim|required|xss_clean');
			$this->form_validation->set_rules('email_address', 'Email Address', 'trim|required|xss_clean');
			//$this->form_validation->set_rules('mobile_no', 'Mobile No', 'trim|required|xss_clean');
			//$this->form_validation->set_rules('address', 'address  ', 'trim|required|xss_clean');
			//$this->form_validation->set_rules('password', 'Password ', 'trim|required|xss_clean');
			$this->form_validation->set_rules('transation', 'transation Number ', 'trim|required|xss_clean');
			$this->form_validation->set_rules('pay', 'pay', 'trim|required|xss_clean');
			$this->form_validation->set_rules('created', 'Date  ', 'trim|required|xss_clean');
			//$temp=$_POST['created'];
			//$data=explode('/',$temp);
			//$result=$data[2].'-'.$data[0].'-'.$data[1];
			//$_POST['created']=$result;
			$type=$_POST['owner_type'];
			if($this->form_validation->run() == TRUE)
				{
					
					$email=$_POST['email_address'];
					$pn=$this->patient->getprofetion($email);
					$on=$this->patient->getowner($email);
					$kn=$this->patient->getkoalaower($email);
					
					echo "<script>alert('$pn-$on-$kn')</script>";
					if($pn==0 and $on==0 and $kn==0)
					{
					$id=$this->patient->add_owner_rsa();
				
					
					
				//$this->user->updateowner($id);
                //$this->patient->add_payment($id);
				
				
				
				$lc=$_POST['licence'];
				$num=count($_POST['licence']);
				
				for($i=0;$i<$num;$i++)
				{
				$mc=$lc[$i];
				@$lids.=$lc[$i]."<br/>";
				$this->patient->add_licence($id,$mc,$email);
				}
/*------------------------------------------------- the email function is start hear -----------------------------------------------------------*/
    		$this->data['flash']=" Owner  has been added successfully ! with licence id's please verify the email id for licence ids";
			$to = $this->session->userdata('username');
			$subject = "Licence Id's From Koala".$type;
			$message = "	
			<html>
			<head>
			<title>HTML email</title>
			</head>
			<body>
			<table>
			<tr>
			<th> Name: </th>
			<td></td>
			</tr>
			<tr>
			<th> Your Licence ids: </th>
			<td>$lids</td>
			</tr> 
			</table>
			</body>
			</html>
			";
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
			// More headers
			$headers .= 'From: '." Koala Team " . "\r\n";
		

		if(mail($to,$subject,$message,$headers)){
			 // echo 'Thank you !';
			}
			else{
			  //echo 'Try again !'; 
			}
					}
					else
					{
						
			$this->data['flash']=" Owner  Email id Already Present In Owner Records.";
						
					}
		}
				
			 //$this->l
				  $this->load->view('admin/add_owner_rsa', $this->data);
			     //$this->load->view('admin/addowner_rsa');	
				
				}
				
				else
				{
					$this->data['flash']="Unsuccesfull, Try again !";
                    $this->load->view('admin/add_owner_rsa', $this->data);

				}
		
		
			$this->load->view('admin/add_owner_rsa', $this->data);
		
		}
		
		//$this->data['caregivers']= $this->user->getCaregiverslist();
		//$this->data['patient'] = $this->patient->get_patient_by_id($this->session->userdata('user_id'));
		//if($this->session->userdata('user_role')=='admin'){
		 //$this->load->view('admin/add_parameters', $this->data);
		//}
		//else{
		 //$this->load->view('home/add_parameters', $this->data);
		//}

	
	
	
	
	function add_con_owner()
	{
		if (!($this->session->userdata('ausername')!="")) 
		{
			redirect('/auth/login/');
		}
		/*echo "<script>alert('ok oko ok ok ok ok ok ok ok ok ')</script>"     cname;*/
		if($_POST){

		    $this->form_validation->set_rules('owner_name', 'Owner Name', 'trim|required|xss_clean');
			$this->form_validation->set_rules('owner_lc', 'owner licence', 'trim|required|xss_clean');
			$this->form_validation->set_rules('cname', 'cname', 'trim|required|xss_clean');
			$this->form_validation->set_rules('email_address', 'Email Address', 'trim|required|xss_clean');
			$this->form_validation->set_rules('mobile_no', 'Mobile No', 'trim|required|xss_clean');
			$this->form_validation->set_rules('address', 'address  ', 'trim|required|xss_clean');
			$this->form_validation->set_rules('password', 'Password ', 'trim|required|xss_clean');
			$this->form_validation->set_rules('transation', 'transation Number ', 'trim|required|xss_clean');
			$this->form_validation->set_rules('pay', 'pay', 'trim|required|xss_clean');
			$this->form_validation->set_rules('created', 'Date  ', 'trim|required|xss_clean');
			//$temp=$_POST['created'];
			//$data=explode('/',$temp);
			//$result=$data[2].'-'.$data[0].'-'.$data[1];
			//$_POST['created']=$result;
			$oid=$_POST['owner_name'];
			if($this->form_validation->run() == TRUE)
				{
					$id=$this->patient->add_owner_cons();
					$this->user->updateownersta($oid);
					$this->patient->add_payment($id);
/*------------------------------------------------- the email function is start hear -----------------------------------------------------------*/
				 $this->data['flash']=" Owner  has been added successfully ! with licence id ".$_POST['owner_lc'];
				 //$this->l
			      $this->load->view('admin/add_owner_consortuim', $this->data);	
				
				}
				else
				{
					$this->data['flash']="Unsuccesfull, Try again !";
                    $this->load->view('admin/add_owner_consortuim', $this->data);

				}
		}
		
		//$this->data['caregivers']= $this->user->getCaregiverslist();
		//$this->data['patient'] = $this->patient->get_patient_by_id($this->session->userdata('user_id'));
		//if($this->session->userdata('user_role')=='admin'){
		 //$this->load->view('admin/add_parameters', $this->data);
		//}
		//else{
		 //$this->load->view('home/add_parameters', $this->data);
		//}
	
	
	}
	
	
	
	
	
	
	
	function add_parameters()
	{
		if($_POST){
		$this->form_validation->set_rules('maximum_pressure', 'Maximum Pressure', 'trim|required|xss_clean');
			$this->form_validation->set_rules('minimum_pressure', 'Minimum Pressure  ', 'trim|required|xss_clean');
			$this->form_validation->set_rules('blood_glucose', 'Blood Glucose ', 'trim|required|xss_clean');
			$this->form_validation->set_rules('heart_rate', 'Heart Rate  ', 'trim|required|xss_clean');
			$this->form_validation->set_rules('weight', 'Weight  ', 'trim|required|xss_clean');
			$this->form_validation->set_rules('patient_id', 'Patient Name ', 'trim|required|xss_clean');
			$this->form_validation->set_rules('created', 'Date  ', 'trim|required|xss_clean');
			$temp=$_POST['created'];
			$data=explode('/',$temp);
			$result=$data[2].'-'.$data[0].'-'.$data[1];
			$_POST['created']=$result;
			if($this->form_validation->run() == TRUE)
				{
					$this->patient->addparameters();
					$this->data['flash']="Parameters has been added !";
				}
					else
				{
					$this->data['flash']="Unsuccesfull, Try again !";

				}
		}
		$this->data['caregivers']= $this->user->getCaregiverslist();
		$this->data['patient'] = $this->patient->get_patient_by_id($this->session->userdata('user_id'));
		if($this->session->userdata('user_role')=='admin'){
		 $this->load->view('admin/add_parameters', $this->data);
		}
		else{

		 $this->load->view('home/add_parameters', $this->data);
		}
	}

	function list_parameters()
	{	   
		$this->data['parameters']=$this->patient->getparameters();
		$this->load->view('admin/list_parameters', $this->data);
	}

	function add_periods()
	{

		if($_POST)
		{
				//$this->form_validation->set_rules('patient_id', 'Patient Name', 'trim|required|xss_clean');
				$this->form_validation->set_rules('sleep_time', 'Sleep Time ', 'trim|required|xss_clean');
				$this->form_validation->set_rules('wake_time', 'Wake Time ', 'trim|required|xss_clean');
				$this->form_validation->set_rules('created', 'Date ', 'trim|required|xss_clean');

				$temp=$_POST['created'];
				$data=explode('/',$temp);
				$result=$data[2].'-'.$data[0].'-'.$data[1];
				$_POST['created']=$result;

			if($this->form_validation->run() == TRUE)
			{
				$this->patient->addperiods();
				$this->data['flash']="Periods has been added !";
			}
				else
				{
					$this->data['flash']="Unsuccesfull, Try again !";
				}
		}
				$this->data['caregivers']= $this->user->getCaregiverslist();
				$this->data['patient'] = $this->patient->get_patient_by_id($this->session->userdata('user_id'));
			if($this->session->userdata('user_role')=='admin')
			{
				$this->load->view('admin/add_periods', $this->data);
			}
		else
		{
			$this->load->view('home/add_periods', $this->data);
		}

	}


	function list_periods()
			{	
				$this->data['periods']=$this->patient->getperiods();
				//print_r($this->data);
				$this->load->view('admin/list_periods', $this->data);
			}

		
    function edit_profile($id)
	{
		//$temp= $this->user->get_users_by_id($this->session->userdata('user_id'));
		//$temp= $temp[0]->id;
		$this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('surname', 'Surname','trim|required|xss_clean');
		$this->form_validation->set_rules('age', 'Age','trim|required|xss_clean');
		$this->form_validation->set_rules('gender', 'Gender','trim|required|xss_clean');
			
		if($this->form_validation->run() == TRUE)
		{
			
		
	if($_POST)
	{
	
    
	
	$name=$_POST['name'];
	$surname=$_POST['surname'];
	$zipcode=$_POST['zipcode'];
	$city=$_POST['city'];
	$address=$_POST['address'];
	$contact=$_POST['photo'];
	$country=$_POST['state'];
	$lat=$_POST['lat'];
	$lon=$_POST['lng'];
	$age=$_POST['age'];
	$gender=$_POST['gender'];
	$admitted_to=$_POST['admitted_to'];
	 $fr1=$_POST['from'];
	
		$nd1=explode("-",$fr1);
	     $from=$nd1[2]."-".$nd1[1]."-".$nd1[0];
		
	$qualification=$_POST['qualification'];
	
	
	  if(!empty($_FILES["photo"]["name"]))
			{

				//echo "Temp file: " . $_FILES["photo"]["tmp_name"] . "<br>";
				
					//$file=$_FILES["photo"]["name"];
					//if(move_uploaded_file($_FILES["photo"]["tmp_name"],
					//"uploads/" . $file))
					//$this->session->set_userdata('user_photo', $file);
				
$file=$_FILES['photo']['name'];
 $type=$_FILES['photo']['type'];
move_uploaded_file($_FILES['photo']['tmp_name'],"uploads/".$file);

if($type=='image/jpeg')
{
	$fi="uploads/".$file;
   $newHeight=100;
   $newWidth=100;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;
    list($width,$height) = getimagesize($fi);
    $img = imagecreatefromjpeg($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    //imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
	imagecopyresampled( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagejpeg($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }
    if($type=='image/png')
    {
    $fi="uploads/".$file;
   $newHeight=100;
   $newWidth=100;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;

    list($width,$height) = getimagesize($fi);
    $img = imagecreatefrompng($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    //imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
	imagecopyresampled( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagepng($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }
    
    if($type=='image/gif')
    {
   $fi="uploads/".$file;
   $newHeight=100;
   $newWidth=100;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;

    list($width,$height) = getimagesize($fi);
    $img = imagecreatefromgif($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    //imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
	imagecopyresampled( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagegif($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }

    unlink($fi);

			}
			else
			{
				$nfile=$_POST['photo'];
				
				}
			
			if(!empty($_FILES["cv"]["name"]))
			{
				$cv=$_FILES["cv"]["name"];
			if ($_FILES["cv"]["error"] > 0) {
				//echo "Error: " . $_FILES["photo"]["error"] . "<br>";
			}else {
				//echo "Temp file: " . $_FILES["photo"]["tmp_name"] . "<br>";
				if (file_exists("upload/cv/" . $_FILES["cv"]["name"])) {
					//$_FILES["photo"]["name"] . " already exists. ";
				} else {
					$cv=uniqid().$_FILES["cv"]["name"];
					if(move_uploaded_file($_FILES["cv"]["tmp_name"],
					"uploads/cv/" . $cv)){
						$this->session->set_userdata('cv', $cv);
						$this->session->set_userdata('cv', $cv);
					}else{
						//$this->session->set_userdata('photo',$this->session->userdata('user_photo'));
						$this->session->set_userdata('cv', '');
					}
				}
			}
			}
			else
			{ $cv=$_POST['cv']; }
	
	//$us=$this->user->getcaregiversbyemail($email);
  
$us1=$this->user->editProfile($id,$nfile,$cv,$lat,$lon,$from); 
	  if($us1)
	  {
	  echo "<script>alert('Updated Successfully')</script>";
      redirect('/prof/search/'); 
		  
	  }
  
	 
	 }
	
}
			 //$this->data['users'] = $this->user->get_users_by_id($temp);
			
			 //print_r($this->data['professionals']);
			 // print_r( $this->data['states']);
			 $username = $this->session->userdata('owner_prof');
			 $pro= $this->user->getprof($username);
			 // $mid=$pro['patid'];
			 //$pro= $this->user->getprof1($username);
			 $this->data['profetional']=$pro;
		 $this->data['professionals'] = $this->user->get_professionals_by_id($id);
		 //$this->data['states']= $this->user->getStates();
		
		$this->load->view('home/professional_edit', $this->data);
	}
	
	
	function edit_caregivers($id=null)
	{
		  $result= $this->user->get_users_by_id($this->session->userdata('user_id'));
		  $result= $result[0]->id;

            
			$this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');
			$this->form_validation->set_rules('surname', 'Surname','trim|required|xss_clean');
			$this->form_validation->set_rules('age', 'Age','trim|required|xss_clean');
			$this->form_validation->set_rules('gender', 'Gender','trim|required|xss_clean');
			//$this->form_validation->set_rules('city', 'City','trim|required|xss_clean');
			$this->form_validation->set_rules('zipcode', 'Zipcode','trim|required|xss_clean');
			$this->form_validation->set_rules('state', 'State','trim|required|xss_clean');
			$this->form_validation->set_rules('address', 'Address','trim|required|xss_clean');
			
			
			
			if($this->form_validation->run() == TRUE)
			{
                 
				if ($_FILES["photo"]["error"] > 0) {
					//echo "Error: " . $_FILES["photo"]["error"] . "<br>";
				}else {
					//echo "Temp file: " . $_FILES["photo"]["tmp_name"] . "<br>";
					if (file_exists("upload/" . $_FILES["photo"]["name"])) {
						//$_FILES["photo"]["name"] . " already exists. ";
					} else {
					    $file_name=uniqid().$_FILES["photo"]["name"];

						if(move_uploaded_file($_FILES["photo"]["tmp_name"],
						"uploads/" . $file_name)){
						  $this->session->set_userdata('photo', $file_name);
						  $this->session->set_userdata('user_photo', $file_name);
						}else{
						  $this->session->set_userdata('photo',$this->session->userdata('user_photo'));
						  $this->session->set_userdata('user_photo', 'no_profile.jpg');
						};

					}
				}

				 $this->user->editcaregiverProfile($this->session->userdata('user_id'));
   
				 $this->session->set_flashdata('flashMessage', 'Profile Edited Successfully !');
				 
				 redirect('/auth/index/');
			}

			//$this->data['states']= $this->user->getStates();
			$this->data['caregivers'] = $this->user->get_caregivers_by_id($result);
			//print_r($this->data['caregivers']);
			$this->data['users'] = $this->user->get_users_by_id($result);
			$this->load->view('home/caregiver_edit', $this->data);

	   
	}
	
	
	public function fetch_patient_details($id=null){		
		$patient_details= $this->patient->get_patient_by_id($id);
		echo $patient_details[0]->name.','.$patient_details[0]->id;
	}
	
/*==========================================///////// get the owner information /////////======================================== */	
	public function fetch_owner($id)
	{
		$patient_details= $this->patient->get_owner($id);
		echo $patient_details[0]->own_licence;
	}
	
    public function add_foods(){
	 if($_POST)
	 {
		$this->form_validation->set_rules('food_name', 'Food Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('food_type', 'Food Type ', 'trim|required|xss_clean');
		$this->form_validation->set_rules('created', 'Created ', 'trim|required|xss_clean');
		
			$temp=$_POST['created'];
			$data=explode('/',$temp);
			$result=$data[2].'-'.$data[0].'-'.$data[1];
			$_POST['created']=$result;
			
			if($this->form_validation->run() == TRUE)
			{
				$this->patient->addfoods();
				$this->data['flash']="Diet has been added !";
			}
			else
			{
				$this->data['flash']="Unsuccesfull, Try again !";

			}	
		}

			$this->session->userdata('user_role');

			$this->data['foods']= $this->admin->getfoodsAll();
			if($this->session->userdata('user_role')=='admin')
			{
				$this->load->view('admin/add_foods', $this->data);
			}
			else
			{
				$this->load->view('home/add_foods', $this->data);
			}


    }


	public function list_foods()
	{	   
		$this->data['foods']=$this->patient->getfoods();
		$this->load->view('admin/list_foods', $this->data);
	}

	public function search()
	{ 
		$username = $this->session->userdata('owner_prof');
		$pro= $this->user->getprof($username);
		// $mid=$pro['patid'];
		//$pro= $this->user->getprof1($username); 
		$this->data['profetional']=$pro;
		$pid= $this->data['profetional'][0]->id;
		$this->session->set_userdata('usertypeid', '2');
		
		$this->session->set_userdata('userid', $pid);
		
		
		if($_POST)
		{  
		  
			if($_POST['form_type']== 'patient_search')
			{    
			    $this->session->set_userdata('search_keyword1',$_POST['keyword']);
			     
				$this->data['patients']= $this->patient->searchPatients();
				$this->data['professionals']= $this->user->getProfessionals();
				
			
			  }
			elseif($_POST['form_type']== 'professional_search')
			{
			    $this->session->set_userdata('search_keyword2',$_POST['keyword']);
				
				//$this->data['patients']= $this->patient->getPatients();
				//$this->data['professionals']= $this->user->searchProfessionals();
				$this->data['patients']= $this->patient->searchPatients();
				$this->data['professionals']= $this->user->getProfessionals();
				
				
				
			}
	      
	     
		}
		else
		{
			$pid= $this->data['profetional'][0]->id;
			$this->data['patients'] = $this->patient->getPatients2($pid);
			$this->data['professionals1'] = $this->user->getnewsProfessionals();
			
			$this->data['professionals'] = $this->user->getProfessionals($pid);
			
			
			
			$this->data['caregivers'] = $this->user->getprofetionals123($pid);
		}
		
		
	
		$pfid=$this->data['profetional'][0]->id;
        $photo=$this->data['profetional'][0]->photo;
        $name=$this->data['profetional'][0]->name;
		//$this->data['profnoti1']= $this->user->getprofnoti_new($pfid);
		$this->data['profnoti1']= $this->user->getprofnoti_new_pop($pfid);
		
		
		
		$this->data['profnoti2']= $this->user->getprofnotinet_new($pfid);
		$this->data['profnoti3']= $this->user->getprofnoticon_new($pfid);
        //$this->data['profnoti4']= $this->user->getprofnoticon_msg($pfid);
		//$this->data['profnoti5']= $this->user->getprofnoticon_msg1($pfid);
		
		$this->load->view('home/search', $this->data);
    	}
	
/*  Chat code is start heat      */

function ajax_newchat()
{
	
$this->data['rid']=$_POST['Recvierid'];
$this->data['ctitle']=$_POST['chatboxtitle'];
$this->data['ctype']=$_POST['chatwith'];
$this->data['receivertype']=$_POST['receivertype'];
$this->load->view('home/ajax_newchat', $this->data);

}

function sendchatinsert($var)
{
        $username = $this->session->userdata('owner_prof');
		$pro= $this->user->getprof($username);
		//$mid=$pro['patid'];
		//$pro= $this->user->getprof1($username); 
		$this->data['profetional']=$pro;
		$pid=  $this->data['profetional'][0]->id;	
        $name= $this->data['profetional'][0]->name;
		$var;
		//$this->data['name']=$name;
		$this->data['action']=$var;	
		//$this->data['to']=$_POST['to'];
		//$this->data['message']=$_POST['message'];
		//$this->data['receivertype']=$_POST['receivertype'];
		//$this->load->view('home/chat', $this->data);
		
		$this->data['action']=$var;	
		/*$this->data['to']=$_POST['to'];
		$this->data['message']=$_POST['message'];
		$this->data['receivertype']=$_POST['receivertype'];
		$this->data['name']=$name;
		*/
		//$this->load->view('home/chat', $this->data);



session_start();
 $action;
	//  $fr=$this->session->userdata('userid');$this->session->userdata('usertypeid');
	$utype=2;
	$from =$pid; 
	$to = $_POST['to'];
	$message =$_POST['message'];
    $receivertype=$_POST['receivertype'];
	$loginusertye=$utype;
   // $_SESSION['openChatBoxes'][$_POST['to']] = date('Y-m-d H:i:s', time());
	
	//$messagesan =$this->sanitize($message);
    $whn=time(); 
$ch="select * from chat where (chat.from='$from' or chat.to='$from') and (chat.from='$to' or chat.to='$to')";
$ce=mysql_query($ch) or die(mysql_error());
$cn=mysql_num_rows($ce);


if($cn<=0)
{
	
$var=  uniqid('', true);

$var;
	
	
$sql = "insert into chat (text_session,whn,receivertype,chat.from,chat.to,message,usertype,sent) values ('$var','$whn','$receivertype','".mysql_real_escape_string($from)."', '".mysql_real_escape_string($to)."','".mysql_real_escape_string($message)."','".$loginusertye."',NOW())";
}
if($cn>0)
{
	
$ch="select * from chat where (chat.from='$from' or chat.to='$from') and (chat.from='$to' or chat.to='$to')";
$ce=mysql_query($ch) or die(mysql_error());
$cr=mysql_fetch_array($ce);
$var=$cr['text_session'];

$sql = "insert into chat (text_session,whn,receivertype,chat.from,chat.to,message,usertype,sent) values ('$var','$whn','$receivertype','".mysql_real_escape_string($from)."', '".mysql_real_escape_string($to)."','".mysql_real_escape_string($message)."','".$loginusertye."',NOW())";


}

	$query = mysql_query($sql) or die(mysql_error());
		 $lid=mysql_insert_id();
	 $this->session->set_userdata('chatMaxID',$lid);
	$_SESSION['chatMaxID']=mysql_insert_id();
	
	



if($utype==2 and $receivertype==2)
{
	$logintype="prof";
}
if($utype==1 and $receivertype==2)
{
	$logintype="care";
}
if($utype==2 and $receivertype==1)
{
	$logintype="careprof";
}


if($receivertype==2)
	{
		$res="professional";
	}
    if($receivertype==1)
	{
		$res="caregiver";
	}

    $rname=$_POST['uname'];
	$message= $name." ha inviato un messaggio";
$ntype="messages".$loginusertye;
$nip=$_SERVER['REMOTE_ADDR'];

	$ds="select * from device_tocken where userid='$to' and user_type='$res'";
	$se=mysql_query($ds) or die(mysql_error());
	$sn=mysql_num_rows($se);
    if($sn>0)
	{	
	while($sr=mysql_fetch_array($se))
	{
		
		$device=$sr['tockenid'];
		$device_platform=$sr['platform'];
		$msg = array(
						'message'       => $message,
						'title'         => $name.'Send The Message',
						'subtitle'      => 'Chat Mssage',
						'sid'           =>$from,
						'rid'           =>$to,
						'sname'         =>$name,
						'recivertype'   =>$loginusertye,
						'logintype'     =>$logintype,
						'chat'          =>"text", 
						'call_type'     =>"text",
						'session_id'    =>$var,
						'rname'         =>$rname,
						'vibrate'        => 1,
						'sound'         => 1
					);
		 $re = $this->sendPushNotificationToGCM($device,$msg,$device_platform,$name,$from,$to,$loginusertye,$logintype,$var,$rname);
	}
	}
$nts="insert into popup_notifications(note_message,n_sid,n_rid,nm_id,status,note_type,n_ip,text_session,ltype) values('$message','$from','$to','$lid','0','$ntype','$nip','$var','$res')";
$nte=mysql_query($nts) or die(mysql_error());
	

	
	
	
	
	
	
	

	
	
	echo "1";
	exit(0);   
	


	
}

function ajax_newchat_loadmore()
{
	
$this->data['rid']=$_POST['Recvierid'];
//$this->data['ctitle']=$_POST['chatboxtitle'];
$this->data['ctype']=$_POST['chatwith'];
$this->data['receivertype']=$_POST['receivertype'];
$this->load->view('home/ajax_newchat_load', $this->data);

}







function ajax_newchathotbeat()
{
$this->data['rid']=$_POST['Recvierid'];
$this->data['ctitle']=$_POST['chatboxtitle'];
$this->data['ctype']=$_POST['chatwith'];
$this->data['receivertype']=$_POST['receivertype'];
	
$this->load->view('home/ajax_chat_hotbeat', $this->data);	
}
function messageread($rid)
{
 $username = $this->session->userdata('owner_prof');
$pro= $this->user->getprof($username);
// $mid=$pro['patid'];
//$pro= $this->user->getprof1($username); 
$this->data['profetional']=$pro;
$pid= $this->data['profetional'][0]->id;
 
 
 $rid;
$us="update chat set recd='1' where chat.to='$pid' and chat.from='$rid'";
$ue=mysql_query($us) or die(mysql_error());

$us1="update popup_notifications set status='1' where n_sid='$rid' and n_rid='$pid'";
$ue1=mysql_query($us1) or die(mysql_error());

}
	public function agenda()
	{ 
	    $username = $this->session->userdata('owner_prof');
		$pro= $this->user->getprof($username);
		$this->data['profetional']=$pro;
		$pid= $this->data['profetional'][0]->id;
		$this->session->set_userdata('usertypeid', '2');
		$this->session->set_userdata('userid', $pid);
		if($_POST)
		{  
			if($_POST['form_type']== 'patient_search')
			{    
			    $this->session->set_userdata('search_keyword1',$_POST['keyword']);
				$this->data['patients']= $this->patient->searchPatients();
				$this->data['professionals']= $this->user->getProfessionals();
			  }
			elseif($_POST['form_type']== 'professional_search')
			{
			    $this->session->set_userdata('search_keyword2',$_POST['keyword']);
				$this->data['patients']= $this->patient->searchPatients();
				$this->data['professionals']= $this->user->getProfessionals();
			}
		}
		else
		{
			$pid= $this->data['profetional'][0]->id;
			$this->data['patients'] = $this->patient->getPatients22($pid);
			$this->data['professionals1'] = $this->user->getnewsProfessionals();
			$this->data['professionals'] = $this->user->getProfessionals($pid);
			$this->data['caregivers'] = $this->user->getprofetionals123($pid);
		}
		
		$pfid=$this->data['profetional'][0]->id;
        $photo=$this->data['profetional'][0]->photo;
        $name=$this->data['profetional'][0]->name;
		$this->data['profnoti1']= $this->user->getprofnoti_new_pop($pfid);
		$this->data['profnoti2']= $this->user->getprofnotinet_new($pfid);
		$this->data['profnoti3']= $this->user->getprofnoticon_new($pfid);
        //$this->data['profnoti4']= $this->user->getprofnoticon_msg($pfid);
		//$this->data['profnoti5']= $this->user->getprofnoticon_msg1($pfid);
		$date1=$_POST['date'];
		if(!empty($date1))
		{
			//$date=$_POST['date'];
			$exp=$_POST['date'];
		$nd1=explode("-",$exp);
	    $date=$nd1[2]."-".$nd1[1]."-".$nd1[0];
			
			
			
			
			
			
		$this->data['agenda'] = $this->user->getagenda($pid,$date);
		$pid= $this->data['profetional'][0]->id;
			$this->data['patients'] = $this->patient->getPatients2($pid);
			$this->data['professionals1'] = $this->user->getnewsProfessionals();
			$this->data['professionals'] = $this->user->getProfessionals($pid);
			$this->data['caregivers'] = $this->user->getprofetionals123($pid);
		
		
		
			
		}
		else
		{
		$date=date("Y-m-d");
    	$this->data['agenda'] = $this->user->getagenda1($pid);
		}
		$pfid=$this->data['profetional'][0]->id;
        $photo=$this->data['profetional'][0]->photo;
        $name=$this->data['profetional'][0]->name;
		$this->data['profnoti1']= $this->user->getprofnoti_new_pop($pfid);
		$this->data['profnoti2']= $this->user->getprofnotinet_new($pfid);
		$this->data['profnoti3']= $this->user->getprofnoticon_new($pfid);
		
		//$this->data['caregivers']= $this->user->getcaregivers_new1($pid,$pfid);
		
		$ps1="select * from cons_request_messages as cons,caregiver as cs  where (recipient='$pid' and subject!='') and           (cons.sender=cs.id)"; 
	    $pe1=mysql_query($ps1) or die(mysql_error());
	    $pn1=mysql_num_rows($pe1);
		
	      $this->data['pn1']=$pn1;	
		
		$this->load->view('home/agenda', $this->data);
	
		
		
		
		 
		 
	}


	
	
	
	public function addagenda($pid)
	{
		  
	 
		$username = $this->session->userdata('owner_prof');
		$pro= $this->user->getprof($username);
		// $mid=$pro['patid'];
		//$pro= $this->user->getprof1($username); 
		$this->data['profetional']=$pro;
		$pid= $this->data['profetional'][0]->id;
		$this->session->set_userdata('usertypeid', '2');
		
		$this->session->set_userdata('userid', $pid);
		
		
		   if(!empty($_POST['form_type']))
		   {
		  
			if($_POST['form_type']== 'patient_search')
			{    
			    $this->session->set_userdata('search_keyword1',$_POST['keyword']);
			     
				$this->data['patients']= $this->patient->searchPatients();
				$this->data['professionals']= $this->user->getProfessionals();
				
			
			  }
			elseif($_POST['form_type']== 'professional_search')
			{
			    $this->session->set_userdata('search_keyword2',$_POST['keyword']);
				
				//$this->data['patients']= $this->patient->getPatients();
				//$this->data['professionals']= $this->user->searchProfessionals();
				$this->data['patients']= $this->patient->searchPatients();
				$this->data['professionals']= $this->user->getProfessionals();
				
				
				
			}
			
		   }
	     	$pid= $this->data['profetional'][0]->id;
			$this->data['patients'] = $this->patient->getPatients22($pid);
			$this->data['professionals1'] = $this->user->getnewsProfessionals();
			
			$this->data['professionals'] = $this->user->getProfessionals($pid);
			
			
			
			$this->data['caregivers'] = $this->user->getprofetionals123($pid);
		
		
		
	
		$pfid=$this->data['profetional'][0]->id;
        $photo=$this->data['profetional'][0]->photo;
        $name=$this->data['profetional'][0]->name;
		$this->data['profnoti1']= $this->user->getprofnoti_new_pop($pfid);
		$this->data['profnoti2']= $this->user->getprofnotinet_new($pfid);
		$this->data['profnoti3']= $this->user->getprofnoticon_new($pfid);
        //$this->data['profnoti4']= $this->user->getprofnoticon_msg($pfid);
		//$this->data['profnoti5']= $this->user->getprofnoticon_msg1($pfid);
		
		if($_POST)
		{
			 $title=$_POST['title'];
			 $details=$_POST['details'];
			 $time=$_POST['time'];
			
			
			$exp=$_POST['date'];
	
	
		$nd1=explode("-",$exp);
	    $date=$nd1[2]."-".$nd1[1]."-".$nd1[0];
			
			
			
			if(!empty($title) and !empty($details) and !empty($time))
			{
				$this->data['agenda'] = $this->user->insert_agenda($title,$details,$time,$date,$pid);
				redirect('/prof/agenda/');
			}
			
			
			
			
			
			
		}
		
		$pfid=$this->data['profetional'][0]->id;
        $photo=$this->data['profetional'][0]->photo;
        $name=$this->data['profetional'][0]->name;
		$this->data['profnoti1']= $this->user->getprofnoti_new_pop($pfid);
		$this->data['profnoti2']= $this->user->getprofnotinet_new($pfid);
		$this->data['profnoti3']= $this->user->getprofnoticon_new($pfid);
		
		//$this->data['caregivers']= $this->user->getcaregivers_new1($pid,$pfid);
		
		
		$ps1="select * from cons_request_messages as cons,caregiver as cs  where (recipient='$pfid' and subject!='') and (cons.sender=cs.id)"; 
$pe1=mysql_query($ps1) or die(mysql_error());
$pn1=mysql_num_rows($pe1);
	$this->data['pn1']=$pn1;	
		
		
		
		
		$this->load->view('home/addagenda', $this->data);
	 
	}
	
	
	
	public function editagenda($aid)
	{
		  
	 
		$username = $this->session->userdata('owner_prof');
		$pro= $this->user->getprof($username);
		// $mid=$pro['patid'];
		//$pro= $this->user->getprof1($username); 
		$this->data['profetional']=$pro;
		$pid= $this->data['profetional'][0]->id;
		$this->session->set_userdata('usertypeid', '2');
		
		$this->session->set_userdata('userid', $pid);
		
		
		if($_POST)
		{  
		  
			if($_POST['form_type']== 'patient_search')
			{    
			    $this->session->set_userdata('search_keyword1',$_POST['keyword']);
			     
				$this->data['patients']= $this->patient->searchPatients();
				$this->data['professionals']= $this->user->getProfessionals();
				
			
			  }
			elseif($_POST['form_type']== 'professional_search')
			{
			    $this->session->set_userdata('search_keyword2',$_POST['keyword']);
				
				//$this->data['patients']= $this->patient->getPatients();
				//$this->data['professionals']= $this->user->searchProfessionals();
				$this->data['patients']= $this->patient->searchPatients();
				$this->data['professionals']= $this->user->getProfessionals();
				
				
				
			}
	      
	     
		}
		else
		{
			$pid= $this->data['profetional'][0]->id;
			$this->data['patients'] = $this->patient->getPatients22($pid);
			$this->data['professionals1'] = $this->user->getnewsProfessionals();
			
			$this->data['professionals'] = $this->user->getProfessionals($pid);
			
			
			
			$this->data['caregivers'] = $this->user->getprofetionals123($pid);
		}
		
		
	
		$pfid=$this->data['profetional'][0]->id;
        $photo=$this->data['profetional'][0]->photo;
        $name=$this->data['profetional'][0]->name;
		$this->data['profnoti1']= $this->user->getprofnoti_new_pop($pfid);
		$this->data['profnoti2']= $this->user->getprofnotinet_new($pfid);
		$this->data['profnoti3']= $this->user->getprofnoticon_new($pfid);
        //$this->data['profnoti4']= $this->user->getprofnoticon_msg($pfid);
		//$this->data['profnoti5']= $this->user->getprofnoticon_msg1($pfid);
		
		if($_POST)
		{
		 $title=$_POST['title'];
		 $details=$_POST['details'];
		 $time=$_POST['time'];
		 //$date=date("Y-m-d");
		$exp=$_POST['date'];
		$nd1=explode("-",$exp);
	    $date=$nd1[2]."-".$nd1[1]."-".$nd1[0];
		
		
		if(!empty($title) and !empty($details) and !empty($time))
		{
		$this->data['agenda'] = $this->user->update_agenda($title,$details,$time,$date,$aid);
		//redirect('/prof/agenda/');
			}
			
		}
		
		
		
		$this->data['agenda'] = $this->user->edit_agenda($aid);
		
		
		
		
	$this->load->view('home/edit_agenda', $this->data);
	 
	}
	
	
	
	
	function delete_agenda($aid)
	{
	 	$id=$this->user->delete_agenda($aid);
		redirect('/prof/agenda/');
	}
	
	public function search2()
	{
		 $username = $this->session->userdata('owner_prof');
		$this->data['profetional']= $this->user->getprof($username);
		$pid=$this->data['profetional'][0]->id;
		$name=$this->data['profetional'][0]->name;
		
		if($_POST)
		{
			$this->session->set_userdata('search_keyword1',$_POST['keyword']);
			$key=$_POST['keyword'];
			
			if($_POST['form_type']== 'patient_search')
			{
			    
			   	//$this->data['patients']= $this->patient->getPatients3();
				//$this->data['professionals']= $this->user->getProfessionals();
				
				$this->data['patients']= $this->patient->searchPatientsprf($pid);
				$this->data['professionals']= $this->user->getProfessionals($pid);
				
				
				
				$str='';
					if(count($this->data['patients'])>0){
					 foreach($this->data['patients'] as $patient){
						 $pth=$patient->photo;
						 //$noimg="profile_pic.jpg";
						 ?>
						 
						<li class="odd">
<div class="img-thumb">
<?php if(!empty($pth)) { ?>
<img src="<?php echo base_url(); ?>uploads/<?php echo $patient->photo; ?>" alt="pat img"  width="60" height="60"/>
<?php } else { ?>
<img src="<?php echo base_url(); ?>img/img-holder2.jpg" alt="pat img"  width="60" height="60"/>
<?php } ?>
</div>
<div class="pat-desc-cont">
<h5><a href="<?php echo base_url(); ?>prof/patient_routine/<?php echo $patient->pat_id; ?>?pid=<?php echo $patient->pat_id; ?>"><?php echo $patient->name; ?> </a></h5>
<p><?php echo $patient->surname; ?> - <?php echo $patient->age; ?> </p>
<span> <?php echo $patient->gender; ?> </span> </div>
</li>
					 <?php
						 
					 }
					}else{
						 $str .='<div class="profiles">';
						 $str .='<img src="'.base_url("img/no_record.gif").'" alt="" />';
						 $str .='</div>';
					}
	
			
			}
			elseif($_POST['form_type']== 'professional_search')
			{
			     $this->session->set_userdata('search_keyword2',$_POST['keyword']);
				
				$this->data['patients']= $this->patient->getPatients2($pid);
				$this->data['professionals']= $this->user->searchProfessionals1($key,$pid);

					$str='';
					if(count($this->data['professionals'])>0){
					foreach($this->data['professionals'] as $professional){
  $noimg="profile_pic.jpg";
  if($pid!=$professional->pfid)
  {  
  $mpid=$professional->pfid;
  ?>
  <li class="odd">
<div class="img-thumb">
<?php if(!empty($professional->photo)) { ?>
<img src="<?php echo base_url(); ?>uploads/<?php echo $professional->photo; ?>" alt="pat img" width="64" height="64" />
<?php } else { ?>
<img src="<?php echo base_url(); ?>img/img-holder2.jpg" alt="pat img"  width="60" height="60"/>
<?php } ?>
</div>
<div class="pat-desc-cont">
<h5><a href="<?php echo base_url() ?>prof/professionaldetails/<?php echo $professional->pfid; ?>"> <?php echo $professional->name.' '.$professional->surname;
	//$name=$professional->qualification;
	
	 ?></a></h5>
<p><?php //echo $professional->surname; ?>  <b>Professione </b> : <?php echo $professional->qualification; ?> 
<span><?php //echo $professional->gender; ?>
<?php //if($mn1<=0) {} 
	  if($professional->netstatus==0 and  $professional->netstatus!=NULL ) 
	  {
	  ?>
<h6> <font color="#FF0000"> <b><?php echo $this->lang->line('pending'); ?> </b> </font> </h6>
<?php 
	  }
	  else
	  {
if($professional->netstatus!=1)
	{  
 ?>
<span id="mn<?php echo $mpid; ?>" style="width:90%; float:left;"> <a href="javascript:void(0)" onclick="sendfallowrequest('<?php echo $pid;  ?>','<?php echo $mpid; ?>')"> <img src="<?php echo base_url(); ?>img/add-icn.png" alt="Add"> </a> </span>
<?php   
	}
	}
   if($professional->netstatus==1) 
	  { 
	   $name=$professional->name;
	   ?>
<div class="icns"> <a href="javascript:void(0)" onclick="javascript:chatWith('<?php echo $professional->pfid; ?>','<?php echo $professional->name; ?>','<?php echo $this->session->userdata('usertypeid'); ?>','<?php echo $name; ?>','<?php echo '2' ?>','<?php echo $photo; ?>')"><img src="<?php echo base_url(); ?>img/msg-icn2.png" alt="MSG" /></a> <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>img/vid-icn.png" alt="MSG"  onClick="startcall('<?php echo $professional->pfid;?>','video_pp');"/></a> </div>
<?php } ?>
</span>
</p>
</div>
  </li>
  <?php   
 	  
  } 

}
}
else
{
$str .='<div class="profiles">';
$str .='<img src="'.base_url("img/no_record.gif").'" alt="" />';
$str .='</div>';
					}
					
			}

				else
				{
					$str .='<div class="profiles">';
						 $str .='<img src="'.base_url("img/no_record.gif").'" alt="" />';
						 $str .='</div>';
				}
		  
		  
	    }

	}
	
	
	function professionaldetails($id=null)
	{
	    ///if (!($this->session->userdata('user_name')!="")) {
	   //redirect('/auth/login/');
	  //} else {
	/*$username = $this->session->userdata('owner_prof');
	$pro= $this->user->getprof($username);
	// $mid=$pro['patid'];
	//$pro= $this->user->getprof1($username);
	$this->data['profetional']=$pro;
		  $this->data['user'] = $this->user->getProfessionals_by_id($id);*/
		  
		  
     			   $username = $this->session->userdata('owner_prof');
			       $pro= $this->user->getprof($username);
				   //$mid=$pro['patid'];
			       //$pro= $this->user->getprof1($username);
				   $this->data['profetional']=$pro;
				   $this->data['professionals'] = $this->user->get_professionals_by_id($id);
	               $this->load->view('home/professionaldetails', $this->data);
		 
		//}
	}
	function caregiverdetails($id=null)
	{
		if (!($this->session->userdata('user_name')!="")) {
		redirect('/auth/login/');
		} else {
		$this->data['user'] = $this->user->getCaregivers_by_id($id);
		$this->load->view('home/caregiverdetails', $this->data);

		}
	}
	
	
	public function patient_routine($pid)
	{
		$username = $this->session->userdata('owner_prof');
		$pro= $this->user->getprof($username);
		
		$this->data['profetional']=$pro;
		$this->data['menu']="routine";
		$id=$pro[0]->id;
		//$this->data['caregivers']= $this->user->getcaregivers_new($pid);
		 $pid;
	    $this->data['profetional1']=$this->user->getprofetionals1($id,$pid);
	    $this->data['profid']=$id;
		$this->data['notification']= $this->user->getnotification_new($pid);
		$this->data['patientdel']= $this->user->getpatient_new($pid);
		$pfid=$this->data['profetional'][0]->id;
        $photo=$this->data['profetional'][0]->photo;
        $name=$this->data['profetional'][0]->name;
		$this->data['profnoti1']= $this->user->getprofnoti_new_pop($pfid);
		$this->data['profnoti2']= $this->user->getprofnotinet_new($pfid);
		$this->data['profnoti3']= $this->user->getprofnoticon_new($pfid);
		$this->data['caregivers']= $this->user->getcaregivers_new1($pid,$pfid);
		
		$this->load->view('home/prof_show_events', $this->data);
	}
	
	
	
	
	
	
	
	
	
function serach_associatte_prof()
{
	$username = $this->session->userdata('owner_prof');
		$pro= $this->user->getprof($username);
		
		$this->data['profetional']=$pro;
	$pfid=$this->data['profetional'][0]->id;
	 $srch=$_POST['keyword'];
	 $pid=$_POST['pid'];
	if($_POST)
		{

if($_POST['form_type']== 'patient_search')
{ 
 
 
	//$this->data['caregivers']= $this->caregiver_model->getcaregivers_new($pid,$cid1);
	$this->data['profetional']=$this->user->getprofetionals1_search($pid,$pfid,$srch);
	//echo count($this->data['profetional']);
if(count($this->data['profetional'])>0){ 

 foreach($this->data['profetional'] as $patient){
	  
	  	  
		   $mpid= $patient->nprfid;
	/*$prid=$patient->rid;
	$mps="select * from professionals where id='$prid' and id!='$profid'";
	$mpe=mysql_query($mps) or die(mysql_error());
	$mpn=mysql_num_rows($mpe);
	$mpr=mysql_fetch_array($mpe);
	
 $mpid=$mpr['id'];
 $mps="select * from professional_networks_prf where (sid='$pid' or rid='$pid') and  (sid='$mpid' or rid='$mpid')";
 $me=mysql_query($mps) or die(mysql_error());
 $mr=mysql_fetch_array($me);
 $mn1=mysql_num_rows($me);
 $send=$mr['netstatus'];
	*/

if($mpid!=$pfid)
  {
	  ?>
<li class="odd">
<div class="img-thumb">
<?php if(!empty($patient->photo)) { ?>
<img src="<?php echo base_url('uploads/'.$patient->photo);?>" alt="pat img"  width="60" height="60"/>
<?php } else { ?>
<img src="<?php echo base_url(); ?>img/pat-img.jpg" alt="pat img" />
<?php } ?>
</div>
	<div class="pat-desc-cont">
		<h5><a href="<?php echo base_url() ?>prof/professionaldetails/<?php echo $patient->nprfid; ?>"><?php  echo $patient->name;   ?> </a></h5>
		<!--<p> <a href=""> Message </a> </p>-->
		<span><b><?php echo $this->lang->line('gender'); ?> </b><?php echo $patient->gender; ?> <b> <?php echo $this->lang->line('age'); ?> </b> <?php echo $patient->age; ?> </span>
<div class="icns">
			
			<span>
            
<?php 
 $status=$patient->netstatus;
 
if($status==0) { ?><!--<span id="mn<?php echo $mpid; ?>" style="width:90%; float:left;">
      <a href="javascript:void(0)" onclick="sendfallowrequest('<?php echo $pid;  ?>','<?php echo $patient->pfid; ?>')"> <img src="<?php echo base_url(); ?>img/add-icn.png" alt="Add"> </a>
      </span>--><?php }    else {  ?>  <div class="icns">

<a href="javascritp:void(0)" onclick="javascript:chatWith('<?php echo $patient->nprfid; ?>','<?php echo $patient->name; ?>','<?php echo $this->session->userdata('usertypeid'); ?>','<?php echo $name; ?>','<?php echo '2' ?>','<?php echo $photo; ?>')"><img src="<?php echo base_url(); ?>img/msg-icn2.png" alt="MSG" /></a>
<a href="javascript:void(0)" onClick="startcall('<?php echo $patient->nprfid;?>','video_pp');"><img src="<?php echo base_url(); ?>img/vid-icn.png" alt="MSG" /></a>
</div> <?php } ?>
</span>
		</div>
	</div>
</li>
<?php 
  }

 
 }


 }
 
 else
 {
	 echo "<li>No Records Found</li>";
 }
	
}

if($_POST['form_type']== 'patient_search2')
{ 

 
//$this->data['caregivers']= $this->caregiver_model->getcaregivers_new($pid,$cid1);
//$this->data['profetional']=$this->caregiver_model->getprofetionalssearch($pid,$careid,$srch);
$this->data['profetional']= $this->user->getcaregivers_new1_search($pid,$pfid,$srch);
//echo count($this->data['profetional']);
if(count($this->data['profetional'])>0){ 
 foreach($this->data['profetional'] as $patient){
 
 
 ?>
	<li class="odd">
	<div class="img-thumb">
                        <?php if(empty($patient->photo)) { ?>
                        <img src="<?php echo base_url(); ?>img/pat-img.jpg" alt="pat img" />
                        <?php }
	else
	{
	 ?>
<img src="<?php echo base_url(); ?>uploads/<?php echo $patient->photo; ?>" alt="pat img"  width="60" height="60"/>
<?php } ?>
</div>
	<div class="pat-desc-cont">
		<h5><a href="<?php echo base_url() ?>prof/view_caregiver/<?php echo $patient->careid; ?>"> <?php echo $patient->name; ?> <?php echo $patient->surname; ?> </a></h5>
		<!--<p> <a href=""> Message </a></p>-->
		<span> <b><?php echo $this->lang->line('age'); ?></b> <?php echo $patient->age; ?> <b> Genere</b>  <?php echo $patient->gender; ?> </span>
	<div class="icns">
			<?php if($patient->status==1) {  ?>
			<a href="javascript:void(0)" onclick="javascript:chatWith('<?php echo $patient->careid; ?>','<?php echo $patient->name; ?>','<?php echo $this->session->userdata('usertypeid'); ?>','<?php echo $name; ?>','<?php echo '1' ?>','<?php echo $photo; ?>')"><img src="<?php echo base_url(); ?>img/msg-icn2.png" alt="MSG" /></a>
			<a href="javascript:void(0)"><img src="<?php echo base_url(); ?>img/vid-icn.png" alt="MSG" onClick="startcall('<?php echo $patient->careid;?>','video_cp');"/></a>
            <?php } ?>
		</div>
                        
                        </div>
</li>
    
    
    
    <?php
 } }
 
 else
 {
	 echo "<li>No Records Found</li>";
 }
	
}





		}
	
	
}
	
	
	
	
	
function showparameter($pid)
  {
	  
/*$username = $this->session->userdata('owner_prof');
		$pro= $this->user->getprof($username);
		//$mpr=$pro;
		// $mid=$pro['patid'];
		//$pro= $this->user->getprof1($username);
		$this->data['profetional']=$pro;
		
		$this->data['menu']="parameter";
		
	
   $eventid=1;
	
		//$this->data['parameters']=$this->user->getparameters1($per_pg,$offset);

		
		$this->data['caregivers']= $this->user->getcaregivers_new1($pid,$pfid);

       $this->data['parameters']=$this->user->fetch_parameter1($pid,$eventid);



$username = $this->session->userdata('owner_prof');
		
		$pro= $this->user->getprof($username);
		// $mid=$pro['patid'];
		//$pro= $this->user->getprof1($username);
		$this->data['profetional']=$pro;
	 
	 
        $id=$pro[0]->id;
		$this->data['caregivers']= $this->user->getcaregivers_new($pid);
	    $this->data['profetional1']=$this->user->getprofetionals1($id,$pid);
	$this->data['profid']=$id;
	$this->data['patientdel']= $this->user->getpatient_new($pid);
	
	
	    $pfid=$this->data['profetional'][0]->id;
       /// $photo=$this->data['profetional'][0]->photo;
        $name=$this->data['profetional'][0]->name;
		$this->data['profnoti1']= $this->user->getprofnoti_new_pop($pfid);
		$this->data['profnoti2']= $this->user->getprofnotinet_new($pfid);
		$this->data['profnoti3']= $this->user->getprofnoticon_new($pfid);
		
		$this->data['caregivers']= $this->user->getcaregivers_new1($pid,$pfid);*/


$eventid=1;
	//$pid="64";
    //$this->data['activity']=$this->user->fetch_activity($pid,$eventid);
	//$this->data['diets']=$this->user->fetch_diets($pid,$eventid);
	 $this->data['parameters']=$this->user->fetch_parameter12($pid,$eventid);
	 
	 
	 $username = $this->session->userdata('owner_prof');
		
		$pro= $this->user->getprof($username);
		// $mid=$pro['patid'];
		//$pro= $this->user->getprof1($username);
		$this->data['profetional']=$pro;
	 
    $this->data['menu']="parameter";
	    $id=$pro[0]->id;
		$this->data['caregivers']= $this->user->getcaregivers_new($pid);
	    $this->data['profetional1']=$this->user->getprofetionals1($id,$pid);
        $this->data['profid']=$id;
	
	$this->data['patientdel']= $this->user->getpatient_new($pid);
	
	$pfid=$this->data['profetional'][0]->id;
        $photo=$this->data['profetional'][0]->photo;
        $name=$this->data['profetional'][0]->name;
		$this->data['profnoti1']= $this->user->getprofnoti_new_pop($pfid);
		$this->data['profnoti2']= $this->user->getprofnotinet_new($pfid);
		$this->data['profnoti3']= $this->user->getprofnoticon_new($pfid);
		
		$this->data['caregivers']= $this->user->getcaregivers_new1($pid,$pfid);
        $this->load->view('home/prof_show_parameter', $this->data);
}




	function showparameter_graph($pid)
    {$username = $this->session->userdata('owner_prof');
		$pro= $this->user->getprof($username);
		//$mpr=$pro;
		// $mid=$pro['patid'];
		//$pro= $this->user->getprof1($username);
		$this->data['profetional']=$pro;
		
		$this->data['menu']="parameter";
		$id=$pro[0]->id;
		$this->data['caregivers']= $this->user->getcaregivers_new($pid);
	    $this->data['profetional1']=$this->user->getprofetionals1($id,$pid);
	    $this->data['profid']=$id;
	$this->data['parameters']=$this->user->get_paremeters();
   $eventid=1;
	
	if($_POST)
	{
		$praname=$_POST['praname'];
		$this->data['paraid']=$praname;
		$date=$_POST['date'];
		$this->data['date']=$date;
		$this->data['paralist']=$this->user->get_paremeters_list($pid,$praname,$date);
		
	}
	else
	{
		 $date=date("Y-m-d");
		  $praname=rand(1,5);
		  $this->data['paraid']=$praname;
		  $this->data['date']=$date;
		$this->data['paralist']=$this->user->get_paremeters_list($pid,$praname,$date);
	}
	
		//$this->data['parameters']=$this->user->getparameters1($per_pg,$offset);

   //$this->data['parameters']=$this->user->fetch_parameter1($pid,$eventid);
        $this->data['patientdel']= $this->user->getpatient_new($pid);
        $pfid=$this->data['profetional'][0]->id;
        $photo=$this->data['profetional'][0]->photo;
        $name=$this->data['profetional'][0]->name;
		$this->data['profnoti1']= $this->user->getprofnoti_new_pop($pfid);
		$this->data['profnoti2']= $this->user->getprofnotinet_new($pfid);
		$this->data['profnoti3']= $this->user->getprofnoticon_new($pfid);
		
		$this->data['caregivers']= $this->user->getcaregivers_new1($pid,$pfid);

   $this->data['parameters']=$this->user->fetch_parameter1($pid,$eventid);
 
$this->load->view('home/prof_show_parameter_graph', $this->data);

}
function show_report($pid)
{

        $username = $this->session->userdata('owner_prof');
		$pro= $this->user->getprof($username);
		//$mpr=$pro;
		// $mid=$pro['patid'];
		//$pro= $this->user->getprof1($username);
		$this->data['profetional']=$pro;
		
		
		$id=$pro[0]->id;
		$this->data['caregivers']= $this->user->getcaregivers_new($pid);
	    $this->data['profetional1']=$this->user->getprofetionals1($id,$pid);
		
		
		
	    $this->data['profid']=$id;
		$this->data['menu']="report";
	 
	     $eventid=4;
	    
if($_POST)
{
	
$from=$_POST['from'];
 $to=$_POST['to'];

		$nd1=explode("-",$from);
	     $ndate1=$nd1[2]."-".$nd1[1]."-".$nd1[0];
		$nd2=explode("-",$to);
	     $ndate2=$nd2[2]."-".$nd2[1]."-".$nd2[0];
$this->data['report']=$this->user->fetch_newreport1($pid,$ndate1,$ndate2);
$this->data['from']=$from;
$this->data['to']=$to;

}
else
{
$this->data['report']=$this->user->fetch_report1($pid);
}


$this->data['patientdel']= $this->user->getpatient_new($pid);


       $pfid=$this->data['profetional'][0]->id;
        $photo=$this->data['profetional'][0]->photo;
        $name=$this->data['profetional'][0]->name;
		$this->data['profnoti1']= $this->user->getprofnoti_new_pop($pfid);
		$this->data['profnoti2']= $this->user->getprofnotinet_new($pfid);
		$this->data['profnoti3']= $this->user->getprofnoticon_new($pfid);
		
		$this->data['caregivers']= $this->user->getcaregivers_new1($pid,$pfid);



$this->load->view('home/prof_show_report', $this->data);
	
}




function view_report2($date,$pid)
{ 
  
  
    $username = $this->session->userdata('owner_prof');
		$pro= $this->user->getprof($username);
		//$mpr=$pro;
		// $mid=$pro['patid'];
		//$pro= $this->user->getprof1($username);
		$this->data['profetional']=$pro;
		
		
		$id=$pro[0]->id;
		$this->data['caregivers']= $this->user->getcaregivers_new($pid);
	    $this->data['profetional1']=$this->user->getprofetionals1($id,$pid);
	    $this->data['profid']=$id;
		$this->data['menu']="report";

    
  
	 
	 $this->data['patient']=$this->user->get_patient($pid);
	 $patname=$this->data['patient'][0]->name;
	 
	 
	 	
	$this->data['report']=$this->user->fetch_report_details($pid,$date);
	
	$this->data['date']=$date;
	$this->data['pname']=$patname;
	
	$this->load->view('home/actionpdf', $this->data);
	
	
		
}
	
   	
 	
	
	
	
	
	
	
	
	
	
	
	public function show_drugs($pid)
	{
	$eventid=2;
	$pid="102";
    //$this->data['activity']=$this->user->fetch_activity($pid,$eventid);
	 $this->data['medicene']=$this->user->fetch_medecine1($pid,$eventid);
	 $username = $this->session->userdata('owner_prof');
		
		$pro= $this->user->getprof($username);
		// $mid=$pro['patid'];
		//$pro= $this->user->getprof1($username);
		$this->data['profetional']=$pro;
	 
	 $this->data['menu']="medicine";
        $id=$pro[0]->id;
		$this->data['caregivers']= $this->user->getcaregivers_new($pid);
	    $this->data['profetional1']=$this->user->getprofetionals1($id,$pid);
	$this->data['profid']=$id;
	$this->data['patientdel']= $this->user->getpatient_new($pid);
	
	
	$pfid=$this->data['profetional'][0]->id;
        $photo=$this->data['profetional'][0]->photo;
        $name=$this->data['profetional'][0]->name;
		$this->data['profnoti1']= $this->user->getprofnoti_new_pop($pfid);
		$this->data['profnoti2']= $this->user->getprofnotinet_new($pfid);
		$this->data['profnoti3']= $this->user->getprofnoticon_new($pfid);
		
		$this->data['caregivers']= $this->user->getcaregivers_new1($pid,$pfid);
	
	
	
	
	
	$this->load->view('home/prof_show_drugs', $this->data);
	}
	
	public function show_diets($pid)
	{
  	 
	$eventid=3;
	//$pid="64";
    //$this->data['activity']=$this->user->fetch_activity($pid,$eventid);
	//$this->data['diets']=$this->user->fetch_diets($pid,$eventid);
	$this->data['diets']=$this->user->fetch_diets1($pid,$eventid);
	
	
	 $username = $this->session->userdata('owner_prof');
		
		$pro= $this->user->getprof($username);
		// $mid=$pro['patid'];
		//$pro= $this->user->getprof1($username);
		$this->data['profetional']=$pro;
	 
    $this->data['menu']="diets";
	    $id=$pro[0]->id;
		$this->data['caregivers']= $this->user->getcaregivers_new($pid);
	    $this->data['profetional1']=$this->user->getprofetionals1($id,$pid);
        $this->data['profid']=$id;
	
	$this->data['patientdel']= $this->user->getpatient_new($pid);
	
	$pfid=$this->data['profetional'][0]->id;
        $photo=$this->data['profetional'][0]->photo;
        $name=$this->data['profetional'][0]->name;
		$this->data['profnoti1']= $this->user->getprofnoti_new_pop($pfid);
		$this->data['profnoti2']= $this->user->getprofnotinet_new($pfid);
		$this->data['profnoti3']= $this->user->getprofnoticon_new($pfid);
		
		$this->data['caregivers']= $this->user->getcaregivers_new1($pid,$pfid);
	
	
	
	
	
	
	$this->load->view('home/prof_show_diets', $this->data);
	 
	
	}
	
	public function show_activity($pid)
	{
	$eventid=4;
	//$pid="64";
    $this->data['activity']=$this->user->fetch_activity1($pid,$eventid);
	$username = $this->session->userdata('owner_prof');
	$pro= $this->user->getprof($username);
	// $mid=$pro['patid'];
	//$pro= $this->user->getprof1($username);
	$this->data['profetional']=$pro;
	
	 $this->data['menu']="activity";
	
	$id=$pro[0]->id;
	$this->data['caregivers']= $this->user->getcaregivers_new($pid);
	$this->data['profetional1']=$this->user->getprofetionals1($id,$pid);
    $this->data['profid']=$id;
	$this->data['patientdel']= $this->user->getpatient_new($pid);
	
	
	
	$pfid=$this->data['profetional'][0]->id;
        $photo=$this->data['profetional'][0]->photo;
        $name=$this->data['profetional'][0]->name;
		$this->data['profnoti1']= $this->user->getprofnoti_new_pop($pfid);
		$this->data['profnoti2']= $this->user->getprofnotinet_new($pfid);
		$this->data['profnoti3']= $this->user->getprofnoticon_new($pfid);
		
		$this->data['caregivers']= $this->user->getcaregivers_new1($pid,$pfid);
	
	
	
	
	
	$this->load->view('home/prof_show_activity', $this->data);
	}
	
	
	
	
function sendPushNotificationToGCM($deviceid,$msg1,$device_platform,$name,$from,$to,$loginusertye,$logintype,$var,$rname)
{

$API_ACCESS_KEY1 = 'AIzaSyBHV-ghmAtzWCzpL7ySyXjztoW7Eo5RBuw';
  $registrationIds = array($deviceid);
  // prep the bundle
  $msg = $msg1;

   //echo $device_platform; exit;
  if($device_platform=="Android"){
  $fields = array
  (
   'registration_ids'  => $registrationIds,
   'data'              => $msg
  );

  $headers = array
  (
   'Authorization: key=' . $API_ACCESS_KEY1,
   'Content-Type: application/json'
  );

  $ch = curl_init();
  curl_setopt($ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
  curl_setopt($ch,CURLOPT_POST, true );
  curl_setopt($ch,CURLOPT_HTTPHEADER, $headers );
  curl_setopt($ch,CURLOPT_RETURNTRANSFER, true );
  curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false );
  curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
  $result = curl_exec($ch );
  curl_close( $ch );

  
  } elseif($device_platform=="iOS"){
   
   $deviceToken =$deviceid;
   $passphrase = 'bravemount';
   $message = $name.'Send The Message';
   
   $path = base_url().'assets/koalaDevpush1.pem';
   
   //echo $path; exit;
   $ctx = stream_context_create();
   
   $entrustCert =  '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/entrust_2048_ca.cer';
   stream_context_set_option($ctx, 'ssl', 'cafile', $entrustCert);
   
   //stream_context_set_option($ctx, 'ssl', 'local_cert', '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/koalaDevpush1.pem');
   stream_context_set_option($ctx, 'ssl', 'local_cert', '/opt/bitnami/apache2/htdocs/koala_mobile/services/assets/KoalaProPushCert.pem');
   
   stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
   
   
   // Open a connection to the APNS server
  // $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);


   //if (!$fp)
    //exit("Failed to connect: $err $errstr" . PHP_EOL);

   //echo 'Connected to APNS' . PHP_EOL;

   // Create the payload body
   $body['aps'] = array(
    'alert' => $message,
    'sound' => 'default',
    'sid'           =>$from,
    'rid'           =>$to,
    'sname'         =>$name,
    'recivertype'   =>$loginusertye,
    'logintype'     =>$logintype,
    'chat'          =>"text", 
    'call_type'     =>"text",
    'session_id'    =>$var,
    'rname'         =>$rname
   );

   // Encode the payload as JSON
   $payload = json_encode($body);

   // Build the binary notification
    
    $msg = chr(o) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
   //$msg = chr (o) . chr (o) . chr (32) . pack ('H*', $deviceToken) . pack ('n', strlen ($payload)) . $payload;
    
    //chr(o) . chr(o) . chr(32) . pack('H*', str_replace(' ', '', $deviceToken)) . chr(o) . chr(strlen($payload)) . $payload;
    //chr (o) . chr (o) . chr (32) . pack ('H*', $tToken) . pack ('n', strlen ($tBody)) . $tBody;
    //chr(o) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

   // Send it to the server
   $result = fwrite($fp, $msg, strlen($msg));

   //if (!$result)
   // echo 'Message not delivered' . PHP_EOL;
   //else
   // echo 'Message successfully delivered' . PHP_EOL;

   // Close the connection to the server
   fclose($fp);
   
     
  }
  
  echo $result;
}
function view_caregiver($id)
{
$username = $this->session->userdata('owner_prof');
$pro= $this->user->getprof($username);
$this->data['profetional']=$pro;
//$this->data['states']= $this->user->getStates();
$this->data['caregiver'] = $this->user->get_caregiver($id);
$this->load->view('home/prof_view_caregiver', $this->data);
	
}
	
	
	
    
	
}
