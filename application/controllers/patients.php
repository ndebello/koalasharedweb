<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		//$this->load->library('security');
		//$this->load->library('tank_auth');
		//$this->lang->load('tank_auth');
		$this->load->helper('url');
		$this->load->model('user');
		$this->load->library('user_agent');
	
	}
    function index()
	{
	    if (!($this->session->userdata('user_name')!="")) {
			redirect('/auth/login/');
		} else {
		  $this->load->view('admin/dashboard',$this->data );
		}
	}
	
	
	
	
	
	
	function users()
	{
	    if (!($this->session->userdata('user_name')!="")) {
			redirect('/auth/login/');
		} else {
		  $this->data['users'] = $this->user->getUsersAll();
	      $this->load->view('admin/users_list', $this->data);
		}   
	}
	function users_professional()
	{
	    if (!($this->session->userdata('user_name')!="")) {
			redirect('/auth/login/');
		} else {
			
		  $this->data['users'] = $this->user->getProfessionalsAll();
	      $this->load->view('admin/professionals_list', $this->data);
		}
	
	}
	function users_caregivers()
	{
	    if (!($this->session->userdata('user_name')!="")) {
			redirect('/auth/login/');
		} else {
		  $this->data['users'] = $this->user->getCaregiversAll();
	      $this->load->view('admin/caregivers_list', $this->data);
		}
	}
	function professional_details($id=null)
	{
	    if (!($this->session->userdata('user_name')!="")) {
			redirect('/auth/login/');
		} else {
		  $this->data['user'] = $this->user->getProfessionalsAll($id);
	      $this->load->view('admin/users_details', $this->data);
		 
		}
	}
	
	function users_details($id=null)
	{
	    if (!($this->session->userdata('user_name')!="")) {
			redirect('/auth/login/');
		} else {
		  $this->data['user'] = $this->user->getCaregiversAll($id);
	      $this->load->view('admin/users_details', $this->data);
		 
		}
	}
	
	
	function edit_professionals($id=null)
	{   
	    if (!($this->session->userdata('user_name')!="")) {
			redirect('/auth/login/');
		} else {
          if($_POST){
			
			$res=$_POST['from'];
			$data=explode('/',$res);
			$result=$data[2].'-'.$data[0].'-'.$data[1];
			$_POST['from']=$result;
					
		   $this->user->updateprofessional($id);
		   redirect( $this->agent->referrer(),  'refresh');

		  }else{
		   
		  }
		}
		
		$this->data['user']= $this->user->professionalDetail($id);
	    $this->load->view('admin/edit_professional', $this->data);
	}
	
	function edit_admin($id=null)
	{   
		 $temp= $this->user->adminDetail($this->session->userdata('user_id'));
		$temp= $temp[0]->id;
		
	    if (!($this->session->userdata('user_name')!="")) {
			redirect('/auth/login/');
		} else {
          if($_POST){
		   $this->user->updateAdmin($id);
		   redirect( $this->agent->referrer(),  'refresh');

		  }else{
		   
		  }
		}
		
		$this->data['admin']= $this->user->adminDetail($temp);
		$this->load->view('admin/edit_admin', $this->data);
	}
	
	
	function edit_caregiver($id=null)
	{   
	    if (!($this->session->userdata('user_name')!="")) {
			redirect('/auth/login/');
		} else {
          if($_POST){
		   $this->user->updatecaregiver($id);
		
		   redirect( $this->agent->referrer(),  'refresh');

		  }else{
		   
		  }
		}
		
		$this->data['user']= $this->user->caregiverDetail($id);
	    $this->load->view('admin/edit_caregiver', $this->data);
	}
	function delete_caregivers($id=null)
	{
	    if (!($this->session->userdata('user_name')!="")) {
			redirect('/auth/login/');
		} else {
          $this->user->deletecaregivers($id);
		  redirect( $this->agent->referrer(),  'refresh');
  
		}
	}
	
	function delete_professional($id=null)
	{
	    if (!($this->session->userdata('user_name')!="")) {
			redirect('/auth/login/');
		} else {
          $this->user->deleteprofessional($id);
		  redirect( $this->agent->referrer(),  'refresh');
  
		}
	}
	
	function edit_user($id=null)
	{   
	    if (!($this->session->userdata('user_name')!="")) {
			redirect('/auth/login/');
		} else {
          if($_POST){
		   $this->user->updateUser($id);
		   redirect( $this->agent->referrer(),  'refresh');

		  }else{
		   
		  }
		}
		
		$this->data['user']= $this->user->get_users_by_id($id);
	    $this->load->view('admin/edit_user', $this->data);
	}
	function delete_users($id=null)
	{
	    if (!($this->session->userdata('user_name')!="")) {
			redirect('/auth/login/');
		} else {
          $this->user->deleteUsers($id);
		  redirect( $this->agent->referrer(),  'refresh');
  
		}
	}
	
	function user_detail($id=null)
	{
	    if (!($this->session->userdata('user_name')!="")) {
			redirect('/auth/login/');
		} else {
		  $this->data['user'] = $this->user->getProfessionalsAll($id);
	      $this->load->view('admin/users_detail', $this->data);
		 
		}
	}
	
	
	
	function change_pass()
	{
	    if (!$this->tank_auth->is_logged_in()) {
			redirect('/auth/login/');
		} else {

			$this->form_validation->set_rules('old_password', 'Old Password', 'trim|required|xss_clean');
			$this->form_validation->set_rules('new_password', 'New Password', 'trim|required|xss_clean|min_length['.$this->config->item('password_min_length', 'tank_auth').']|max_length['.$this->config->item('password_max_length', 'tank_auth').']|alpha_dash');
			$this->form_validation->set_rules('confirm_new_password', 'Confirm new Password', 'trim|required|xss_clean|matches[new_password]');

			$this->data['errors'] = array();

			if ($this->form_validation->run()) {								// validation ok
			if ($this->tank_auth->change_password(
			$this->form_validation->set_value('old_password'),
			$this->form_validation->set_value('new_password'))) {	// success
			$this->_show_message($this->lang->line('auth_message_password_changed'));

			} else {														// fail
				$errors = $this->tank_auth->get_error_message();
				foreach ($errors as $k => $v)	$this->data['errors'][$k] = $this->lang->line($v);
			}
			}
			
			$this->load->view('admin/change_password',$this->data);
		  
		}
	}

	function add_medicine()
	{
		if($_POST){
		$this->form_validation->set_rules('medicine_name', 'Medicine Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('medicine_format', 'Medicine Format ', 'trim|required|xss_clean');
		$this->form_validation->set_rules('note', 'Note ', 'trim|required|xss_clean');
		$this->form_validation->set_rules('quantity', 'Quantity ', 'trim|required|xss_clean');
		$this->form_validation->set_rules('purchase_date', 'Purchase Date ', 'trim|required|xss_clean');
		$this->form_validation->set_rules('expiration_date', 'Expiration Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('dosage', 'Dosage ', 'trim|required|xss_clean');
		$this->form_validation->set_rules('meal_time', 'Meal Time ', 'trim|required|xss_clean');
		$this->form_validation->set_rules('price', 'Price ', 'trim|required|xss_clean');
		
				$temp=$_POST['purchase_date'];
				$data=explode('/',$temp);
				$result=$data[2].'-'.$data[0].'-'.$data[1];
				$_POST['purchase_date']=$result;
				
				$temp=$_POST['expiration_date'];
				$data=explode('/',$temp);
				$result=$data[2].'-'.$data[0].'-'.$data[1];
				$_POST['expiration_date']=$result;
				
			
		
			if($this->form_validation->run() == TRUE)
			{
				$this->user->addMedicines();
				$this->data['flash']="Diet has been added !";
				redirect('/admin/list_medicine/');
				
			}
			else
			{
				$this->data['flash']="Unsuccesfull, Try again !";

			}
			
	    }			
		
	   $this->load->view('admin/add_medicine', $this->data);
	}
	function list_medicine()
	{	   
		 $this->data['medicines']=$this->user->getMedicines();
		 $this->load->view('admin/list_medicine', $this->data);
	}
	function edit_medicine($id=null)
	{
     if($_POST){
	     $this->user->updateMedicine($id);
		 redirect('/admin/list_medicine/');
	 }
		 $this->data['medicine']=$this->user->medicineDetail($id);
		 $this->load->view('admin/edit_medicine', $this->data);

	}
	function details_medicine($id=null)
	{

		$this->data['medicine']= $this->user->medicineDetail($id);
		$this->load->view('admin/medicine_details',$this->data );

	}
	function delete_medicine($id)
	{

		 $this->user->deleteMedicines($id);
		 redirect('/admin/list_medicine/');
	}


	function edit_periods($id=null)
	{
     if($_POST){
	 
			$temp=$_POST['created'];
			$data=explode('/',$temp);
			$result=$data[2].'-'.$data[0].'-'.$data[1];
			$_POST['created']=$result;
			
	     $this->user->updatePeriods($id);
		 redirect('/home/list_periods/');
	 }
	 $this->data['periods']=$this->user->periodDetail($id);
	 $this->load->view('admin/edit_period', $this->data);

	}
	function details_period($id=null)
	{

		$this->data['periods']= $this->user->periodDetail($id);
		$this->load->view('admin/period_details',$this->data );

	}
	function delete_period($id)
	{

	 $this->user->deleteperiods($id);
     redirect('/home/list_periods/');
	}


	function edit_food($id=null)
	{
		if($_POST){
		$temp=$_POST['created'];
				$data=explode('/',$temp);
				$result=$data[2].'-'.$data[0].'-'.$data[1];
				$_POST['created']=$result;
				
		$this->user->updateFood($id);
		redirect('/home/list_foods/');
		
				
	}
		$this->data['foods']=$this->user->foodDetail($id);
		$this->load->view('admin/edit_food', $this->data);

	}
	function details_food($id=null)
	{

		$this->data['foods']= $this->user->foodDetail($id);
		$this->load->view('admin/food_details',$this->data );

	}

	function delete_food($id)
	{

		$this->user->deletefoods($id);
		redirect('/home/list_foods/');
	}


	function edit_parameters($id=null)
	{

		if($_POST){
		$this->user->updateparameters($id);
		redirect('/home/list_parameters/');
	}
		$this->data['parameters']=$this->user->parametersDetail($id);
		$this->load->view('admin/edit_parameters', $this->data);

	}
	function details_parameters($id=null)
	{

		$this->data['parameters']= $this->user->parametersDetail($id);
		$this->load->view('admin/parameters_details',$this->data );

	}

	function delete_parameters($id)
	{

		$this->user->deleteparameters($id);
		redirect('/home/list_parameters/');
	}





	
	
	
}
