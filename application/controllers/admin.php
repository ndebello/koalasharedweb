<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller
{
function __construct()
	{
		
		parent::__construct();
		$this->load->library('form_validation');
		//$this->load->library('security');
		//$this->load->library('tank_auth');
		//$this->lang->load('tank_auth');
		$this->load->helper('url');
		$this->load->model('user');
		$this->load->model('admin_one');
		// $this->load->model('caregiver_model');
		 $this->load->model('patient');
		 
		 
		 
		 
		//$this->load->library('user_agent');
		
		$username = $this->session->userdata('ausername');
		
		if(empty($username))
		{
	   redirect('/auth/login/');
		}
	   //$this->data['logged_users'] = $this->user->get_users_by_username($username);
		//if(!$this->session->userdata('user_name')){
	   	 // redirect('/auth/login/'); 
	   // }
	
	}
function viewpatient_home($pid,$oid)
{
	$this->data['id']=$pid;
	$this->data['oid']=$oid;
	$this->session->set_userdata('patientid', $pid);
	$this->session->set_userdata('ownerid', $oid);
	
	
	$this->data['amenu']="routine";
	$this->load->view('admin/prof_show_events', $this->data);
}
	
function view_patient_report($pid)
{  
  
	//echo $this->data['id']=$pid;
	//echo $this->data['oid']=$oid;
	//$this->session->set_userdata('patientid', $pid);
	//$this->session->set_userdata('ownerid', $oid);
	
	$this->data['amenu']="reports";
	
	
	$eventid=4;
	    
if($_POST)
{
	
$from=$_POST['from'];
$to=$_POST['to'];
$this->data['report']=$this->user->fetch_newreport1($pid,$from,$to);
$this->data['from']=$from;
$this->data['to']=$to;

}
else
{
$this->data['report']=$this->user->fetch_report1($pid);
}
	
	
	
	
	$this->load->view('admin/prof_show_reports', $this->data);
}

function view_patient_parameter($pid)
{  
  	//echo $this->data['id']=$pid;
	//echo $this->data['oid']=$oid;
	//$this->session->set_userdata('patientid', $pid);
	//$this->session->set_userdata('ownerid', $oid);
	$this->data['amenu']="parameter";
	$eventid=1;
	//$this->data['parameters']=$this->user->getparameters1($per_pg,$offset);

    $this->data['parameters']=$this->user->fetch_parameter1($pid,$eventid);
	$this->load->view('admin/owner_view_pat_parameter', $this->data);
}

function view_patient_parameter_graph($pid)
{  
 $pid;

  	//echo $this->data['id']=$pid;
	//echo $this->data['oid']=$oid;
	//$this->session->set_userdata('patientid', $pid);
	//$this->session->set_userdata('ownerid', $oid);
	$this->data['amenu']="parameter";
	//$eventid=1;
		$this->data['parameters']=$this->user->get_paremeters();
	//$this->data['parameters']=$this->user->getparameters1($per_pg,$offset);
if($_POST)
	{
		$praname=$_POST['praname'];
		$this->data['paraid']=$praname;
		$date=$_POST['date'];
		$this->data['date']=$date;
		$this->data['paralist']=$this->user->get_paremeters_list($pid,$praname,$date);
		
	}
	else
	{
		 $date=date("Y-m-d");
		  $praname=rand(1,2);
		  $this->data['paraid']=$praname;
		  $this->data['date']=$date;
		$this->data['paralist']=$this->user->get_paremeters_list($pid,$praname,$date);
	}
	
	$this->load->view('admin/view_patient_parameter_graph', $this->data);
}













	

function view_report2($date,$pid)
{ 
  
  
    $username = $this->session->userdata('owner_prof');
		$pro= $this->user->getprof($username);
		//$mpr=$pro;
		// $mid=$pro['patid'];
		//$pro= $this->user->getprof1($username);
		$this->data['profetional']=$pro;
		
		
		$id=$pro[0]->id;
		$this->data['caregivers']= $this->user->getcaregivers_new($pid);
	    $this->data['profetional1']=$this->user->getprofetionals1($id,$pid);
	    $this->data['profid']=$id;
		$this->data['menu']="report";

    
  
	 
	 $this->data['patient']=$this->user->get_patient($pid);
	 $patname=$this->data['patient'][0]->name;
	 
	 
	 	
	$this->data['report']=$this->user->fetch_report_details($pid,$date);
	
	$this->data['date']=$date;
	$this->data['pname']=$patname;
	
	$this->load->view('home/actionpdf', $this->data);
	
	
		
}





	
	
function index()
	{
		
		 $name=$this->session->userdata('ausername');
	    if ($this->session->userdata('ausername')=="") 
		{
			redirect('/auth/login/');
			
		}
		else 
		{
		  //$this->load->view('admin/list_owners' );
		  redirect('/admin/list_owners/');
		  
		  
		}
	}
	
	
/* ==========================================================  Activity  is start hear ====================================================================================== */	
function add_activity()
{
	if($_POST)
	 {
		$this->form_validation->set_rules('act_name', 'act_name ', 'trim|required|xss_clean');
		$this->form_validation->set_rules('actdetails', 'actdetails  ', 'trim|required|xss_clean');
		//$this->form_validation->set_rules('created', 'Created ', 'trim|required|xss_clean');
		//$this->form_validation->set_rules('dateofweek', 'dateofweek ', 'trim|required|xss_clean');
		//$this->form_validation->set_rules('food_type', 'Food Type ', 'trim|required|xss_clean');
		//$this->form_validation->set_rules('created', 'Created ', 'trim|required|xss_clean');
		
		
		
			//$temp=$_POST['created'];
			//$data=explode('/',$temp);
			//$result=$data[2].'-'.$data[0].'-'.$data[1];
			//$_POST['created']=$result;
			
			if($this->form_validation->run() == TRUE)
			{
				$this->user->addactivity();
				$this->data['flash']="Activity  has been added Successfully !";
			}
			else
			{
				$this->data['flash']="Unsuccesfull, Try again !";

			}	
		}
	
	
	$this->data['menu']="act";
	
	
	@$this->load->view('admin/add_activity' ,$this->data);
}
function activity_list()
{
	
	    $data['base']=$this->config->item('base_url');
		$total=$this->user->count_activity();
		$per_pg=10;
		$offset=$this->uri->segment(3);
		$this->load->library('pagination');
		$config['base_url'] = $data['base'].'/admin/activity_list/';
	    $config['total_rows'] = $total;
	    $config['per_page'] = $per_pg;
		$config["num_links"]=40;
		$config['full_tag_open'] = '<div id="pagination">';
		$config['full_tag_close'] = '</div>';
        $this->pagination->initialize($config);
        $this->data['pagination']=$this->pagination->create_links();
	
	
	$this->data['foods']=$this->user->activitylist($per_pg,$offset);
	$this->data['menu']="act";
    $this->load->view('admin/list_activity', $this->data);
	
	
}
function details_activity($id=null)
	{

		$this->data['activity']= $this->admin_one->activity_details($id);
		$this->data['menu']="act";
		$this->load->view('admin/activity_details',$this->data );

	}
	
		function delete_activity($id)
	{

		$this->admin_one->deleteactiviy($id);
		redirect('/admin/activity_list/');
	}
	
/*================================================= Professional Is Start Hear =============================================== */	
	function list_profetionals()
	{
		   
	  
		$data['base']=$this->config->item('base_url');
		$total=$this->user->count_profetionals();
		$per_pg=10;
		$offset=$this->uri->segment(3);
		$this->load->library('pagination');
		$config['base_url'] = $data['base'].'/admin/list_profetionals/';
	    $config['total_rows'] = $total;
	    $config['per_page'] = $per_pg;
		$config["num_links"]=40;
		$config['full_tag_open'] = '<div id="pagination">';
		$config['full_tag_close'] = '</div>';
        $this->pagination->initialize($config);
        $this->data['pagination']=$this->pagination->create_links();
	
	 
	$this->data['prof']=$this->user->getprofetionals($per_pg,$offset);
	$this->data['menu']="prf";
	
	$this->load->view('admin/list_profetionals', $this->data);
	 
	
	}
	
	
	public function searchprofetional()
	{         
		if($_POST)
		{
			
if(@$_POST['form_type']== 'patient_search')
		{
			$this->session->set_userdata('search_keyword1',$_POST['keyword']);
			 $srch=$_POST['keyword'];
			 @ $ctype=$_POST['ctype'];
			//getPatients searchPatients
			$this->data['prof']= $this->user->serach_profetionals($srch);
			//$this->data['professionals']= $this->user->getProfessionals();
			
		$str='';
		if(count($this->data['prof'])>0)
		{
			$i=0;
		 foreach($this->data['prof'] as $temp)
		 {
			 $i++;
		?> 
<tr>
<td><?php echo $i;?></td>
<td><?php echo $temp->name." ".$temp->surname; ?></td>
<td><?php echo  $temp->email;?></td>
<!--<td><?php //echo  $temp->age;?></td>
<td><?php //echo  $temp->gender;?></td>-->
<td><?php echo  $temp->zipcode;?></td>
<td><?php echo  $temp->address;?></td>
<!--<td><?php //echo  $temp->qualification;?></td>
<td><?php //echo  $temp->phone;?></td>-->
<td>
<a href="<?php echo base_url(); ?>admin/details_prof/<?php echo $temp->id;?>">Visualizza</a> /
<a href="<?php echo base_url(); ?>admin/eidt_prof/<?php echo $temp->id;?>">Edit</a> /
<a href="<?php echo base_url(); ?>admin/delete_prof/<?php echo $temp->id;?>" onclick="return confirm('are you sure to delete')">Delete </a>
</td>
</tr>

         <?php
         }
		}
		
		 }
		 
		 
		 if($_POST['form_type1']== 'datesearch')
		{  
		
			$this->session->set_userdata('search_keyword1',$_POST['keyword']);
			echo $date1=$_POST['date1'];
			echo  $date2=$_POST['keyword'];
			  //@$ctype=$_POST['ctype'];
			//getPatients searchPatients
			$this->data['patients']= $this->user->serach_prof_owner_date($date1,$date2);
			//$this->data['professionals']= $this->user->getProfessionals();
			
		$str='';
		if(count($this->data['patients'])>0)
		{
			$i=0;
		 foreach($this->data['patients'] as $temp)
		 {
			 $i++;
		?> 

<tr>
<td><?php echo $i;?></td>
<td><?php echo $temp->name." ".$temp->surname; ?></td>
<td><?php echo  $temp->email;?></td>
<!--<td><?php //echo  $temp->age;?></td>
<td><?php //echo  $temp->gender;?></td>-->
<td><?php echo  $temp->zipcode;?></td>
<td><?php echo  $temp->address;?></td>
<!--<td><?php //echo  $temp->qualification;?></td>
<td><?php //echo  $temp->phone;?></td>-->
<td>
<a href="<?php echo base_url(); ?>admin/details_prof/<?php echo $temp->id;?>">Visualizza</a> /
<a href="<?php echo base_url(); ?>admin/eidt_prof/<?php echo $temp->id;?>">Edit</a> /
<a href="<?php echo base_url(); ?>admin/delete_prof/<?php echo $temp->id;?>" onclick="return confirm('are you sure to delete')">Delete </a>
</td>
</tr>




         <?php
         }
		 
	 
		}
		else
		{
			echo "<tr align='center'><td colspan='6' align='center'> No Records Found   </td></tr>";
		}
		
		
		
		 
		 
		   }



if($_POST['form_type1']== 'dates')
		{  
		
			$this->session->set_userdata('search_keyword1',$_POST['keyword']);
			$date1=$_POST['date1'];
			$date2=date("Y-m-d");
			  //@$ctype=$_POST['ctype'];
			//getPatients searchPatients
			$this->data['patients']= $this->user->serach_profnew_owner_date($date1,$date2);
			//$this->data['professionals']= $this->user->getProfessionals();
			
		$str='';
		if(count($this->data['patients'])>0)
		{
			$i=0;
		 foreach($this->data['patients'] as $temp)
		 {
			 $i++;
		?> 
            <tr>
            <td><?php echo $i;?></td>
            <td><?php echo $temp->name." ".$temp->surname; ?></td>
            <td><?php echo  $temp->email;?></td>
            <!--<td><?php //echo  $temp->age;?></td>
            <td><?php //echo  $temp->gender;?></td>-->
            <td><?php echo  $temp->zipcode;?></td>
            <td><?php echo  $temp->address;?></td>
            <!--<td><?php //echo  $temp->qualification;?></td>
            <td><?php //echo  $temp->phone;?></td>-->
            <td>
          <a href="<?php echo base_url(); ?>admin/details_prof/<?php echo $temp->id;?>">Visualizza</a> /
                                
                                <a href="<?php echo base_url(); ?>admin/eidt_prof/<?php echo $temp->id;?>">Edit</a> /
                                           <a href="<?php echo base_url(); ?>admin/delete_prof/<?php echo $temp->id;?>" onclick="return confirm('are you sure to delete')">Delete </a>
            </td>
            </tr>
         <?php
         }
		 
	 
		}
		else
		{
			echo "<tr align='center'><td colspan='6' align='center'> No Records Found   </td></tr>";
		}
		
		
		
		 
		 
		   }
		
		 
		 
		 
		 
		}
	        }
		
		
		
	
	function delete_prof($pfid)	
		{
         $pfs="delete from professionals where id='$pfid'";
		 $pfe=mysql_query($pfs) or die(mysql_error());
		 $pf1="delete from professional_networks where rid='$pfid'";
		 //$pe1=mysql_query($pf1) or die(mysql_error());
		 $pf2="delete from professional_networks_prf where sid='$pfid' or rid='$pfid'";
		 $pe2=mysql_query($pf2) or die(mysql_error());
		 $cd="delete from chat where chat.from='$pfid' and chat.to='$pfid'";
		 $ce=mysql_query($cd) or die(mysql_error());
		 $cs="delete from cons_request_messages where sender='$pfid' or recipient='$pfid'";
		 $ce=mysql_query($cs);
		 
		 
		 redirect("admin/list_profetionals");
		 		 
		}
	
	
/* list of caregivers is sh  */
function list_caregivers()
{
	$data['base']=$this->config->item('base_url');
		$total=$this->user->count_caregivers();
		$per_pg=10;
		$offset=$this->uri->segment(3);
		$this->load->library('pagination');
		$config['base_url'] = $data['base'].'/admin/list_caregivers/';
	    $config['total_rows'] = $total;
	    $config['per_page'] = $per_pg;
		$config["num_links"]=40;
		$config['full_tag_open'] = '<div id="pagination">';
		$config['full_tag_close'] = '</div>';
        $this->pagination->initialize($config);
        $this->data['pagination']=$this->pagination->create_links();
 
		$this->data['prof']=$this->user->get_caregivers($per_pg,$offset);
		$this->data['menu']="care";
		
		$this->load->view('admin/list_caregivers', $this->data);
}
	
public function searchcaregivers()
	{         
		if($_POST)
		{
			
if(@$_POST['form_type']== 'patient_search')
		{
			$this->session->set_userdata('search_keyword1',$_POST['keyword']);
			 $srch=$_POST['keyword'];
			 @ $ctype=$_POST['ctype'];
			//getPatients searchPatients
			$this->data['prof']= $this->user->serach_caregivers($srch);
			//$this->data['professionals']= $this->user->getProfessionals();
			
		$str='';
		if(count($this->data['prof'])>0)
		{
			$i=0;
		 foreach($this->data['prof'] as $temp)
		 {
			 $i++;
		?> 
<tr>
<td><?php echo $i;?></td>
<td><?php echo $temp->name." ".$temp->surname; ?></td>
<td><?php echo  $temp->email;?></td>
<!--<td><?php //echo  $temp->age;?></td>
<td><?php //echo  $temp->gender;?></td>-->
<td><?php echo  $temp->from;?></td>
<td><?php $ctype=$temp->care_type_id; 
if($ctype==1)
{
	echo "Primary Caregiver ";
}
if($ctype==2)
{
	echo "Secondary Caregiver";
}
							
if($ctype==3)
{
	echo "Monitoring User";
}
?>
</td>
<td><?php echo  $temp->address;?></td>

<!--<td><?php //echo  $temp->qualification;?></td>
<td><?php //echo  $temp->phone;?></td>-->
<td>
<a href="<?php echo base_url(); ?>admin/view_care_details/<?php echo $temp->id;?>">Visualizza</a> /
<a href="<?php echo base_url(); ?>admin/eidt_caregiver/<?php echo $temp->id;?>">Edit</a> /
<a href="<?php echo base_url(); ?>admin/delete_caregiver/<?php echo $temp->id;?>" onclick="return confirm('are you sure to delete')">Delete </a> 
</td>
</tr>

         <?php
         }
		}
		
		 }
		 
		 
		 if(@$_POST['form_type1']== 'datesearch')
		{  
		
			$this->session->set_userdata('search_keyword1',$_POST['keyword']);
			echo $date1=$_POST['date1'];
			echo  $date2=$_POST['keyword'];
			  //@$ctype=$_POST['ctype'];
			//getPatients searchPatients
			$this->data['patients']= $this->user->serach_caregiver_owner_date($date1,$date2);
			//$this->data['professionals']= $this->user->getProfessionals();
			
		$str='';
		if(count($this->data['patients'])>0)
		{
			$i=0;
		 foreach($this->data['patients'] as $temp)
		 {
			 $i++;
		?> 
<tr>
<td><?php echo $i;?></td>
<td><?php echo $temp->name." ".$temp->surname; ?></td>
<td><?php echo  $temp->email;?></td>
<!--<td><?php //echo  $temp->age;?></td>
<td><?php //echo  $temp->gender;?></td>-->
<td><?php echo  $temp->from;?></td>
<td><?php $ctype=$temp->care_type_id; 
if($ctype==1)
{
	echo "Primary Caregiver ";
}
if($ctype==2)
{
	echo "Secondary Caregiver";
}
							
if($ctype==3)
{
	echo "Monitoring User";
}
?>
</td>
<td><?php echo  $temp->address;?></td>
<!--<td><?php //echo  $temp->qualification;?></td>
<td><?php //echo  $temp->phone;?></td>-->
<td>
<a href="<?php echo base_url(); ?>admin/view_care_details/<?php echo $temp->id;?>">Visualizza</a> /
<a href="<?php echo base_url(); ?>admin/eidt_caregiver/<?php echo $temp->id;?>">Edit</a> /
<a href="<?php echo base_url(); ?>admin/delete_caregiver/<?php echo $temp->id;?>" onclick="return confirm('are you sure to delete')">Delete </a> 
</td>
</tr>




         <?php
         }
		 
	 
		}
		else
		{
			echo "<tr align='center'><td colspan='6' align='center'> No Records Found   </td></tr>";
		}
		
		
		
		 
		 
		   }



if(@$_POST['form_type1']== 'dates')
		{  
		
			$this->session->set_userdata('search_keyword1',$_POST['keyword']);
			$date1=$_POST['date1'];
			$date2=date("Y-m-d");
			  //@$ctype=$_POST['ctype'];
			//getPatients searchPatients
			$this->data['patients']= $this->user->serach_carenew_owner_date($date1,$date2);
			//$this->data['professionals']= $this->user->getProfessionals();

		$str='';
		if(count($this->data['patients'])>0)
		{
			$i=0;
		 foreach($this->data['patients'] as $temp)
		 {
			 $i++;
		?> 
            <tr>
            <td><?php echo $i;?></td>
            <td><?php echo $temp->name." ".$temp->surname; ?></td>
            <td><?php echo  $temp->email;?></td>
            <!--<td><?php //echo  $temp->age;?></td>
            <td><?php //echo  $temp->gender;?></td>-->
            <td><?php echo  $temp->from;?></td>
          <td><?php $ctype=$temp->care_type_id; 
					if($ctype==1)
					{
						echo "Primary Caregiver ";
					}
					if($ctype==2)
					{
						echo "Secondary Caregiver";
					}
												
					if($ctype==3)
					{
						echo "Monitoring User";
					}
					?>
</td>
<td><?php echo  $temp->address;?></td>
            <!--<td><?php //echo  $temp->qualification;?></td>
            <td><?php //echo  $temp->phone;?></td>-->
            <td>
         <a href="<?php echo base_url(); ?>admin/view_care_details/<?php echo $temp->id;?>">Visualizza</a> /
<a href="<?php echo base_url(); ?>admin/eidt_caregiver/<?php echo $temp->id;?>">Edit</a> /
<a href="<?php echo base_url(); ?>admin/delete_caregiver/<?php echo $temp->id;?>" onclick="return confirm('are you sure to delete')">Delete </a> 
            </td>
            </tr>
         <?php
         }
		 
	 
		}
		else
		{
			echo "<tr align='center'><td colspan='6' align='center'> No Records Found   </td></tr>";
		}
		 
		   }

		}
	        }
		
	
	
	
	
	
	
	
/* ====================================================== parameters is start hear ========================================  */	
	function list_parameters()
	{	   
	
	    $data['base']=$this->config->item('base_url');
		$total=$this->user->count_parameter();
		$per_pg=10;
		$offset=$this->uri->segment(3);
		$this->load->library('pagination');
		$config['base_url'] = $data['base'].'/admin/list_parameters/';
	    $config['total_rows'] = $total;
	    $config['per_page'] = $per_pg;
		$config["num_links"]=40;
		$config['full_tag_open'] = '<div id="pagination">';
		$config['full_tag_close'] = '</div>';
        $this->pagination->initialize($config);
        $this->data['pagination']=$this->pagination->create_links();
	
	
	
		$this->data['parameters']=$this->user->getparameters1($per_pg,$offset);
		
		//print_r($this->data['parameters']);
		
		$this->data['menu']="pra";
		$this->load->view('admin/list_parameters', $this->data);
	}
	
	
	function add_parameters()
	{
		$this->data['menu']="pra";
		if($_POST){
		$this->form_validation->set_rules('maximum_pressure', 'Maximum Pressure', 'trim|required|xss_clean');
			$this->form_validation->set_rules('minimum_pressure', 'Minimum Pressure  ', 'trim|required|xss_clean');
			$this->form_validation->set_rules('blood_glucose', 'Blood Glucose ', 'trim|required|xss_clean');
			$this->form_validation->set_rules('heart_rate', 'Heart Rate  ', 'trim|required|xss_clean');
			$this->form_validation->set_rules('weight', 'Weight  ', 'trim|required|xss_clean');
			//$this->form_validation->set_rules('patient_id', 'Patient Name ', 'trim|required|xss_clean');
			$this->form_validation->set_rules('created', 'Date  ', 'trim|required|xss_clean');
			//$temp=$_POST['created'];
			//$data=explode('/',$temp);
			//$result=$data[2].'-'.$data[0].'-'.$data[1];
			$_POST['created'];
			if($this->form_validation->run() == TRUE)
				{
					$this->patient->addparameters();
					$this->data['flash']="Parameters has been added !";
				}
					else
				{
					$this->data['flash']="Unsuccesfull, Try again !";

				}
		}
		$this->data['caregivers']= $this->user->getCaregiverslist();
		//$this->session->userdata('user_id')
		//$this->data['patient'] = $this->patient->get_patient_by_id(90);
		//if($this->session->userdata('user_role')=='admin'){
			
			$this->data['menu']="pra";
			
		 $this->load->view('admin/add_parameters', $this->data);
		//}
		/*else{

		 $this->load->view('home/add_parameters', $this->data);
		}*/
	}
	
	
	
	
	
/* View the profetionals  */	

function details_prof($id=null)
	{
        $this->data['menu']="prf";
		$this->data['prof']= $this->user->profetionalDetail($id);
		$this->load->view('admin/prof_details',$this->data );

	}
	
function eidt_prof($id)	
{
	    $this->data['menu']="prf";
		$this->data['prof']= $this->user->profetionalDetail($id);
		$this->data['pid']=$id;
		
		
		
		$this->load->view('admin/edit_profesational',$this->data );
}

function eidt_caregiver($cid)
{
	$this->data['prof']= $this->user->careDetail($cid);
	$this->data['cid']=$cid;
	$this->data['menu']="care";
	$this->load->view('admin/edit_caregiver',$this->data);
	
}
function update_caregiver($cid)
{
	$this->data['menu']="care";
	$name=$_POST['name'];
	$surname=$_POST['surname'];
	$zipcode=$_POST['zipcode'];
	$city=$_POST['city'];
	$address=$_POST['address'];
	$contact=$_POST['phone'];
	$country=$_POST['country'];
	$lat=$_POST['lat'];
	$lon=$_POST['lng'];
	$phone=$_POST['phone'];
	
	//$cid,$lat,$lon,$cname,$surname,$zipcode,$city,$address,$contact,$age,$gender,$country,$photo
	
	 if(!empty($_FILES["photo"]["name"]))
			 {  
			  

				//echo "Temp file: " . $_FILES["photo"]["tmp_name"] . "<br>";
				
					//$file=$_FILES["photo"]["name"];
					//if(move_uploaded_file($_FILES["photo"]["tmp_name"],
					//"uploads/" . $file))
					//$this->session->set_userdata('user_photo', $file);
				
$file=$_FILES['photo']['name'];
 $type=$_FILES['photo']['type'];
move_uploaded_file($_FILES['photo']['tmp_name'],"uploads/".$file);

if($type=='image/jpeg')
{
	$fi="uploads/".$file;
   //$newHeight=100;
   //$newWidth=100;
   $newHeight=200;
   $newWidth=200;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;
    list($width,$height) = getimagesize($fi);
    $img = imagecreatefromjpeg($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    //imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
	imagecopyresampled( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagejpeg($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }
    if($type=='image/png')
    {
    $fi="uploads/".$file;
   //$newHeight=100;
   //$newWidth=100;
   $newHeight=200;
   $newWidth=200;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;

    list($width,$height) = getimagesize($fi);
    $img = imagecreatefrompng($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    //imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
	imagecopyresampled( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagepng($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }
    
    if($type=='image/gif')
    {
   $fi="uploads/".$file;
   //$newHeight=100;
   //$newWidth=100;
   $newHeight=200;
   $newWidth=200;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;

    list($width,$height) = getimagesize($fi);
    $img = imagecreatefromgif($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    //imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
	imagecopyresampled( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagegif($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }

    unlink($fi);

			  
			 }
			else
			{
				$nfile=$_POST['photo'];
			}
			
$this->data['prof']= $this->user->updatecaregiversbycid($cid,$lat,$lon,$name,$surname,$zipcode,$city,$address,$contact,$age,$gender,$country,$nfile);		
			
redirect("admin/list_caregivers");
	
}


	
function update_prof($id)	
	{
		
	$name=$_POST['pname'];
	$surname=$_POST['sname'];
	$zipcode=$_POST['zipcode'];
	$city=$_POST['city'];
	$address=$_POST['address'];
	$contact=$_POST['photo'];
	$country=$_POST['country'];
	$lat=$_POST['lat'];
	$lon=$_POST['lng'];
	$phone=$_POST['phone'];
	//$age=$_POST['age'];
	//$gender=$_POST['gender'];
	$admitted_to=$_POST['admitted_to'];
	//$from=$_POST['from'];
	
	$qualification=$_POST['qualification'];
		
		
		
		
		 if(!empty($_FILES["photo"]["name"]))
			 {  
			  

				//echo "Temp file: " . $_FILES["photo"]["tmp_name"] . "<br>";
				
					//$file=$_FILES["photo"]["name"];
					//if(move_uploaded_file($_FILES["photo"]["tmp_name"],
					//"uploads/" . $file))
					//$this->session->set_userdata('user_photo', $file);
				
$file=$_FILES['photo']['name'];
 $type=$_FILES['photo']['type'];
move_uploaded_file($_FILES['photo']['tmp_name'],"uploads/".$file);

if($type=='image/jpeg')
{
	$fi="uploads/".$file;
   //$newHeight=100;
   //$newWidth=100;
   $newHeight=200;
   $newWidth=200;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;
    list($width,$height) = getimagesize($fi);
    $img = imagecreatefromjpeg($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    //imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
	imagecopyresampled( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagejpeg($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }
    if($type=='image/png')
    {
    $fi="uploads/".$file;
   //$newHeight=100;
   //$newWidth=100;
   $newHeight=200;
   $newWidth=200;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;

    list($width,$height) = getimagesize($fi);
    $img = imagecreatefrompng($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    //imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
	imagecopyresampled( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagepng($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }
    
    if($type=='image/gif')
    {
   $fi="uploads/".$file;
   //$newHeight=100;
   //$newWidth=100;
   $newHeight=200;
   $newWidth=200;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;

    list($width,$height) = getimagesize($fi);
    $img = imagecreatefromgif($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    //imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
	imagecopyresampled( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagegif($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }

    unlink($fi);

			  
			 }
			else
			{
				$nfile=$_POST['photo'];
				
			}
			
		$us1=$this->user->editProfile1($id,$nfile,$cv,$lat,$lon); 	
		if($us1)
		{
			redirect("admin/list_profetionals/");
		}
			
			
	}
	
	
function edit_activity($id=null)
	{
		$this->data['menu']="act";
		if($_POST){
		//$temp=$_POST['created'];
				//$data=explode('/',$temp);
				//$result=$data[2].'-'.$data[0].'-'.$data[1];
				//$_POST['created']=$result;
			$id=$_POST['id'];	
		$this->admin_one->updateactivity($id);
		redirect('/admin/activity_list/');
		
				
	}
		$this->data['activity']= $this->admin_one->activity_details($id);
		$this->load->view('admin/edit_activity', $this->data);

	}
	
	/* Search Medicine*/
	public function searchdrug()
	{
		if($_POST)
		{
			
if($_POST['form_type']== 'patient_search')
		{
			$this->session->set_userdata('search_keyword1',$_POST['keyword']);
			 $srch=$_POST['keyword'];
			 @ $ctype=$_POST['ctype'];
			//getPatients searchPatients
			$this->data['patients']= $this->user->serach_drug($srch);
			//$this->data['professionals']= $this->user->getProfessionals();
			
		$str='';
		if(count($this->data['patients'])>0)
		{
			$i=0;
		 foreach($this->data['patients'] as $temp)
		 {
			 $i++;
		?> 
<tr>
<td><?php echo $i;?></td>
<td><?php echo $temp->med_name;?></td>
<td><?php echo  $temp->medformat;?></td>
<td><?php echo  $temp->med_note;?></td>
<td><?php echo  $temp->med_quantity;?></td>
<td><?php echo  $temp->med_purchase_date;?></td>
<td><?php echo  $temp->med_expirartion_date;?></td>
<td><?php echo  $temp->med_dose;?></td>
<td><?php echo  $temp->med_meal_time;?></td>
<td><?php echo  $temp->med_price;?></td>
<td>
<a href="<?php echo base_url(); ?>admin/details_medicine/<?php echo $temp->med_id;?>">Visualizza</a>/ <a href="<?php echo base_url(); ?>admin/edit_medicine/<?php echo $temp->med_id;?>">Edit</a>/<a href="<?php echo base_url(); ?>admin/delete_medicine/<?php echo $temp->med_id;?>" onclick="return confirm('are you sure to delete')">Delete </a>
</td>
</tr>

         <?php
         }
		}
		else
		{
			echo "<tr align='center'><td colspan='11' align='center'> No Records Found   </td></tr>";
		}
		 }
		 
		}
	}

/* 
Admin Edit The Patient Details


*/
function edit_owner_patient($pid)
		{
			//$this->data['states']= $this->user->getStates();
		//$this->data['caregiver'] = $this->user->edit_caregiver($id);
		$this->data['patient']= $this->user->patient_details($pid);
		/*echo "<pre>";
		
		print_r($this->data['patient']);
		echo "</pre>";*/
		if($_POST)
		{ 
		 
		       
	//--------uploading--------------  //
	if(!empty($_FILES["photo"]["name"]))
	{

				//echo "Temp file: " . $_FILES["photo"]["tmp_name"] . "<br>";
				
					//$file=$_FILES["photo"]["name"];
					//if(move_uploaded_file($_FILES["photo"]["tmp_name"],
					//"uploads/" . $file))
					//$this->session->set_userdata('user_photo', $file);
				
$file=$_FILES['photo']['name'];
$type=$_FILES['photo']['type'];
move_uploaded_file($_FILES['photo']['tmp_name'],"uploads/".$file);

if($type=='image/jpeg')
{
	$fi="uploads/".$file;
   //$newHeight=100;
   //$newWidth=100;
   $newHeight=200;
   $newWidth=200;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;
    list($width,$height) = getimagesize($fi);
    $img = imagecreatefromjpeg($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    //imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
	imagecopyresampled( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagejpeg($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }
    if($type=='image/png')
    {
    $fi="uploads/".$file;
   //$newHeight=100;
   //$newWidth=100;
   $newHeight=200;
   $newWidth=200;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;

    list($width,$height) = getimagesize($fi);
    $img = imagecreatefrompng($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    //imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
	imagecopyresampled( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagepng($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }
    
    if($type=='image/gif')
    {
   $fi="uploads/".$file;
   //$newHeight=100;
   //$newWidth=100;
   $newHeight=200;
   $newWidth=200;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;

    list($width,$height) = getimagesize($fi);
    $img = imagecreatefromgif($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    //imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
	imagecopyresampled( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagegif($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }

    unlink($fi);
$this->session->set_userdata('photo', $nfile);

			}
	else
	{
		$this->session->set_userdata('photo', $_POST['photo']);
	}
	    	$this->session->set_userdata('name', $_POST['name']);
			//$this->session->set_userdata('licenceid', $_POST['licenceid']);
			$this->session->set_userdata('surname', $_POST['surname']);
			$this->session->set_userdata('age', $_POST['age']);
			$this->session->set_userdata('gender', $_POST['gender']);
			$this->session->set_userdata('zipcode', $_POST['zipcode']);
			$this->session->set_userdata('city', $_POST['city']);
			$this->session->set_userdata('state', $_POST['state']);
			$this->session->set_userdata('address', $_POST['address']);
			$this->session->set_userdata('phone', $_POST['phone']);
			$this->session->set_userdata('phone2', $_POST['phone2']);
			$this->session->set_userdata('phone3', $_POST['phone3']);
			$this->session->set_userdata('contact1', $_POST['contact1']);
			$this->session->set_userdata('contact2', $_POST['contact2']);
			$this->session->set_userdata('contact3', $_POST['contact3']);
			$this->session->set_userdata('pathology', $_POST['pathology']);
			$this->session->set_userdata('allergies', $_POST['allergies']);
			$this->session->set_userdata('intolerances', $_POST['intolerances']);
			$this->session->set_userdata('note', $_POST['note']); 
			//$this->session->set_userdata('qr_code', $_POST['qr_code']);
     		//redirect('/auth/patient_registerstep2/'.$id, 'refresh');
			$oid=$this->user->updatepatient($pid);
			
			/*echo "<script>alert('Owner Details Updated Successfully')</script>";*/
			$this->data['flash']="<font color='red'> <b> Patient Details Updated </b> </font> ";
			//redirect('/front/details_updatepatient/'.$pid);
			
			
			  
	    }
		$this->load->view('admin/admin_edit_patient', $this->data);	
			
		}


function edit_owner_patient1($ownid,$pid)
{
	
$this->data['patient']= $this->user->patient_details($pid);
if($_POST)
	{
	  
		 
		       
	//--------uploading--------------  //
	if(!empty($_FILES["photo"]["name"]))
	{

				//echo "Temp file: " . $_FILES["photo"]["tmp_name"] . "<br>";
				
					//$file=$_FILES["photo"]["name"];
					//if(move_uploaded_file($_FILES["photo"]["tmp_name"],
					//"uploads/" . $file))
					//$this->session->set_userdata('user_photo', $file);
				
$file=$_FILES['photo']['name'];
$type=$_FILES['photo']['type'];
move_uploaded_file($_FILES['photo']['tmp_name'],"uploads/".$file);

if($type=='image/jpeg')
{
	$fi="uploads/".$file;
   //$newHeight=100;
   //$newWidth=100;
   $newHeight=200;
   $newWidth=200;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;
    list($width,$height) = getimagesize($fi);
    $img = imagecreatefromjpeg($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    //imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
	imagecopyresampled( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagejpeg($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }
    if($type=='image/png')
    {
    $fi="uploads/".$file;
   //$newHeight=100;
   //$newWidth=100;
   $newHeight=200;
   $newWidth=200;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;

    list($width,$height) = getimagesize($fi);
    $img = imagecreatefrompng($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    //imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
	imagecopyresampled( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagepng($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }
    
    if($type=='image/gif')
    {
   $fi="uploads/".$file;
   //$newHeight=100;
   //$newWidth=100;
   $newHeight=200;
   $newWidth=200;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;

    list($width,$height) = getimagesize($fi);
    $img = imagecreatefromgif($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    //imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
	imagecopyresampled( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagegif($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }

    unlink($fi);
$this->session->set_userdata('photo', $nfile);

			}
	else
	{
		$this->session->set_userdata('photo', $_POST['photo']);
	}
	    	$this->session->set_userdata('name', $_POST['name']);
			//$this->session->set_userdata('licenceid', $_POST['licenceid']);
			$this->session->set_userdata('surname', $_POST['surname']);
			$this->session->set_userdata('age', $_POST['age']);
			$this->session->set_userdata('gender', $_POST['gender']);
			$this->session->set_userdata('zipcode', $_POST['zipcode']);
			$this->session->set_userdata('city', $_POST['city']);
			$this->session->set_userdata('state', $_POST['state']);
			$this->session->set_userdata('address', $_POST['address']);
			$this->session->set_userdata('phone', $_POST['phone']);
			$this->session->set_userdata('phone2', $_POST['phone2']);
			$this->session->set_userdata('phone3', $_POST['phone3']);
			$this->session->set_userdata('contact1', $_POST['contact1']);
			$this->session->set_userdata('contact2', $_POST['contact2']);
			$this->session->set_userdata('contact3', $_POST['contact3']);
			$this->session->set_userdata('pathology', $_POST['pathology']);
			$this->session->set_userdata('allergies', $_POST['allergies']);
			$this->session->set_userdata('intolerances', $_POST['intolerances']);
			$this->session->set_userdata('note', $_POST['note']); 
			//$this->session->set_userdata('qr_code', $_POST['qr_code']);
     		//redirect('/auth/patient_registerstep2/'.$id, 'refresh');
			$oid=$this->user->updatepatient($pid);
			
			/*echo "<script>alert('Owner Details Updated Successfully')</script>";*/
			$this->data['flash']="<font color='red'> <b> Patient Details Updated </b> </font> ";
			redirect('/admin/details_owner_show/'.$ownid);
			
			
			  
	    
	}
		 
		 
$this->load->view("admin/owner_edit_patient_details",$this->data);		 
		 
		 

}





/*
=============================================
admin view caregivers


=============================================

*/
function view_caregiver($id)
{
//$this->data['states']= $this->user->getStates();
$this->data['caregiver'] = $this->user->get_caregiver($id);

$this->load->view('admin/admin_view_caregiver', $this->data);
	
}

/*
=============================================
admin view Profestional Details


=============================================

*/


function edit_profile($id)
	{   
	 
		//$temp= $this->user->get_users_by_id($this->session->userdata('user_id'));
		//$temp= $temp[0]->id;
		$this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('surname', 'Surname','trim|required|xss_clean');
		$this->form_validation->set_rules('age', 'Age','trim|required|xss_clean');
		$this->form_validation->set_rules('gender', 'Gender','trim|required|xss_clean');
			
		if($this->form_validation->run() == TRUE)
		{
			
		
	if($_POST)
	{
	
    
	
	$name=$_POST['name'];
	$surname=$_POST['surname'];
	$zipcode=$_POST['zipcode'];
	$city=$_POST['city'];
	$address=$_POST['address'];
	$contact=$_POST['photo'];
	$country=$_POST['state'];
	$lat=$_POST['lat'];
	$lon=$_POST['lng'];
	$age=$_POST['age'];
	$gender=$_POST['gender'];
	$admitted_to=$_POST['admitted_to'];
	$from=$_POST['from'];
	
	$qualification=$_POST['qualification'];
	
	
	  if(!empty($_FILES["photo"]["name"]))
			{

				//echo "Temp file: " . $_FILES["photo"]["tmp_name"] . "<br>";
				
					//$file=$_FILES["photo"]["name"];
					//if(move_uploaded_file($_FILES["photo"]["tmp_name"],
					//"uploads/" . $file))
					//$this->session->set_userdata('user_photo', $file);
				
$file=$_FILES['photo']['name'];
 $type=$_FILES['photo']['type'];
move_uploaded_file($_FILES['photo']['tmp_name'],"uploads/".$file);

if($type=='image/jpeg')
{
	$fi="uploads/".$file;
   //$newHeight=100;
   //$newWidth=100;
   $newHeight=200;
   $newWidth=200;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;
    list($width,$height) = getimagesize($fi);
    $img = imagecreatefromjpeg($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    //imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
	imagecopyresampled( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagejpeg($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }
    if($type=='image/png')
    {
    $fi="uploads/".$file;
   //$newHeight=100;
   //$newWidth=100;
   $newHeight=200;
   $newWidth=200;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;

    list($width,$height) = getimagesize($fi);
    $img = imagecreatefrompng($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    //imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
	imagecopyresampled( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagepng($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }
    
    if($type=='image/gif')
    {
   $fi="uploads/".$file;
   //$newHeight=100;
   //$newWidth=100;
   $newHeight=200;
   $newWidth=200;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;

    list($width,$height) = getimagesize($fi);
    $img = imagecreatefromgif($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    //imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
	imagecopyresampled( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagegif($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }

    unlink($fi);

			}
			else
			{
				$nfile=$_POST['photo'];
				
				}
			
			if(!empty($_FILES["cv"]["name"]))
			{
				$cv=$_FILES["cv"]["name"];
			if ($_FILES["cv"]["error"] > 0) {
				//echo "Error: " . $_FILES["photo"]["error"] . "<br>";
			}else {
				//echo "Temp file: " . $_FILES["photo"]["tmp_name"] . "<br>";
				if (file_exists("upload/cv/" . $_FILES["cv"]["name"])) {
					//$_FILES["photo"]["name"] . " already exists. ";
				} else {
					$cv=uniqid().$_FILES["cv"]["name"];
					if(move_uploaded_file($_FILES["cv"]["tmp_name"],
					"uploads/cv/" . $cv)){
						$this->session->set_userdata('cv', $cv);
						$this->session->set_userdata('cv', $cv);
					}else{
						//$this->session->set_userdata('photo',$this->session->userdata('user_photo'));
						$this->session->set_userdata('cv', '');
					}
				}
			}
			}
			else
			{ $cv=$_POST['cv']; }
	
	//$us=$this->user->getcaregiversbyemail($email);
	
	
  
  
$us1=$this->user->editProfile($id,$nfile,$cv,$lat,$lon); 
	  if($us1)
	  {
		  echo "<script>alert('Updated Successfully')</script>";
		  
	  }
  
	 
	 }
	
		
			
			
			
		}
		 $this->data['professionals'] = $this->user->get_professionals_by_id($id);	 
		 //$this->data['states']= $this->user->getStates();
		
		$this->load->view('admin/admin_view_professional', $this->data);
	  
	}






/*
================================================
admin view the patient details from admin 


================================================
*/

public function show_drugs($pid)
	{
	$eventid=2;
	//$pid="64";
    //$this->data['activity']=$this->user->fetch_activity($pid,$eventid);
	 $this->data['medicene']=$this->user->fetch_medecine($pid,$eventid);
     $this->data['amenu']="drugs";
	$this->load->view('admin/owner_view_pat_medicine', $this->data);
	}

public function show_diets($pid)
	{
	$eventid=3;
	//$pid="64";
    //$this->data['activity']=$this->user->fetch_activity($pid,$eventid);
	 $this->data['diets']=$this->user->fetch_medecine($pid,$eventid);
	 $this->data['amenu']="diets";
	$this->load->view('admin/owner_view_pat_diets', $this->data);
	}	
	public function show_activity($pid)
	{
	$eventid=4;
	//$pid="64";
    $this->data['activity']=$this->user->fetch_activity($pid,$eventid);
	 $this->data['amenu']="activity";
	$this->load->view('admin/owner_view_pat_activity', $this->data);
	}

/* Search For Diets */		 
		 
 function searchdiets()
{
	
		
		
		if($_POST)
		{
			
if($_POST['form_type']== 'patient_search')
		{
			$this->session->set_userdata('search_keyword1',$_POST['keyword']);
			 $srch=$_POST['keyword'];
			  @$ctype=$_POST['ctype'];
			//getPatients searchPatients
			$this->data['patients']= $this->user->serach_deits($srch);
			//$this->data['professionals']= $this->user->getProfessionals();
			
		$str='';
		if(count($this->data['patients'])>0)
		{
			
			$i=0;
		 foreach($this->data['patients'] as $temp)
		 {
			 $i++;
	$sbid=$temp->food_type;
    if(!empty($sbid))
	{
	$s1="select * from sub_event_type where sub_id='$sbid'";
	$e1=mysql_query($s1) or die(mysql_error());
	$r1=mysql_fetch_array($e1);
	}
			 
			 
		?> 
<tr>
	<td><?php echo  $i+1;?></td>
<!--<td><?php  // echo  $temp->name;?></td>-->
	<td><?php echo  $temp->food_name;?></td>
    <td><?php echo  $temp->food_desc;?></td>
	<!--<td align="center">
    <?php //if(!empty($temp->food_image)) { ?>
    <img src="<?php //echo base_url() ?>/foods/<?php //echo $temp->food_image; ?>"  width="60" height="60"/>
    <?php //} else
	//{
	 ?>
<img src="<?php //echo base_url() ?>/images/diets.jpg"  width="60" height="60"/>
     <?php  //}?>
     
     
     
	<?php //echo  $temp->diet_day;?>
    </td>-->
<td>
	<a href="<?php echo base_url(); ?>admin/details_diets/<?php echo $temp->id;?>">Visualizza</a>/ <a href="<?php echo base_url(); ?>admin/edit_diets/<?php echo $temp->id;?>">Edit</a>/<a href="<?php echo base_url(); ?>home/delete_diets/<?php echo $temp->id;?>" onclick="return confirm('are you sure to delete')">Delete </a>
</td>
	
</tr> 

         <?php
         }
		}
		else
		{
			echo "<tr align='center'><td colspan='6' align='center'> No Records Found   </td></tr>";
		}
		
		
		
		}
    }
	
}
/* Search Activity   */
function searchactivity()
	{
		if($_POST)
		{
			
if($_POST['form_type']== 'patient_search')
		{
			$this->session->set_userdata('search_keyword1',$_POST['keyword']);
			echo  $srch=$_POST['keyword'];
			  //@$ctype=$_POST['ctype'];
			//getPatients searchPatients
			$this->data['patients']= $this->user->serach_activity($srch);
			//$this->data['professionals']= $this->user->getProfessionals();
			
		$str='';
		if(count($this->data['patients'])>0)
		{
			$i=0;
		 foreach($this->data['patients'] as $temp)
		 {
			 $i++;
	//$sbid=$temp->act_type;
	//$s1="select * from sub_event_type where sub_id='$sbid'";
	//$e1=mysql_query($s1) or die(mysql_error());
	//$r1=mysql_fetch_array($e1);
	//$did=$temp->act_details;
	//$d1="select * from sub_even_details where id='$did'";
	//$e2=mysql_query($d1) or die(mysql_error());
	//$r2=mysql_fetch_array($e2);
			 
			 
		?> 
			<tr>
<td><?php echo $i;?></td>
<td><?php echo  $temp->act_name;?></td>
<td><?php echo $temp->act_details; ?></td>
<!--<td><?php// echo  $temp->addedby;?></td>-->
<td><?php echo  $temp->act_date;?></td>
<td>
<a href="<?php echo base_url(); ?>admin/details_activity/<?php echo $temp->act_id;?>">Visualizza</a>/ <a href="<?php echo base_url(); ?>admin/edit_activity/<?php echo $temp->act_id;?>">Edit</a>/<a href="<?php echo base_url(); ?>admin/delete_activity/<?php echo $temp->act_id;?>" onclick="return confirm('are you sure to delete')">Delete </a>

</td>
</tr>


         <?php
         }
		 
		}
		else
		{
			echo "<tr align='center'><td colspan='6' align='center'> No Records Found   </td></tr>";
		}
		
		
		
		}
    }
	
	}
/*  Search  parameter   */	
function searchparameter()
{
     if($_POST)
	  {  
	   
			
if($_POST['form_type']== 'patient_search')
		{
			$this->session->set_userdata('search_keyword1',$_POST['keyword']);
			echo  $srch=$_POST['keyword'];
			  //@$ctype=$_POST['ctype'];
			//getPatients searchPatients
			$this->data['patients']= $this->user->serach_parameter($srch);
			//$this->data['professionals']= $this->user->getProfessionals();
			
		$str='';
		if(count($this->data['patients'])>0)
		{
			$i=0;
		 foreach($this->data['patients'] as $temp)
		 {
			 $i++;
          ?> 
<tr>
<td><?php echo $i;?></td>
<td><?php echo  $temp->pname;?></td>
<td><?php echo  $temp->p_date;?></td>
<td>
<!--<a href="<?php echo base_url(); ?>admin/details_parameters/<?php echo $temp->id;?>">Visualizza</a>/--> 
 <a href="<?php echo base_url(); ?>admin/edit_parameters/<?php echo $temp->id;?>">Edit</a>/<a href="<?php echo base_url(); ?>admin/delete_parameters/<?php echo $temp->id;?>" onclick="return confirm('are you sure to delete')">Delete </a>
</td>
</tr>
<?php
         }
		 
		}
		else
		{
			echo "<tr align='center'><td colspan='6' align='center'> No Records Found   </td></tr>";
		}
		
		
		
		}
     
	 }
	
}
	
	
	
/* Search Owner Start Hear */	
	
	function searchowner()
	{
		
		
		if($_POST)
		{
			
if(@$_POST['form_type']== 'patient_search')
		{
			$this->session->set_userdata('search_keyword1',$_POST['keyword']);
			echo  $srch=$_POST['keyword'];
			  //@$ctype=$_POST['ctype'];
			//getPatients searchPatients
			$this->data['patients']= $this->user->serach_owner($srch);
			//$this->data['professionals']= $this->user->getProfessionals();
			
		$str='';
		if(count($this->data['patients'])>0)
		{
			$i=0;
		 foreach($this->data['patients'] as $temp)
		 {
			 $i++;
		?> 

<tr>
<td><?php echo $i+1;?></td>
<td><?php echo  $temp->own_name;?></td>
<!--<td><?php //echo $temp->own_licence;?></td>-->
<td> <a href="javascript:void(0)" onclick="javascript:showDiv('w1',1,'ali','<?php echo $temp->own_id; ?>','0')">  <?php echo  $temp->owner_email;?> </a></td>
<!--<td><?php //echo  $temp->own_address;?></td>-->
<td><?php echo  $temp->own_status;?></td>
<td><?php echo  $temp->own_date;?></td>
<td><?php echo  $temp->l_type;?></td>
<td>
<a href="<?php echo base_url(); ?>admin/details_owner/<?php echo $temp->own_id;?>">Edit </a>/
 <a href="<?php echo base_url(); ?>admin/details_owner_show/<?php echo $temp->own_id;?>">View </a>
/<a href="<?php echo base_url(); ?>admin/delete_owner/<?php echo $temp->own_id;?>" onclick="return confirm('are you sure to delete')">Delete </a>
</td>
</tr>




         <?php
         }
		 
	 
		}
		else
		{
			echo "<tr align='center'><td colspan='6' align='center'> No Records Found   </td></tr>";
		}
		
		
		
		}
		



if($_POST['form_type1']== 'datesearch')
		{  
		
			$this->session->set_userdata('search_keyword1',$_POST['keyword']);
			echo $date1=$_POST['date1'];
			echo  $date2=$_POST['keyword'];
			  //@$ctype=$_POST['ctype'];
			//getPatients searchPatients
			$this->data['patients']= $this->user->serach_owner_date($date1,$date2);
			//$this->data['professionals']= $this->user->getProfessionals();
			
		$str='';
		if(count($this->data['patients'])>0)
		{
			$i=0;
		 foreach($this->data['patients'] as $temp)
		 {
			 $i++;
		?> 

<tr>
<td><?php echo $i+1;?></td>
<td><?php echo  $temp->own_name;?></td>
<!--<td><?php //echo $temp->own_licence;?></td>-->
<td> <a href="javascript:void(0)" onclick="javascript:showDiv('w1',1,'ali','<?php echo $temp->own_id; ?>','0')">  <?php echo  $temp->owner_email;?> </a></td>
<!--<td><?php //echo  $temp->own_address;?></td>-->
<td><?php echo  $temp->own_status;?></td>
<td><?php echo  $temp->own_date;?></td>
<td><?php echo  $temp->l_type;?></td>
<td>
<a href="<?php echo base_url(); ?>admin/details_owner/<?php echo $temp->own_id;?>">Edit </a>/
 <a href="<?php echo base_url(); ?>admin/details_owner_show/<?php echo $temp->own_id;?>">View </a>
/<a href="<?php echo base_url(); ?>admin/delete_owner/<?php echo $temp->own_id;?>" onclick="return confirm('are you sure to delete')">Delete </a>
</td>
</tr>




         <?php
         }
		 
	 
		}
		else
		{
			echo "<tr align='center'><td colspan='6' align='center'> No Records Found   </td></tr>";
		}
		
		
		
		 
		 
		   }



if($_POST['form_type1']== 'dates')
		{  
		
			$this->session->set_userdata('search_keyword1',$_POST['keyword']);
			echo $date1=$_POST['date1'];
			$date2=date("Y-m-d");
			  //@$ctype=$_POST['ctype'];
			//getPatients searchPatients
			$this->data['patients']= $this->user->serach_owner_date($date1,$date2);
			//$this->data['professionals']= $this->user->getProfessionals();
			
		$str='';
		if(count($this->data['patients'])>0)
		{
			$i=0;
		 foreach($this->data['patients'] as $temp)
		 {
			 $i++;
		?> 

<tr>
<td><?php echo $i+1;?></td>
<td><?php echo  $temp->own_name;?></td>
<!--<td><?php //echo $temp->own_licence;?></td>-->
<td> <a href="javascript:void(0)" onclick="javascript:showDiv('w1',1,'ali','<?php echo $temp->own_id; ?>','0')">  <?php echo  $temp->owner_email;?> </a></td>
<!--<td><?php //echo  $temp->own_address;?></td>-->
<td><?php echo  $temp->own_status;?></td>
<td><?php echo  $temp->own_date;?></td>
<td><?php echo  $temp->l_type;?></td>
<td>
<a href="<?php echo base_url(); ?>admin/details_owner/<?php echo $temp->own_id;?>">Edit </a>/
 <a href="<?php echo base_url(); ?>admin/details_owner_show/<?php echo $temp->own_id;?>">View </a>
/<a href="<?php echo base_url(); ?>admin/delete_owner/<?php echo $temp->own_id;?>" onclick="return confirm('are you sure to delete')">Delete </a>
</td>
</tr>




         <?php
         }
		 
	 
		}
		else
		{
			echo "<tr align='center'><td colspan='6' align='center'> No Records Found   </td></tr>";
		}
		
		
		
		 
		 
		   }
		
		
		
		
		
		
		
    }
	
	}
	
/*================================================***** Add the owner Code by ali  *****=====================================================   */	
	
	function getmoreevents($id)
{
	
	//echo "okokokokokokoko";
	$this->data['events'] = $this->admin_one->get_moreevent($id);
	//$this->data['flash']=$pid;
	$this->load->view('home/final1admin', $this->data,$this->data);
	
}
	
	
	
	
	function addowner_rsa()
	{ 
	    if (!($this->session->userdata('ausername')!="")) 
		{
			redirect('/auth/login/');
		}
		else 
		{
		  
	      
		  $this->load->view('admin/add_owner_rsa');
		 	  
		}   
	 
	}
	
	function addowner_consortuim()
	{ 
	
	    if (!($this->session->userdata('ausername')!="")) 
		{
			redirect('/auth/login/');
		}
		else 
		{
		  //$this->data['users'] = $this->user->getUsersAll();
	      //$this->load->view('admin/users_list', $this->data);
		  $this->data['owner_table']= $this->user->getowners();
		  
		  $this->load->view('admin/add_owner_consortuim');
		   
		  
		}   
	 
	}
	
	function list_owners()
	{	 
	if (!($this->session->userdata('ausername')!="")) 
		{
			redirect('/auth/login/');
		}
		else
		{
		$data['base']=$this->config->item('base_url');
		$total=$this->user->message_count();
		$per_pg=20;
		$offset=$this->uri->segment(3);
		$this->load->library('pagination');
		$config['base_url'] = $data['base'].'/admin/list_owners/';
	    $config['total_rows'] = $total;
	    $config['per_page'] = $per_pg;
		$config["num_links"]=40;
		$config['full_tag_open'] = '<div id="pagination">';
		$config['full_tag_close'] = '</div>';
        $this->pagination->initialize($config);
        $this->data['pagination']=$this->pagination->create_links();
		$this->data['owner_table']=$this->user->getowners($per_pg,$offset);
		
		$this->data['menu']="owa";
		
		$this->load->view('admin/list_owners', $this->data);
		}
	}
/*=============> Owner Details are start heat  ===========================================================================>     */	
	
	function delete_patient($pid)
	{
		$ds="delete from patients where id='$pid'";
		$de=mysql_query($ds) or die(mysql_error());
		//$this->load->view('admin/list_owners', $this->data);
		redirect("admin/list_owners");
	}
	
	function edit_careigvers($cid)
	{
		$this->data['caregiver']= $this->user->careDetail($cid);
		
	if($_POST)
	 {  
	 
    $cid;
    $password=$this->session->userdata('passwordvalue');
	$email=$this->session->userdata('email');

	$cname=mysql_escape_string(trim($_POST['cname']));
	$surname=mysql_escape_string(trim($_POST['surname']));
	$zipcode=mysql_escape_string(trim($_POST['zipcode']));
	$city=mysql_escape_string(trim($_POST['city']));
	$address=mysql_escape_string(trim($_POST['address']));
	$contact=mysql_escape_string(trim($_POST['contact']));
	$country=mysql_escape_string(trim($_POST['state']));
	$lat=mysql_escape_string(trim($_POST['lat']));
	$lon=mysql_escape_string(trim($_POST['lng']));
	$age=mysql_escape_string(trim($_POST['age']));
	$gender=mysql_escape_string(trim($_POST['gender']));
	if(!empty($_FILES['photo']['name']))
	{  
 

				//echo "Temp file: " . $_FILES["photo"]["tmp_name"] . "<br>";
				
					//$file=$_FILES["photo"]["name"];
					//if(move_uploaded_file($_FILES["photo"]["tmp_name"],
					//"uploads/" . $file))
					//$this->session->set_userdata('user_photo', $file);
				
$file=$_FILES['photo']['name'];
 $type=$_FILES['photo']['type'];
move_uploaded_file($_FILES['photo']['tmp_name'],"uploads/".$file);

if($type=='image/jpeg')
{
	$fi="uploads/".$file;
   //$newHeight=100;
   //$newWidth=100;
   $newHeight=200;
   $newWidth=200;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;
    list($width,$height) = getimagesize($fi);
    $img = imagecreatefromjpeg($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    //imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
	imagecopyresampled( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagejpeg($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }
    if($type=='image/png')
    {
    $fi="uploads/".$file;
   //$newHeight=100;
   //$newWidth=100;
   $newHeight=200;
   $newWidth=200;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;

    list($width,$height) = getimagesize($fi);
    $img = imagecreatefrompng($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    //imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
	imagecopyresampled( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagepng($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }
    
    if($type=='image/gif')
    {
   $fi="uploads/".$file;
   //$newHeight=100;
   //$newWidth=100;
   $newHeight=200;
   $newWidth=200;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;

    list($width,$height) = getimagesize($fi);
    $img = imagecreatefromgif($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    //imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
	imagecopyresampled( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagegif($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }

    unlink($fi);  
	}
	else
	{
		$nfile=$_POST['photo'];
	}
	
	$us=$this->user->getcaregiversbyid($cid);
	
	
   if($us>0)
   {
 //$lat= constant("lat");
 //$lon= constant("lon");
  $us1=$this->user->updatecaregiversbycid1($cid,$lat,$lon,$cname,$surname,$zipcode,$city,$address,$contact,$age,$gender,$country,$nfile); 
 if($us1)
 {
	/* echo "<script>alert('caregiver Activated Successfully please login to continue')</script>";*/
	 
	 // redirect('/auth/after_caregiver_register/', 'refresh');
	// $this->data['flash']=" Caregier Details Updates Successfully ";
	 redirect("admin/list_owners");
	 
	 
 }
   }
   else
   {
   /* echo "<script>alert('Please Enter the correct details again ')</script>";
	redirect('/auth/careregister/'.$cid, 'refresh');*/
	$this->data['flash']=" Not Success ";
	
	
   
   }
   
	 
	 
	 redirect("admin/list_owners");
	 }
		
	$this->load->view('admin/owner_edit_care_details',$this->data);
		
	}
	
	
	
	function delete_caregivers($cid)
	{
		$ds="delete from caregiver where id='$cid'";
		$de=mysql_query($ds) or die(mysql_error());
		$d1="delete from  cons_request_messages where sender='$cid'";
		$e1=mysql_query($d1) or die(mysql_error());
		$ls="delete from list_caregiver where careid='$cid'";
		$le=mysql_query($ls) or die(mysql_error());
		$chs="delete from chat where chat.from='$cid' or chat.to='$cid' and (usertype='1' or receivertype='1')";
	    $che=mysql_query($chs) or die(mysql_error());
		redirect("admin/list_owners");
		//$this->load->view('admin/list_owners', $this->data);
		
		
	}
	
	
	
	
	function details_owner_show($id)
	{
	$this->data['owners']= $this->user->ownerdetail($id);
	    $this->data['menu']="owa";	
		$this->load->view('admin/owner_edit',$this->data );
		
	}
	function care_details($oid,$pid)
	{
		$this->data['user']= $this->user->careDetail($pid);
		$this->load->view('admin/care_details',$this->data);
		
	}
	
	function view_care_details($cid)
	{
		$this->data['user']= $this->user->careDetail($cid);
		$this->data['menu']="care";
		
		$this->data['apatients']= $this->user->care_associatedpatients($cid);
		
		
		
		$this->load->view('admin/care_details',$this->data);
		
	}
	
	
	
	
	
	function care_details1($pid)
	{
		$this->data['user']= $this->user->careDetail($pid);
		$this->load->view('admin/ajaxcare_details',$this->data);
		
	}
	function delete_owner($ownid)
	{
		
		
		$d2="select * from list_caregiver where own_id='$ownid'";
		$e2=mysql_query($d2) or die(mysql_error());
		while($r2=mysql_fetch_array($e2))
		{
			$nd="delete from notifications where care_id='$r2[careid]'";
			$ne=mysql_query($nd) or die(mysql_error());
			$pd="delete from professional_networks where sid='$r2[careid]' ";
			$pe=mysql_query($pd);
			$cds="delete from cons_request_messages where sender='$r2[careid]' or recipient ='$r2[careid]'";
			$cde=mysql_query($cds) or die(mysql_error());
			$chs="delete from chat where chat.from='$r2[careid]' or chat.to='$r2[careid]' and (usertype='1' or receivertype='1')";
			$che=mysql_query($chs) or die(mysql_error());
			$d1="delete from parameters where care_id='$r2[careid]'";
			$e1=mysql_query($d1) or die(mysql_error());
			
		}
		
		$nd="delete from caregiver where own_id='$ownid'";
			$ne=mysql_query($nd) or die(mysql_error());
		$mcs="delete from list_caregiver where own_id='$ownid'";
		$mce=mysql_query($mcs) or die(mysql_error());
			
		$psd="delete from patients where own_id='$ownid'";
		$pse=mysql_query($psd) or die(mysql_error());
		$d1="delete from owner_table where own_id='$ownid'";
		$e1=mysql_query($d1) or die(mysql_error());
		$old="delete from owner_licence where own_id='$ownid'";
		$ole=mysql_query($old) or die(mysql_error());
		redirect("admin/list_owners");
		
		
		
	}
	
	
	function details_owner($id)
	{

		$this->data['owners']= $this->user->ownerdetail($id);
		
		if(isset($_POST['submit']))
		{
		$name=$_POST['oname'];
		$mobile=$_POST['mobile'];
		$address=$_POST['address'];
		$status=$_POST['status'];
		$trns=$_POST['trns'];
		$pay=$_POST['pay'];
		//$cre=explode("/",$_POST['created']);
		//$date=$cre[2]."-".$cre[1]."-".$cre[0];
		
		$id=$this->user->update_owner($id,$name,$mobile,$address,$status,$trns,$pay);
		$this->data['flash']=" Details Updated Successfully  ";
		redirect('/admin/list_owners/');
		}
		
		$this->data['menu']="owa";	
			
		$this->load->view('admin/owner_details',$this->data );

	}
/* edit Owner details   */	
	
	
	
	
	function getlicence($oid)
	{
      $this->data['ownerid']= $oid;
      $this->load->view('admin/owner_licence',$this->data );
		
	}
	
	
	
	
	
	
	
	function users()
	{
	    if (!($this->session->userdata('ausername')!="")) {
			redirect('/auth/login/');
		} else {
		  $this->data['users'] = $this->user->getUsersAll();
	      $this->load->view('admin/users_list', $this->data);
		  
		}   
	}
	
	
	
	
	
	
	function users_professional()
	{
	    if (!($this->session->userdata('ausername')!="")) {
			redirect('/auth/login/');
		} else {
			
		  $this->data['users'] = $this->user->getProfessionalsAll();
	      $this->load->view('admin/professionals_list', $this->data);
		}
	
	}
	function users_caregivers()
	{
	    if (!($this->session->userdata('user_name')!="")) {
			redirect('/auth/login/');
		} else {
		  $this->data['users'] = $this->user->getCaregiversAll();
	      $this->load->view('admin/caregivers_list', $this->data);
		}
	}
	function professional_details($id=null)
	{
	    if (!($this->session->userdata('user_name')!="")) {
			redirect('/auth/login/');
		} else {
		  $this->data['user'] = $this->user->getProfessionalsAll($id);
	      $this->load->view('admin/users_details', $this->data);
		 
		}
	}
	
	function users_details($id=null)
	{
	    if (!($this->session->userdata('user_name')!="")) {
			redirect('/auth/login/');
		} else {
		  $this->data['user'] = $this->user->getCaregiversAll($id);
	      $this->load->view('admin/users_details', $this->data);
		 
		}
	}
	
	
	function edit_professionals($id=null)
	{   
	    if (!($this->session->userdata('user_name')!="")) {
			redirect('/auth/login/');
		} else {
          if($_POST){
			
			$res=$_POST['from'];
			$data=explode('/',$res);
			$result=$data[2].'-'.$data[0].'-'.$data[1];
			$_POST['from']=$result;
					
		   $this->user->updateprofessional($id);
		   redirect( $this->agent->referrer(),  'refresh');

		  }else{
		   
		  }
		}
		
		$this->data['user']= $this->user->professionalDetail($id);
	    $this->load->view('admin/edit_professional', $this->data);
	}
	
	function edit_admin($id=null)
	{   
		 $temp= $this->user->adminDetail($this->session->userdata('user_id'));
		$temp= $temp[0]->id;
		
	    if (!($this->session->userdata('user_name')!="")) {
			redirect('/auth/login/');
		} else {
          if($_POST){
		   $this->user->updateAdmin($id);
		   redirect( $this->agent->referrer(),  'refresh');

		  }else{
		   
		  }
		}
		
		$this->data['admin']= $this->user->adminDetail($temp);
		$this->load->view('admin/edit_admin', $this->data);
	}
	
	
	function edit_caregiver($id=null)
	{   
	    if (!($this->session->userdata('user_name')!="")) {
			redirect('/auth/login/');
		} else {
          if($_POST){
		   $this->user->updatecaregiver($id);
		
		   redirect( $this->agent->referrer(),  'refresh');

		  }else{
		   
		  }
		}
		
		$this->data['user']= $this->user->caregiverDetail($id);
	    $this->load->view('admin/edit_caregiver', $this->data);
	}
	function delete_caregiver($id=null)
	{
	    $cd="delete from caregiver where id='$id'";
		$ce=mysql_query($cd) or die(mysql_error());
		
		$lds="delete from list_caregiver where careid='$id'";
		$lde=mysql_query($lds) or die(mysql_error());
		$nsd="delete from notifications where care_id='$id'";
		$nse=mysql_query($nsd) or die(mysql_error());
		
		$pfs="delete from professional_networks where sid='$id' or rid='$id'";
		$pfe=mysql_query($pfs) or die(mysql_error());
		$sn="delete from set_notifications where car_id='$id'";
		$se=mysql_query($sn) or die(mysql_error());
		
		redirect("admin/list_caregivers");
		
		
		
	}
	
	function delete_professional($id=null)
	{
	    if (!($this->session->userdata('user_name')!="")) {
			redirect('/auth/login/');
		} else {
          $this->user->deleteprofessional($id);
		  redirect( $this->agent->referrer(),  'refresh');
  
		}
	}
	
	function edit_user($id=null)
	{   
	    if (!($this->session->userdata('user_name')!="")) {
			redirect('/auth/login/');
		} else {
          if($_POST){
		   $this->user->updateUser($id);
		   redirect( $this->agent->referrer(),  'refresh');

		  }else{
		   
		  }
		}
		
		$this->data['user']= $this->user->get_users_by_id($id);
	    $this->load->view('admin/edit_user', $this->data);
	}
	function delete_users($id=null)
	{
	    if (!($this->session->userdata('user_name')!="")) {
			redirect('/auth/login/');
		} else {
          $this->user->deleteUsers($id);
		  redirect( $this->agent->referrer(),  'refresh');
  
		}
	}
	
	function user_detail($id=null)
	{
	    if (!($this->session->userdata('user_name')!="")) {
			redirect('/auth/login/');
		} else {
		  $this->data['user'] = $this->user->getProfessionalsAll($id);
	      $this->load->view('admin/users_detail', $this->data);
		 
		}
	}
	
	
	
	function change_pass()
	{
	    if (!$this->tank_auth->is_logged_in()) {
			redirect('/auth/login/');
		} else {

			$this->form_validation->set_rules('old_password', 'Old Password', 'trim|required|xss_clean');
			$this->form_validation->set_rules('new_password', 'New Password', 'trim|required|xss_clean|min_length['.$this->config->item('password_min_length', 'tank_auth').']|max_length['.$this->config->item('password_max_length', 'tank_auth').']|alpha_dash');
			$this->form_validation->set_rules('confirm_new_password', 'Confirm new Password', 'trim|required|xss_clean|matches[new_password]');

			$this->data['errors'] = array();

			if ($this->form_validation->run()) {// validation ok
			if ($this->tank_auth->change_password(
			$this->form_validation->set_value('old_password'),
			$this->form_validation->set_value('new_password'))) {	// success
			$this->_show_message($this->lang->line('auth_message_password_changed'));

			} else {						// fail
				$errors = $this->tank_auth->get_error_message();
				foreach ($errors as $k => $v)	$this->data['errors'][$k] = $this->lang->line($v);
			}
			}
			
			$this->load->view('admin/change_password',$this->data);
		  
		}
	}

	function add_medicine()
	{
		if($_POST){
		/*$this->form_validation->set_rules('medicine_name', 'Medicine Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('medicine_format', 'Medicine Format ', 'trim|required|xss_clean');
		$this->form_validation->set_rules('note', 'Note ', 'trim|required|xss_clean');
		$this->form_validation->set_rules('quantity', 'Quantity ', 'trim|required|xss_clean');
		$this->form_validation->set_rules('purchase_date', 'Purchase Date ', 'trim|required|xss_clean');
		$this->form_validation->set_rules('expiration_date', 'Expiration Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('dosage', 'Dosage ', 'trim|required|xss_clean');
		$this->form_validation->set_rules('meal_time', 'Meal Time ', 'trim|required|xss_clean');
		$this->form_validation->set_rules('price', 'Price ', 'trim|required|xss_clean');*/
		
				//$temp=$_POST['purchase_date'];
				//$data=explode('/',$temp);
				//$result=$data[2].'-'.$data[0].'-'.$data[1];
				$_POST['purchase_date'];
				
				//$temp=$_POST['expiration_date'];
				//$data=explode('/',$temp);
				//$result=$data[2].'-'.$data[0].'-'.$data[1];
				$_POST['expiration_date'];
				
			$this->form_validation->set_rules('medicine_name', 'Medicine Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('medicine_format', 'Medicine Format ', 'trim|required|xss_clean');
		$this->form_validation->set_rules('note', 'Note ', 'trim|required|xss_clean');
		
			if($this->form_validation->run() == TRUE)
			//if(!empty($_POST['medicine_name']))
			{
				
				echo $name=$_POST['medicine_name'];
				 echo $num=$this->user->medicinebyname($name);
				 echo "<script>alert($num)</script>";
				if($num<=0)
				{
					$this->user->addMedicines();
					$this->data['flash']="Diet has been added !";
					redirect('/admin/list_medicine/');
				}
				else
				{
					$this->data['flash']="Medicine Name Already present 	";
				}
				
			
			
			}
			else
			{
				$this->data['flash']="Unsuccesfull, Try again !";

			}
			
	    }			
		$this->data['menu']="med";
	   @$this->load->view('admin/add_medicine', $this->data);
	}
	function list_medicine()
	{  
	
	    $data['base']=$this->config->item('base_url');
		$total=$this->user->medicine_count();
		$per_pg=20;
		$offset=$this->uri->segment(3);
		$this->load->library('pagination');
		$config['base_url'] = $data['base'].'/admin/list_medicine/';
	    $config['total_rows'] = $total;
	    $config['per_page'] = $per_pg;
		$config["num_links"]=40;
		$config['full_tag_open'] = '<div id="pagination">';
		$config['full_tag_close'] = '</div>';
        $this->pagination->initialize($config);
        $this->data['pagination']=$this->pagination->create_links();
	
	
	
	
	
	
	 
	$this->data['medicines']=$this->user->getMedicines($per_pg,$offset);
	$this->data['menu']="med";
	
	$this->load->view('admin/list_medicine', $this->data);
	}
	function edit_medicine($id=null)
	{
     if($_POST){
	     $this->user->updateMedicine($id);
		 redirect('/admin/list_medicine/');
	 }
	 $this->data['menu']="med";
		 $this->data['medicine']=$this->user->medicineDetail($id);
		 $this->load->view('admin/edit_medicine', $this->data);

	}
	function list_diets()
	{	 
		$data['base']=$this->config->item('base_url');
		$total=$this->user->diets_count();
		$per_pg=20;
		$offset=$this->uri->segment(3);
		$this->load->library('pagination');
		$config['base_url'] = $data['base'].'/admin/list_diets/';
	    $config['total_rows'] = $total;
	    $config['per_page'] = $per_pg;
		$config["num_links"]=40;
		$config['full_tag_open'] = '<div id="pagination">';
		$config['full_tag_close'] = '</div>';
        $this->pagination->initialize($config);
        $this->data['pagination']=$this->pagination->create_links();
	  
		$this->data['diets']=$this->user->getdiets($per_pg,$offset);
		$this->data['menu']="dit";
		$this->load->view('admin/list_diets', $this->data);
	}
	
	public function add_diets()
	{
	if($_POST){
				
			//$this->form_validation->set_rules('food_type', 'Food Type ', 'trim|required|xss_clean');
			$this->form_validation->set_rules('food_name', 'Food Name ', 'trim|required|xss_clean');
			//$this->form_validation->set_rules('diet_day', 'Diet Day ', 'trim|required|xss_clean');
			//$temp=$_POST['diet_day'];
			//$data=explode('/',$temp);
			//$result=$data[2].'-'.$data[0].'-'.$data[1];
			//$_POST['diet_day']=$result;
			if($this->form_validation->run() == TRUE)
			{	
			   $fname=$_POST['food_name'];
			   $ds="select * from diets where food_name='$fname'";
			   $de=mysql_query($ds) or die(mysql_error());
			   $dn=mysql_num_rows($de);
			   if($dn<=0)
			   {
				$this->patient->adddiets();
				$this->data['flash']="Diet has been added !";
			   }
			   else
			   {
				 $this->data['flash']="Trying To Enter Same Diet Name Please Enter Anether Name !";  
			   }
			   
			}
			else
			{
				$this->data['flash']="Unsuccesfull, Try again !";

			}	
		
		}
		
			$this->data['caregivers']= $this->user->getCaregiverslist();
			//$this->data['patient'] = $this->patient->get_patient_by_id($this->session->userdata('user_id'));
			
		$this->data['menu']="dit";
		//if($this->session->userdata('user_role')=='admin')
		//{
			$this->load->view('admin/add_diets', $this->data);
		//}
		
		/*else
		{
			$this->load->view('home/add_diets', $this->data);
		}*/

	}
	function details_diets($id=null)
	{
		$this->data['diets']= $this->patient->dietsDetail($id);
		$this->data['menu']="dit";
		$this->load->view('admin/diet_details',$this->data );
	}
	
	function edit_diets($id=null)
	{
	if($_POST)
		{
		$this->patient->updatediets($id);
			//redirect('/home/list_diets/');
			
			$this->data['flash']=" Updated Successfully ";
			//redirect("admin/list_diets");
			
			
		}
		$this->data['menu']="dit";
			$this->data['diets']=$this->patient->dietsDetail($id);
			$this->load->view('admin/edit_diets', $this->data);

	}
	
	
	
	
	
	function details_medicine($id=null)
	{

		$this->data['medicine']= $this->user->medicineDetail($id);
		$this->data['menu']="med";
		
		$this->load->view('admin/medicine_details',$this->data );

	}
	function delete_medicine($id)
	{

		 $this->user->deleteMedicines($id);
		 redirect('/admin/list_medicine/');
	}


	function edit_periods($id=null)
	{
     if($_POST){
	 
			$temp=$_POST['created'];
			$data=explode('/',$temp);
			$result=$data[2].'-'.$data[0].'-'.$data[1];
			$_POST['created']=$result;
			
	     $this->user->updatePeriods($id);
		 redirect('/home/list_periods/');
	 }
	 $this->data['periods']=$this->user->periodDetail($id);
	 $this->load->view('admin/edit_period', $this->data);

	}
	function details_period($id=null)
	{

		$this->data['periods']= $this->user->periodDetail($id);
		$this->load->view('admin/period_details',$this->data );

	}
	function delete_period($id)
	{

	 $this->user->deleteperiods($id);
     redirect('/home/list_periods/');
	}


	function edit_food($id=null)
	{
		if($_POST){
		$temp=$_POST['created'];
				$data=explode('/',$temp);
				$result=$data[2].'-'.$data[0].'-'.$data[1];
				$_POST['created']=$result;
				
		$this->user->updateFood($id);
		redirect('/home/list_foods/');
		
				
	}
		$this->data['foods']=$this->user->foodDetail($id);
		$this->load->view('admin/edit_food', $this->data);

	}
	function details_food($id=null)
	{

		$this->data['foods']= $this->user->foodDetail($id);
		$this->load->view('admin/food_details',$this->data );

	}

	function delete_food($id)
	{

		$this->user->deletefoods($id);
		redirect('/home/list_foods/');
	}


	function edit_parameters($id=null)
	{     
	 
		
		if($_POST)
		{
		$pname=$_POST['pname'];
		
		$lc=$_POST['column1'];	
	    $num=count($_POST['column1']);
		
		 $this->user->updateparameters($id,$pname);
		 $ms="select * from parameter_columns where para_id='$id'";
		$me=mysql_query($ms) or die(mysql_error());
		$i=0;
		while($mr=mysql_fetch_array($me))
		{
		$name=$lc[$i];
		$mid=$mr['id'];	
			 echo $ps="update parameter_columns set paraname='$name' where id='$mid'";
		 
		   
		 $pe=mysql_query($ps) or die(mysql_error());
		
		$i++ ;
		
		}
		
		/*for($i=0;$i<$num;$i++)
				{
				$name=$lc[$i];
				//$this->user->update_paracolumns($id,$name);
				//$ps="update parameter_columns set paraname='$name' where para_id='$id'";
				//$pe=mysql_query($ps) or die(mysql_error());
				
				
				}*/
				
		@$cn=$_POST['column'];	
	    @$cnum=count($_POST['column']);
		if($cnum>0)
		{
			for($i=0;$i<$cnum;$i++)
				{
				$name=$cn[$i];
				$this->patient->add_paracolumns($id,$name);
				
				}
		}
		
		
		redirect('/admin/list_parameters/');
		
		}
		
		
		
			$this->data['menu']="pra";
		$this->data['parameters']=$this->user->parametersDetail($id);
		$this->load->view('admin/edit_parameters', $this->data);

	  
	  
	}
	function details_parameters($id=null)
	{

		$this->data['parameters']= $this->user->parametersDetail($id);
		$this->load->view('admin/parameters_details',$this->data );

	}

	function delete_parameters($id)
	{

		$this->user->deleteparameters($id);
		redirect('/admin/list_parameters/');
	}




	
	
	
}
