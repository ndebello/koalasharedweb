<?php
// Errors
//koala italian strings
$lang['title'] = "Titolo";
$lang['Italiano']="Italiano";
$lang['loginas']="login come";
$lang['username']="nome utente";
$lang['associatedprofessionals']="Professionisti collegati";
$lang['associatedcaregivers']="caregivers collegati";
$lang['professional']="Professionisti";
$lang['associatedpatients']="Paziente associato";
$lang['registeryourself']   ="Registrati come";
$lang['select']  ="Seleziona";
$lang['private']  ="Privato";
$lang['business']  ="Business";
$lang['type']="Tipologia";
$lang['delete']="cancellare";
$lang['registration']="Registrazione";
$lang['name']="Nome";
$lang['email']="e-mail";
$lang['reenteremail']="Inserisci nuovamente la tua e-mail";
$lang['password']="password";
$lang['cancle']="Cancella";
$lang['forward']="Continua";
$lang['licencetype']="Tipo di licenza";
$lang['chooseyourlicence']="Scegli la tua licenza";
$lang['chooseyourbusinessmodel']="Scegli il tuo piano Business";
$lang['payment']="Pagamento";
$lang['amounttopay']="Quota dovuta";
$lang['search']="Cerca";
$lang['editprofile']="Modifica profilo";
$lang['edit']="Modifica";
$lang['routine']="Routine";
$lang['all_diete']="Tutte le diete";
$lang['create']="Crea"; 
$lang['by_meals']="Vicino ai Pasti";
$lang['between_meals']="Lontano dai pasti";
$lang['add_new_drug']="Nuovo farmaco";
$lang['peram_res_msg']="Nessun parametro trovato. Imposta un intervallo temporale";
$lang['from']="Dal";
$lang['to']="Al";
$lang['get']="Ottieni";
$lang['parameter']="Parametri";
$lang['parameters']="Parametri";
$lang['parameters_graph_btn']="Grafico parametri";
$lang['parameters_new_btn']="Nuovo parametro";
$lang['medicines']="Farmaci";
$lang['diets']="Diete";
$lang['new_diete']="Nuova dieta";
$lang['activities']="Attività";
$lang['report']="Report";
$lang['notificationsettings']="Impostazione notifiche";
$lang['newevent']="Nuovo evento";
$lang['details']="Dettagli";
$lang['time']="Ora";
$lang['drug']="Farmaco";
$lang['new']="Nuova";
$lang['frequency']=" Frequenza ";
$lang['selectdays']="Imposta giorni";
$lang['monday']="Lunedì";
$lang['tuesday']="Martedì";
$lang['wednesday']="Mercoledì";
$lang['thursday']="Giovedì";
$lang['friday']="Venerdì";
$lang['saturday']="Sabato";
$lang['sunday']="Domenica";
$lang['confirm']="Conferma";
$lang['intolerance']="Intolleranze";//"Sgradito";
$lang['favourites']="Preferiti";//"Gradito";
$lang['date']="Data";
$lang['action']="Azione";
$lang['status']="Stato";
$lang['details']="Dettagli";
$lang['like']="Preferenza";
$lang['food_details']="Dettagli cibo";
$lang['attachment']=" Vedi Allegato";
$lang['patient']="Paziente";
$lang['father']="padre";
$lang['mother']="madre";
$lang['brother']="fratello";
$lang['sister']="sorella";
$lang['son']="Figlio";
$lang['daughter']="Figlia";
$lang['uncle']="zio";
$lang['aunt']="zia";
$lang['Cousin']="cugino";
$lang['new_activity']="Nuova attività";


$lang['male']="Uomo";
$lang['female']="Donna";


$lang['pathology']="Patologie";
$lang['allergies']="Allergie";
$lang['intolerances']="Intolleranze";

$lang['gender']="Sesso";
$lang['surname']="Cognome";
$lang['age']="Età";
$lang['address']="Indirizzo";
$lang['city']="Città";
$lang['zipcode']="C.A.P";
$lang['country']="Nazione";
$lang['contact']="Telefono";
$lang['caregiver_details']="Profilo caregiver";

$lang['uploadphoto']="Carica foto";
$lang['back']="Indietro";
$lang['update']="Aggiorna";
$lang['logout']="Esci";
$lang['cregiverdetails']="Dettagli Caregiver";
$lang['notifications']="Notifiche";
$lang['pending']="In attesa";
$lang['requestsend']="Richiesta inviata";
$lang['accept']="Accetta";
$lang['onlyprimarycaregiveraddthenewevent']="Solo il caregiver primario può creare nuovi eventi";
$lang['event']="eventi";
$lang['norecordsfound']="Nessun elemento trovato";
$lang['addpatients']="Aggiungi assistiti";
$lang['addcaregiver']="Aggiungi caregiver";
$lang['buyanotherlicence']="Ottieni una nuova licenza";
$lang['upgradelicence']="Implementa la licenza";
$lang['doyouwantalsotobeacaregiver']="Vuoi essere anche caregiver?";
$lang['set_notifications']="Impostazione notifiche";
$lang['iam_with_patient']="Sono con il paziente";
$lang['iam_not_with_patient']="Non sono con il paziente";
$lang['yes']="Sì";
$lang['no']="No";
$lang['save']="Salva";
$lang['enterlicencenumber']="Inserisci il numero di licenza";
$lang['contacts']="Contatti";
$lang['map']="Mappa";
$lang['agenda']="Agenda";
$lang['details']="Dettagli";
$lang['data']="dati";
$lang['dataentry']="inserimento Dati";
$lang['consultationdetails']="Dettagli consulto";
$lang['noassociatedprofetionalsadded']="Nessun professionista collegato";
$lang['quantitytopurchase']="Quantità acquistata";
$lang['medicinedose']="Quantità da somministrare";
$lang['price']="Prezzo";
$lang['dateofpurchase']="Data di acquisto";
$lang['date']="Data";
$lang['notifination']="notifiche";
$lang['dateofexpiry']="Data di scadenza";
$lang['note']="Note";
$lang['expiry']="Scadenza";
 
$lang['suppository']="suppository";
$lang['ointment']="ointment";
$lang['eyedrops']="eye drops";
$lang['transdermalpatch']="transdermal patch";
$lang['syrup']="syrup";
$lang['tablet']="tablet";
$lang['forgotpassword']="Password dimenticata";
$lang['logintoyouraccount']="Accedi al tuo account";
$lang['rememberme']="Ricorda";
$lang['donhaveanaccountyet']="Non sono ancora registrato";
$lang['createanaccount']="Crea un account";
$lang['howitworks']="Come funziona";
$lang['requestinformation']="Richiesta informazioni";
$lang['admittedtodoctorsboardof']="Iscritto al registro dei medici di ";
$lang['sendchatmessage']="Had Send The Message";
$lang['videocall']="Hai una richiesta di video-chiamata da";
$lang['sendvideocall']="Invia una richiesta di video-chiamata";

$lang['sleepwake']="Sonno / veglia";
$lang['event']="Evento";
$lang['success']="Successo";
$lang['consultationrequest']="Ha risposto alla tua richiesta di consulto";
$lang['hometitle']="Il primo supporto al caregiving per la malattia di Alzheimer";
$lang['registercomplete']="Registrazione completata con successo ";
$lang['clicktologin']="Clicca qui per accedere e aggiungere pazienti e caregivers";
$lang['nocaregiversadded']="Nessun caregiver presente";
$lang['nopatientadded']="Nessun assistito presente";
$lang['correctlicence']="Per favore, inserire un numero di licenza corretto";
$lang['licence']="licenza";

















$lang['auth_incorrect_password'] = "Password non corretta";
$lang['auth_incorrect_login'] = "Login non corretto";
//$lang['title'] = "Login o indirizzo e-mail non esistenti";
$lang['auth_email_in_use'] = "Indirizzo e-mail gi&agrave; presente. Perfavore, seleziona un nuovo indirizzo.";
$lang['auth_username_in_use'] = "Username gi&agrave; esistente. Perfavore seleziona un nuovo username.";
$lang['auth_current_email'] = "Questa &egrave; la tua e-mail attuale";
$lang['auth_incorrect_captcha'] = "Il codice inserito non &egrave; uguale a quello dell'immagine.";
$lang['auth_captcha_expired'] = "Il tuo codice di conferma &egrave; scaduto. Perfavore riprova.";

// Notifications
$lang['auth_message_logged_out'] = "Logout avvenuto con successo.";
$lang['auth_message_registration_disabled'] = "La registrazione &egrave; disabilitata";
$lang['auth_message_registration_completed_1'] = "Registrazione avvenuta con successo. Controlla il tuo indirizzo e-mail per attivare il tuo account.";
$lang['auth_message_registration_completed_2'] = "Registrazione avvenuta con successo";
$lang['auth_message_activation_email_sent'] = "Una nuova e-mail di attivazione &egrave; stata inviata a %s. Segui le istruzioni nella e-mail per attivare il tuo account.";
$lang['auth_message_activation_completed'] = "Il tuo account &egrave; stato attivato.";
$lang['auth_message_activation_failed'] = "Il codice di attivazione inserito &egrave; errato o scaduto";
$lang['auth_message_password_changed'] = "La tua password &egrave; stata cambiata con successo";
$lang['auth_message_new_password_sent'] = "Ti &egrave; stata inviata una e-mail con le istruzioni su come cambiare la password";
$lang['auth_message_new_password_activated'] = "Cambio della password avvenuto con successo";
$lang['auth_message_new_password_failed'] = "Il tuo codice di attivazione &egrave; errato. Perfavora controlla l'e-mail e riesegui le operazioni.";
$lang['auth_message_new_email_sent'] = "Una e-mail di conferma &egrave; stata inviata a %s. Segui le istruzioni contenute nella e-mail per cambiare il tuo indirizzo e-mail";
$lang['auth_message_new_email_activated'] = "E-mail cambiata con successo";
$lang['auth_message_new_email_failed'] =  "Il tuo codice di attivazione &egrave; errato o scaduto. Perfavore controlla la tua e-mail e segui le istruzioni"; 
$lang['auth_message_banned'] = "Sei stato bannato";
$lang['auth_message_unregistered'] = "Il tuo account &egrave; stato cancellato...";

// Email subjects
$lang['auth_subject_welcome'] = "Benvanuto in %s!";
$lang['auth_subject_activate'] = "Benvenuto in %s!";
$lang['auth_subject_forgot_password'] = "Hai dimenticato la tua password su %s?";
$lang['auth_subject_reset_password'] = "La tua nuova password su %s";
$lang['auth_subject_change_email'] = "Il tuo indirizzo e-mail &egrave; %s";

$lang['no_event_set_this_day']= "Oggi non ci sono eventi";
$lang['enter_your_search'] ="Cerca";
$lang['copy_rights'] ="Koala Copyright © 2014, Tutti i marchi sono proprietà dei loro rispettivi proprietari.";
$lang['parameters_name'] ="Nome Parametro";
$lang['number_measurement']= "Numero Misurazioni";
$lang['add_1'] ="Aggiungi";
$lang['injection'] ="Iniezione";
$lang['capsule'] ="Capsule";
$lang['tablet'] ="Pastiglie";
$lang['syrup'] ="Sciroppo";
$lang['aerosol'] ="Aerosol";
$lang['suppository'] ="Supposte";
$lang['ointment'] ="Pomata";
$lang['eye_drops'] ="Collirio";
$lang['transdermal_patch'] ="Cerotto";
 $lang['optional'] ="Opzionale";

$lang['breakfast']="Colazione";
 $lang['lunch']="Pranzo";
 $lang['dinner']="Cena";
  $lang['snack']="spuntino";
 $lang['Pastiglie']="Pastiglie";
$lang['all_dets']="Tutte le Diete";
$lan['view']="View";
$lang['caregiver_name']="Nome Caregiver";
$lang['done']="Fatto";
$lang['not_done']="Non Fatto";
$lang['not_attended']="Non fatto";
$lang['enter_email_address']="Inserisci indirizzo email";
$lang['send_email']="Invia email";
$lang['event_name']="Nome dell'evento";
$lang['clinical_parameters'] ="Parametri clinici";
$lang['drugs']="Farmaci";
$lang['diet']="Dieta";
$lang['activities']="Attività";
$lang['sleep_wake']="Sonno/Veglia";
$lang['only_this_week']="Solo questa settimana";
$lang['only_this_month']="Solo questo mese";
$lang['forever']="Sempre";
$lang['all_day'] ="Tutti i giorni";
$lang['delete_sure']="Sei sicuro di volerlo eliminare?";
$lang['select_all']="Tutti";
$lang['On']="del";
$lang['wake_up_calls']= "Servizio sveglia";
$lang['withdrawal']= "ritiro";
$lang['of']= "di";
/* End of file tank_auth_lang.php */
/* Location: ./application/language/italian/tank_auth_lang.php */
