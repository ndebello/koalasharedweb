<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('to_excel')) {
	
  function to_excel($query,$filename,$date) {
    $headers = ''; // just creating the var for field headers to append to below
    $data = ''; // just creating the var for field data to append to below
    
    $objt =& get_instance();

    $fields = array('Date','Patient Name','Caregiver Name','Event Name','Details','Time','Status');
     
    
    if ($query->num_rows() == 0) {
       http://54.200.49.194/koalatest/caregiver/view_report/2015-05-20
       redirect('caregiver/view_report/'.$date);
    } else {
      foreach ($fields as $field) {
        $headers .= $field . "\t";
      }

      foreach ($query->result() as $row) {
        $line = '';
        foreach($row as $value) {                                            
          if ((!isset($value)) OR ($value == "")) {
            $value = "\t";
          } else {
            $value = str_replace("\n", " ", $value); // for stupid line breaks that mess up the spreadsheet
            $value = str_replace('"', '""', $value);
            $value = '"' . $value . '"' . "\t";
          }
          $line .= $value;
        }
        $data .= trim($line)."\n";
      }

      $data = str_replace("\r","",$data);

      header("Content-type: application/x-msdownload");
      header("Content-Disposition: attachment; filename=$filename.xls");
      echo "$headers\n$data";  
    }
  }
}
?> 