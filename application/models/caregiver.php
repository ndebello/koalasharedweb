<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class caregiver extends CI_Model {
    function __construct() {
        parent::__construct();
    }
	// get patient all
    function getPatients() {		
		$query = $this->db->get('patients');
		return $query->result();
	}
 /*=====================================================////////  get owner details    /////////================================================ */

function getowner($email)
	{
	
       $this->db->select();
       $this->db->where('owner_email', $email); 
       //$this->db->where('own_id', $oid); 
       $query = $this->db->get('owner_table');
      // return $query->result();
		return $query->num_rows();
	}
	
	

function getprofetion($email)
	{
	
       $this->db->select();
       $this->db->where('email', $email); 
       //$this->db->where('own_id', $oid); 
       $query = $this->db->get('professionals');
       //return $query->result();
	   return $query->num_rows();
		
	}

function getkoalaower($email)
   {
	   $this->db->select();
       $this->db->where('owner_email', $email); 
       //$this->db->where('own_id', $oid); 
       $query = $this->db->get('koala_owner');
       //return $query->result();
	   return $query->num_rows();;
		
	   
	}




function get_owner($id)
{
	$query = $this->db->get_where('owner_table', array('own_id' => $id), 1);
		if( $query->num_rows() > 0 )
		{
		//return $query->row_array();
	    return $query->result();
		//return false;
		}
}


	// get patient by id
	function get_patient_by_id( $id=null ) {
	
		$query = $this->db->get_where('patients', array('caregiver_id' => $id), 1);
		if( $query->num_rows() > 0 ) 
		//return $query->row_array();
	    return $query->result();
		return false;
	}
	
    // search patient all
    function searchPatients() {
	    $data=explode(' ', $_POST['keyword']);
		$searchkey = 'where ';
		foreach($data as $temp){
			if($temp !=''){
				$searchkey .= '`name` like "%'.mysql_real_escape_string($temp).'%"  OR ';
				$searchkey .= '`surname` like "%'.mysql_real_escape_string($temp).'%"  OR ';
				$searchkey .= '`zipcode` like "%'.mysql_real_escape_string($temp).'%"  OR ';	
			}
		} 
		if($searchkey =='where ')
		{
			$searchkey = 'where  1=1';
		}else{
			$searchkey  = substr($searchkey,0,-3);
		}

		$query = $this->db->query("SELECT * FROM patients $searchkey");			
				
		return $query->result();
	}
	
    //------------Periods-------------------//
	function getperiods() 
			{
			$this->db->select('pe.*, p.name'); //or whatever you're selecting
			$this->db->join('patients p', 'pe.patient_id = p.id');
			//$this->db->where('pp.entry_id', $entry_id);
			$query = $this->db->get('periods pe');
			
			return $query->result();
			}
			
	function addperiods() 
		{
			$this->patient_id   = $_POST['patient_id']; 
			$this->sleep_time = $_POST['sleep_time'];
			$this->wake_time = $_POST['wake_time'];
			$this->created = $_POST['created'];
			$this->db->insert('periods', $this);
		}
	//--------------End of Periods------------//
	//----- Diets--------//
	
		function getdiets() 
		{
			$this->db->select('d.*, p.name'); //or whatever you're selecting
			$this->db->join('patients p', 'd.patient_id = p.id');
			//$this->db->where('pp.entry_id', $entry_id);
			$query = $this->db->get('diets d');
			return $query->result();
		}

	
		function adddiets() 
		{
			$this->patient_id   = $_POST['patient_id']; 
			$this->food_type = $_POST['food_type'];
			$this->food_name = $_POST['food_name'];
			$this->diet_day = $_POST['diet_day'];
			$this->db->insert('diets', $this);
		}
		
		function dietsDetail($id) 
		{
			$this->db->where('id', $id);  
			$query = $this->db->get('diets');
			return $query->result();

		}
		
		function updatediets($id) {
				$this->food_type   = $_POST['food_type']; 
				$this->food_name = $_POST['food_name'];
				$this->diet_day = $_POST['diet_day'];
				$this->db->update('diets', $this, array('id' => $_POST['id']));
			}
		function deletediets($id) 
		{
				$this->db->delete('diets', array('id' => $id));

		} 
	//----end od diets--//
	//------------Parameters---------//
		function getparameters() 
		{
			$this->db->select('pp.*, p.name'); //or whatever you're selecting
			$this->db->join('patients p', 'pp.patient_id = p.id');
			//$this->db->where('pp.entry_id', $entry_id);
			$query = $this->db->get('parameters pp');
			return $query->result();
		}
				
		function addparameters() 
		{
			$this->maximum_pressure   = $_POST['maximum_pressure']; 
			$this->minimum_pressure = $_POST['minimum_pressure'];
			$this->blood_glucose = $_POST['blood_glucose'];
			$this->heart_rate = $_POST['heart_rate'];
			$this->weight = $_POST['weight'];
			$this->patient_id = $_POST['patient_id'];
			$this->created = $_POST['created'];
			
			$this->db->insert('parameters', $this);
			
			
			
			
			
			
			
			
		}
		





		function add_owner_rsa() 
		{
			
		//owner_name,owner_type,email_address,mobile_no,address,password,transation,pay,created
		    $this->l_type       = $_POST['owner_type']; 
			$this->owner_email      = $_POST['email_address'];
			$this->trno            = $_POST['transation'];
			$this->payamount          = $_POST['pay'];
			$this->owner_type      ="business";
			//$this->pay_date         = $_POST['created'];
			////$this->own_date           = $_POST['created'];
			$this->own_ipaddress      = $_SERVER['REMOTE_ADDR'];
			//$this->l_type             = $_POST['owner_type'];
			$this->db->insert('owner_table', $this);
     		return $id=$this->db->insert_id();
					
			
		}
		
		
		
		
		
		
			
		function add_owner_cons() 
		{
						//owner_name,owner_type,email_address,mobile_no,address,password,transation,pay,created
						
			$this->own_name           = $_POST['cname']; 
			$this->owner_type         = "business";
			$this->own_licence        = $_POST['owner_lc'];
			$this->owner_email        = $_POST['email_address'];
			$this->own_address        = $_POST['mobile_no'];
			$this->own_address        = $_POST['address'];
			$this->own_passwd         = md5($_POST['password']);
			$this->own_date           = $_POST['created'];
			$this->own_ipaddress      = $_SERVER['REMOTE_ADDR'];
			$this->l_type             = "Consortuim";
			$this->db->insert('owner_table', $this);
     		return $id=$this->db->insert_id();
					
			}	
			
			
		
		function add_payment($id)
		{
		
	$data=array(
	'transaction_id'=>$_POST['transation'],
    'payment_amount'=>$_POST['pay'],
	'payment_date'=>$_POST['created'],
	'payment_ip'=>$_SERVER['REMOTE_ADDR'],
	'user_id'=>$id,
    );
	$this->db->insert('payments', $data);	
	//$this->transaction_id            = $_POST['transation'];
	//$this->payment_amount            = $_POST['pay'];
	//$this->payment_date              = $_POST['created'];
	//$this->payment_ip                = $_SERVER['REMOTE_ADDR'];
	//$this->user_id                   = $id;
	
	}
		
		
		
function add_licence($id,$lid,$email)
		{
	$data=array(
	'own_id'=>$id,
    'licence_no'=>$lid,
	'oemail'=>$email,
	    );
	$this->db->insert('owner_licence', $data);	
	//$this->transaction_id            = $_POST['transation'];
	//$this->payment_amount            = $_POST['pay'];
	//$this->payment_date              = $_POST['created'];
	//$this->payment_ip                = $_SERVER['REMOTE_ADDR'];
	//$this->user_id                   = $id;
	
	}
		
		
		
		
		
		
		

	//-----------end of parameter-------------//
	//------------------Add Foods-------------------//

function fetch_medecine() 
{
		$query = $this->db->get('medicine');
		return $query->result();
}
	



function getfoods() 
		{
		$query = $this->db->get('foods');
		return $query->result();
		}
	function addfoods() 
	{
		$this->food_name   = $_POST['food_name']; 
		$this->food_type = $_POST['food_type'];
		$this->created = $_POST['created'];
		$this->db->insert('foods', $this);
	}
	
   
}

