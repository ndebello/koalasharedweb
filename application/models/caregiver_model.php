<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class caregiver_model extends CI_Model {
    function __construct() {
        parent::__construct();
    }
	// get patient all
    function getPatients() {		
		$query = $this->db->get('patients');
		return $query->result();
	}
	
	function getcaregivers_new($pid,$cid) {		
		//$query = $this->db->get('patients');
	  $query = $this->db->query("select * from caregiver where pat_id='$pid' and id!='$cid'");
       return $query->result();
		//return $query->num_rows();
		
	}
	function getcaregivers_new_search($pid,$cid,$srch) {		
		//$query = $this->db->get('patients');
	  $query = $this->db->query("select * from caregiver where pat_id='$pid' and id!='$cid' and CONCAT( name,  ' ',surname ) like '%$srch%'");
       return $query->result();
		//return $query->num_rows(); CONCAT( name,  ' ',surname ) like '%$srch%'
		
	}
	
	
	
	
	
	function get_caregiver_list($cid,$pid)
	{
	   $query = $this->db->query("select * from list_caregiver where pat_id='$pid' and careid='$cid'");
       return $query->result();
	}
	
	
	function get_paremeters($pid)
	{
$query = $this->db->query("select * from parameter_name where (pat_id='$pid' or pat_id='0') and is_delete!='1'");
       return $query->result();
		
	}
	
	
	function num_notification($eid)
	{
	   $query = $this->db->query("select * from notifications  where note_id='$eid'");
       return $query->result();
		
	}
	
	function get_carenotification($cid)
	{
$query = $this->db->query("select * from cons_request_messages as cons,caregiver as cs where (recipient='$cid' and cons.read='0' or cons.read='1') and (cons.recipient=cs.id) group by cons.recipient order by cons.id desc limit 5");
		
       return $query->result();
	}
	
	function get_carenotificationupdate($cid)
	{
       $query = $this->db->query("update cons_request_messages set cons_request_messages.read='1' where cons_request_messages.recipient='$cid'");
		 
      
	}
	
	function get_carenoti_prof_update($cid)
	{
		$query = $this->db->query("update professional_networks set cread='1' where sid='$cid'");
		
	}
	
function get_carenoti_msg_update($cid)
{
$query =$this->db->query("update chat set chat.recd='1' where chat.to='$cid'");
}
	



function get_carenoti_caremsgupdate($cid)
{
$query =$this->db->query("update chat set chat.recd='1' where chat.to='$cid'");
}	

function get_carenoti_care_all_popup($cid)
{
$query =$this->db->query("update popup_notifications set popup_notifications.status='1' where popup_notifications.n_rid='$cid'");
}



function get_carenoti_caremsgcout($cid)	
{
$query =$this->db->query("select *,prf.id as pfid from chat as cht,caregiver as prf where cht.from=prf.id and (cht.recd='0') and  cht.to='$cid' ");
return $query->result();	
}
	
	
	
	
	
	
	function get_carenoti_prof($cid)
	{
$query = $this->db->query("select *,pnf.id as pnid from professional_networks pnf, professionals as prf where pnf.sid='$cid' and (pnf.cread='0' or pnf.cread='1') and prf.id=pnf.rid group by prf.id order by pnf.id desc limit 5");
return $query->result();
	}
function get_carenoti_msg($cid)
{

$query =$this->db->query("select c.id as cid,c.message,c.recd,c.from,c.to,prf.id as pfid,prf.name,prf.photo from professionals as prf inner join (SELECT i1.* FROM chat AS i1 LEFT JOIN chat AS i2 ON (i1.from = i2.from AND i1.id < i2.id) WHERE i2.from IS NULL and i1.to='$cid') c on prf.id=c.from ");
return $query->result();
	
}

function get_carenoti_caremsg($cid)
{
//$query =$this->db->query("select *,prf.id as pfid from chat as cht,caregiver as prf where cht.from=prf.id and (cht.recd='0' or cht.recd='1') and  cht.to='$cid' group by prf.id order by cht.id desc");

$query =$this->db->query("select c.id as cid,c.message,c.recd,c.from,c.to,prf.id as pfid,prf.name,prf.photo from caregiver as prf inner join (SELECT i1.* FROM chat AS i1 LEFT JOIN chat AS i2 ON (i1.from = i2.from AND i1.id < i2.id) WHERE i2.from IS NULL and i1.to='$cid') c on prf.id=c.from ");



return $query->result();
	
}	







	
function get_carenoti_msg_count($cid)
{
$query =$this->db->query("select *,prf.id as pfid from chat as cht,professionals as prf where cht.from=prf.id and cht.recd='0' and cht.to='$cid' ");
return $query->result();
	
}

	function get_paremeters_list($pid,$praname,$date1,$date2)
	{
		
		$query = $this->db->query("select * from parameters where patient_id='$pid' and para_id='$praname' and (created BETWEEN '$date1' AND '$date2')");
        return $query->result();
	}
	
	
	
	
	function getprofetionals($pid,$cid)
	{
		
		// $query = $this->db->query("select *,prf.id as nprfid from professional_networks as prnt,professionals as prf,professional_networks_prf as mprf  where (prnt.patid='$pid') and prf.id=prnt.rid and (mprf.sid='$id' or mprf.rid='$id') group by prnt.rid");
		
		$query= $this->db->query("select *,prf.id as pfid from professional_networks as npfs,professionals as prf where (npfs.sid='$cid' or npfs.patid='$pid') and npfs.rid=prf.id group by prf.id");
		return $query->result();
	}
	function get_notify($cid,$pid)
	{
 $query = $this->db->query("select * from set_notifications where car_id='$cid' and pat_id='$pid' order by note_id desc");
       return $query->result();
	}
	function editnotification($nid)
	{
$query = $this->db->query("select * from set_notifications where note_id='$nid' order by note_id desc");
return $query->result();
	}
	
	
	function getprofetionalssearch($pid,$cid,$srch)
	{
//$query= $this->db->query("select * from professional_networks where ((sid='$cid' or patid='$pid') and netstatus=1) and name like '%$srch%' ");
//return $query->result();

$query= $this->db->query("select *,prf.id as pfid from professional_networks as npfs,professionals as prf where (npfs.sid='$cid' or npfs.patid='$pid') and npfs.rid=prf.id and CONCAT( prf.name,  ' ', prf.surname ) like '%$srch%' group by prf.id");
return $query->result(); //CONCAT( prf.name,  ' ', prf.surname ) like '%$srch%'
		
		
	}
	
	
	function update_event1($eid)
	{
	  // $query = $this->db->query("update notifications set status='1' where note_id='$eid'");
      //$query->result();
	   
	   $data=array('status'=>1,);
	   
	   
	   return $this->db->update('notifications', $data, array('note_id' => $eid));
	   
	   
	   
	   
	}
	
	
	
	
	
/*  Consultaion Requests  */	
function get_con($cid)	
{
		$query = $this->db->query("SELECT * FROM cons_request_messages where sender='$cid' and subject!=''");			
		return $query->result();
		
}
/*  get The List of Events    */ 
function get_event_list($pid,$date)
{
	$query = $this->db->query("select * from notifications as ns,event_type as evs where  ns.pat_id='$pid' and ns.note_date='$date' and evs.evn_id=ns.event_type_id order by note_id desc");			
	return $query->result();
}

/* list of events */
function eventlist()
{
	$query = $this->db->query("SELECT * FROM event_type ");			
	return $query->result();
		
}

/* list of Pamerets */
function parameteslist()
{
	$query = $this->db->query("SELECT * FROM parameter_name ");			
	return $query->result();
		
}




/*  edit notification  */
function edit_notification($id)
{
	$query = $this->db->query("SELECT * FROM notifications where note_id='$id'");			
	return $query->result();
}




/*   get the event name */


function get_event_data($eid)
{
	$query = $this->db->query("select * from event_type where  evn_id='$eid'");			
	return $query->result();
}


/* get patient details   */	
function get_patient($pid)	
	{
		$query = $this->db->query("SELECT * FROM patients where id='$pid'");			
		return $query->result();
	}
	
	
 /*=====================================================////////  get owner details    /////////================================================ */
function insert_set_notification($patient_id,$caregiver_id,$parameter,$medicine,$diet,$activity,$cdate,$withp)
{

    $data=array(
	'pat_id'=>$patient_id,
	'car_id'=>$caregiver_id,
	'parameter_status'=>$parameter,
	'medical_status'=>$medicine,
	'diets_status'=>$diet,
	'activity_status'=>$activity,
	'cdate'=>$cdate,
	'withp'=>$withp,
	'ip'=>$_SERVER['REMOTE_ADDR'],
     );
	 return $this->db->insert('set_notifications', $data);
	 $id=$this->db->insert_id();
}

function update_set_notification($patient_id,$caregiver_id,$parameter,$medicine,$diet,$activity,$cdate,$nid,$withp)
{ 

	$data=array('pat_id'=>$patient_id,'car_id'=>$caregiver_id,'parameter_status'=>$parameter,'medical_status'=>$medicine,'diets_status'=>$diet,
	'activity_status'=>$activity, 'withp'=>$withp, 'cdate'=>$cdate,'ip'=>$_SERVER['REMOTE_ADDR'],);
	
	return $this->db->update('set_notifications', $data, array('note_id' => $nid));
}

function get_caregiver($email)
{
	     //$this->db->select();
        //$this->db->where('owner_email', $email); 
       //$this->db->where('own_id', $oid); 
      //$query = $this->db->get('owner_table');
     //return $query->result();
	//return $query->num_rows();
		$query = $this->db->query("SELECT * FROM caregiver where email='$email'");			
		return $query->result();
		
}
function get_caregiver1($email)
{
	     //$this->db->select();
        //$this->db->where('owner_email', $email); 
       //$this->db->where('own_id', $oid); 
      //$query = $this->db->get('owner_table');
     //return $query->result();
	//return $query->num_rows();
		$query = $this->db->query("SELECT * FROM caregiver where email='$email'");			
		//return $query->result();
		return $row=$query->row_array();
		
}
function get_medicine($id)
{
$query = $this->db->query("SELECT * FROM medicine where med_id='$id'");			
//return $row=$query->row_array();
return $query->result();		
}

function get_activity($id)
{
$query = $this->db->query("SELECT * FROM activities where act_id='$id'");			

return $query->result();		
}

function get_activity_name($name,$pid)
{
//$query = $this->db->query("SELECT * FROM activities where act_name='$name' and pat_id='$pid'");			
$query = $this->db->query("SELECT * FROM activities where act_name='$name' and pat_id='$pid' and is_delete = 0");			
return $query->result();		
}



function get_diets($id)
{
	// as dit,foodlike as flike where (dit.id='$id' or flike.food
$query = $this->db->query("SELECT * FROM diets where id='$id'");			
return $query->result();		

}




function insert_medicine($patient_id,$caregiver_id,$medname,$med_type,$med_note,$med_qua,$pre_date,$exp_date,$med_price,$meal_type,$med_dose)
{
	$date=date("Y-m-d");
	$data=array(
	'pat_id'=>$patient_id,
	'care_id'=>$caregiver_id,
	'med_name'=>$medname,
	'medformat'=>$med_type,
	'med_note'=>$med_note,
	'med_quantity'=>$med_qua,
	'med_purchase_date'=>$pre_date,
	'med_expirartion_date'=>$exp_date,
	'med_price'=>$med_price,
	'med_meal_time'=>$meal_type,
	'med_dose'=>$med_dose,
	'created'=>$date,
     );
	 
	 //print_r($data); exit;
	return $this->db->insert('medicine', $data);
	
}

function update_medicine($patient_id,$caregiver_id,$medname,$med_type,$med_note,$med_qua,$pre,$exp,$med_price,$meal_type,$med_dose,$mid)
{
	//'care_id'=>$caregiver_id,
	$date=date("Y-m-d");
	$data=array(
	'pat_id'=>$patient_id,
	'upd_carid'=>$caregiver_id,
	'med_name'=>$medname,
	'medformat'=>$med_type,
	'med_note'=>$med_note,
	'med_quantity'=>$med_qua,
	'med_purchase_date'=>$pre,
	'med_expirartion_date'=>$exp,
	'med_price'=>$med_price,
	'med_meal_time'=>$meal_type,
	'med_dose'=>$med_dose,
	'updated'=>$date,
     );
	return $this->db->update('medicine', $data, array('med_id' => $mid));
}

function get_medlist($name,$pid)
{
/*
       $this->db->select();
       $this->db->where('med_name', $name); 
       $this->db->where('pat_id', $pid); 
       $query = $this->db->get('medicine');
       return $query->num_rows();
*/
    	// 04/01/2016
		// Added condition on "is_delete"
       $this->db->select();
       $this->db->where('med_name', $name); 
       $this->db->where('pat_id', $pid); 
	   $this->db->where('is_delete', 0); 
       $query = $this->db->get('medicine');
       return $query->num_rows();
}

function update_activity($patient_id,$caregiver_id,$actdetails,$acttype,$actname,$dateofweek,$file,$id)
{
	$date=date("Y-m-d");
	$data=array(
	'pat_id'=>$patient_id,
	'care_id'=>$caregiver_id,
	'act_type'=>$acttype,
	'act_details'=>$actdetails,
	'act_name'=>$actname,
	'act_attached'=>$file,
	'act_dayof_week'=>$dateofweek,
	'upated_date'=>$date,
	'e_care_id'=>$caregiver_id,
	);
	return $this->db->update('activities', $data, array('act_id' => $id));
	
	
}


function insert_activity($patient_id,$caregiver_id,$actdetails,$acttype,$actname,$dateofweek,$file)
{
	$date=date("Y-m-d");
	$data=array(
	'pat_id'=>$patient_id,
	'care_id'=>$caregiver_id,
	'act_type'=>$acttype,
	'act_details'=>$actdetails,
	'act_name'=>$actname,
	'act_attached'=>$file,
	'act_dayof_week'=>$dateofweek,
	'act_date'=>$date,
	);
	return $this->db->insert('activities', $data);
	
}


function insert_diet($patient_id,$caregiver_id,$foodname,$foodtype,$fooddel,$dateofweek,$file,$foodlike)
{
	
	
	$date=date("Y-m-d");
	$data=array(
	'pat_id'=>$patient_id,
	'care_id'=>$caregiver_id,
	'food_name'=>$foodname,
	'food_type'=>$foodtype,
	'food_desc'=>$fooddel,
	'food_image'=>$file,
	'diet_day'=>$dateofweek,
	'created_date'=>$date,
	'food_like'=>$foodlike,
	);
	$this->db->insert('diets', $data);
	return $id=$this->db->insert_id();
	
	
	
}

function insert_notification($patient_id,$caregiver_id,$acttype,$actdetails,$fr,$to,$dayweek,$frequency,$place,$time)
{
	
	
	
	$date=date("Y-m-d");
	$data=array(
	'pat_id'=>$patient_id,
	'care_id'=>$caregiver_id,
	'event_type_id'=>$acttype,
	'sub_event_id'=>$actdetails,
	'note_time'=>$time,
	'note_date'=>date("Y-m-d"),
	'note_time'=>$time,
	'note_date_from'=>$fr,
	'note_date_to'=>$to,
	'note_place' =>$place,
	'note_dat_week' =>$dayweek,
	'note_frequency' =>$frequency,
	'note_ipaddress'=>$_SERVER['REMOTE_ADDR'],
	'status'=>0,
	'read'=>0,
	);
	 $this->db->insert('notifications', $data);
	return $id=$this->db->insert_id();
	
}


function insert_care_notification($patient_id,$caregiver_id,$acttype,$actdetails,$fr,$to,$dayweek,$frequency,$place,$time)
{
	$date=date("Y-m-d");
	$data=array(
	'pat_id'=>$patient_id,
	'care_id'=>$caregiver_id,
	'event_type_id'=>$acttype,
	'sub_event_id'=>$actdetails,
	'note_time'=>$time,
	'note_date'=>date("Y-m-d"),
	'note_time'=>$time,
	'note_date_from'=>$fr,
	'note_date_to'=>$to,
	'note_place' =>$place,
	'note_dat_week' =>$dayweek,
	'note_frequency' =>$frequency,
	'note_ipaddress'=>$_SERVER['REMOTE_ADDR'],
	'status'=>0,
	'read'=>0,
	);
	 $this->db->insert('set_notifications', $data);
	return $id=$this->db->insert_id();
	
}






function insert_event($patient_id,$caregiver_id,$acttype,$actdetails,$time,$date)
{
	
	//$date=date("Y-m-d");
	$data=array(
	'pat_id'=>$patient_id,
	'care_id'=>$caregiver_id,
	'event_type_id'=>$acttype,
	'sub_event_id'=>$actdetails,
	'note_time'=>$time,
	'note_date'=>$date,
	'note_ipaddress'=>$_SERVER['REMOTE_ADDR'],
	'status'=>0,
	'read'=>0,
	);
	 $this->db->insert('notifications', $data);
	return $id=$this->db->insert_id();
}


function update_diet($patient_id,$caregiver_id,$foodname,$foodtype,$fooddel,$dateofweek,$file,$foodlike,$id)
{
	$date=date("Y-m-d");
	$data=array(
	'pat_id'=>$patient_id,
	'upd_careid'=>$caregiver_id,
	'food_name'=>$foodname,
	'food_type'=>$foodtype,
	'food_desc'=>$fooddel,
	'food_image'=>$file,
	'diet_day'=>$dateofweek,
	'updated_date'=>$date,
	'food_like'=>$foodlike,
	);
	
	return $this->db->update('diets', $data, array('id' => $id));
	
}

function update_caregiver($caregiver_id,$cname,$sname,$age,$gender,$city,$state,$phone,$qua,$file,$address)
{
	$date=date("Y-m-d");
	$data=array(
	'name'=>$cname,
	'surname'=>$sname,
	'age'=>$age,
	'gender'=>$gender,
	'city'=>$city,
	'state'=>$state,
	'phone'=>$phone,
	'qualification'=>$qua,
	'photo'=>$file,
	'address'=>$addres,
	);
	
	return $this->db->update('caregiver', $data, array('id' => $caregiver_id));
	
}


function get_event($id)
{
	$query = $this->db->get_where('sub_even_details', array('sub_id' => $id));
	return $query->result();
	
}

function get_moreevent($id)
{
	$query = $this->db->get_where('sub_event_type', array('evn_id' => $id));
	return $query->result();
	
}

function get_moreparameter($id)
{
	$query = $this->db->get_where('parameter_columns', array('para_id' => $id));
	return $query->result();
}
function parametesinsert($caregiver_id,$patient_id,$praid,$opt1,$opt2,$opt3,$opt4,$opt5)
{
	
$data=array(
	'option1'=>$opt1,
	'option2'=>$opt2,
	'option3'=>$opt3,
	'option4'=>$opt4,
	'option5'=>$opt5,
	'care_id'=>$caregiver_id,
	'patient_id'=>$patient_id,
	'para_id'=>$praid,
	'created'=>date("Y-m-d"),
	);
	
$this->db->insert('parameters', $data);	
	
	
	

}







function get_mormedicinies($id)
{
	$query = $this->db->get_where('medicine', array('med_id' => $id));
	return $query->result();
	
}


function addparameters() 
		{
			$this->maximum_pressure   = $_POST['maximum_pressure']; 
			$this->minimum_pressure = $_POST['minimum_pressure'];
			$this->blood_glucose = $_POST['blood_glucose'];
			$this->heart_rate = $_POST['heart_rate'];
			$this->weight = $_POST['weight'];
			$this->patient_id = $_POST['patient_id'];
			$this->created = $_POST['created'];
			$this->event_id=$_POST['event_id'];
			$this->care_id=$_POST['caregiver_id'];
			
			return $this->db->insert('parameters', $this);
				
			
		}
		
		

function updateparameters($nid) 
		{
			$this->maximum_pressure   = $_POST['maximum_pressure']; 
			$this->minimum_pressure = $_POST['minimum_pressure'];
			$this->blood_glucose = $_POST['blood_glucose'];
			$this->heart_rate = $_POST['heart_rate'];
			$this->weight = $_POST['weight'];
			$this->patient_id = $_POST['patient_id'];
			$this->updated = date("Y-m-d");
			$this->event_id=$_POST['event_id'];
			$this->care_id=$_POST['caregiver_id'];
			
			//return $this->db->insert('parameters', $this);
			return $this->db->update('parameters', $this, array('event_id' => $nid));
				
			
		}





		

function updatenotif($nid,$cid)
{
	$date=date("Y-m-d");
	$data=array(
	'status'=>1,
	'edit_cid'=>$cid,
	
		);
	
	return $this->db->update('notifications', $data, array('note_id' => $nid));
}

function updatenotifm($nid)
{
	$date=date("Y-m-d");
	$data=array(
	'read1'=>1,
	);
	
	return $this->db->update('care_notification', $data, array('note_id' => $nid));
}





function getowner($email)
	{
	
       $this->db->select();
       $this->db->where('owner_email', $email); 
       //$this->db->where('own_id', $oid); 
       $query = $this->db->get('owner_table');
      // return $query->result();
		return $query->num_rows();
	}
	
	

function getprofetion($email)
	{
	
       $this->db->select();
       $this->db->where('email', $email); 
       //$this->db->where('own_id', $oid); 
       $query = $this->db->get('professionals');
       //return $query->result();
	   return $query->num_rows();
		
	}

function getkoalaower($email)
   {
	   $this->db->select();
       $this->db->where('owner_email', $email); 
       //$this->db->where('own_id', $oid); 
       $query = $this->db->get('koala_owner');
       //return $query->result();
	   return $query->num_rows();;
		
	   
	}




function get_owner($id)
{
	$query = $this->db->get_where('owner_table', array('own_id' => $id), 1);
		if( $query->num_rows() > 0 )
		{
		//return $query->row_array();
	    return $query->result();
		//return false;
		}
}


	// get patient by id
	function get_patient_by_id( $id=null ) {
	
		$query = $this->db->get_where('patients', array('caregiver_id' => $id), 1);
		if( $query->num_rows() > 0 ) 
		//return $query->row_array();
	    return $query->result();
		return false;
	}
	
    // search patient all
    function searchPatients() {
	    $data=explode(' ', $_POST['keyword']);
		$searchkey = 'where ';
		foreach($data as $temp){
			if($temp !=''){
				$searchkey .= '`name` like "%'.mysql_real_escape_string($temp).'%"  OR ';
				$searchkey .= '`surname` like "%'.mysql_real_escape_string($temp).'%"  OR ';
				$searchkey .= '`zipcode` like "%'.mysql_real_escape_string($temp).'%"  OR ';	
			}
		} 
		if($searchkey =='where ')
		{
			$searchkey = 'where  1=1';
		}else{
			$searchkey  = substr($searchkey,0,-3);
		}

		$query = $this->db->query("SELECT * FROM patients $searchkey");			
				
		return $query->result();
	}
	
    //------------Periods-------------------//
	function getperiods() 
			{
			$this->db->select('pe.*, p.name'); //or whatever you're selecting
			$this->db->join('patients p', 'pe.patient_id = p.id');
			//$this->db->where('pp.entry_id', $entry_id);
			$query = $this->db->get('periods pe');
			
			return $query->result();
			}
			
	function addperiods() 
		{
			$this->patient_id   = $_POST['patient_id']; 
			$this->sleep_time = $_POST['sleep_time'];
			$this->wake_time = $_POST['wake_time'];
			$this->created = $_POST['created'];
			$this->db->insert('periods', $this);
		}
	//--------------End of Periods------------//
	//----- Diets--------//
	
		function getdiets() 
		{
			$this->db->select('d.*, p.name'); //or whatever you're selecting
			$this->db->join('patients p', 'd.patient_id = p.id');
			//$this->db->where('pp.entry_id', $entry_id);
			$query = $this->db->get('diets d');
			return $query->result();
		}

	
		function adddiets() 
		{
			$this->patient_id   = $_POST['patient_id']; 
			$this->food_type = $_POST['food_type'];
			$this->food_name = $_POST['food_name'];
			$this->diet_day = $_POST['diet_day'];
			$this->db->insert('diets', $this);
		}
		
		function dietsDetail($id) 
		{
			$this->db->where('id', $id);  
			$query = $this->db->get('diets');
			return $query->result();

		}
		
		function updatediets($id) {
				$this->food_type   = $_POST['food_type']; 
				$this->food_name = $_POST['food_name'];
				$this->diet_day = $_POST['diet_day'];
				$this->db->update('diets', $this, array('id' => $_POST['id']));
			}
		function deletediets($id) 
		{
				$this->db->delete('diets', array('id' => $id));

		} 
	//----end od diets--//
	//------------Parameters---------//
		function getparameters() 
		{
			$this->db->select('pp.*, p.name'); //or whatever you're selecting
			$this->db->join('patients p', 'pp.patient_id = p.id');
			//$this->db->where('pp.entry_id', $entry_id);
			$query = $this->db->get('parameters pp');
			return $query->result();
		}
				
		

		
function update_event($nid,$cid,$status)
{
$date=date("Y-m-d");
	$data=array(
	'status'=>$status,
	'edit_cid'=>$cid,
	
		);
	
	return $this->db->update('notifications', $data, array('note_id' => $nid));
		
}



		function add_owner_rsa() 
		{
			
		//owner_name,owner_type,email_address,mobile_no,address,password,transation,pay,created
		    $this->l_type       = $_POST['owner_type']; 
			$this->owner_email      = $_POST['email_address'];
			$this->trno            = $_POST['transation'];
			$this->payamount          = $_POST['pay'];
			$this->owner_type      ="business";
			//$this->pay_date         = $_POST['created'];
			////$this->own_date           = $_POST['created'];
			$this->own_ipaddress      = $_SERVER['REMOTE_ADDR'];
			//$this->l_type             = $_POST['owner_type'];
			$this->db->insert('owner_table', $this);
     		return $id=$this->db->insert_id();
					
			
		}
		
		
		
		
		
		
			
		function add_owner_cons() 
		{
						//owner_name,owner_type,email_address,mobile_no,address,password,transation,pay,created
						
			$this->own_name           = $_POST['cname']; 
			$this->owner_type         = "business";
			$this->own_licence        = $_POST['owner_lc'];
			$this->owner_email        = $_POST['email_address'];
			$this->own_address        = $_POST['mobile_no'];
			$this->own_address        = $_POST['address'];
			$this->own_passwd         = md5($_POST['password']);
			$this->own_date           = $_POST['created'];
			$this->own_ipaddress      = $_SERVER['REMOTE_ADDR'];
			$this->l_type             = "Consortuim";
			$this->db->insert('owner_table', $this);
     		return $id=$this->db->insert_id();
					
			}	
			
			
		
		function add_payment($id)
		{
		
	$data=array(
	'transaction_id'=>$_POST['transation'],
    'payment_amount'=>$_POST['pay'],
	'payment_date'=>$_POST['created'],
	'payment_ip'=>$_SERVER['REMOTE_ADDR'],
	'user_id'=>$id,
    );
	$this->db->insert('payments', $data);	
	//$this->transaction_id            = $_POST['transation'];
	//$this->payment_amount            = $_POST['pay'];
	//$this->payment_date              = $_POST['created'];
	//$this->payment_ip                = $_SERVER['REMOTE_ADDR'];
	//$this->user_id                   = $id;
	
	}
		
		
		
function add_licence($id,$lid,$email)
		{
	$data=array(
	'own_id'=>$id,
    'licence_no'=>$lid,
	'oemail'=>$email,
	    );
	$this->db->insert('owner_licence', $data);	
	//$this->transaction_id            = $_POST['transation'];
	//$this->payment_amount            = $_POST['pay'];
	//$this->payment_date              = $_POST['created'];
	//$this->payment_ip                = $_SERVER['REMOTE_ADDR'];
	//$this->user_id                   = $id;
	
	}
		
		
		
		
		
		
		

	//-----------end of parameter-------------//
	//------------------Add Foods-------------------//

function fetch_medecine($pid,$per_pg,$offset) 
{
		//$query = $this->db->get('medicine');
		//Edited line ==>where pat_id='$pid' or pat_id=0
		
		//$query = $this->db->query("SELECT * FROM medicine ");	
     	//return $query->result();
	  
	   $this->db->order_by('med_id','desc');
	   $this->db->select();
       //$this->db->where('med_name', $name); 
       $this->db->where('pat_id', $pid); 
       $query = $this->db->get('medicine',$per_pg,$offset);
	   return $query->result();   
       /*$this->db->order_by('med_id','desc'); 
$this->db->select();
$query = $this->db->get('medicine',$per_pg,$offset);*/
		
}

function fetch_parameter($pid) 
{
		//$query = $this->db->get('medicine');
		$query = $this->db->query("SELECT * FROM parameters where patient_id='$pid'");	
     	return $query->result();
}

function fetch_activity($pid,$per_pg,$offset) 
{
	
		//$query = $this->db->get('activities');
		//return $query->result();where pat_id='$pid'
		///$query = $this->db->query("SELECT * FROM activities ");	
     	//return $query->result();
		
		$this->db->order_by('act_id','desc'); 
		$this->db->select();
	    $query = $this->db->get('activities',$per_pg,$offset);
		return $query->result();
		
		
		
		
		
		
}
function fetch_diets($pid,$per_pg,$offset) 
{
	
		//$query = $this->db->get('activities');
		//return $query->result();
		//edit code  ==> where pat_id='$pid'
		//$query = $this->db->query("SELECT * FROM  diets ");	
     	//return $query->result();
		
		$this->db->order_by('id','desc'); 
			$this->db->select();
	        $query = $this->db->get('diets',$per_pg,$offset);
		    return $query->result();
}	
	
		function list_dies_likes($pid)
		{
		//$query = $this->db->query("SELECT * FROM  foodlike where patid='$pid' and foodlike='Intolerance'");	
		$query = $this->db->query("SELECT * FROM  foodlike as flike, diets as dit where flike.patid='$pid' and  flike.foodlike='Intolerance' and dit.id=flike.foodid and dit.is_delete!='1'");
		return $query->result();
		
		}
		
		function list_dies_flikes($pid)
		{
			
		$query = $this->db->query("SELECT * FROM  foodlike as flike, diets as dit where flike.patid='$pid' and flike.foodlike='Favourites' and dit.id=flike.foodid and dit.is_delete!='1'" );	
			return $query->result();
		}







function getfoods() 
		{
		$query = $this->db->get('foods');
		return $query->result();
		}
	function addfoods() 
	{
		$this->food_name   = $_POST['food_name']; 
		$this->food_type = $_POST['food_type'];
		$this->created = $_POST['created'];
		$this->db->insert('foods', $this);
	}
	
   
}

