<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Koala | Embrace your needs</title>
    <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>css/bootstrap.css" rel="stylesheet">
	 <!--<link href="css/bootstrap-theme.css" rel="stylesheet">-->
	 <link href="<?php echo base_url(); ?>style.css" rel="stylesheet">
	 <link href="<?php echo base_url(); ?>css/res.css" rel="stylesheet">
	 <link href="<?php echo base_url(); ?>css/jasny-bootstrap.min.css" rel="stylesheet">
	 <link href="<?php echo base_url(); ?>css/bootstrap-select.css" rel="stylesheet">
	 <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,300,600,700' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  
  <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=places"></script>
    <script>
// This example displays an address form, using the autocomplete feature
// of the Google Places API to help users fill in the information.

var placeSearch, autocomplete;
var componentForm = {
   postal_code: 'short_name'
};

function initialize() {
  // Create the autocomplete object, restricting the search
  // to geographical location types.
//  alert('autocomplete');

autocomplete = new google.maps.places.Autocomplete(
  
  
  /** @type {HTMLInputElement} */(document.getElementById('autocomplete')),
      { types: ['geocode'] });
  // When the user selects an address from the dropdown,
  // populate the address fields in the form.
  google.maps.event.addListener(autocomplete, 'place_changed', function() {
    fillInAddress();
  });
}

function initialize2()
{ 
   
	//alert('a');
	var address=document.getElementById("autocomplete").value;
	//alert(address);
	var locationDetails=address;
            var  value=locationDetails.split(",");
            
            count=value.length;
            
             country=value[count-1];
             state=value[count-2];
             city=value[count-3];
		
		//alert(city);
		 document.getElementById("city").value=city;
	document.getElementById("state").value=country;
	
	
}


        function initialize1(a) {
        var address = a;
            geocoder = new google.maps.Geocoder();
            geocoder.geocode({
            'address': address
            }, function(results, status) {      
                var lat=document.getElementById("lat").value=results[0].geometry.location.lat();    
                var lng=document.getElementById("lng").value=results[0].geometry.location.lng();        
            });
        }
        google.maps.event.addDomListener(window, 'load', initialize1);
   








// [START region_fillform]
function fillInAddress() {
  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();

  for (var component in componentForm) {
    document.getElementById(component).value = '';
    document.getElementById(component).disabled = false;
  }

  // Get each component of the address from the place details
  // and fill the corresponding field on the form.
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
      document.getElementById(addressType).value = val;
    }
  }
}
// [END region_fillform]

// [START region_geolocation]
// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = new google.maps.LatLng(
          position.coords.latitude, position.coords.longitude);
	  
	  
	// alert(geolocation);
	  
	  
      autocomplete.setBounds(new google.maps.LatLngBounds(geolocation,
          geolocation));
	  //alert(geolocation);
    });
  }
}
// [END region_geolocation]

    </script>
<script>
</script>
  <?php
$segs = $this->uri->segment_array();

 $id=$segs[3];
 $os="select * from owner_table where own_id='$id'";
 $oe=mysql_query($os) or die(mysql_error());
 $or=mysql_fetch_array($oe);
 $id1=$or['own_id'];
 //$licence=$or['own_licence'];
 $otype=$or['l_type'];
  $lice_type=$or['own_licence_type'];
/*

foreach ($segs as $segment)
{echo $segment;
echo '<br />';
}

*/
$email=$this->session->userdata('owner_name');
if(empty($email))
{
?>
<script> window.location="<?php  echo base_url();?>";  </script>
<?php
 }
?>
  
  
  <script src="<?php echo base_url('js/jquery-1.11.1.min.js');?>"></script>
<script src="<?php echo base_url('js/jquery.validate.min.js');?>"></script>
  
<body onLoad="initialize()">
    <!--___________________________________________________Header____________________________________________________________---> 
	<?php $this->load->view('includes/ownerheader.php'); ?>
		<div class="clearfix"></div>
	</div>
	<script type="text/javascript">
	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
			//$('#blah').attr('src', e.target.result);
	         document.getElementById("uploadphoto").src=e.target.result;
			}
			reader.readAsDataURL(input.files[0]);
		}
	}
</script>
    <script>
function generate_qrcode(){
var name= $('#name').val();
var surname=$('#surname').val();
var age=$('#age').val();
var gender=$('#gender').val();
var autocomplete=$('#autocomplete').val();
if(name=='' || surname==''){
 alert('Please fill the form to generate QR Code');
}else{
$.ajax({   
  type:"post",
url:"<?php echo base_url()?>auth/generate_qr", 
data:'name='+name+'&surname='+surname,
cache:false,
success:function(result){
alert(result);
$('#qrcode').attr('src', '<?php echo base_url() ?>/uploads/qrcode/54ad1fe710ed6.png');
//$('#imgs').attr('src',e.target.result);




$('#qr_code').val(result);
}
})
}
}

function formvalidate()
{
	var int=document.getElementById("licence").value;
	//alert(int);
	var a=document.getElementById("name").value;
	var b=document.getElementById("surname").value;
	var c=document.getElementById("age").value;
	var d=document.getElementById("gender").value;
	var e=document.getElementById("autocomplete").value;
    var f=document.getElementById("postal_code").value;
    var g=document.getElementById("country").value;	
	var city=document.getElementById("city").value;
	if(int=='' || int==null)
	{
		alert("Enter The Licence Number");
		document.getElementById("licence").focus();
		return false;
	}
	
	if(a=='' || a==null)
	{
		alert("Enter The Patient Name");
		document.getElementById("name").focus();
		return false;
	}
	
	if(b=='' || b==null)
	{
		alert("Enter The Surname Name");
		document.getElementById("surname").focus();
		return false;
	}
	if(c=='' || c==null)
	{
		alert("Enter Select The Age");
		document.getElementById("age").focus();
		return false;
	}
	if(d=='' || d==null)
	{
		alert("Enter Select The Gender");
		document.getElementById("gender").focus();
		return false;
	}
	if(e=='' || e==null)
	{
		alert("Please Enter The Address");
		document.getElementById("autocomplete").focus();
		return false;
	}
	if(city=='' || city==null)
	{
		alert("Please Enter The City Name");
		document.getElementById("city").focus();
		return false;
	}
	
	
	
	
	if(f=='' || f==null)
	{
		alert("Please Enter The Zipcode");
		document.getElementById("postal_code").focus();
		return false;
	}
	if(g=='' || g==null)
	{
		alert("Please Select The Country ");
		document.getElementById("country").focus();
		return false;
	}
	
	
}

function redirect()
{
	window.location='<?php echo base_url(); ?>front/after_owner_login/<?php echo $id; ?>';
}

</script>
    
    
    
    
    
    
	<!--___________________________________________________Header____________________________________________________________--->
<div class="clearfix"></div>
<div class="container">
    
<?php


 if($otype=="owner"   or $otype=="Freelancer"  ) { ?>
		<div class="upgrd-lic">
			<button type="button" class="btn btn-success" aria-label="Left Align" > <a href="<?php echo base_url() ?>auth/buy_anether_licence/<?php echo $id; ?>" style="color:#FFF;"> Buy another licence </a> </button>
			<button type="button" class="btn btn-success" aria-label="Left Align" >
<?php if($lice_type!='Extended')  {  ?>
            <a href="<?php echo base_url() ?>auth/buy_extended_licence/<?php echo $id; ?>" style="color:#FFF;"> Upgrade licence </a> 
            <?php  }  else {  ?>
<a href="javascript:void(0)" onClick="alert('Licence Already Extended')" style="color:#FFF;">  Upgrade licence </a>            
             
            <?php } ?>
             </button>
		
		</div>
<?php } ?>        
        
		<div class="clearfix"></div>
		<!--<div class="inner-cont top">
		
			<label>Register Yourself as</label><select class="selectpicker">
											<option>Select</option>
											<option>Owner</option>
											<option>Professional</option>
										</select>
		</div>-->
        		<?php //echo form_open_multipart('auth/owner_register_patient/'.$id, array('name'=>'form-register', 'id'=>'form-register'));?>
                
<form method="post" enctype="multipart/form-data" action="<?php echo base_url() ?>auth/owner_register_patient/<?php echo $id; ?>" onSubmit="return formvalidate()">                
                
                
		<div class="inner-cont btm prof-cont patient-data">
		<div class="nmber"><p><span class="txt">Patient Data</span><!--<span class="ryt"><button type="button" class="btn btn-success back-btn" aria-label="Left Align">
<span class="glyphicon glyphicon-pencil nxt-arrow" aria-hidden="true"></span>	  Edit 
</button></span>--></p> </div>
		<div class="clearfix"></div>
			<div class="form-inn-cont2 prof-reg">
              <p><div class="error" align="center"> <?php if(!empty($flash)) { echo $flash; } ?></div></p>
            
				<div class="top-div">
					<div class="lft">
					<div class="fileinput fileinput-new" data-provides="fileinput">
					  <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
						<img data-src="holder.js/200%x200%" alt="..." src="<?php echo base_url(); ?>img/img-holder2.jpg"  id="uploadphoto" width="200" height="150"/>
					  </div>
						<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
						<div>
						<span class="btn btn-success btn-file"><span class="glyphicon glyphicon-upload upld" aria-hidden="true"></span><span class="fileinput-new">Upload Photo</span><span class="fileinput-exists">Change</span>
                        <input type="file" name="photo" id="logo" style="opacity:0" onchange='readURL(this)' accept="image/png,image/jpeg,image/gif," />
                        
                        
                        </span>
						<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput"><span class="glyphicon glyphicon-remove remove" aria-hidden="true"></span>Remove</a>
						</div>
</div>
					</div>
					<div class="ryt">
						<div class="fieldset span2">
                        <label>Licence<span class="rqrd">*</span></label><div class="clearfix"></div>
                         <input type="text" id="licence" name="licence" value="<?php echo @$_GET['licence']; ?>"  required/>
							<div class="lft">
								<label>Name<span class="rqrd">*</span></label><div class="clearfix"></div>
								<input type="text" id="name" name="name" value="<?php echo @$_GET['name']; ?>" required />
							</div>
							<div class="ryt">
								<label>Surname<span class="rqrd">*</span></label><div class="clearfix"></div>
								<input type="text" id="surname" name="surname" value="<?php echo @$_GET['sname']; ?>" required />
							</div>
                            
                            <input type="hidden" name="lat"  id="lat" />
<input type="hidden" name="lng"  id="lng" />
                            
						</div>
						<div class="fieldset span2">
							<div class="lft select-sm">
								<label>Age<span class="rqrd">*</span></label>
								<select class="selectpicker" name="age" id="age">
                                <option value="">--select--</option>
                                <?php for($i=20;$i<121;$i++){ ?>
                                <?php if(@$this->session->userdata('age')==$i){ ?>
                                <option value="<?php echo $i; ?>" selected><?php echo $i; ?></option>
                                <?php }else{ ?>
                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php } ?>
                                <?php } ?>
								</select>
							</div>
							<div class="ryt select-sm">
								<label>Gender<span class="rqrd">*</span></label>
								<select class="selectpicker" name="gender" id="gender">
									<option value="">Select</option>
                                    <?php if(@$this->session->userdata('gender')=='male'){ ?>
                                    <option value="male" selected>Male</option>
                                    <option value="female">Female</option>
                                    <?php }elseif(@$this->session->userdata('gender')=='female'){ ?>
                                    <option value="male">Male</option>
                                    <option value="female" selected>Female</option>
                                    <?php }else{ ?>
                                    <option value="male">Male</option>
                                    <option value="female" >Female</option>
                                    <?php } ?>
											
								</select>
							</div>
						</div>
						<div class="fieldset">
							<label>Address<span class="rqrd">*</span></label><textarea name="address" id="autocomplete" onFocus="geolocate()" onblur="initialize1(this.value)" onfocusout="initialize2()"  ></textarea>
						</div>
						<div class="fieldset span2">
							<div class="lft">
								<label>City<span class="rqrd">*</span></label><div class="clearfix"></div>
								<input type="text" name="city" id="city" onFocus="initialize2()" required/>
							</div>
							<div class="ryt">
								<label>Zipcode<span class="rqrd">*</span></label><div class="clearfix"></div>
								<input type="text" id="postal_code" name="zipcode" required/>
							</div>
						</div>
						<div class="fieldset span2">
							<div class="lft select-lg">
								<label>Country<span class="rqrd">*</span></label><div class="clearfix"></div>
								  <input type="text" name="state" id="state">   
							</div>
							<!--<div class="ryt">
								<label>e-mail<span class="rqrd">*</span></label><div class="clearfix"></div>
								<input type="text" name="email" />
							</div>-->
						</div>
						<!--<div class="fieldset span2">
							<div class="lft select-lg">
								<label>Licence<span class="rqrd">*</span></label><div class="clearfix"></div>
								<input type="text" name="contact" />
							</div>
						</div>-->
						
					</div>
					<div class="qrcode-div">
					<label>ID Card</label>
						<div class="qrcode-cont"><img src="<?php echo base_url(); ?>img/qr-code.jpg" alt="qr-code" id="qrcode"/></div>		
						<button type="button" class="btn btn-success" onclick="generate_qrcode();">Create</button>
  <input type="hidden" id="qr_code" name="qr_code" value="<?php echo @$this->session->userdata('qr_code'); ?>" required/>
					</div>
				</div>
				<div class="bottom-div">
				<h3 class="nob"><span>Optional Data</span></h3>
				<div class="clearfix"></div>
				<div class="fieldset span3">
							<div class="col-lg-12 col-md-12">
							<div class="col-md-3">
								<label>Contacts</label>
							</div>
							<div class="col-md-3">
								<input type="text" id="phone" name="phone" />
								<div class="clearfix"></div>
								<select class="selectpicker" id="contact1" name="contact1">
									<option value="">select</option>
                                    <option value="Father">Father</option>
                                    <option value="Mother">Mother</option>
                                    <option value="Brother">Brother</option>
                                    <option value="Sister">Sister</option>
                                    <option value="Son"> Son </option>
                                    <option value="Daughter"> Daughter </option>
                                    <option value="Uncle"> Uncle </option>
                                    <option value="Aunt"> Aunt </option>
                                    <option value="Cousin"> Cousin </option>
                               </select>
							</div>
							<div class="col-md-3">
								<input type="text"  id="phone2" name="phone2" />
								<div class="clearfix"></div>
								<select class="selectpicker" id="contact2" name="contact2">
									<option value="">select</option>
                                    <option value="Father">Father</option>
                                    <option value="Mother">Mother</option>
                                    <option value="Brother">Brother</option>
                                    <option value="Sister">Sister</option>
                                    <option value="Son"> Son </option>
                                    <option value="Daughter"> Daughter </option>
                                    <option value="Uncle"> Uncle </option>
                                    <option value="Aunt"> Aunt </option>
                                    <option value="Cousin"> Cousin </option>
								</select>
							</div>
							<div class="col-md-3">
								<input type="text" id="contact3" name="contact3" />
								<div class="clearfix"></div>
								<select class="selectpicker" id="contact3" name="contact3">
									<option value="">select</option>
                                    <option value="Father">Father</option>
                                    <option value="Mother">Mother</option>
                                    <option value="Brother">Brother</option>
                                    <option value="Sister">Sister</option>
                                    <option value="Son"> Son </option>
                                    <option value="Daughter"> Daughter </option>
                                    <option value="Uncle"> Uncle </option>
                                    <option value="Aunt"> Aunt </option>
                                    <option value="Cousin"> Cousin </option>
								</select>
							</div>
							<!--<div class="clearfix"></div>
							<div class="col-md-3"></div>
							<div class="col-md-3">
								<select class="selectpicker">
									<option>Select</option>
									<option>1</option>
									<option>2</option>
								</select>
							</div>
							<div class="col-md-3">
								<select class="selectpicker">
									<option>Select</option>
									<option>1</option>
									<option>2</option>
								</select>
							</div>
							<div class="col-md-3">
								<select class="selectpicker">
									<option>Select</option>
									<option>1</option>
									<option>2</option>
								</select>
							</div>
							-->
							<div class="clearfix"></div>
							<div class="col-md-4 txtarea">
								<label>Pathology</label>
								<textarea id="pathology" name="pathology"></textarea>
							</div>
							<div class="col-md-4 txtarea">
								<label>Allergies</label>
								<textarea id="allergies" name="allergies"></textarea>
							</div>
							<div class="col-md-4 txtarea">
								<label>Intolerances</label>
								<textarea id="intolerances" name="intolerances"></textarea>
							</div>
							</div>
							<div class="clearfix"></div>
							<div class="col-md-12 note">
								<label>Note</label>
								<textarea id="note" name="note"></textarea>
							</div>
							
						</div>
				<!--<div class="fieldset span2">
							<div class="lft">
								<label>Admitted to doctors board of<span class="rqrd">*</span></label><div class="clearfix"></div>
								<input type="text" name="admitted" />
							</div>
							<div class="ryt">
								<label>From<span class="rqrd">*</span></label><div class="clearfix"></div>
								<input type="text" name="from" />
							</div>
						</div>	-->	
				</div>
				<div class="clearfix"></div>
					<div class="col-md-5 pull-left btn no-padding">
							<input type="button" value="Cancel" class="btn btn-success" style="width:100px" onClick="redirect()">
							<input type="submit" value="Save" class="btn btn-success" style="width:100px">
							
							
						</div>				
				
						<div class="clearfix"></div>
			</div>		
		</div>
		</form>
	</div>

<?php $this->load->view("includes/footer.php"); ?>
  </body>
</html>