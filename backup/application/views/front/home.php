<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Koala | Embrace your needs</title>
    <!-- Bootstrap -->
    <link href="<?php echo base_url('css/bootstrap.css');?>" rel="stylesheet">
	 <!--<link href="css/bootstrap-theme.css" rel="stylesheet">-->
	 <link href="<?php echo base_url('style.css');?>" rel="stylesheet">
	 <link href="<?php echo base_url('css/res.css') ?>" rel="stylesheet">
	 <link href="<?php echo base_url('css/bootstrap-select.css') ?>" rel="stylesheet">
	 <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,300,600,700' rel='stylesheet' type='text/css'>
<link href="<?php echo base_url('css/bootstrap_tooltip.min.css');?>" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  <link href="css/bootstrap.css" rel="stylesheet">
	 <!--<link href="css/bootstrap-theme.css" rel="stylesheet">-->
	 <link href="style.css" rel="stylesheet">
	 <link href="css/res.css" rel="stylesheet">
	 <link href="css/bootstrap-select.css" rel="stylesheet">
	 <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,300,600,700' rel='stylesheet' type='text/css'>
  
  
  
  
  
  
 
  
  
  </head>
  <body>
    <!--___________________________________________________Header____________________________________________________________---> 
	<div class="header">
		<div class="container">
			<div class="col-lg-4 col-md-4 pull-right logo-cont"><h1><a href="index.html"><img src="<?php echo base_url('img/logo.png');?>" alt="logo"/></a></div>
		</div>
		<div class="clearfix"></div>
		<div class="banner-cont">
			<div class="container">
				
				<div class="col-md-6 col-lg-6 pull-right top-frm">
					<div class="ban-form">
						<h2>The first support for home care to someone with Alzheimer's</h2>
						<div class="form-cont">
                        
                        <span style="color:red;"> <?php if(!empty($flash)) echo $flash; ?> </span>
                        
							<form action="<?php echo base_url(); ?>front/index" id="form-register" method="post">
								<div class="fieldset">
									<div class="lft">
										<label>Login to your account</label>
									</div>
									<div class="ryt">
										<select name="selectype"  class="selectpicker" required>
											<option>Login as</option>
											<option value="owner">Owner</option>
											<option value="caregiver">Caregiver</option>
											<option value="professional">Professional</option>
										</select>
									</div>
								</div>
								<div class="fieldset">
									<div class="lft">
										<div class="input-group">
										  <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
										  <!--<input type="text" class="form-control" placeholder="Username">-->
                                          
                                          <input id="username" name="username" placeholder="username"  type="email"  value="<?php if(isset($_COOKIE['remember_me'])) echo $_COOKIE['remember_me']; ?>" required class="form-control" />
                                          
                                          
										</div>
									</div>
									<div class="ryt">
										<div class="input-group">
										  <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
										  <!--<input type="password" class="form-control" placeholder="Password">-->
                                          
                                           <input id="password" name="password" placeholder="Password" type="password" required  value="<?php if(isset($_COOKIE['remember_pass'])) echo $_COOKIE['remember_pass']; ?>" class="form-control"/>
                                          
                                          
                                          
										</div>
									</div>
								</div>
								<div class="fieldset omega">
									<div class="lft"><p>Don't have an account yet ? <a href="<?php echo base_url();?>auth/owner_register_step1">Create an account</a><span class="forgot-pass"><a href="<?php echo base_url();?>auth/forgetpassword">Forgot Password ?</a></span></p>   </div>
									<div class="ryt">
									<div class="col-md-7 pull-left rem-me">
										<input type="checkbox" name="remember" id="remember" <?php if(isset($_COOKIE['remember_me'])) { echo 'checked="checked"'; } else { echo ''; } ?> /><label class="rem">Remember me</label>
									</div>
									<div class="col-md-4 pull-right login-btn">
										<button type="submit" class="btn btn-success" name="submit" aria-label="Left Align">
										  Login
										</button>
									</div>
									</div>
								</div>
                                
							</form>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
	<!--___________________________________________________Header____________________________________________________________--->

    
    <!--___________________________________________________Mid-Content______________________________________________________________-->

    <div class="container mid-cont">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-7 content">
			<div class="col-md-6 omega">
			  <h3 class="hdr">How it works</h3>
			 </div>
			 <div class="clearfix"></div>
          <p>Through a few simple settings, the caregiver will be able to generate a routine activity on which to base any aspect of? Assistance to the patient: drug therapy, diet, clinical and cognitive aspects could be handled remotely interfacing with? Domestic assistant..
<br><br>
The data, summarized in reports, will allow a constant interface with health professionals registered on? Special and chosen through the portal? Application for? Build? a service network around the needs of the patient
<br><br>
Through a few simple settings, the caregiver will be able to generate a routine activity on which to base any aspect of? Assistance to the patient: drug therapy, diet, clinical and cognitive aspects could be handled remotely interfacing with? Domestic assistant.</p>
          
        </div>
        <div class="col-md-5 pull-right vid">
          <div class="vid-cont">
			<iframe width="100%" height="315" src="//www.youtube.com/embed/1DaFRA28C9o" frameborder="0" allowfullscreen></iframe>
		  </div>
       </div>
        
      </div>



      
    </div> <!-- /container -->
	<!--___________________________________________________/Mid-Content______________________________________________________________-->
	
	<!--___________________________________________________Mid-Btm______________________________________________________________-->
	<div class="container mid-btm">
		<div class="row">
			<div class="col-md-7">
			<div class="col-md-7 omega">
			  <h3 class="hdr">Request Information</h3>
			 </div>
			 <div class="clearfix"></div>
			 <div class="frm-cont">
				<form action="">
					<label>Name <span class="rqrd">*</span></label><br>
					<input type="text" name="name" required />
					<label>Email <span class="rqrd">*</span></label><br>
					<input type="email" name="email" required />
					<label>Subject <span class="rqrd">*</span></label><br>
					<input type="text" name="subject" required/>
					<label>Message</label>
					<textarea></textarea>
					<div class="filedset-btn"></div>
					<div class="clearfix"></div>
					<div class="col-md-5 pull-left btn no-padding">
						<input type="submit" value="send" class="btn btn-success">
						<input type="reset" value="cancel" class="btn btn-default">
					</div>
				</form>
			 </div>
			</div>
			<div class="col-md-5 pull-right">
			<div class="ryt-content-cont">
			<p>Koala is the solution designed to support the family and the caregiver typically in the management of the patient's disease or other dementias.

<br><br>
Thanks to a network of qualified health professionals on the care process takes on even greater value when played at home. Thanks to a network of qualified health professionals the process.
<br><br>

Request information by filling out the form to the right or through the channels Facebook and Twitter.</p>
				<div class="clearfix"></div>
				<div class="socio">	
					<ul>
						<li><a href="#"><img src="<?php echo base_url('img/fb-icn.png');?>" alt="fb" /></a></li>
						<li><a href="#"><img src="<?php echo base_url('img/soc-icn.png'); ?>" alt="social media" /></a></li>
						<li><a href="#"><img src="<?php echo base_url('img/gp-icn.png'); ?>" alt="gp" /></a></li>
						<li><a href="#"><img src="<?php echo base_url('img/in-icn.png'); ?>" alt="in" /></a></li>
						<li><a href="#"><img src="<?php echo base_url('img/rss-icn.png'); ?>" alt="rss" /></a></li>
					</ul>
				</div>
			</div>
			</div>
		</div>
	</div>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<!--___________________________________________________/Mid-Btm______________________________________________________________-->
<?php //include("../includes/footer.php"); ?>

<?php $this->load->view('includes/footer.php');?>


  </body>
</html>