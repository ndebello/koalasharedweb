<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Koala | Embrace your needs</title>

    <!-- Bootstrap -->
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <!--___________________________________________________Header____________________________________________________________---> 
	<?php $this->load->view('includes/header.php');?>
	
	<!--___________________________________________________Header____________________________________________________________--->
<div class="clearfix"></div>
    <div class="container">
		
		<!--<div class="inner-cont top">
		
			<label>Register Yourself as</label><select class="selectpicker">
											<option>Select</option>
											<option>Owner</option>
											<option>Professional</option>
										</select>
		</div>-->

		
		<div class="clearfix"></div>
			<div class="success-cont">
				<p><span class="green-txt"> Caregiver Activated  Successfully!!</span><br><br>
				
				</p>
				<a href="<?php echo base_url();  ?>" class="btn btn-success">Click here to Login</a>
				
						<div class="clearfix"></div>
			</div>		
		
	</div>

<?php $this->load->view('includes/footer.php');?>
  </body>
</html>