<?php $this->load->view('includes/adminheader.php');?>

	<!-- Bootstrap Core CSS -->
 <style type="text/css">
.hidden1 { position:fixed; visibility:hidden;background: #fff; border: 1px solid maroon; padding: 10px;width:60%; height:500px;z-index:999; overflow:auto; overflow-style:marquee-line !important;  border-radius:5px; box-shadow: 5px 5px 5px 5px #888; font-family:Calibri; font:Arial, Helvetica, sans-serif;   opacity:1; margin-left:1%; margin-top:-229px;} 

table tr td{ color:#000;}
table tr td select{ width:60%; height:40px;}
table tr td input[type="text"],input[type="password"]{ width:60%; }
</style>
	
<?php
$segs = $this->uri->segment_array();
//print_r($segs);
//$ownerid = $segs[3];
?>
		<script src="<?php echo base_url('js/datetimepicker.js');?>"></script>	
		
		<?php error_reporting(0);    ?>
        <div id="page-wrapper">
<?php  $this->load->view("includes/admin_left.php");?>
            <div class="container" style="color:#333;">
		

		<!--<div class="inner-cont top">
		
			<label>Register Yourself as</label><select class="selectpicker">
											<option>Select</option>
											<option>Owner</option>
											<option>Professional</option>
										</select>
		</div>-->
<!--<div class="upgrd-lic">
			<button type="button" class="btn btn-success" aria-label="Left Align" >Buy another licence</button>
			<button type="button" class="btn btn-success" aria-label="Left Align" >Upgrade licence</button>
		
		</div>-->
		
		<div class="clearfix"></div>
		
		  <!-- Page Heading -->
             
                <!-- /.row -->
<div class="container-fluid">

<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
<h1 class="page-header">
   Medicine <small></small>
</h1>
<!--<ol class="breadcrumb">
    <li class="active">
<i class="fa fa-dashboard"></i> Edit Medicine
    </li>
</ol>-->
    </div>
</div>
<!-- /.row -->
<div class="row">
<div class="col-lg-12">
	<!--<h2>User table</h2>-->
	<div class="table-responsive">
	  <form name="add" id="add-forms"  method="POST" action="<?php echo base_url(); ?>admin/edit_medicine">
	    <table class="table table-bordered table-hover">
	      <input type="hidden" name="id" value="<?php echo $medicine[0]->med_id ?>" />
	      <tr>
	        <th>Medicine Name</th>
	        <td><input type="text" id="medicine_name" name="medicine_name" value="<?php echo $medicine[0]->med_name;?>"/></td>
	        </tr>
	      <tr></tr>
  <th>Medicine Format </th>
<?php  $farmat=$medicine[0]->medformat;?>
    <td><!-- <input type="text" id="medicine_format" name="medicine_format" value="<?php //echo $medicine[0]->medformat;?>"/>-->
      <select name="medicine_format" id="medicine_format">
        <option value="">- select type-</option>
        <option <?php if($farmat=="injection") { ?> selected="selected"<?php } ?>>injection</option>
        <option <?php if($farmat=="capsule") { ?> selected="selected"<?php } ?>>capsule</option>
        <option <?php if($farmat=="tablet") { ?> selected="selected"<?php } ?>>tablet</option>
        <option <?php if($farmat=="syrup") { ?> selected="selected"<?php } ?>>syrup</option>
        <option <?php if($farmat=="aerosol") { ?> selected="selected"<?php } ?>>aerosol</option>
        <option <?php if($farmat=="suppository") { ?> selected="selected"<?php } ?>>suppository</option>
        <option <?php if($farmat=="ointment") { ?> selected="selected"<?php } ?>>ointment</option>
        <option <?php if($farmat=="eye drops") { ?> selected="selected"<?php } ?>>eye drops</option>
        <option <?php if($farmat=="transdermal patch") { ?> selected="selected"<?php } ?>>transdermal patch</option>
        <option <?php if($farmat=="drops") { ?> selected="selected"<?php } ?>>drops</option>
      </select></td>
  </tr>
  <tr>
    <th>Note </th>
    <td><input type="text" id="note"  name="note" value="<?php echo $medicine[0]->med_note;?>"/></td>
  </tr>
  <tr>
    <th>Quantity </th>
    <td><input type="text" id="quantity" name="quantity" value="<?php echo $medicine[0]->med_quantity;?>"/></td>
  </tr>
  <tr>
    <th>Purchase Date </th>
    <td><input type="text" id="demo1" name="purchase_date" value="<?php echo $medicine[0]->med_purchase_date;?>"/>
      <a href="javascript:NewCal('demo1','YYYYMMDD',false,24)"><img src="<?php echo base_url(); ?>img/cal.png" width="16" height="16" border="0" alt="Pick a date" style="margin-left:5px;" /></a></td>
  </tr>
  <tr>
    <th>Expiration Date </th>
    <td><input type="text" id="demo2" name="expiration_date" value="<?php echo $medicine[0]->med_expirartion_date;?>"/>
      <a href="javascript:NewCal('demo2','YYYYMMDD',false,24)"><img src="<?php echo base_url(); ?>img/cal.png" width="16" height="16" border="0" alt="Pick a date" /></a></td>
  </tr>
  <tr>
    <th>Dosage </th>
    <td><input type="text" id="dosage" name="dosage" value="<?php echo $medicine[0]->med_dose;?>"/></td>
  </tr>
  <tr>
    <th>Meal time </th>
    <td><!--<input type="text" id="meal_time" name="meal_time" value="<?php //echo $medicine[0]->med_meal_time;?>"/>-->
    <?php $ftime=$medicine[0]->med_meal_time;?>
      <select name="meal_time" id="meal_time">
        <option value="">-select-</option>
        <option <?php if($ftime=="Before Meal") { ?> selected="selected" <?php } ?>>Before Meal</option>
        <option <?php if($ftime=="At Meal") { ?> selected="selected"<?php } ?>>At Meal</option>
        <option <?php if($ftime="Both") { ?> selected="selected"<?php } ?>>Both</option>
      </select>
      
      
      </td>
  </tr>
  <tr>
    <th>Price </th>
    <td><input type="text" id="price" name="price" value="<?php echo $medicine[0]->med_price;?>"/></td>
  </tr>
  <tr>
  <td> </td>
    <td>
    <!--<input type="submit" value="Edit" />-->
     <a href="<?php echo base_url(); ?>admin/list_medicine"><input type="button" value="cancel" class="btn btn-default"></a> 
                                        &nbsp;&nbsp;
                                        <button type="submit" class="btn btn-success" aria-label="Left Align">
										  Add 
										</button>
                                
    
    
    </td>
  </tr>
	      </table>
	  </form>
</div>
</div>

	
</div>
<!-- /.row -->

    </div>
                
                <!-- /.row -->
			
			
			
			
		
	</div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

		
<?php $this->load->view('includes/footer.php');?>