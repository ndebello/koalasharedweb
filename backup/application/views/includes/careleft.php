<div class="col-md-2 col-lg-2 leftsd" >
				<div class="pat-dp">
                <?php 
				 $pimg=$patient[0]->photo;
				if(empty($pimg))
				{
				?>
					<img src="<?php echo base_url(); ?>img/img-holder2.jpg" alt="patient image" />
				<?php } else { ?>
                <img src="<?php echo base_url(); ?>uploads/<?php echo $pimg; ?>" alt="patient image" />
                <?php } ?>
                	<div class="clearfix"></div>
					<span class="pat-name"> <?php echo $patient[0]->name;  ?> </span>
				</div>
				<div class="clearfix"></div>
				<div class="pat-desc" style="background:#F2F1F1; width:92%; padding-left:5%; padding-top:3%; margin-top:-4%;">
					<ul>
						<li><strong>Age</strong><br><?php echo $patient[0]->age; ?>yrs</li>
						<li><strong>Contact</strong><br><?php echo $patient[0]->phone; ?></li>
						<li><strong>Gender</strong><br> <?php echo $patient[0]->gender; ?></li>
						<li><strong>Pathologies</strong><br><?php echo $patient[0]->pathology; ?></li>
						<li><strong>Allergies</strong><br><?php echo $patient[0]->allergies; ?></li>
						<li><strong>Intolerance</strong><br><?php echo $patient[0]->intolerances; ?></li>
					</ul>
				</div>
			</div>