<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Koala | Embrace your needs</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
	 <!--<link href="css/bootstrap-theme.css" rel="stylesheet">-->
	 <link href="style.css" rel="stylesheet">
	 <link href="css/res.css" rel="stylesheet">
	 <link href="css/component.css" rel="stylesheet">
	 <link href="css/bootstrap-select.css" rel="stylesheet">
	 <link href="css/font-awesome.css" rel="stylesheet">
	 <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,300,600,700' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
 
<body class="cbp-spmenu-push" onload=dis()>
    <!--___________________________________________________Header____________________________________________________________---> 
	<?php  $this->load-view('includes/careheader.php');     ?>
    
	<?php //$this->load->view('includes/careheader.php'); ?>
	<!--___________________________________________________Header____________________________________________________________--->
<div class="clearfix"></div>
    <div class="container">
		
		<!--<div class="inner-cont top">
		
			<label>Register Yourself as</label><select class="selectpicker">
											<option>Select</option>
											<option>Owner</option>
											<option>Professional</option>
										</select>
		</div>-->
<!--<div class="upgrd-lic">
			<button type="button" class="btn btn-success" aria-label="Left Align" >Buy another licence</button>
			<button type="button" class="btn btn-success" aria-label="Left Align" >Upgrade licence</button>
		
		</div>-->
		
		<div class="clearfix"></div>
		<div class="login-hm-cont caregiver-routine">
			<div class="col-md-2 col-lg-2 leftsd">
				<div class="pat-dp">
					<img src="img/img-holder2.jpg" alt="patient image" />
					<div class="clearfix"></div>
					<span class="pat-name">Patient Name</span>
				</div>
				<div class="clearfix"></div>
				<div class="pat-desc">
					<ul>
						<li><strong>Age</strong><br>36yrs</li>
						<li><strong>Contact</strong><br>1234</li>
						<li><strong>Email</strong><br>pat@gmail.com</li>
						<li><strong>Pathologies</strong><br>Pathologies 1<br>Pathologies 2<br>Pathologies 3</li>
						<li><strong>Allergies</strong><br>Allergies 1<br>Allergies 2<br>Allergies 3</li>
						<li><strong>Intolerance</strong><br>Intolerance 1<br>Intolerance 2<br>Intolerance 3</li>
					</ul>
				</div>
			</div>
			<div class="col-md-7 col-lg-7 midsd">
				<div class="menu-cont">
					<ul>
						<li class="active"><a href="#"><span class="icn-clock"></span><span class="txt">Routine</span></a></li>
						<li><a href="#"><span class="icn-param"></span><span class="txt">Parameters</span></a></li>
						<li><a href="#"><span class="icn-medi"></span><span class="txt">Medicines</span></a></li>
						<li><a href="#"><span class="icn-diet"></span><span class="txt">Diet</span></a></li>
						<li><a href="#"><span class="icn-activities"></span><span class="txt">Activities</span></a></li>
						<li><a href="#"><span class="icn-report"></span><span class="txt">Report</span></a></li>
					</ul>
				</div>
				<div class="clearfix"></div>
				<nav  class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
    <h3>Menu</h3>
    <li class="active"><a href="#"><span class="icn-clock"></span><span class="txt">Routine</span></a></li>
						<li><a href="#"><span class="icn-param"></span><span class="txt">Parameters</span></a></li>
						<li><a href="#"><span class="icn-medi"></span><span class="txt">Medicines</span></a></li>
						<li><a href="#"><span class="icn-diet"></span><span class="txt">Diet</span></a></li>
						<li><a href="#"><span class="icn-activities"></span><span class="txt">Activities</span></a></li>
						<li><a href="#"><span class="icn-report"></span><span class="txt">Report</span></a></li>	
</nav>
<button id="showLeft" class="btn btn-success menu-btn">Menu</button>
				<div class="clearfix"></div>
				<div class="content-cont">
					<div class="circ-cont">
						<div class="btn-cont"><button class="btn btn-success" style="width:125px;">NEW EVENT</button></div>
						<div class="clearfix"></div>
						<div class="date-cont">
							<div class="lst-btn pull-left"><a href="#"><img src="img/last-img-icn.png" alt=""/><span>Last</span></a></div>
							<div class="date-time pull-left"><span class="dt">Nov 13/11/2014</span><span class="time">07.45</span></div>
							<div class="nxt-btn pull-left"><a href="#"><img src="img/next-img-icn.png" alt=""/><span>Next</span></a></div>
						</div>
						<div class="clearfix"></div>
						<div class="schedule-cont">
							<div class="lft-syd">
								Alarm<br>
								Breakfast<br>
								Lunch<br>
								Snack<br>
								Dinner<br>
								Sleep<br>
								Wake Up
							</div>
							<div class="ryt-syd">
								20:00<br>
								00:15<br>
								18:00<br>
								14:15<br>
								00:25<br>
								10:25<br>
								10:25
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="comment-cont">
				<a href="#"><img src="img/add-btn.png" alt="" /></a><textarea></textarea>
				</div>
				
			</div>
			<div class="col-md-3 col-lg-3 leftsd">
				<div class="hdr-txt">
					<h2><span class="icn"><i class="fa fa-cogs"></i></span>Associated Professionals</h2>
				</div>
				<div class="clearfix"></div>
				<div class="search-cont">
					<button><span class="srch-icn"><i class="fa fa-map-marker mp-icn"></i>Search</span></button>
					<!--<span class="srch-icn"><i class="fa fa-map-marker mp-icn"></i>Search</span>-->
					<input type="text" placeholder="Enter your search" />
				</div>
				<div class="clearfix"></div>
				<div class="pat-list">
					<ul>
					<li class="odd">
						<div class="img-thumb"><img src="img/pat-img.jpg" alt="pat img" /></div>
						<div class="pat-desc-cont">
							<h5><a href="#">Pazionto Bota</a></h5>
							<p>Donec id elit non</p>
							<span>mi porta</span>
						</div>
					</li>
					<li class="evn">
						<div class="img-thumb"><img src="img/pat-img.jpg" alt="pat img" /></div>
						<div class="pat-desc-cont">
							<h5><a href="#">Pazionto Bota</a></h5>
							<p>Donec id elit non</p>
							<span>mi porta</span>
						</div>
					</li>
					<li class="odd">
						<div class="img-thumb"><img src="img/pat-img.jpg" alt="pat img" /></div>
						<div class="pat-desc-cont">
							<h5><a href="#">Pazionto Bota</a></h5>
							<p>Donec id elit non</p>
							<span>mi porta</span>
						</div>
					</li>
					
				</ul>
				</div>
				<div class="clearfix"></div>
				<div class="hdr-txt">
					<h2><span class="icn"><i class="fa fa-cogs"></i></span>Associated Caregivers</h2>
				</div>
				<div class="clearfix"></div>
				<div class="search-cont">
					<button><span class="srch-icn"><i class="fa fa-map-marker mp-icn"></i>Search</span></button>
					<!--<span class="srch-icn"><i class="fa fa-map-marker mp-icn"></i>Search</span>-->
					<input type="text" placeholder="Enter your search" />
				</div>
				<div class="clearfix"></div>
				<div class="pat-list">
					<ul>
					<li class="odd">
						<div class="img-thumb"><img src="img/pat-img.jpg" alt="pat img" /></div>
						<div class="pat-desc-cont">
							<h5><a href="#">Pazionto Bota</a></h5>
							<p>Donec id elit non</p>
							<span>mi porta</span>
						</div>
					</li>
					<li class="evn">
						<div class="img-thumb"><img src="img/pat-img.jpg" alt="pat img" /></div>
						<div class="pat-desc-cont">
							<h5><a href="#">Pazionto Bota</a></h5>
							<p>Donec id elit non</p>
							<span>mi porta</span>
						</div>
					</li>
					<li class="odd">
						<div class="img-thumb"><img src="img/pat-img.jpg" alt="pat img" /></div>
						<div class="pat-desc-cont">
							<h5><a href="#">Pazionto Bota</a></h5>
							<p>Donec id elit non</p>
							<span>mi porta</span>
						</div>
					</li>
					
				</ul>
				</div>
			</div>
			
		</div>		
		
	</div>

<footer>
	<div class="container">
        <p>Koala Copyright © 2014, All other trademarks are the property of their respective owners.</p>
		</div>
      </footer>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
	<script src="js/bootstrap-select.js"></script>
	<script>
		 $('.selectpicker').selectpicker({
			style: 'btn-info',
			size: 4
		});
	</script>
	<script src="js/modernizr.custom.js"></script>
	<script src="js/classie.js"></script>
	<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				menuRight = document.getElementById( 'cbp-spmenu-s2' ),
				menuTop = document.getElementById( 'cbp-spmenu-s3' ),
				menuBottom = document.getElementById( 'cbp-spmenu-s4' ),
				showLeft = document.getElementById( 'showLeft' ),
				showRight = document.getElementById( 'showRight' ),
				showTop = document.getElementById( 'showTop' ),
				showBottom = document.getElementById( 'showBottom' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				showRightPush = document.getElementById( 'showRightPush' ),
				body = document.body;

			showLeft.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeft' );
			};
			

			function disableOther( button ) {
				if( button !== 'showLeft' ) {
					classie.toggle( showLeft, 'disabled' );
				}
				
			}
			$(function(){
  $(document).click(function(e){
    if($(e.target).is('#showLeft') || $(e.target).closest('.cbp-spmenu').length>0){
     
    } else{
      if($('.cbp-spmenu.cbp-spmenu-open').length>0){
        $('#showLeft').trigger('click');
      }
    }
  });
});
		</script>
  </body>
</html>