<?php
/********************************************************************************
* Convert text via Google to voice using Jquery and PHP 
* Written by Vasplus Programming Blog
* Website: www.vasplus.info
* Email: vasplusblog@gmail.com or info@vasplus.info

*********************************Copyright Info***********************************
* This script has been released with the hope that it will be useful
* You are not allowed to sell the script
* Please do not remove this copyright information from the top of this page
* All Copy Rights Reserved by Vasplus Programming Blog
***********************************************************************************/
session_start();
ini_set('error_reporting', 'E_NONE'); 
@set_time_limit(0);
@ini_set('memory_limit', '64M');
@header("Content-type: text/html;charset=utf-8");

//This function converts the text to mp3 audio file
//$vpb_text_to_convert="This function converts the text to mp3 audio file";


function vpb_convert_text_to_voice($vpb_text_to_convert) 
{
	
	
	
	$vpb_max_characters = 100; 
	$vpb_counted_text_length = 0; 
	$vpb_counted_words = 0;
	$vpb_convert_to_language = 'en';
	//audio/  
	$vpb_converted_file_dir = "audio/"; 
	 
	// Accepts only 100 characters
	$vpb_text_to_convert = substr($vpb_text_to_convert, 0, $vpb_max_characters); 
 
	// The total of the text to convert
	$vpb_counted_text_length = strlen($vpb_text_to_convert); 
	 
	// The total of the words to convert
	$vpb_counted_words = str_word_count($vpb_text_to_convert); 
	 
	// Makes the text to be converted URL encoding which is a necessity
	$vpb_text_to_convert = urlencode($vpb_text_to_convert); 
	 
	// Check if the directory to save the out put file is not available and create the directory otherwise, do nothing man
	if (!is_dir($vpb_converted_file_dir)) { 
		mkdir($vpb_converted_file_dir, 777) or die('Sorry, the system could not create the audio directory: ' . $vpb_converted_file_dir); 
	} else {}
	 
	// Rename the out put file with a special name such as day-month-year-random-number
	$vpb_filename = date('d-m-Y-').rand(12345,09876).".mp3";
	$_SESSION["vpb_filename"] = $vpb_filename;
	$vpb_output_filename = $vpb_converted_file_dir . $vpb_filename; 
	 
	// Check if the new file needed has not been created and then create it
	if (!file_exists($vpb_output_filename) || filesize($vpb_output_filename) == 0) 
	{
		$dataLink = "http://translate.google.com/translate_tts?ie=UTF-8&q={$vpb_text_to_convert}&tl={$vpb_convert_to_language}&total={$vpb_counted_words}&idx=0&textlen={$vpb_counted_text_length}";
		
		// If php curl is not enaled on your server, use the php function called file_get_contents for the conversion via Google 
		if (!function_exists('curl_init')) 
		{
			// Return error if the conversion is unsuccessful otherwise, return the converted file
			$output = file_get_contents($dataLink) ? file_get_contents($dataLink) : "error";
		} else {
			// If the php curl function is enabled then use it instead for the conversion via Google
			$v_process = curl_init();  
			curl_setopt($v_process, CURLOPT_URL, $dataLink);  
			curl_setopt($v_process, CURLOPT_AUTOREFERER, true);  
			curl_setopt($v_process, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1");  
			curl_setopt($v_process, CURLOPT_HEADER, 0);  
			curl_setopt($v_process, CURLOPT_RETURNTRANSFER, true);  
			curl_setopt($v_process, CURLOPT_TIMEOUT, 10);  
			$output = curl_exec($v_process) ? curl_exec($v_process) : "error";  
			curl_close($v_process);  
		}
		if($output == "error") 
		{
			// Return error if the conversion is unsuccessful
			$vpb_output_filename = "error";
		} else {
			// Otherwise, return the converted file
			file_put_contents($vpb_output_filename, $output); 
		}
	}
	return $vpb_output_filename; // Display the result
}

function vpb_download_file($file){
    $vpb_download_dir = "audio/";
    $vpb_path_to_file = $vpb_download_dir.$file;

    //Check to see if file exists
	if(substr(strip_tags($vpb_path_to_file), 0, 3)!='../' && substr(strip_tags($vpb_path_to_file), -3) == strip_tags("mp3"))
	{
		if (!file_exists($vpb_path_to_file)) {
			//file does not exist
			die("ERROR: File does not exist");
		} else {
			// file exists; set headers
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename="'.basename($vpb_path_to_file).'"'); // adding quotes and stripping full path
			header('Content-Transfer-Encoding: binary');
			header('Connection: Keep-Alive');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header("Cache-Control: private",false); // required for certain browsers 
			header('Pragma: public');
			header('Content-Length: ' . filesize($vpb_path_to_file));
	
			readfile($vpb_path_to_file);
			die(); // end here to make sure no trailing characters happen.
		}
	} else {
		die("ERROR: There was no proper data received.<br />Thank you!");
	}
}


// Check to be sure that the text needed to be converted was actually posted to this page
if(isset($_POST['vpb_text_to_convert']) && !empty($_POST['vpb_text_to_convert']))
{
	// Trim the text to be sure that its not an empty field
	$vpb_text_to_convert = trim(strip_tags($_POST['vpb_text_to_convert']));
	
	if($vpb_text_to_convert == "")
	{
		echo "Sorry, the process has been terminated because no proper data was received.<br>Please try again or contact us to report this issue if you feel that something is wrong.<br />Thank You!";
	}
	else if(vpb_convert_text_to_voice($vpb_text_to_convert) == "error") // Check if the result brought from the above function is an error
	{
		echo "Sorry, the process has been terminated because there was an error.<br>Please try again or contact us to report this issue if the problem persist.<br />Thank You!";
	}
	else
	{
		// Display the converted mp3 audio file on the screen to the user
		echo '<audio controls="controls" autoplay="autoplay"><source src="'.base_url('audio/16-01-2015-7371.mp3').'" type="audio/mp3" /></audio>';
		
		// Display a link to download the file
		echo '<br /><br />To download the file, please <span class="ccc"><a href="vpb_converter.php?load-file='.$_SESSION["vpb_filename"].'" target="_blank">click here</a></span><br />';
	}
}
elseif(isset($_GET['load-file']) && !empty($_GET['load-file'])) // For file downloads
{
	$vfile = trim(strip_tags($_GET['load-file']));
	if($vfile == "") 
	{
		echo "Sorry, the process has been terminated because no proper data was received.<br>Please try again or contact us to report this issue if you feel that something is wrong.<br />Thank You!";
	}
	else { vpb_download_file($vfile); }
}
else
{
	echo "Sorry, the process has been terminated because there was an error.<br>Please try again or contact us to report this issue if the problem persist.<br />Thank You!";
}
?>