<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Medicine extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_last_ten_entries()
    {
        $query = $this->db->get('tbl_person', 100);
        return $query->result();
    }
    function get($id){
        echo $id;
        $sql = "SELECT * FROM tbl_person WHERE id = ?";

        $query =$this->db->query($sql, array($id)); 
       
         echo $this->db->last_query();
        return $query->result();
    }

    function insert_entry()
    {
        $this->name   = $_POST['name']; // please read the below note
        $this->gender = $_POST['gender'];
        $this->dob = $_POST['dob'];
        
        $this->db->insert('tbl_person', $this);
    }

    function update_entry()
    {
         $this->name   = $_POST['name']; // please read the below note
        $this->gender = $_POST['gender'];
        $this->dob = $_POST['dob'];

        $this->db->update('tbl_person', $this, array('id' => $_POST['id']));
    }
    function delete_entry($id)
    {
         

        $this->db->delete('tbl_person', array('id' => $id));
    }

}
   
    
}
