<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    
	
	function getUsersAll() {
        $this->db = $this->load->database('default', TRUE);  
        $this->db->select();
        $query = $this->db->get('users');
        return $query->result();
    }
	function get_carenoti_care_all_popup($pid)
	{
		$query =$this->db->query("update popup_notifications set popup_notifications.status='1' where popup_notifications.n_rid='$pid'");
	}
	function getnotification_new($pid)
	{
		$query = $this->db->query("select * from notifications as noti ,event_type as ety where noti.event_type_id=ety.evn_id and noti.pat_id='$pid'");
        return $query->result();
	}
	
	function getpatient_new($pid)
	{
		$query = $this->db->query("select * from patients where id='$pid'");
        return $query->result();
	}
	
function get_owner_caretype($pid,$oid)	
	{
		$query = $this->db->query("select * from patients where id='$pid' and own_id='$oid'");
        return $query->result();
	}
	
	
	
	
	
	function getnew_consultion($iRecipient,$iPid)
	{


/*		
$query = $this->db->query("select *,care.id as cid from cons_request_messages as con, caregiver as care WHERE ((con.`sender` = '{$iRecipient}' && con.stype=1) || (con.`recipient` = '{$iPid}' && con.rtype=1) || (con.`sender` = '{$iRecipient}' && con.stype=0) || (con.`recipient` = '{$iPid}' && con.rtype=1) ) || ((con.`recipient` = '{$iRecipient}' && con.stype=1) || (con.`sender` = '{$iPid}' && con.rtype=1) || (con.`recipient` = '{$iRecipient}' && con.stype=1) && (con.`sender` = '{$iPid}' && con.rtype=0) ) and care.id=con.sender order by con.id asc");
return $query->result() or die(mysql_error());*/	

$query= $this->db->query("select * from cons_request_messages as cons   WHERE ((cons.`sender` = '{$iRecipient}' && cons.stype=1) && (cons.`recipient` = '{$iPid}' && cons.rtype=1) || (cons.`sender` = '{$iRecipient}' && cons.stype=0) && (cons.`recipient` = '{$iPid}' && cons.rtype=1) ) || ((cons.`recipient` = '{$iRecipient}' && cons.stype=1) && (cons.`sender` = '{$iPid}' && cons.rtype=1) || (cons.`recipient` = '{$iRecipient}' && cons.stype=1) && (cons.`sender` = '{$iPid}' && cons.rtype=0) )  order by cons.id asc");
	return $query->result();





	
	}
	
		
	
	
	function get_paremeters_list($pid,$praname,$date)
	{
		
		$query = $this->db->query("select * from parameters where patient_id='$pid' and para_id='$praname' and created='$date'");
        return $query->result();
	}
	
	function get_paremeters()
	{
	   $query = $this->db->query("select * from parameter_name ");
       return $query->result();
		
	}
	
	
	function getagenda($pid,$date)
	{
	   $query = $this->db->query("select * from agenda where prof_id='$pid' and date='$date'");
       return $query->result();
		
	}
	function delete_agenda($aid)
	{
	//$query = $this->db->query("delete  from agenda where id='$aid'");
	$this->db->delete('agenda', array('id' => $aid));
	// $query->result();
		
	}
	
	function getconsultaion_msg1($pfid)
	{
		$query = $this->db->query("select * from cons_request_messages as cons,caregiver as cs  where (recipient='$pfid' and subject!='') and (cons.sender=cs.id)");
		
		return $query->result();
		
		
		
	}
	
	
	
	
	
	function insert_agenda($title,$details,$time,$date,$pid)
	{
	$data=array(
	'title'=>$title,
	'details'=>$details,
	'time'=>$time,
	'date'=>$date,
	'prof_id'=>$pid,
	
	);
	return $this->db->insert('agenda', $data);
		
		
	}
	
	
	function update_agenda($title,$details,$time,$date,$pid)
	{
		 
	 
	$data=array(
	'title'=>$title,
	'details'=>$details,
	'time'=>$time,
	'date'=>$date,
	
	);
	
	$this->db->update('agenda', $data, array('id' => $pid));	
		
	
	
	}
	
	function update_parient_qr($pid,$image_name)
	{
	$data=array(
	'id_card'=>$image_name,
	);
	$this->db->update('patients', $data, array('id' => $pid));	
			
	}
	
	
	
	
	
	function edit_agenda($aid)
	{
		$query = $this->db->query("SELECT * FROM agenda where id='$aid'");
       return $query->result();
	}
	
	
	
	/* get patient details   */	
function get_patient($pid)	
	{
		$query = $this->db->query("SELECT * FROM patients where id='$pid'");			
		return $query->result();
	}

function caredetailsbyid($cid)
{
	$query = $this->db->query("SELECT * FROM caregiver where id='$cid'");			
	return $query->result();
}

	function getprofetionals123($pid)
	{
        $query= $this->db->query("select *,care.id as cid from professional_networks as pnet, caregiver as care  where (pnet.rid='$pid') and pnet.netstatus=1 and care.id=pnet.sid");
		return $query->result();
		
	}
	
	function getcaregivers_new($pid) {		
		//$query = $this->db->get('patients');
	  $query = $this->db->query("select * from caregiver where pat_id='$pid'");
      return $query->result();
		//return $query->num_rows();
		
	}
	
	function getcaregivers_new1($pid,$pfid) {		
		//$query = $this->db->get('patients');
	  $query = $this->db->query("SELECT *,care.id as careid FROM `caregiver` as care, list_caregiver as listc ,professional_networks as prf WHERE listc.pat_id='$pid' and listc.careid=prf.sid and prf.rid='$pfid' group by care.id");
      return $query->result();
		//return $query->num_rows();
		
	}
	
	
	
	
	
	function getprofetionals1($id,$pid)
	{
		//$query= $this->db->query("select *,prf.id as pfid from professional_networks as prfn, professionals as prf ,professional_networks_prf as mprf where (prfn.patid='$pid') and (mprf.sid=prf.id or mprf.rid=prf.id) group by prf.id");
		 
		 $query = $this->db->query("select *,prf.id as nprfid from professional_networks as prnt,professionals as prf,professional_networks_prf as mprf  where (prnt.patid='$pid') and prf.id=prnt.rid and (mprf.sid='$id' or mprf.rid='$id') group by prnt.rid");
      return $query->result();
		
		
		
		
		
		return $query->result();
	}
	
	
	function getconsultaion_new($iRecipient,$iPid)
	{
	$query= $this->db->query("select * from cons_request_messages as cons   WHERE ((cons.`sender` = '{$iRecipient}' && cons.stype=1) && (cons.`recipient` = '{$iPid}' && cons.rtype=1) || (cons.`sender` = '{$iRecipient}' && cons.stype=0) && (cons.`recipient` = '{$iPid}' && cons.rtype=1) ) || ((cons.`recipient` = '{$iRecipient}' && cons.stype=1) && (cons.`sender` = '{$iPid}' && cons.rtype=1) || (cons.`recipient` = '{$iRecipient}' && cons.stype=1) && (cons.`sender` = '{$iPid}' && cons.rtype=0) )  order by cons.id asc");
	return $query->result();
		
	}
	
	function getprofnoti_new_pop($pid)
	{
	//and popup_notifications.status='0'
	$query = $this->db->query("select  *  from popup_notifications where n_rid='$pid' ");
	return $query->result();
	
	}
	
	function getprofnoti_new_count($pfid)
	{
		
	$query = $this->db->query("select  * from popup_notifications where n_rid='$pfid' and popup_notifications.status='0' ");
	return $query->result();
	
	}
	
	
	
	function getprofnoti_new($pid)
	{
		$query = $this->db->query("select  *, pnet.id as nid from professional_networks as pnet,caregiver as care where  pnet.rid='$pid' and (pnet.netstatus=0 or pnet.netstatus=1) and care.id=pnet.sid group by care.id");
      return $query->result();
	}
	
	
	
	function getprofnotinet_new($pid)
	{
		$query = $this->db->query("select * ,prfnet.id as npid from professional_networks_prf as prfnet,professionals as prf where                                       prfnet.rid='$pid' and (prfnet.netstatus=0 or prfnet.netstatus=1) and prfnet.sid=prf.id");
		 return $query->result();	
	}
	function getprofnoticon_new($pid)
	{
$query = $this->db->query("select * from cons_request_messages as cons,caregiver as cs  where (recipient='$pid' and subject!='' and (cons.read='0' or cons.read='1')) and (cons.sender=cs.id)");
        return $query->result();
	}
	
	function getprofnoti_new_update($pid)
	{
		$query = $this->db->query("update professional_networks  set netstatus=1 where  rid='$pid' ");
      
	}
	function getprofnotinet_new_update($pid)
	{
$query = $this->db->query("update professional_networks_prf  set netstatus=1  where professional_networks_prf.rid='$pid' ");
	
	}
	
	function getprofnoticon_new_update($pid)
	{
$query = $this->db->query("update cons_request_messages  set cons_request_messages.read='1' where cons_request_messages.recipient='$pid'");
       
	}
	function getprofnoticon_msg_update($cid)
{
$query =$this->db->query("update chat set chat.recd='1'  where  chat.to='$cid'");

	
}

function getprofnoticon_msg1_update($cid)
{
$query =$this->db->query("update chat  set chat.recd='1' where cht.to='$cid'");
	
}

	
	
	
	
	function get_owner1($email)
	{
	
       $this->db->select();
       $this->db->where('owner_email', $email); 
       //$this->db->where('own_id', $oid); 
       $query = $this->db->get('owner_table');
      // return $query->result();
		return $query->num_rows();
	}
	public function message_count()
	{
		return $this->db->count_all('owner_table');
	}
	function getowners($per_pg,$offset) {
        $this->db = $this->load->database('default', TRUE); 
		$this->db->order_by('own_id','desc'); 
        $this->db->select();
        $query = $this->db->get('owner_table',$per_pg,$offset);
		return $query->result();
    }
	
	
	function fetch_report1($pid)
	{
		$query = $this->db->query("SELECT * FROM notifications where pat_id='$pid' group by note_date order by note_id desc");	
     	return $query->result();
	}
	function fetch_report_details($pid,$date)
	{
		$query = $this->db->query("SELECT * FROM notifications where pat_id='$pid' and note_date='$date' ORDER BY `note_date` ASC");
		//$ms="select * from notifications as noti,caregiver as care, event_type as ent ,";	
     	return $query->result();
	}
	
	function fetch_newreport1($pid,$from,$to)
	{
		
$query = $this->db->query("SELECT * FROM notifications where pat_id='$pid' and note_date BETWEEN '$from' and '$to' group by note_date  ORDER BY `note_date` ASC");
return $query->result();
	
	}
	function fetch_newreport2($pid,$from,$to)
	{
		
$query = $this->db->query("SELECT * FROM notifications where pat_id='$pid' and note_date BETWEEN '$from' and '$to'   ORDER BY `note_date` ASC");
return $query->result();
	
	}
	
	function fetch_medecine($pid,$eid) 
{
//$query = $this->db->get('medicine');
//Edited line ==>where pat_id='$pid' or pat_id=0

 $query = $this->db->query("SELECT * FROM notifications as noti, medicine as med where noti.event_type_id='$eid' and noti.pat_id='$pid' and med.med_id=noti.sub_event_id");
     	return $query->result();
      }
function fetch_diets($pid,$eventid)
{
$query = $this->db->query("SELECT * FROM notifications as noti, diets as dts where noti.event_type_id='$eventid' and noti.pat_id='$pid' and dts.id=noti.sub_event_id");
     	return $query->result();
	
}


function getprofnoticon_msg($cid)
{
//$query =$this->db->query("select *,prf.id as pfid from chat as cht,professionals as prf where cht.from=prf.id and (cht.recd='0' or cht.recd='1') and  cht.to='$cid' group by prf.id ");

$query =$this->db->query("select c.id as cid,c.message,c.recd,c.from,c.to,prf.id as pfid,prf.name,prf.photo from professionals as prf inner join (SELECT i1.* FROM chat AS i1 LEFT JOIN chat AS i2 ON (i1.from = i2.from AND i1.id < i2.id) WHERE i2.from IS NULL and i1.to='$cid') c on prf.id=c.from ");





return $query->result();
	
}

function getprofnoticon_prfmsgcount($cid)
{
$query =$this->db->query("select *,prf.id as pfid from chat as cht,professionals as prf where cht.from=prf.id and cht.recd='0' and  cht.to='$cid' ");
return $query->result();	
}



function getprofnoticon_msg1($cid)
{
//$query =$this->db->query("select *,car.id as cfid from chat as cht,caregiver as car where cht.from=car.id and (cht.recd='0' or cht.recd='1') and  cht.to='$cid' group by car.id ");
$query =$this->db->query("select c.id as cid,c.message,c.recd,c.from,c.to,prf.id as cfid,prf.name,prf.photo from caregiver as prf inner join (SELECT i1.* FROM chat AS i1 LEFT JOIN chat AS i2 ON (i1.from = i2.from AND i1.id < i2.id) WHERE i2.from IS NULL and i1.to='$cid') c on prf.id=c.from");

return $query->result();
	
}
function getprofnoticon_caremsgcount($cid)
{
$query =$this->db->query("select *,car.id as cfid from chat as cht,caregiver as car where cht.from=car.id and cht.recd='0' and  cht.to='$cid'  ");
return $query->result();
	
}


	  
	  
	
	function fetch_medecine1($pid,$eid,$date) 
      {
		//$query = $this->db->get('medicine'); 
		//Edited line ==>where pat_id='$pid'    or pat_id=0not.event_type_id='$eid' and not.pat_id='$pid' and
		//group by nat.sub_event_id order by med.med_id desc 
		
		$query = $this->db->query("SELECT * FROM notifications as nat, medicine as med  where  nat.sub_event_id=med.med_id and nat.pat_id='$pid' and nat.event_type_id='$eid' and nat.note_date='$date'");	
     	return $query->result();
      }
	
	
		function fetch_medecine1_search($pid,$eid,$srch) 
      {
		//$query = $this->db->get('medicine');
		//Edited line ==>where pat_id='$pid' or pat_id=0not.event_type_id='$eid' and not.pat_id='$pid' and
		
$query = $this->db->query("SELECT * FROM notifications as nat, medicine as med  where  nat.sub_event_id=med.med_id and nat.pat_id='$pid' and nat.event_type_id='$eid' and med_name like '%$srch%' group by nat.sub_event_id order by med.med_id desc ");	
     	return $query->result();
      }
	
	
	
	
	
function fetch_diets1($pid,$eid,$date) 
{
//$query = $this->db->get('medicine');
//Edited line ==>where pat_id='$pid' or pat_id=0not.event_type_id='$eid' and not.pat_id='$pid' and 
//group by nat.sub_event_id order by det.id desc
				$query = $this->db->query("SELECT * FROM notifications as nat, diets as det  where  nat.sub_event_id=det.id and                                           nat.pat_id='$pid' and nat.event_type_id='$eid' and  nat.note_date='$date'");	
		return $query->result();
      }
	
	
	function fetch_activity1($pid,$eid,$date) 
      {
		//$query = $this->db->get('medicine');
		//Edited line ==>where pat_id='$pid' or pat_id=0not.event_type_id='$eid' and not.pat_id='$pid' and
		//group by nat.sub_event_id order by act.act_id desc
$query = $this->db->query("SELECT * FROM notifications as nat, activities as act  where  nat.sub_event_id=act.act_id and nat.pat_id='$pid' and nat.event_type_id='$eid' and  nat.note_date='$date' ");	
     	return $query->result();
      }
	function fetch_parameter1($pid,$eid,$date) 
      {
		//$query = $this->db->get('medicine');
		//Edited line ==>where pat_id='$pid' or pat_id=0not.event_type_id='$eid' and not.pat_id='$pid' and
		//group by nat.sub_event_id  order by nat.note_id desc
		$query = $this->db->query("SELECT * FROM notifications as nat, parameter_name as pra  where  nat.sub_event_id=pra.id and nat.pat_id='$pid' and nat.event_type_id='$eid'  and  nat.note_date='$date' ");	
     	return $query->result();
      }
	  
	  
	  
	  
	
	function fetch_activity($pid,$eid) 
{
      //$query = $this->db->get('activities');
		//return $query->result();where pat_id='$pid'
		$query = $this->db->query("SELECT * FROM notifications as noti,activities as act  where noti.event_type_id='$eid' and noti.pat_id='$pid' and act.act_id=noti.sub_event_id");
		return $query->result();
		
}
	
	public function count_profetionals()
	{
		return $this->db->count_all('professionals');
	}
	
	function getprofetionals($per_pg,$offset) 
	{
	        $this->db->order_by('id','desc'); 
			$this->db->select();
	        $query = $this->db->get('professionals',$per_pg,$offset);
		    return $query->result();
	}
	
	public function count_caregivers()
	{
		return $this->db->count_all('caregiver');
	}
	
	
	
	
	function get_caregivers($per_pg,$offset)
	{
		//$query = $this->db->query("SELECT * FROM caregiver order by id desc limit $per_pg,$offset ");			

         //return $query->result();
		 
		 $this->db->order_by('id','desc'); 
			$this->db->select();
	        $query = $this->db->get('caregiver',$per_pg,$offset);
		    return $query->result();
		 
		 
		 
	}
	
	
		function serach_caregivers()
			{ 
				   
				$data=explode(' ', $_POST['keyword']);
				$searchkey = 'where ';
				foreach($data as $temp){
					if($temp !='')
					{
						$searchkey .= '`name` like "%'.mysql_real_escape_string($temp).'%"  OR ';
						$searchkey .= '`surname` like "%'.mysql_real_escape_string($temp).'%"  OR ';
					}
				} 
				if($searchkey =='where ')
				{
					$searchkey = 'where  1=1';
				}else{
					$searchkey  = substr($searchkey,0,-3);
				}
				$query = $this->db->query("SELECT * FROM caregiver $searchkey");			
						
				return $query->result();
		
				  
			}
			
	
	
	
	
	
	function serach_profetionals()
	{      
		$data=explode(' ', $_POST['keyword']);
		$searchkey = 'where ';
		foreach($data as $temp){
			if($temp !='')
			{
				$searchkey .= '`name` like "%'.mysql_real_escape_string($temp).'%"  OR ';
				$searchkey .= '`surname` like "%'.mysql_real_escape_string($temp).'%"  OR ';
			}
		} 
		if($searchkey =='where ')
		{
			$searchkey = 'where  1=1';
		}else{
			$searchkey  = substr($searchkey,0,-3);
		}
		$query = $this->db->query("SELECT * FROM professionals $searchkey");			
				
		return $query->result();

	    }

	function profetionalDetail($id) {
	  $this->db->where('id', $id);  
	  $query = $this->db->get('professionals');		
	  return $query->result();
	
	}
	

/*


========================================>view the caregiver  =========================================================> */
function get_caregiver($id)
{
$query = $this->db->query("SELECT * FROM caregiver where id='$id'");			

return $query->result();		
}

function updatetransation($id,$trans)
{


$this->payamount= 50;
$this->tran_status= "Success";
$this->own_status="active";
$this->trno= $trans;
$this->pay_date=date("Y-m-d");
$this->db->update('owner_table', $this, array('own_id' => $id));





	
}

function addactivity()
{
				$file=time().$_FILES['attached']['name'];
				$dis="activity/";
				$dis=$dis.$file;
				move_uploaded_file($_FILES['attached']['tmp_name'],$dis);
	            $this->act_name   = $_POST['act_name']; 
				$this->act_details = $_POST['actdetails'];
				$this->act_attached = $file;
				$this->act_date = date("Y-m-d");
				$this->e_care_id=1;
				//$this->act_dayof_week=$_POST['dateofweek'];
				$this->db->insert('activities', $this);
}
	
	
	
	
function updateownerlicencestatus($oid)	
{
$data=array(
'own_licence_type'=>"Extended",
     );
$this->db->update('owner_table', $data, array('own_id' => $oid));
}
function updateownerlicencestatus1($licence)	
{
$data=array(
'licencetype'=>"Extended",
     );
$this->db->update('owner_licence', $data, array('licence_no' => $licence));
}






function consultation_request($oid)
{
 $query = $this->db->query("select * from cons_request_messages where recipient='$oid' and subject!=''");
  return $query->result();
}

	function get_owner_licence($oid)
	{
      
	   $query = $this->db->query("select * from owner_licence where own_id='$oid'");
      //return $query->result();
	   return $query->num_rows();
	}	
	
	
	function get_owner_by_licence($licence,$oid)
	{
      
	   $query = $this->db->query("select * from owner_licence where licence_no='$licence' and own_id='$oid'");
      //return $query->result();
	   return $query->num_rows();
	}	
	
	
function insert_anether_olicence($oid,$email,$num)	
	{
	$lic="koala".$oid."_".$num;
	$data=array(
	'own_id'=>$oid,
	'licence_no'=>$lic,
	'oemail'=>$email,
	
	);
	return $this->db->insert('owner_licence', $data);
	}
	
function get_caregiver_tologin($username,$password)
{
		$this->db->select();
       $this->db->where('email', $username); 
	   $this->db->where('passwd', $password); 
       $query = $this->db->get('caregiver');
       //return $query->result();
	   return $query->num_rows();
}
	



function getcaregiversbyid($id)
	{
      
	   $query = $this->db->query("select * from caregiver where id='$id'");
      //return $query->result();
	   return $query->num_rows();
	}




	
function getcaregiversbyemail($email)
	{
      
	   $query = $this->db->query("select * from caregiver where email='$email'");
      //return $query->result();
	   return $query->num_rows();
	}

	
	function updatecaregiversbycid($cid,$password,$lat,$lon,$cname,$surname,$zipcode,$city,$address,$contact,$age,$gender,$country,$nfile)
	{
	   //$query = $this->db->query("update caregiver set passwd='$password' where email='$email'");
      //return $query->result();
	   //return $query->num_rows();
	   $date=date("Y-m-d");
	   
	   //$pass=md5($password);'passwd'=>$pass,
$data=array('status'=>1,
			'lat'=>$lat,
			'lon'=>$lon,
			'name'=>$cname,
			'surname'=>$surname,
			'age'=>$age,
			'gender'=>$gender,
			'zipcode'=>$zipcode,
			'city'=>$city,
			'state'=>$country,
			'address'=>$address,
			'phone'=>$contact,
			'photo'=>$nfile,
			'from'=>$date,
			'passwd'=>$password,
            );
return $this->db->update('caregiver', $data, array('id' => $cid));
	}

	
	
	
	function updatecaregiversbycid2($cid,$password,$lat,$lon,$cname,$surname,$zipcode,$city,$address,$contact,$age,$gender,$country,$nfile)
	{
	   //$query = $this->db->query("update caregiver set passwd='$password' where email='$email'");
      //return $query->result();
	   //return $query->num_rows();
	   $date=date("Y-m-d");
	   
	   //$pass=md5($password);'passwd'=>$pass,
$data=array('status'=>1,
			'lat'=>$lat,
			'lon'=>$lon,
			'name'=>$cname,
			'surname'=>$surname,
			'age'=>$age,
			'gender'=>$gender,
			'zipcode'=>$zipcode,
			'city'=>$city,
			'state'=>$country,
			'address'=>$address,
			'phone'=>$contact,
			'photo'=>$nfile,
			'from'=>$date,
			
            );
return $this->db->update('caregiver', $data, array('id' => $cid));
	}
	
	
	
	
	
	function updatecaregiversbycid1($id,$lat,$lon,$cname,$surname,$zipcode,$city,$address,$contact,$age,$gender,$country,$nfile)
	{  
	
	
	   //$query = $this->db->query("update caregiver set passwd='$password' where email='$email'");
      //return $query->result();
	   //return $query->num_rows();
	   $date=date("Y-m-d");
	   
	   //$pass=md5($password);'passwd'=>$pass,
$data=array('status'=>1,
			'lat'=>$lat,
			'lon'=>$lon,
			'name'=>$cname,
			'surname'=>$surname,
			'age'=>$age,
			'gender'=>$gender,
			'zipcode'=>$zipcode,
			'city'=>$city,
			'state'=>$country,
			'address'=>$address,
			'phone'=>$contact,
			'photo'=>$nfile,
			'from'=>$date,
			
            );
return $this->db->update('caregiver', $data, array('id' => $id));
	 
	
	}
	
	
	
	
	function get_profdetails($username,$password)
	{
		
       $this->db->select();
       $this->db->where('email', $username); 
	   $this->db->where('password', $password); 
       $query = $this->db->get('professionals');
       //return $query->result();
	   return $query->num_rows();
	}
	
	function getallcaregivers($oid)
	{
       $this->db->select();
       $this->db->where('own_id', $oid); 
	   $query = $this->db->get('caregiver');
       //return $query->result();
	   return $query->num_rows();
	}
	function getmorecaregivers($oid)
	{
	   $query = $this->db->query("select * from caregiver where care_type_id!=1 and own_id='$oid'");
      //return $query->result();
	   return $query->num_rows();
	}
	
	function getcaregiversemail($email)
	{
      
	   $query = $this->db->query("select * from caregiver where email='$email'");
      //return $query->result();
	   return $query->num_rows();
	}
	
function insertintocaregiver($ar,$snname,$cemail,$ctype,$patient,$id)
	{
return $query = $this->db->query("insert into caregiver (name,surname,email,care_type_id,pat_id,status,own_id) values('$ar','$snname','$cemail','$ctype','$patient','0','$id')");
//return $query->result();
//return $query->num_rows();
	}
	
	function get_ownerdet($username)
    {
	    $query = $this->db->get_where('owner_table', array('owner_email' => $username),1);
	    //$this->db->where("owner_email",$username);
		//$this->db->where("own_passwd",$password);
		$query=$this->db->get("owner_table"); 
		  return $query->result();
		if( $query->num_rows() > 0 )
		{
			
		//return $query->row_array();
	    return $query->result();
		//return false;
		}
    }
	
   function getProfessionalsAll() {
        $this->db = $this->load->database('default', TRUE);  
        $this->db->select('us.*, pfl.*');
		$this->db->join('professionals pfl', 'pfl.professional_id  = us.id');
		$this->db->where('role', 'professional' );
        $query = $this->db->get('users us');
		//print_r($query->result());
		return $query->result();
  }
   function getCaregiverslist() {
        $this->db = $this->load->database('default', TRUE);  
       // $this->db->select();
		//$this->db->where('role', 'caregiver' );
        //$query = $this->db->get('users');
        //return $query->result();
		$query = $this->db->query("SELECT * FROM caregiver");			
		return $query->result();
		
		
		
    }
	
	
	
	
   function getCaregiversAll() {
        $this->db = $this->load->database('default', TRUE);  
         $this->db->select('us.*, pat.*');
		 $this->db->join('patients pat', 'pat.caregiver_id = us.id');
		$this->db->where('role', 'caregiver' );
        $query = $this->db->get('users us');
		//print_r($query->result() );
        return $query->result();
    }
	
	// get user by username
	function get_users_by_username( $username ) {
		$query = $this->db->get_where('users', array('username' => $username), 1);
		if( $query->num_rows() > 0 ) 
		return $query->row_array();
		return false;
	}
	
	// get user by id
	function get_users_by_id( $id=null ) {
		$query = $this->db->get_where('users', array('id' => $id), 1);
		if( $query->num_rows() > 0 ) 
		//return $query->row_array();
	    return $query->result();
		return false;
	}
	function get_users_name( $name ) {
		$query = $this->db->get_where('owner_table', array('owner_email' => $name), 1);
		if( $query->num_rows() > 0 ) 
		return $row=$query->row_array();
	    
		
	}
	
	function get_ajax_owner($email,$type) 
	{
		
		//$this->db->select();
      // $this->db->where('l_type', $type); 
      // $this->db->where('owner_email', $email); 
       //$query = $this->db->get('owner_table');
	   
	   $query = $this->db->query("select * from owner_table where owner_email='$email' and l_type='$type'");
       return $query->num_rows();
	
	}
	
	
	function get_own_care($cemail,$id) 
	{
		
		$this->db->select();
       $this->db->where('email', $cemail); 
       $this->db->where('own_id', $id); 
       $query = $this->db->get('caregiver');
       return $query->num_rows();
	
	}
	
	
	
function addmulpat($oid,$pname,$sname,$ownid) 
{
	
	$data=array(
	'own_licence'=>$oid,
    'name'=>$pname,
	'surname'=>$sname,
	'own_id'=>$ownid,
	);
		
	return $this->db->insert('patients', $data);
}
	
function insert_own_care($name,$sname,$cemail,$ctype,$patient,$id)	
	{
	$data=array(
	'name'=>$name,
    'surname'=>$sname,
	'email'=>$cemail,
	'care_type_id'=>$ctype,
	'pat_id'=>$patient,
	'own_id'=>$id,
	);
	return $this->db->insert('caregiver', $data);
	}
	
	
	
	
	
	
	
function get_con_rsa_lice($email,$lice)
	{
	
       $this->db->select();
       $this->db->where('licence_no', $lice); 
       $this->db->where('oemail', $email); 
       $query = $this->db->get('owner_licence');
       //return $query->result();
		return $query->num_rows();
		
	}
	
 function getpatient_details($oid,$pn,$spn,$lice)
 {
	   $this->db->select();
       $this->db->where('own_id', $oid); 
      $this->db->where('own_licence', $lice);
       $query = $this->db->get('patients');
       return $query->num_rows();
		
 }
 function getowner($email)
	{
       $this->db->select();
       $this->db->where('owner_email', $email); 
       //$this->db->where('own_id', $oid); 
       $query = $this->db->get('owner_table');
       //return $query->result();
	   //return $query->num_rows();
	   return $row=$query->row_array();
	}
	function getowner1($email)
	{
       //$this->db->select();
       //$this->db->where('owner_email', $email); 
       //$this->db->where('own_id', $oid); 
       //$query = $this->db->get('owner_table');
       //return $query->result();
	   //return $query->num_rows();
	   $query = $this->db->query("select * from owner_licence where oemail='$email' order by id desc");
	   return $row=$query->row_array();
	}

 function get_patient_details($oid)
 {
	$query = $this->db->query("select * from patients where own_id='$oid'"); 
	return $query->result();
 }

	
	
	function get_ajax_owner_licence($email,$lice) 
	{
	   $this->db->select();
       $this->db->where('licence_no', $lice); 
       $this->db->where('oemail', $email); 
       $query = $this->db->get('owner_licence');
       return $query->num_rows();
	
	}
	
	
	
	
	
	
	function get_users_lice( $name ) {
		$query = $this->db->get_where('owner_table', array('own_id' => $name), 1);
		if( $query->num_rows() > 0 ) 
		//return $row=$query->row_array();
	   return $data=$query->result();
		
	}
	function get_owner_email( $email ) {
		$query = $this->db->get_where('owner_table', array('owner_email' => $email), 1);
		if( $query->num_rows() > 0 ) 
		//return $row=$query->row_array();
	   return $data=$query->result();
		
	}
	
	
	
	
	
	
	

	function ownlogin($username,$password)
	{
		$this->db->where("owner_email",$username);
		$this->db->where("own_passwd",$password);
		$query=$this->db->get("owner_table"); 
		return $query->num_rows();
	}



function login1($username,$password)
	{
		$this->db->where("kname",$username);
		$this->db->where("kpasswd",$password);
		 $query=$this->db->get("koala_admin"); 
		return $query->num_rows();
		//$data=$query->result();
	}





	function login($username,$password)
	{
		$this->db->where("username",$username);
		$this->db->where("password",$password);
		$query=$this->db->get("users"); 
		
		
		
		if($query->num_rows()>0)
		{
			$data=$query->result();
			$user_id=$data[0]->id;
			if($data[0]->role=="caregiver")
			{
				$this->db->where("caregiver_id",$user_id);
				$result=$this->db->get("patients"); 
				$details= $result->result();
                $photo= $details[0]->photo;
				//print_r($result);
			}
			else if($data[0]->role=="professional")
			{
			    $this->db->where("professional_id ",$user_id);
				$result=$this->db->get("professionals"); 
				$details= $result->result();
                $photo= $details[0]->photo;
			}
			else
			{
			$photo= '';
			}
			foreach($query->result() as $rows)
			{
				//add all data to session
				$newdata = array(
				  'user_id'  => $rows->id,
				  'user_name'  => $rows->username,
				  'user_email'    => $rows->email,
				  'user_role'    => $rows->role,
				  'logged_in'  => TRUE,
				  'user_photo'  => $photo,
				);
			}
			 $this->session->set_userdata($newdata);
			
			 return true;
		}
			return false;
		}
	
	public function add_user()
	{   
		$data=array(
		'username'=>$this->input->post('username'),
		'email'=>$this->input->post('email'),
		'password'=>md5($this->input->post('password'))
		);
		$this->db->insert('users',$data);
	}
		
	public function updateowntype($id,$type)
	{
	$this->care_type= $type; 
	$this->db->update('owner_table', $this, array('own_id' => $id));	
		}
	//
	
	
	
	
	public function getpatient($owid)
	{
	  $this->db = $this->load->database('default', TRUE);  
	  $query = $this->db->query("select * from patients where own_id='$owid'");
    return $query->num_rows();
	}
	
	public function getpatient1($owid,$licence)
	{
	 $this->db = $this->load->database('default', TRUE);  
	 $query = $this->db->query("select * from patients where own_id='$owid' and own_licence='$licence'");
    return $query->num_rows();
	}
	
	
	
	public function get_owner($id)
	{
	  $this->db = $this->load->database('default', TRUE);  
      /*$this->db->where('own_id', $id);  
	  $query = $this->db->get('owner_table');	
	  return $query->result();
	  */
	  $query = $this->db->query("select * from owner_table where own_id='$id'");
/*   foreach ($query->result_array() as $row)
   {/*
    $row['own_licence'];
    $row['own_name'];
  
  $data=array(
	'own_id'=>$id,			 
	'email'=>$this->session->userdata('owner_email'),
	'passwd'=>$this->session->userdata('own_passwd'),
	'care_type_id'=>1,
	'name'=>$this->session->userdata('own_name'),
	'phone'=>$this->session->userdata('own_mobile'),
	); 
    }*/
	
	
	}
//============//////////// function about the patient insert and add owner,update owner,addpayment /////////////////=============================   
	function owner_insertpatient($id,$licence,$lat,$lon)
	{
	 $data=array(
	'own_id'=>$id,
	'own_licence'=>$licence,
	'name'=>$this->session->userdata('name'),
	'photo'=>$this->session->userdata('photo'),
    'surname'=>$this->session->userdata('surname'),
	'age'=>$this->session->userdata('age'),
	'gender'=>$this->session->userdata('gender'),
	'zipcode'=>$this->session->userdata('zipcode'),
	'city'=>$this->session->userdata('city'),
	'state'=>$this->session->userdata('state'),
	'address'=>$this->session->userdata('address'),
	'phone'=>$this->session->userdata('phone'),
	'phone2'=>$this->session->userdata('phone2'),
	'phone3'=>$this->session->userdata('phone3'),
	'contact1'=>$this->session->userdata('contact1'),
	'contact2'=>$this->session->userdata('contact2'),
	'contact3'=>$this->session->userdata('contact3'),
	'pathology'=>$this->session->userdata('pathology'),
	'allergies'=>$this->session->userdata('allergies'),
	'intolerances'=>$this->session->userdata('intolerances'),
	'note'=>$this->session->userdata('note'),
	'id_card'=>$this->session->userdata('qr_code'),
	'lat'=>$lat,
	'lon'=>$lon,
	'created'=>date("Y-m-d"),
	 );
	 return $this->db->insert('patients',$data);
    }
	
	
	public function add_session_user123($lat,$lon)
	{   

	$ly=$this->session->userdata('licence');
	if(!empty($ly))
	{
	$mly=$ly;	
	}
	else
	{
	$mly=$this->session->userdata('ltype');	
	}
	$lay=$this->session->userdata('ltype');
	if(!empty($lay))
	{
	$ly=$this->session->userdata('ltype');	
	}
	else
	{
		$ly="owner";
	}
	
	$data=array(
	'own_name'=>$this->session->userdata('uname'),
    'owner_email'=>$this->session->userdata('username'),
	'own_mobile'=>$this->session->userdata('omobile'),
	'own_address'=>$this->session->userdata('oaddress'),
	'own_passwd'=>md5($this->session->userdata('passwordvalue')),
    'own_ipaddress'=>$_SERVER['REMOTE_ADDR'],
	'own_date'=>date("Y-m-d"),
	'lat'=>$lat,
	'lon'=>$lon,
	'owner_type'=>$mly,
	'own_status'=>'inactive',
	'l_type'=>$ly,
	);
	
	$this->db->insert('owner_table',$data);
	return $id=$this->db->insert_id();
	
	}
	
	public function add_session_con_owner($lat,$lon)
	{ 
	
	$data=array(
	'own_name'=>$this->session->userdata('uname'),
    'owner_email'=>$this->session->userdata('username'),
	'own_mobile'=>$this->session->userdata('omobile'),
	'own_address'=>$this->session->userdata('oaddress'),
	'own_passwd'=>md5($this->session->userdata('passwordvalue')),
    'own_ipaddress'=>$_SERVER['REMOTE_ADDR'],
	'own_date'=>date("Y-m-d"),
	'owner_type'=>"business",
	'l_type'=>"Consortium",
	'lat'=>$lat,
	'lon'=>$lon,
	);
	
	$this->db->insert('owner_table',$data);
	return $id=$this->db->insert_id();
	
	}
	
	
	
	
	
	
	function in_owner_lice($id)
	{
	/*echo "<script>alert($id)</script>";*/
	$lice="koala".$id;
	$mdata=array(
	'own_id'=>$id,
    'licence_no'=>$lice,
	'oemail'=>$this->session->userdata('username'),
	'otype'=>$this->session->userdata('ltype'),
	);
	$this->db->insert('owner_licence',$mdata);	
	}
	
	function in_owner_lice1($id,$licence)
	{
		
	/*echo "<script>alert($id)</script>";*/
	$lice=$licence;
	$mdata=array(
	'own_id'=>$id,
    'licence_no'=>$lice,
	'oemail'=>$this->session->userdata('username'),
	'otype'=>$this->session->userdata('ltype'),
	);
	$this->db->insert('owner_licence',$mdata);	
	}
	
	
	public function insert_owner_rsa_con()
	{
	$data=array(
	'own_name'=>$this->session->userdata('uname'),
    'owner_email'=>$this->session->userdata('username'),
	'own_mobile'=>$this->session->userdata('omobile'),
	'own_address'=>$this->session->userdata('oaddress'),
	'own_passwd'=>md5($this->session->userdata('passwordvalue')),
    'own_ipaddress'=>$_SERVER['REMOTE_ADDR'],
	'own_date'=>date("Y-m-d"),
	'owner_type'=>$this->session->userdata('licence'),
	'l_type'=>$this->session->userdata('ltype'),
	);
	
	$this->db->insert('owner_table',$data);
	return $id=$this->db->insert_id();

	}
	
	public function update_owner_rsa_con()
	{
	$data=array(
	'own_address'=>$this->session->userdata('oaddress'),
	'own_passwd'=>md5($this->session->userdata('passwordvalue')),
	);
	$email=$this->session->userdata('username');
	//$this->db->insert('owner_table',$data);
	$this->db->update('owner_table', $data, array('owner_email' => $email));
	//return $id=$this->db->insert_id();
	}
	public function update_owner($id,$name,$mobile,$address,$status,$trns,$pay)
	{
     //'own_date'=>$date,
	$data=array(
	'own_name'=>$name,
	'own_mobile'=>$mobile,
	'own_address'=>$address,
	'own_status'=>$status,
	'payamount'=>$pay,
	'trno'=>$trns,
	);
	//$this->db->insert('owner_table',$data);
	$this->db->update('owner_table', $data, array('own_id' => $id));
	//return $id=$this->db->insert_id();
	}

   public function updateowner($id)
 {	
	    $this->own_licence ="koala".$id;
        
        $this->db->update('owner_table', $this, array('own_id' => $id));
	}
	
	
	public function  add_payment($id)
	{
	
	$data=array(
	'payment_type'=>$this->session->userdata('ctype'),
    'transaction_id'=>$this->session->userdata('cnumber'),
	'payment_amount'=>3000,
	'payment_date'=>date("Y-m-d"),
	'payment_ip'=>$_SERVER['REMOTE_ADDR'],
	'user_id'=>$id,
	);
	return $this->db->insert('payments',$data);	
			
	}
	
	
	
	
	public function updateownersta($id)
    {	
	$data=array(
	'own_status'=>"inactive",
    );
	 //$this->own_status ="inactive";
     $this->db->update('owner_table', $data, array('own_id' => $id));
	}
	
	function ownerdetail($id) {
		$this->db->where('own_id', $id);  
		$query = $this->db->get('owner_table');		
		return $query->result();

	}
	
	function patient_details($id) 
	{
		$this->db->where('id', $id);  
		$query = $this->db->get('patients');		
		return $query->result();

	}
	
	function care_details($id) 
	{
		$this->db->where('id', $id);  
		$query = $this->db->get('caregiver');		
		return $query->result();

	}
	
	
	
	
	
	
    public function add_session_user()
	{   
		$data=array(
			'username'=>$this->session->userdata('username'),
			'email'=> $this->session->userdata('email'),
			'password'=>md5($this->session->userdata('passwordvalue')),
			'role'=>$this->session->userdata('role'),
		);
		$this->db->insert('users',$data);
		$last_id=$this->db->insert_id();
		
		if($this->session->userdata('role')=='caregiver'){
		
			$data=array(
			'city'=>$this->session->userdata('city'),
			'state'=>$this->session->userdata('state'),
			'address'=>$this->session->userdata('address'),
			'name'=>$this->session->userdata('name'),
			'surname'=>$this->session->userdata('surname'),
			'age'=>$this->session->userdata('age'),
			'gender'=>$this->session->userdata('gender'),
			'zipcode'=>$this->session->userdata('zipcode'),
			'photo'=>$this->session->userdata('photo'),
			'phone'=>$this->session->userdata('phone'),
			'phone2'=>$this->session->userdata('phone2'),
			'phone3'=>$this->session->userdata('phone3'),
			'contact1'=>$this->session->userdata('contact1'),
			'contact2'=>$this->session->userdata('contact2'),
			'contact3'=>$this->session->userdata('contact3'),
			'pathology'=>$this->session->userdata('pathology'),
			'allergies'=>$this->session->userdata('allergies'),
			'intolerances'=>$this->session->userdata('intolerances'),
			'note'=>$this->session->userdata('note'),
			'id_card'=>$this->session->userdata('qr_code'),
			'caregiver_id'=>$last_id,
			);
			$this->db->insert('patients',$data);
			}	
		elseif($this->session->userdata('role')=='professional'){
		   $data=array(
			'name'=>$this->session->userdata('name'),
			'surname'=>$this->session->userdata('surname'),
			'age'=>$this->session->userdata('age'),
			'gender'=>$this->session->userdata('gender'),
			'zipcode'=>$this->session->userdata('zipcode'),
			'photo'=>$this->session->userdata('photo'),
			'phone'=>$this->session->userdata('phone'),
			'address'=>$this->session->userdata('address'),
			'city'=>$this->session->userdata('city'),
			'state'=>$this->session->userdata('state'),
			'qualification'=>$this->session->userdata('qualification'),
			'doctor_board_admitted_to'=>$this->session->userdata('admitted_to'),
			'from'=>$this->session->userdata('from'),
			'cv'=>$this->session->userdata('cv'),
			//'note'=>$this->session->userdata('note'),
			'professional_id'=>$last_id,
			);
		    $this->db->insert('professionals',$data);
		
		}
		
	}
    
	function add_profetional($lat,$lon)
	{       $data=array(
			'name'=>$this->session->userdata('name'),
			'surname'=>$this->session->userdata('surname'),
			'age'=>$this->session->userdata('age'),
			'gender'=>$this->session->userdata('gender'),
			'zipcode'=>$this->session->userdata('zipcode'),
			'photo'=>$this->session->userdata('photo'),
			'phone'=>$this->session->userdata('phone'),
			'address'=>$this->session->userdata('address'),
			'city'=>$this->session->userdata('city'),
			'state'=>$this->session->userdata('state'),
			'qualification'=>$this->session->userdata('qualification'),
			'doctor_board_admitted_to'=>$this->session->userdata('admitted_to'),
			'from'=>$this->session->userdata('from'),
			'cv'=>$this->session->userdata('cv'),
			'email'=>$this->session->userdata('pro_email'),
			'password'=>md5($this->session->userdata('passwordvalue')),
			'lat'=>$lat,
			'lon'=>$lon,
			
			//'note'=>$this->session->userdata('note'),
			//'professional_id'=>$last_id,
			);
		   $this->db->insert('professionals',$data);
		   return $id=$this->db->insert_id();
			}
			
			
	function update_prof_transation($pid,$item_transaction)
	{
		$this->trans_no = $item_transaction;
		$this->pay_status="Success";
        $this->pay_date = date("Y-m-d");
        $this->db->update('professionals', $this, array('id' => $pid));
	}
	
			
			
	
	function updateUser($id)
    {
         // please read the below note
        $this->email = $_POST['email'];
        $this->role = $_POST['role'];
        $this->db->update('users', $this, array('id' => $_POST['id']));
    }
	
	function professionalDetail($id) {
	  $this->db->where('id', $id);  
	  $query = $this->db->get('professionals');		
	  return $query->result();
	
	}
	//--------------admin details-----//
	function adminDetail($id) {
	  $this->db->where('id', $id);  
	  $query = $this->db->get('users');	
	return $query->result();
	
	}
	//--------------end--------------//
	
	function caregiverDetail($id) {
	  $this->db->where('id', $id);  
	  $query = $this->db->get('patients');		
	  return $query->result();
	
	}
	
	
	function updateprofessional($id)
    {
         // please read the below note
        $this->name = $_POST['name'];
        $this->surname = $_POST['surname'];
        $this->age = $_POST['age'];
        $this->gender = $_POST['gender'];
        $this->city = $_POST['city'];
        $this->address = $_POST['address'];
        $this->qualification = $_POST['qualification'];
        $this->photo = $_POST['photo'];
        $this->from = $_POST['from'];
        $this->doctor_board_admitted_to = $_POST['doctor_board_admitted_to'];
        $this->db->update('professionals', $this, array('id' => $_POST['id']));
    }
	function updatepatient($id)
	{
	
	 $data=array(
	'name'=>$this->session->userdata('name'),
	'photo'=>$this->session->userdata('photo'),
    'surname'=>$this->session->userdata('surname'),
	'age'=>$this->session->userdata('age'),
	'gender'=>$this->session->userdata('gender'),
	'zipcode'=>$this->session->userdata('zipcode'),
	'city'=>$this->session->userdata('city'),
	'state'=>$this->session->userdata('state'),
	'address'=>$this->session->userdata('address'),
	'phone'=>$this->session->userdata('phone'),
	'phone2'=>$this->session->userdata('phone2'),
	'phone3'=>$this->session->userdata('phone3'),
	'contact1'=>$this->session->userdata('contact1'),
	'contact2'=>$this->session->userdata('contact2'),
	'contact3'=>$this->session->userdata('contact3'),
	'pathology'=>$this->session->userdata('pathology'),
	'allergies'=>$this->session->userdata('allergies'),
	'intolerances'=>$this->session->userdata('intolerances'),
	'note'=>$this->session->userdata('note'),
	 );
	
	$this->db->update('patients', $data, array('id' => $id));
	
	
	
	
		
	}
	
	//-----------Admin update---------//
	function updateAdmin($id)
    {
		
		$this->username = $_POST['username'];
        $this->password = $_POST['password'];
		$this->db->update('users', $this, array('id' => $_POST['id']));
    }
	//----------end----------//
		function updatecaregiver($id)
    {
		$this->db = $this->load->database('default', TRUE);
        $this->db->select();
        $this->name = $_POST['name'];
        $this->surname = $_POST['surname'];
        $this->age = $_POST['age'];
        $this->gender = $_POST['gender'];
        $this->photo = $_POST['photo'];
        $this->city = $_POST['city'];
        $this->state = $_POST['state'];
        $this->address = $_POST['address'];
        $this->phone = $_POST['phone'];
        $this->pathology = $_POST['pathology'];
        $this->allergies = $_POST['allergies'];
        $this->intolerances = $_POST['intolerances'];
        $this->db->update('patients', $this, array('id' => $_POST['id']));
    }
	function deleteprofessional($id) {
		$this->db->delete('professionals', array('id' => $id));

	}
	function deletecaregivers($id) {
		$this->db->delete('patients', array('id' => $id));

	}
    // delete user by id
	function deleteUsers($id) {
	    $this->db->delete('users', array('id' => $id));;
	}
	//------------------------Periods---------//
	
	function periodDetail($id) {
			$this->db->where('id', $id);  
			$query = $this->db->get('periods');		
			return $query->result();

			}
	function updatePeriods($id) {

			//$this->patient_id   = $_POST['patient_id']; 
			$this->sleep_time = $_POST['sleep_time'];
			$this->wake_time = $_POST['wake_time'];
			$this->created = $_POST['created'];
			$this->db->update('periods', $this, array('id' => $_POST['id']));
			}
	function deleteperiods($id) {
			$this->db->delete('periods', array('id' => $id));

			}

	//---------end-----------------//

	//------------medicince module-------------//
	
	
	public function medicine_count()
	{
		return $this->db->count_all('medicine');
	}
	
	
	function getMedicines($per_pg,$offset) {
		$this->db->order_by('med_id','desc'); 
        $this->db->select();
	    $query = $this->db->get('medicine',$per_pg,$offset);
		return $query->result();
	}
	
	function diets_count()
	{
	return $this->db->count_all('diets');	
	}
	
	
	
		function getdiets($per_pg,$offset) 
		{
			/*$this->db->select('d.*, p.name'); //or whatever you're selecting
			$this->db->join('patients p', 'd.pat_id = p.id');
			//$this->db->where('pp.entry_id', $entry_id);
			$query = $this->db->get('diets d');
			return $query->result();
			*/
			//$query = $this->db->query("select * from diets order by id desc limit '$per_pg','$offset'");
			$this->db->order_by('id','desc'); 
			$this->db->select();
	        $query = $this->db->get('diets',$per_pg,$offset);
			return $query->result();
			
		}
		
		function count_parameter()
	{
	return $this->db->count_all('parameter_name');	
	}
		
		
		function getparameters1($per_pg,$offset) 
		{
			//$this->db->select('pp.*, p.name'); //or whatever you're selecting
			//$this->db->join('patients p', 'pp.patient_id = p.id');
			//$this->db->where('pp.entry_id', $entry_id);
			//$query = $this->db->get('parameters pp');
			//return $query->result();
		//$query = $this->db->query("SELECT * FROM parameter_name");			
		//return $query->result();
		    $this->db->order_by('id','desc'); 
			$this->db->select();
	        $query = $this->db->get('parameter_name',$per_pg,$offset);
		    return $query->result();
		}
	
	
	function count_activity()
		{
			return $this->db->count_all('activities');	
		}
	
	
	
	function activitylist($per_pg,$offset)
{
			  // $query = $this->db->query("select * from activities ");
			 //return $query->result();
			 //return $query->num_rows();
	        $this->db->order_by('act_id','desc'); 
			$this->db->select();
	        $query = $this->db->get('activities',$per_pg,$offset);
		    return $query->result();
	 
	 
}
	
		
		
		
	function getpatients($oid)
	{
		/*
	  $query = $this->db->get('patients');
	  $this->db->where('own_licence', 33);  
	  return $query->result();
	  */
	  // $this->db = $this->load->database('default', TRUE);  
       $this->db->select();
       //$this->db->where('own_licence', $lid); 
	   $this->db->where('own_id', $oid); 
       $query = $this->db->get('patients');
	   return $query->result();
		
		
	}
	
	function getcaregiver($oid)
	{
		/*
	  $query = $this->db->get('patients');
	  $this->db->where('own_licence', 33);  
	  return $query->result();
	  */
	  // $this->db = $this->load->database('default', TRUE);  
       $this->db->select();
       //$this->db->where('own_licence', $lid); 
	   $this->db->where('own_id', $oid); 
       $query = $this->db->get('caregiver');
	   return $query->result();
		
		
	}	
	
	function medicinebyname($name) {
	  $this->db->where('med_name', $name);  
	  $query = $this->db->get('medicine');		
	 // return $query->result();
	  return $query->num_rows();
	}
	
	
	function addMedicines() {
		
	    $this->med_name   = $_POST['medicine_name']; 
		$this->medformat = $_POST['medicine_format'];
		$this->med_note = $_POST['note'];
		$this->med_quantity = $_POST['quantity'];
		$this->med_purchase_date = $_POST['purchase_date'];
		$this->med_expirartion_date = $_POST['expiration_date'];
		$this->med_dose = $_POST['dosage'];
		$this->med_meal_time = $_POST['meal_time'];
		$this->med_price = $_POST['price'];
		
		$this->db->insert('medicine', $this);
	}
	
	
	
	
   	function medicineDetail($id) {
	  $this->db->where('med_id', $id);  
	  $query = $this->db->get('medicine');		
	  return $query->result();
	
	}
	
	function updateMedicine($id) {
	  
	    $this->med_name   = $_POST['medicine_name']; 
		$this->medformat = $_POST['medicine_format'];
		$this->med_note = $_POST['note'];
		$this->med_quantity = $_POST['quantity'];
		$this->med_purchase_date = $_POST['purchase_date'];
		$this->med_expirartion_date = $_POST['expiration_date'];
		$this->med_dose = $_POST['dosage'];
		$this->med_meal_time = $_POST['meal_time'];
		$this->med_price = $_POST['price'];
		
        $this->db->update('medicine', $this, array('med_id' => $_POST['id']));
	}
	function deleteMedicines($id,$evnid,$time)
	{
		
			//$this->db->delete('notifications', array('event_type_id' => $id,'sub_event_id' => $evnid));event_type_id='$evnid' and sub_event_id='$id'
			$query = $this->db->query("delete from notifications where note_id='$id' and  note_time<'$time'");
	}
	
   //---------profesional Details---------//
    function getProfessionalsAlls() {
        $this->db = $this->load->database('default', TRUE);  
        $this->db->select();
		 $this->db->where('professional_id', $id); 
        $query = $this->db->get('professionals');
        return $query->result();
    }
	
	
	//---------caregiver details-----------//
	function get_caregivers_by_id( $id=null ) {
	   $this->db = $this->load->database('default', TRUE);  
       $this->db->select();
       $this->db->where('caregiver_id', $id); 
       $query = $this->db->get('patients');
	   return $query->result();
	}
	//---------professional details-----------//
    function get_professionals_by_id( $id=null ) {
	
	   $this->db = $this->load->database('default', TRUE);  
       $this->db->select();
       $this->db->where('id', $id); 
       $query = $this->db->get('professionals');
	   return $query->result();
	}
	
	//------------Search module----------//
	function caregiverList() {
	 $this->db->where('role', 'caregiver');
	 $query = $this->db->get('users');
	 return $query->result();
	
	}
	
	function getprof($email)
	{
		$query = $this->db->query("select * from professionals where email='$email'");
	   // return $row=$query->row_array();
	   
	   return $row=$query->result();
	   
	}
	
	
	function caregiverSearch() {
		$this->db->like('username', $_POST['search']);
		$this->db->where('role', 'caregiver');
		$query = $this->db->get('users');
		return $query->result();
	
	}
	function professionalList() {
	 $this->db->where('role', 'professional');
	 $query = $this->db->get('users');
	 return $query->result();
	
	}
	function professionalSearch() {
		$this->db->like('username', $_POST['search']);
		$this->db->where('role', 'professional');
		$query = $this->db->get('users');
		return $query->result();
	
	}
	
	
	function getSates() {
        $this->db = $this->load->database('default', TRUE);  
        $this->db->select();
        $query = $this->db->get('users');
        return $query->result();
		
    }
	
	//-------Edit profile--------//
	function editProfile($id,$nfile,$cv,$lat,$lng) {
		//$this->email   = mysql_real_escape_string($_POST['email']);
		$this->name   =mysql_real_escape_string($_POST['name']); 
		$this->surname = mysql_real_escape_string($_POST['surname']);
		$this->age = mysql_real_escape_string($_POST['age']);
		$this->gender = mysql_real_escape_string($_POST['gender']);
		$this->city = mysql_real_escape_string($_POST['city']);
		$this->zipcode =mysql_real_escape_string($_POST['zipcode']);
		$this->state = mysql_real_escape_string($_POST['state']);
		$this->cv = $cv;
		$this->lat =$lat;
		$this->lon=$lng;
		$this->address = mysql_real_escape_string($_POST['address']);
		//$this->phone = mysql_real_escape_string($_POST['phone']);
		$this->qualification = mysql_real_escape_string($_POST['qualification']);
		$this->doctor_board_admitted_to = mysql_real_escape_string($_POST['admitted_to']);
		$this->from = mysql_real_escape_string($_POST['from']);
		$this->atime=mysql_real_escape_string($_POST['atime']);
		@$this->photo = $nfile;
		
		//$this->session->set_userdata('user_photo',$this->session->userdata('photo'));
		//die('ok');
       return $this->db->update('professionals', $this, array('id' => $id));
		
	}
	
	function editProfile1($id,$nfile,$cv,$lat,$lng) {
		//$this->email   = mysql_real_escape_string($_POST['email']);
		$this->name   =mysql_real_escape_string($_POST['name']); 
		$this->surname = mysql_real_escape_string($_POST['surname']);
		//$this->age = mysql_real_escape_string($_POST['age']);
		//$this->gender = mysql_real_escape_string($_POST['gender']);
		$this->city = mysql_real_escape_string($_POST['city']);
		$this->zipcode =mysql_real_escape_string($_POST['zipcode']);
		$this->state = mysql_real_escape_string($_POST['country']);
		$this->cv = $cv;
		$this->lat =$lat;
		$this->lon=$lng;
		$this->address = mysql_real_escape_string($_POST['address']);
		$this->phone = mysql_real_escape_string($_POST['phone']);
		//$this->phone = mysql_real_escape_string($_POST['phone']);
		$this->qualification = mysql_real_escape_string($_POST['qualification']);
		$this->doctor_board_admitted_to = mysql_real_escape_string($_POST['admitted_to']);
		//$this->from = mysql_real_escape_string($_POST['from']);
		//$this->atime=mysql_real_escape_string($_POST['atime']);
		@$this->photo = $nfile;
		
		//$this->session->set_userdata('user_photo',$this->session->userdata('photo'));
		//die('ok');
       return $this->db->update('professionals', $this, array('id' => $id));
		
	}
	
	
	
	
	
	//---------------edit caregivers profile---//
	//-------Edit profile--------//
	function editcaregiverProfile($id) {
	
		///$this->username   =$_POST['username']; 
		//$this->role   =$_POST['role'];
		//$this->db->update('users', $this, array('id' => $_POST['id']));		
		$this->name   =$_POST['name']; 
		$this->surname = $_POST['surname'];
		$this->age = $_POST['age'];
		$this->gender = $_POST['gender'];
		$this->zipcode = $_POST['zipcode'];
		//$this->city = $_POST['city'];
		$this->state = $_POST['state'];
		$this->address = $_POST['address'];
		$this->phone = $_POST['phone'];
		$this->phone2 = $_POST['phone2'];
		$this->phone3 = $_POST['phone3'];
		$this->contact1 = $_POST['contact1'];
		$this->contact2 = $_POST['contact2'];
		$this->contact3 = $_POST['contact3'];
		$this->pathology = $_POST['pathology'];
		$this->allergies = $_POST['allergies'];
		$this->intolerances = $_POST['intolerances'];
		$this->note = $_POST['note'];
		$this->photo = $this->session->userdata('photo');
		if($_POST['qr_code']){
		 $this->id_card=$_POST['qr_code'];
		}
		//print_r($_POST);
		//die();
		
		$this->db->update('patients', $this, array('caregiver_id' => $_POST['id']));
	}
	
	
	
	//------end------------//
	//------------------------Update foods---------------//
	
			
	function updateFood($id) {

				$this->food_name   = $_POST['food_name']; 
				$this->food_type = $_POST['food_type'];
				$this->created = $_POST['created'];
				$this->db->update('foods', $this, array('id' => $_POST['id']));
			}	
	function foodDetail($id) {
				$this->db->where('id', $id);  
				$query = $this->db->get('diets');		
				return $query->result();

			}
	function deletefoods($id) {
			$this->db->delete('foods', array('id' => $id));

			}
			
	//--------------------------end---------------------------//

	//------------------------Update Parameters---------------//
	
			
	function updateparameters($id,$pname,$num) 
	{
				$data=array(
			'pname'=>$pname,
			'ncolumns'=>$num,
			
			      );
	$this->db->update('parameter_name', $data, array('id' => $id));
			}


function update_paracolumns($id,$name)
{
	$data=array(
			'para_id'=>$id,
			'paraname'=>$name,
      );
	  
	  $this->db->update('parameter_columns', $data, array('para_id' => $id));
	}
			
			
			
				
	function parametersDetail($id) {
		$this->db->where('id', $id);  
		$query = $this->db->get('parameter_name');	
		//$query = $this->db->query("select *  from parameter_name as pname,parameter_columns as pcol  where pname.id='$id' and pcol.para_id='$id'");
		
			
		return $query->result();

	}
	function deleteparameters($id) 
	{
		//$this->db->delete('parameter_name', array('id' => $id));
		//$this->db->delete('parameter_columns', array('para_id' => $id));
	$query = $this->db->query("delete from notifications where note_id='$id'");
	
	
	
	
	} 
	function patientDetail($id) {
	  $this->db->where('id', $id);  
	  $query = $this->db->get('patients');		
	  return $query->result();
	
	}
	function careDetail($id) {
	  $this->db->where('id', $id);  
	  $query = $this->db->get('caregiver');		
	  return $query->result();
	
	}
function care_associatedpatients($cid)	
{

$query =$this->db->query("SELECT *,pat.id as pid FROM patients as pat, list_caregiver as clist where pat.id=clist.pat_id and clist.careid='$cid'");
			
return $query->result();

}
	
		
	/* serach patients   */	
	function serach_patients($oid,$srch)
	{ 
	 
	    $oid=$_POST['oid'];
		$data=explode(' ', $_POST['keyword']);
		$searchkey = 'where ';
		foreach($data as $temp){
			if($temp !='')
			{
				$searchkey .= '`name` like "%'.mysql_real_escape_string($temp).'%"  and ';
				//$searchkey .= '`surname` like "%'.mysql_real_escape_string($temp).'%"  and ';
				
			
			}
		} 
		if($searchkey =='where ')
		{
			$searchkey = 'where  1=1';
		}else{
			$searchkey  = substr($searchkey,0,-3);
		}
		
		$query = $this->db->query("SELECT * FROM patients where name  like '%$srch%' and own_id='$oid'");			
				
		return $query->result();

	
	
	 
	 
	}	
	
	
	function serach_caregiver($oid,$srch)
	{
	  //$oid=$_POST['oid1'];
		/*$data=explode(' ', $_POST['keyword']);
		$searchkey = 'where ';
		foreach($data as $temp){
			if($temp !='')
			{
				$searchkey .= '`name` like "%'.mysql_real_escape_string($temp).'%"  and ';
				//$searchkey .= '`surname` like "%'.mysql_real_escape_string($temp).'%"  OR ';
				
			
			}
		} 
		if($searchkey =='where ')
		{
			$searchkey = 'where  1=1';
		}else{
			$searchkey  = substr($searchkey,0,-3);
		}
		*/
		
		$query = $this->db->query("SELECT * FROM caregiver where name like '%$srch%' and own_id='$oid'");			
				
		return $query->result();

	
	
	}
	
	
	
		
			
		
    //-- get professional all--//
    function getProfessionals($pid) {  
	$query = $this->db->query("SELECT prf.id as pfid,pnet.netstatus,prf.name,prf.age,prf.gender,prf.photo,pnet.rid,pnet.sid FROM professionals as prf left join professional_networks_prf as pnet on (prf.id=pnet.rid or prf.id=pnet.sid) and ( pnet.rid='$pid' or pnet.sid='$pid') where prf.id!='$pid' group by prf.id");
	
	
		
		
		return $query->result();
		
		
		
	}
	
	  function getProfessionals2($cid) {  
		$query = $this->db->query("SELECT * FROM professional_networks where sid='$cid'");			
		return $query->result();
	}
	function get_patient_licence($pid)
	{
		$query = $this->db->query("SELECT * FROM patients where id='$pid'");			
		return $query->result();
	}
	
	function get_patient_type($lice)
	{
		$query = $this->db->query("SELECT * FROM owner_licence where licence_no='$lice'");			
		return $query->result();
	}
	
	
	function getProfessionals11($cid) {  
		
		$query = $this->db->query("SELECT prf.id as pfid,pnet.netstatus,prf.name,prf.age,prf.gender,prf.photo FROM professionals as prf left join professional_networks as pnet on prf.id=pnet.rid and pnet.sid='$cid' group by prf.id");			
				
		return $query->result();
		
		
		
	}
	
	
	
//--  search Drug   --//	
	function serach_drug()
	{
		$data=explode(' ', $_POST['keyword']);
		$searchkey = 'where ';
		foreach($data as $temp){
			if($temp !='')
			{
				$searchkey .= '`med_name` like "%'.mysql_real_escape_string($temp).'%"  OR ';
			}
		} 
		if($searchkey =='where ')
		{
			$searchkey = 'where  1=1';
		}else{
			$searchkey  = substr($searchkey,0,-3);
		}
		$query = $this->db->query("SELECT * FROM medicine $searchkey");			
				
		return $query->result();

	}
	
	
	function serach_deits()
	{
		
		$data=explode(' ', $_POST['keyword']);
		$searchkey = 'where ';
		foreach($data as $temp){
			if($temp !='')
			{
				$searchkey .= '`food_name` like "%'.mysql_real_escape_string($temp).'%"  OR ';
			}
		} 
		if($searchkey =='where ')
		{
			$searchkey = 'where  1=1';
		}else{
			$searchkey  = substr($searchkey,0,-3);
		}
		$query = $this->db->query("SELECT * FROM diets $searchkey");			
				
		return $query->result();

	
	}
	
	
	
	function serach_activity()
	{
	
		$data=explode(' ', $_POST['keyword']);
		$searchkey = 'where ';
		foreach($data as $temp){
			if($temp !='')
			{
				$searchkey .= '`act_name` like "%'.mysql_real_escape_string($temp).'%"  OR ';
			}
		} 
		if($searchkey =='where ')
		{
			$searchkey = 'where  1=1';
		}else{
			$searchkey  = substr($searchkey,0,-3);
		}
		$query = $this->db->query("SELECT * FROM activities $searchkey");			
				
		return $query->result();

	
	
	}
	

function serach_owner()
	{
	
		$data=explode(' ', $_POST['keyword']);
		$searchkey = 'where ';
		foreach($data as $temp){
			if($temp !='')
			{
				$searchkey .= '`own_name` like "%'.mysql_real_escape_string($temp).'%"  OR ';
			}
		} 
		if($searchkey =='where ')
		{
			$searchkey = 'where  1=1';
		}else{
			$searchkey  = substr($searchkey,0,-3);
		}
		$query = $this->db->query("SELECT * FROM owner_table $searchkey");			
				
		return $query->result();

	
	
	}

function serach_owner_date($date1,$date2)
	{
	
		
		$query = $this->db->query("SELECT * FROM owner_table where own_date between '$date1' and '$date2' ");			
				
		return $query->result();
	
	}
function serach_prof_owner_date($date1,$date2)	
	{
		$query = $this->db->query("SELECT * FROM professionals where professionals.from between '$date1' and '$date2' ");			
				
		return $query->result();
	}


function serach_caregiver_owner_date($date1,$date2)	
	{
		$query = $this->db->query("SELECT * FROM caregiver where caregiver.from between '$date1' and '$date2' ");			
				
		return $query->result();
	}

function serach_carenew_owner_date($date1,$date2)	
	{
	$query = $this->db->query("SELECT * FROM caregiver where caregiver.from between '$date1' and '$date2' ");			
	return $query->result();
	}




	
	
function serach_profnew_owner_date($date1,$date2)	
	{
		$query = $this->db->query("SELECT * FROM professionals where professionals.from between '$date1' and '$date2' ");			
				
		return $query->result();
	}	
	
	
	
function getnewsProfessionals()
{
	$query = $this->db->query("SELECT * FROM professionals");			
    return $query->result();
}


 function searchProfessionals1($key,$pid) 
 {  
//$query = $this->db->query("SELECT prf.id as pfid,pnet.netstatus,prf.name,prf.age,prf.gender,prf.photo,pnet.rid,pnet.sid FROM professionals as prf left join professional_networks_prf as pnet on (prf.id=pnet.rid or prf.id=pnet.sid) and ( pnet.rid='5' or pnet.sid='5') where prf.id!='5' and prf.name like '%$key%' group by prf.id ");

$query = $this->db->query("SELECT prf.id as pfid,pnet.netstatus,prf.name,prf.age,prf.gender,prf.photo,pnet.rid,pnet.sid FROM professionals as prf left join professional_networks_prf as pnet on (prf.id=pnet.rid or prf.id=pnet.sid) and ( pnet.rid='$pid' or pnet.sid='$pid') where prf.id!='$pid' and prf.name like '%$key%' group by prf.id ");




			
return $query->result();

}




	
	
	//-- search professional --//
    function searchProfessionals($key,$cid) { 
		/*$data=explode(' ', $_POST['keyword']);
		$searchkey = 'where ';
		foreach($data as $temp){
			if($temp !=''){
				$searchkey .= '`name` like "%'.mysql_real_escape_string($temp).'%"  OR ';
				//$searchkey .= '`surname` like "%'.mysql_real_escape_string($temp).'%"  OR ';
				//$searchkey .= '`zipcode` like "%'.mysql_real_escape_string($temp).'%"  OR ';	
			}
		} 
		if($searchkey =='where ')
		{
			$searchkey = 'where  1=1';
		}else{
			$searchkey  = substr($searchkey,0,-3);
		}*/
		
		
		$query = $this->db->query("SELECT prf.id as pfid,pnet.netstatus,prf.name,prf.age,prf.gender,prf.photo,pnet.netstatus as status FROM professionals as prf left join professional_networks as pnet on prf.id=pnet.rid and pnet.sid='$cid' where prf.name like '%$key%' group by prf.id");			
				
		return $query->result();
		
		//$query = $this->db->query("SELECT * FROM professionals where name like '%$key%'");			
				
		//return $query->result();

		/*	
        $this->db->like('name', $_POST['keyword']);		
        $this->db->like('surname', $_POST['keyword']);		
		$query = $this->db->get('professionals');
		return $query->result(); */
	}
	
    //-----Get States---//
    
	function getStates() {
        $this->db = $this->load->database('default', TRUE); 
        $this->db->select();
        $query = $this->db->get('countries');
        return $query->result();
    }
	
	///////////////////////////////////////////
    //---------caregiver details-----------//
	function getCaregivers_by_id( $id=null ) {
	   $this->db = $this->load->database('default', TRUE);  
       $this->db->select();
       $this->db->where('id', $id); 
       $query = $this->db->get('patients');
	   return $query->result();
	}
	//---------professional details-----------//
    function getProfessionals_by_id( $id=null ) {
	
	   $this->db = $this->load->database('default', TRUE);  
       $this->db->select();
       $this->db->where('id', $id); 
       $query = $this->db->get('professionals');
	   return $query->result();
	}
	
	function deleteactiviy($id)
{
$query = $this->db->query("delete  from notifications where note_id='$id'");
//$this->db->delete('activities', array('act_id' => $id));


   // return $query->result();
}

function updateactivity($id) {
if(!empty($_FILES['attached']['name']))
{
$file=time().$_FILES['attached']['name'];
				$dis="activity/";
				$dis=$dis.$file;
				move_uploaded_file($_FILES['attached']['tmp_name'],$dis);
}
else
{
	$file=$_POST['attached'];
}
	            $this->act_name   = $_POST['act_name']; 
				$this->act_details = $_POST['actdetails'];
				$this->act_attached = $file;
				$this->upated_date = date("Y-m-d");
				//$this->act_dayof_week=$_POST['dateofweek'];
				//$this->db->insert('activities', $this);
				$this->db->update('activities', $this, array('act_id' => $_POST['id']));

			}	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
   //deleting the the patient data 
	function deletepatient($id) 
	{
	$this->db->delete('patients', array('id' => $id));
	} 
	
}

