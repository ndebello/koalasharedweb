<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Front extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		//$this->load->library('user_agent');
		$this->load->library('form_validation');
		$this->load->model('user');
		$this->load->library('email');
		$this->load->model('admin');
	    $this->load->model('patient');
		
   $this->data='';
	}
	
	public function test_register3()
	{
	 $this->load->view('front/test_register3', $this->data);	
	}
	
	
	function carete_pdf()
	{
		$this->load->library('cezpdf');
		$this->load->helper('pdf');
		
		prep_pdf(); // creates the footer for the document we are creating.

		$db_data[] = array('name' => 'Jon Doe', 'phone' => '111-222-3333', 'email' => 'jdoe@someplace.com');
		$db_data[] = array('name' => 'Jane Doe', 'phone' => '222-333-4444', 'email' => 'jane.doe@something.com');
		$db_data[] = array('name' => 'Jon Smith', 'phone' => '333-444-5555', 'email' => 'jsmith@someplacepsecial.com');
		
		$col_names = array(
'name' => 'Name',
'phone' => 'Phone Number',
'email' => 'E-mail Address'
		);
		
		$this->cezpdf->ezTable($db_data, $col_names, 'Contact List', array('width'=>550));
		$this->cezpdf->ezStream();
		
		
		
		
	}
	
	
	
	
	
	
	
	
	//owner login funtion is start hear
	public function owner_login()
	{
	$this->form_validation->set_rules('own_name', 'Username', 'trim|required');
	$this->form_validation->set_rules('own_passwd', 'Password', 'trim|required|min_length[4]|max_length[15]');
	
	 $username=$this->input->post('username');
	 $password=md5($this->input->post('password'));
	 
	 
	 
	 if(!empty($username) and !empty($password))
	 {
	 $result=$this->user->ownlogin($username,$password);
	
	 if($result>0)
	 {
		  $this->data['flash']='';
		redirect('/admin/index/', 'refresh'); 
	 }
	  if(@$result<=0)
	 {
		 $this->data['flash']='Username and password does not match !'; 
		 
		  //$this->load->view('front/home', $this->data);
		  redirect('/',  $this->data);
	 }
	    
	 }
	 
	 
	
	 
	
	}
	
	
	public function index()
	{
		$this->load->helper('date');
		
		$now = time();
$timezone = 'UM8';
$local_timestamp = '1140153693';
$daylight_saving = TRUE;

//echo $gmt_conversion = convert_to_gmt($local_timestamp, $timezone, $daylight_saving);

//echo gmt_to_local($timestamp, $timezone, $daylight_saving); 
	
	   // echo date("H:s");
	 
    //echo 'date_default_timezone_set: ' . date_default_timezone_get() . '<br />';

//echo get_date_from_gmt ( date( 'Y-m-d H:i:s', $my_timestamp ), get_option('date_format') . ' - '. get_option('time_format') );

	  
	$this->form_validation->set_rules('own_name', 'Username', 'trim|required');
	$this->form_validation->set_rules('own_passwd', 'Password', 'trim|required|min_length[4]|max_length[15]');
	if(isset($_POST['submit']))
	{
	 $username=$this->input->post('username');
	 $pass=$this->input->post('password');
	 $password=md5($this->input->post('password'));
	  $selectype=$this->input->post('selectype');
	  @$_REQUEST['remember'];
	 if(@$_REQUEST['remember'] == "on") {    // if user check the remember me checkbox        
setcookie('remember_me', $username, time()+60*60*24*100, "/");
setcookie('remember_pass', $pass, time()+60*60*24*100, "/");
    }
    else {   // if user not check the remember me checkbox
           setcookie('remember_me', 'gone', time()-60*60*24*100, "/"); 
		   setcookie('remember_pass', 'gone', time()-60*60*24*100, "/");
    }
	}
	 
	 if(!empty($username) and !empty($password) and $selectype=="owner")
	 {
		 
		 
		// $this->data['user'] = $this->user->getCaregiversAll($id);
		 $use=$this->user->get_users_name($username);
		$status=$use['own_status'];
		$type=$use['l_type'];
		$owner_id=$use['own_id'];
		//$owner_lic=$use['own_licence'];
		 
		 
	if($status=="active")
	{
	 $result=$this->user->ownlogin($username,$password);
	}
	
	 if($status=="active" and $result>0 and $type!="RSA" and $type!="Consortium")
	 {
		 /*echo "<script>alert('$type')</script>";*/
		  $this->session->set_userdata('owner_name', $username);
		 
		  $this->data['flash']='';
		redirect('/front/after_owner_login/'.$owner_id, 'refresh'); 
	 }
	  if($status=="active" and $result>0 and ($type=="RSA"))
	 {
		 $this->session->set_userdata('owner_name', $username);
		$this->data['flash']='';
		redirect('/front/after_consortiam_login/'.$owner_id, 'refresh'); 
 
	 }
	 
	 if($status=="active" and $result>0 and ($type=="Consortium"))
	 {
		 $this->session->set_userdata('owner_name', $username);
		$this->data['flash']='';
		redirect('/front/after_consortiam_login/'.$owner_id, 'refresh'); 
 
	 }
	 
	 
	 
	 
	  if($status=="inactive")
	 {
		 
		 
		 
		 $this->data['flash']='User status is in active !'; 
		  //$this->load->view('front/home', $this->data);
		  
	 }
	 if(@$result<=0)
	 {
		 //echo "sdfasdfasf";
		 $this->data['flash']='Username and password does not match !'; 
		  //$this->load->view('front/home', $this->data);
		  
	 }

	 }
	
	 if(!empty($username) and !empty($password) and $selectype=="caregiver")
	{
		$use=$this->user->get_caregiver_tologin($username,$password);
		if($use<=0)
		{
$this->data['flash']='Please enter correct username or password of caregiver!';
		}
		else
		{
	    $this->session->set_userdata('owner_care', $username);
		//$this->load->view('home/search', $this->data);
		redirect('/caregiver/search/', 'refresh');
		}
		
		
		
	}
	 if(!empty($username) and !empty($password) and $selectype=="professional")
	{
		
		
		 $use=$this->user->get_profdetails($username,$password);

		//$this->load->view('home/search', $this->data);
		if($use<=0)
		{
$this->data['flash']='Please enter correct username or password of professional!';
		
		}
		else
		{
 $this->session->set_userdata('owner_prof', $username);
		//$this->load->view('home/search', $this->data);
		redirect('/prof/search/', 'refresh');
		}
		
		
	}
	
	
	//$this->data['flash']='Username and password does not match !'; 
	
	 $this->load->view('front/home', $this->data);
	


/*$this->email->from('ali.m@bravemount.com', 'Your Name');
$this->email->to('mahamoodphp@gmail.com'); 
$this->email->cc('ahmeawebdev2011@gmail.com'); 
$this->email->bcc('ali@brightstartech.com'); 

$this->email->subject('Email Test');
$this->email->message('Testing the email class.');	

$this->email->send();

echo $this->email->print_debugger();
*/	
	
	
	
	
	}
	
	public function after_rsa_login()
	{
		 //$use=$this->user->get_users_lice($oid);
		 //$lic=$use['own_licence'];
		/* echo "<script> alert('$lic') </script>";*/
		 //$this->data['patients']=$this->user->getpatients($lic,$oid);
		 //$this->load->view('admin/list_medicine', $this->data);
		
		 $this->load->view('auth/after_rsa_login', $this->data);
	}
    public function after_consortiam_login($oid)
	{
		
		 $email=$this->session->userdata('owner_name');
	  	 if(empty($email))
		{ 
redirect('/');
		}
	

       $this->data['ownerdetails']=$this->user->get_owner_email($email);
		
		
		
		 $use=$this->user->get_users_lice($oid);
		// $lic=$use['own_licence'];
		/* echo "<script> alert('$lic') </script>";*/
		 $this->data['patients']=$this->user->getpatients($oid);
		  $this->data['caregiver']=$this->user->getcaregiver($oid);
		 //$this->load->view('admin/list_medicine', $this->data);
		$this->load->view('auth/after_consortiam_login', $this->data);
	}
    public function after_owner_login($oid)
	{
		 $this->data['ownerdetails']=$this->user->get_users_lice($oid);
		  $this->data['ownerdetails'][0]->own_id;
		  $email=$this->session->userdata('owner_name');
		 //$lic=$use['own_licence'];
		/* echo "<script> alert('$lic') </script>";*/
		 $this->data['patients']=$this->user->getpatients($oid);
		 $this->data['caregiver']=$this->user->getcaregiver($oid);
		 	 
		 //$this->load->view('admin/list_medicine', $this->data);
		 $this->load->view('auth/after_owner_login', $this->data);
	}
	
	/*  Search Patients is start hear       */
	function serachpatient()
	{ 
	//echo $id;
		if($_POST)
		{ 

if($_POST['form_type']== 'patient_search')
		{
$oid=$_POST['oid'];
$this->session->set_userdata('search_keyword1',$_POST['keyword']);
$srch=$_POST['keyword'];
 // @$ctype=$_POST['ctype'];
//getPatients searchPatients
$this->data['patients']= $this->user->serach_patients($oid,$srch);
//$this->data['professionals']= $this->user->getProfessionals();

		$str='';
		if(count($this->data['patients'])>0)
		{

 $i=0;
		  //foreach($patients as $patient)
		   foreach($this->data['patients'] as $patient)
		  {
    
 $pth=$patient->photo;
$noimg="profile_pic.jpg";
//$ps="select SELECT * FROM patients T1 INNER JOIN professional_networks T2 ON T1.id = T2.patid"; 
  ?>
        
        
		<li class="odd">
<div class="img-thumb">
<?php if(!empty($pth)) { ?>
<img src="<?php echo base_url(); ?>uploads/<?php echo $patient->photo; ?>" alt="pat img"  width="60" height="60"/>
<?php } else { ?>
     <img src="<?php echo base_url(); ?>img/img-holder.jpg" alt="pat img"  width="60" height="60"/>   
        <?php } ?>
</div>
<div class="pat-desc-cont">
<h5><a href="#"><?php echo $patient->name; ?> </a></h5>
<p><?php echo $patient->surname; ?> -  <?php echo $patient->age; ?> </p>
<span> <?php echo $patient->gender; ?>  </span>
</div>
		</li>
<?php  

		} 
		}
		else
		{
echo "<tr align='center'><td colspan='6' align='center'> No Records Found   </td></tr>";
		}
		
		
		
		
		}
     }
		
		
	}
	
	function serachcaregiver()
	{
		if($_POST)
		{ 

if($_POST['form_type']== 'care_search')
		{
$oid=$_POST['oid1'];
$this->session->set_userdata('search_keyword2',$_POST['keyword']);
  $srch=$_POST['keyword'];
 // @$ctype=$_POST['ctype'];
//getPatients searchPatients
$this->data['caregiver']= $this->user->serach_caregiver($oid,$srch);
//$this->data['professionals']= $this->user->getProfessionals();

		$str='';
		if(count($this->data['caregiver'])>0)
		{

 $i=0;
		  //foreach($patients as $patient)
		   foreach($this->data['caregiver'] as $patient)
		  {
    
 $pth=$patient->photo;
$noimg="profile_pic.jpg";
//$ps="select SELECT * FROM patients T1 INNER JOIN professional_networks T2 ON T1.id = T2.patid"; 
  ?>
        
        
		<li class="odd">
<div class="img-thumb">
<?php if(!empty($pth)) {   ?>
<img src="<?php echo base_url(); ?>uploads/<?php echo $patient->photo; ?>" alt="pat img"  width="60" height="60"/>
<?php } else { ?>
<img src="<?php echo base_url(); ?>img/img-holder2.png" alt="pat img"  width="60" height="60"/>
<?php } ?>
</div>
<div class="pat-desc-cont">
<h5><a href="#"><?php echo $patient->name; ?> </a></h5>
<p><?php echo $patient->surname; ?> -  <?php echo $patient->age; ?> </p>
<span> <?php echo $patient->city; ?>  </span>
</div>
		</li>
<?php  

		} 
		}
		else
		{
echo "<tr align='center'><td colspan='6' align='center'> No Records Found   </td></tr>";
		}
		
		
		
		
		}
     }
	}
	
	
function getownet_ascare($pid)
	{
$email=$this->session->userdata('owner_name');		
$this->data['ownerdetails']=$this->user->get_owner_email($email);
$id=$this->data['ownerdetails'][0]->own_id;
$this->data['pdetails']=$this->user->get_owner_caretype($pid,$id);
$octype=$this->data['pdetails'][0]->owner_caretype;		
if(empty($octype))
{
	echo "0";
}
else
{
	echo "1";
}
		
	}
	
	
	
	
	
	
	function addcaregivers()
	{ 
	 
	   $email=$this->session->userdata('owner_name');
	   
	   
	   
	   
	  /*echo "<script> alert('$email')</script>";*/
	 
	 if(empty($email))
		{ 
redirect('/');
		}
	

       $this->data['ownerdetails']=$this->user->get_owner_email($email);
	   
	   $id=$this->data['ownerdetails'][0]->own_id;
	   $lice_type=$this->data['ownerdetails'][0]->own_licence_type;
	   $this->data['pdetails']=$this->user->get_patient_details($id);
	   $opasswd=$this->data['ownerdetails'][0]->own_passwd;
	    @$lat= substr(constant("lat"),0,9);
        @$lon= substr(constant("lon"),0,9);
	   
	   if($_POST)
	   {
		 
			$mail = new PHPMailer();
			$mail->IsSMTP();
			$mail->SMTPAuth   = true; 
			$mail->SMTPSecure = "ssl"; 
			$mail->Port       = 465;
			$mail->Host       = "smtp.yandex.com";
			$mail->Username   = "notification@Koala.Care";
			$mail->Password   = "No1L@S@pp1@mo";
			$mail->SetFrom('notification@Koala.Care', 'Koala Team');
			$mail->Subject = "Caregiver Login Details"; //subject      

			$ar=$_POST['cname'];
			$snname=$_POST['sname'];
			$cemail=$_POST['cemail'];
			$ctype=$_POST['ctype'];
			$patient=$_POST['patient'];
			@$owtype=$_POST['owtype'];



		$ct=count($ar);
		$p=0;
		for($j=0;$j<$ct;$j++)
		{
			if($ctype[$j]==1)
			{
			$p++;	
			}
		}
		
		
		  
		          


		if($p<=2)
		{
		
		for($i=0;$i<$ct;$i++)
		{ 
		
		//echo "if(!empty($ar[$i])  and !empty($cemail[$i]) and !empty($ctype[$i]) and !empty($patient[$i]))";
		
		if(!empty($ar[$i])  and !empty($cemail[$i]) and !empty($ctype[$i]) and !empty($patient[$i]))
		{ 
		
		 $ctype[$i];
		 $patid=$patient[$i];
		$this->data['patientlicence']=$this->user->get_patient_licence($patid);
		 $liceid=$this->data['patientlicence'][0]->own_licence;
		$this->data['plicencetype']=$this->user->get_patient_type($liceid);
		$lice_type=$this->data['plicencetype'][0]->licencetype;
		@$pname=$this->data['plicencetype'][0]->name;
		
		$s1="select * from list_caregiver where care_type_id=1 and pat_id='$patid'";
		$e1=mysql_query($s1) or die(mysql_error());
		$n1=mysql_num_rows($e1);
		
		$s2="select * from list_caregiver where care_type_id!=1 and pat_id='$patid'";
		$e2=mysql_query($s2) or die(mysql_error());
		$n2=mysql_num_rows($e2);
	
		
		if($owtype=="yes")
		{
			$pus="update patients set owner_caretype='$ctype[$i]' where own_id='$id' and id='$patid'";
			$pue=mysql_query($pus) or die(mysql_error());
		}
		
		
		
		
		if($lice_type!="Extended" or empty($lice_type))
{
		 $limit=5;
		$mlit=3;
}
if($lice_type=="Extended")
{
		$limit=10;
		$mlit=8;
}

		 $mc="select * from list_caregiver where pat_id='$patid'";
		$me=mysql_query($mc) or die(mysql_error());
		 $mn=mysql_num_rows($me);
		
		if($mn<$limit)
		{   
		 
$mn=count($ctype[$i]);
$cr="select * from caregiver where email='$cemail[$i]'";
$ce=mysql_query($cr) or die(mysql_error());
$mcr=mysql_fetch_array($ce);
$cn=mysql_num_rows($ce);
if($cn<=0)
{
		//echo "if(($n1!=2) && ($n2<3))";
		if($n1<2 and $ctype[$i]==1)
		{ 
		/*if($cemail[$i]==$email)
		{
		$us="update owner_table set care_type='$ctype[$i]' where own_id='$id'";
		$ue=mysql_query($us) or die(mysql_error());

		  $is="insert into caregiver (name,surname,email,care_type_id,pat_id,status,own_id,passwd,lat,lon) values('$ar[$i]','$snname[$i]','$cemail[$i]','$ctype[$i]','$patient[$i]','0','$id','$opasswd','$lat','$lon')";
		}*/
		
		 $is="insert into caregiver (name,surname,email,care_type_id,pat_id,status,own_id,lat,lon) values('$ar[$i]','$snname[$i]','$cemail[$i]','$ctype[$i]','$patient[$i]','0','$id','$lat','$lon')";
		
		$ie=mysql_query($is) or die(mysql_error());
		$cid=mysql_insert_id();
		
$mc="insert into list_caregiver (careid,own_id,care_type_id,pat_id) value ('$cid','$id','$ctype[$i]','$patient[$i]')";		
$me=mysql_query($mc) or die(mysql_error());	
		if($ie)
{
$body = "
<html>
<head>
<title>HTML email</title>
</head>
<body>
<table>
<tr>
<th>Name:</th>
<td>$ar[$i]</td>
</tr>
<tr>
<th>Email:</th>
<td>$cemail[$i]</td>
</tr>
<tr>
<th>lick the below link to activate your account </th>
<td> <a href='".base_url().'auth/careregister/'.$cid."'> click here </a> </td>
</tr>
</table>
</body>
</html>
";

           @$body = eregi_replace("[\]",'',$body);
           $mail->MsgHTML($body);
   $to = $cemail[$i];
//$subject = "Caregiver Registration Details";
$mail->AddAddress($to, "Test Recipient"); 


if ($mail->Send()) { 
		 $this->data['msg']="Caregiver Added Successfully ";
}

//Error
else { 
		 $this->data['msg']="Caregiver Added Successfully ";
} 
		

	
		$this->data['msg']="Caregiver Added Successfully ";
}



		 }
		if($n1==2 and $ctype[$i]==1)
		{
 $this->data['msg']="You Have Entered More Than 2 Primary Caregivers With This Patient ";
		}
		 $n2;
		if($n2<=$mlit and $ctype[$i]!=1)
		{ 
		 
		
		/*if($cemail[$i]==$email)
		{
		$us="update owner_table set care_type='$ctype[$i]' where own_id='$id'";
		$ue=mysql_query($us) or die(mysql_error());
		$is="insert into caregiver (name,surname,email,care_type_id,pat_id,status,own_id,passwd,lat,lon) values('$ar[$i]','$snname[$i]','$cemail[$i]','$ctype[$i]','$patient[$i]','0','$id','$opasswd','$lat','$lon')";
		}*/
		
		
		$is="insert into caregiver (name,surname,email,care_type_id,pat_id,status,own_id,lat,lon) values('$ar[$i]','$snname[$i]','$cemail[$i]','$ctype[$i]','$patient[$i]','0','$id','$lat','$lon')";
		
		
		
		$ie=mysql_query($is) or die(mysql_error());
		$cid=mysql_insert_id();
		
   $mc="insert into list_caregiver (careid,own_id,care_type_id,pat_id) value ('$cid','$id','$ctype[$i]','$patient[$i]')";		
	$me=mysql_query($mc) or die(mysql_error());
		
		
		
		if($ie)
{
		
	
          $body = "
			<html>
			<head>
			<title>HTML email</title>
			</head>
			<body>
			<table>
			<tr>
			<th>Name:</th>
			<td>$ar[$i]</td>
			</tr>
			<tr>
			<th>Email:</th>
			<td>$cemail[$i]</td>
			</tr>
			<tr>
			<th>lick the below link to activate your account </th>
			<td> <a href='".base_url().'auth/careregister/'.$cid."'> click here </a> </td>
			</tr>
			</table>
			</body>
			</html>
			";

          @$body = eregi_replace("[\]",'',$body);
           $mail->MsgHTML($body);
   $to = $cemail[$i];
//$subject = "Caregiver Registration Details";
       $mail->AddAddress($to, "Test Recipient"); 


if ($mail->Send()) { 
 $this->data['msg']="Caregiver Added Successfully ";
}

//Error
else { 
	 $this->data['msg']="Caregiver Added Successfully ";
} 

	//}
		//if($cemail[$i]!=$email)
		//{
//echo "You Have Successfully Registered As a".$ctype['$i']."Please Login with your old password";
		//}
}

		
		
		  
		  }
		
		}
		
		if($cn>0)
{
		
		 $careid=$mcr['id'];
		 $mcemail=$mcr['email'];		
   $mcs="select * from list_caregiver where careid='$careid' and own_id='$id' and pat_id='$patient[$i]'";		
  $mce=mysql_query($mcs) or die(mysql_error()); 		
  $mcn=mysql_num_rows($mce);
		
   if($mcn<=0) 		
	{	
		if($n1<2 and $ctype[$i]==1)
		{ 
		if($cemail[$i]==$email)
		{
		//$us="update owner_table set care_type='$ctype[$i]' where own_id='$id'";
		//$ue=mysql_query($us) or die(mysql_error());

		  //$is="insert into caregiver (name,surname,email,care_type_id,pat_id,status,own_id,passwd,lat,lon) values('$ar[$i]','$snname[$i]','$cemail[$i]','$ctype[$i]','$patient[$i]','0','$id','$opasswd','$lat','$lon')";
		}
		
		//$careid=$cr[''];
		
		
   $mc="insert into list_caregiver (careid,own_id,care_type_id,pat_id) value ('$careid','$id','$ctype[$i]','$patient[$i]')";		
	$me=mysql_query($mc) or die(mysql_error());	
		
		if($ie)
{
		//if($cemail[$i]!=$email)
	//{
		
		
		
	
//subject

//message
//$body = "This is a test message.";
$body = "
<html>
<head>
<title>HTML email</title>
</head>
<body>
<table>
<tr>
<th>Name:</th>
<td>$ar[$i]</td>
</tr>
<tr>

</tr>
<tr>
<th> you are assigned as $ctype[$i] to patient - $patient </th>

</tr>
</table>
</body>
</html>
";

           @$body = eregi_replace("[\]",'',$body);
           $mail->MsgHTML($body);
   $to = $mcemail;
//$subject = "Caregiver Registration Details";
$mail->AddAddress($to, "Caregiver Notification"); 


if ($mail->Send()) 
{ 
 $this->data['msg']="Caregiver Added Successfully ";
}

else { 
	 $this->data['msg']="Caregiver Added Successfully ";
} 
		

	//}
		//if($cemail[$i]!=$email)
		//{
//echo "You Have Successfully Registered As a".$ctype['$i']."Please Login with your old password";
		//}
		$this->data['msg']="Caregiver Added Successfully ";
}



		 }
		if($n1==2 and $ctype[$i]==1)
		{
$this->data['msg']="You Have Entered More Than 2 Primary Caregivers ";
		}
		 $n2;
		if($n2<=$mlit and $ctype[$i]!=1)
		{ 
		
		
		
   $mc="insert into list_caregiver (careid,own_id,care_type_id,pat_id) value ('$careid','$id','$ctype[$i]','$patient[$i]')";		
	$me=mysql_query($mc) or die(mysql_error());
		
		
		
		 $body = "
<html>
<head>
<title>HTML email</title>
</head>
<body>
<table>
<tr>
<th>Name:</th>
<td>$ar[$i]</td>
</tr>
<tr>

</tr>
<tr>
<th> you are assigned as $ctype[$i] to patient - $patient </th>

</tr>
</table>
</body>
</html>
";

           @$body = eregi_replace("[\]",'',$body);
           $mail->MsgHTML($body);
   $to = $mcemail;
//$subject = "Caregiver Registration Details";
$mail->AddAddress($to, "Caregiver Notification"); 


if ($mail->Send()) 
{ 
 $this->data['msg']="Caregiver Added Successfully ";
}

else { 
	 $this->data['msg']="Caregiver Added Successfully ";
} 

		
		
		 }
		
		
	}
	else
	{
		 $this->data['msg']="Trying To Enter Same Caregiver And Patient Again ";
	}
		
		
		}
		
		
		



		





		 
		}

		else { 	
		 $this->data['msg']="<font color='red'>you have already added the $limit caregivers please take extended licence (or) anether licence to add more caregivers</font>";
		 }
	
		
		
		
		 }
		
		else
		{
$this->data['msg']="<font color='red'>please fill the all input fields </font>";
		}
		}
		}
		/*else{ echo $msg="You have alredy two primary caregivers";}*/
		      
	   
		
		/*else { 	
		 $this->data['msg']="<font color='red'>you have already added the $limit caregivers please take extended licence (or) anether licence to add more caregivers</font>";
		 
		 }
*/		      
	   }
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
		$this->load->view('auth/add_caregiver', $this->data);
		
	 
	}
	
	function addcaregivers1($id)
	{
	   $email=$this->session->userdata('owner_name');
	  /*echo "<script> alert('$email')</script>";*/
	 
	 if(empty($email))
		{ 
redirect('/');
		}
		
	$ar=$_POST['cname'];
	$snname=$_POST['sname'];
	$cemail=$_POST['cemail'];
	$ctype=$_POST['ctype'];
    $patient=$_POST['patient'];
	$ct=count($ar);
    $p=0;
for($j=0;$j<$ct;$j++)
{
if($ctype[$j]==1)
{
	$p++;	
}
}
 $mn=$this->user->getallcaregivers($id);
if($mn<=5)
{
	$n1=$this->user->getmorecaregivers($id);
		echo "<script>alert('$n1')</script>";
if($p<=2)
{
	
for($i=0;$i<$ct;$i++)
{
	//$cr="select * from caregiver where email='$cemail[$i]'";
	//$ce=mysql_query($cr) or die(mysql_error());
	//$cn=mysql_num_rows($ce);
	$cn=$this->user->getcaregiversemail($cemail[$i]);

	if($cn<=0)
	{
if($n1<3)
{
$is="insert into caregiver (name,surname,email,care_type_id,pat_id,status,own_id) values('$ar[$i]','$snname[$i]','$cemail[$i]','$ctype[$i]','$patient[$i]','0','$id')";
//$ie=mysql_query($is) or die(mysql_error());
$ie=$this->user->insertintocaregiver($ar[$i],$snname[$i],$cemail[$i],$ctype[$i],$patient[$i],$id);

if($ie)
	{
		    $to = "{$cemail['$i']}";
$subject = "Caregiver Registration Details";
$message = "
<html>
<head>
<title>HTML email</title>
</head>
<body>
<table>
<tr>
<th>Name:</th>
<td>$ar[$i]</td>
</tr>
<tr>
<th>Email:</th>
<td>$cemail[$i]</td>
</tr>
<tr>
<th>lick the below link to activate your account </th>
<td> <a href='$_SERVER[REMOTE_HOST]/koala/auth/careregister/$cemail[$i]'> click here </a> </td>
</tr>
</table>
</body>
</html>
";
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
$headers .= "From: Koala \r\n";
		    $mail=mail($to,$subject,$message);
if($mail){
 /// echo 'Thank you !';
}else{
  //echo 'Try again !'; 
}
 $this->data['msg']="Caregiver Added Successfully Please check your email to verify your account";
	}

}
else {  $this->data['msg']=" You Have Already 5 caregivers ";  }
	}
	else
	{
		echo $this->data['msg']="email id of caregiver already present";
	}
	
}

}














}
		
		//$this->load->view('auth/add_caregiver');
		redirect('/front/addcaregivers/', 'refresh');
		
		
		
		
		
		
		
	}
	
	
	
	
	
	public function login()
	{
      $this->load->view('front/login');
	}
public function test_register()
	{
		$this->form_validation->set_rules('username', 'User Name', 'trim|required|min_length[4]|xss_clean|is_unique[users.username]');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|isunique');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[4]|max_length[15]');
		$this->form_validation->set_rules('cpassword', 'Password Confirmation', 'trim|required|matches[password]');

      if($this->form_validation->run() == FALSE)
		{
//$this->index();
		}
		else
		{
$this->user->add_user();
redirect('/auth/login/', 'refresh');
		}
		//$this->load->view('auth/register_form', $this->data);
		$this->load->view('front/test_register', $this->data);
	}
		public function send_mail ()
	{	
		if (isset($_POST['name'])) 
		{
 
$to = "saketghish054@gmail.com";
$subject = $_POST['subject'];
$message = "
<html>
<head>
<title>HTML email</title>
</head>
<body>
<table>
<tr>
<th>Name:</th>
<td>{$_POST['name']}</td>
</tr>
<tr>
<th>Email:</th>
<td>{$_POST['email']}</td>
</tr>
<tr>
<th>Subject:</th>
<td>{$_POST['subject']}</td>
</tr> 
<tr>
<th>Message</th>
<td>{$_POST['message']}</td>
</tr>
</table>
</body>
</html>
";
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
// More headers
$headers .= 'From: '.$_POST['email'] . "\r\n";
		
if(mail($to,$subject,$message,$headers)){
  echo 'Thank you !';
}else{
  echo 'Try again !'; 
}
		
		}
		die();
	
	//$this->load->view('front/home');040   
	}
	
	
	function details_patient($id)
	{
        
		$this->data['patient']= $this->user->patient_details($id);
		$this->load->view('auth/patient_details',$this->data );

	}
	
	function details_caregiver($id)
	{
        
		$this->data['caregiver']= $this->user->care_details($id);
		$this->load->view('auth/care_details',$this->data );

	}
	
	
	
	
	
	function details_updatepatient($id)
	{
        $this->data['states']= $this->user->getStates();
		$this->data['patient']= $this->user->patient_details($id);
		$this->load->view('auth/edit_patient_details',$this->data );

	}
	
	
	
	public function profile_edit()
	{
		$this->load->view('front/testprofile');
	}
	}