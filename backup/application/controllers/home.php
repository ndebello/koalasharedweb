<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Home extends CI_Controller
{
	function __construct()
	{
	  parent::__construct();

	  $this->load->library('form_validation');
	  //$this->load->library('security');
	  //$this->load->library('tank_auth');
	  //$this->lang->load('tank_auth');
	  
	  $this->load->helper('url');
	  $this->load->model('user');
	  $this->load->model('admin');
	  $this->load->model('patient');
	  $this->load->library('user_agent');
  
	  //$this->data['patient'] = $this->patient->get_patient_by_id($this->session->userdata('user_id'));

	  $this->data='';
	  $this->data['flash']='';
	  //if(!$this->session->userdata('user_id')){
	   	//redirect('/');
	  //}
	$username = $this->session->userdata('ausername');
	if(empty($username))
		{
	   redirect('/auth/login/');
		}
		  
	  
	}
   function index()
	{
	
	}
	function search_caregiver()
	{ 
	  if($_POST){
	    $this->data['caregivers']= $this->user->caregiverSearch();
	  }
	  else
	  {
	   $this->data['caregivers']= $this->user->caregiverList();
	  }
	  $this->load->view('home/caregiver_list', $this->data);
	}
	function search_professional()
	{ 
	  if($_POST)
	  {
	  $this->data['professionals']= $this->user->professionalSearch();
	  }else
	  {
	   $this->data['professionals']= $this->user->professionalList();
	  }
	  $this->load->view('home/professional', $this->data);
	}

	function professional_details($id=null)
	{
	 if (!($this->session->userdata('user_name')!="")) {
			redirect('/auth/login/');
		} else 
		{
		  $this->data['user'] = $this->user->getProfessionalsAll($id);
	      $this->load->view('home/professional_details', $this->data);
		 }
	}
	function caregivers_details($id=null)
	{
		if (!($this->session->userdata('user_name')!="")) {
		redirect('/auth/login/');
		} else {
		$this->data['user'] = $this->user->getCaregiversAll($id);
		$this->load->view('home/caregiver_details', $this->data);
		}
	}
	public function add_diets()
	{
	if($_POST){
				
			//$this->form_validation->set_rules('food_type', 'Food Type ', 'trim|required|xss_clean');
			$this->form_validation->set_rules('food_name', 'Food Name ', 'trim|required|xss_clean');
			//$this->form_validation->set_rules('diet_day', 'Diet Day ', 'trim|required|xss_clean');
			//$temp=$_POST['diet_day'];
			//$data=explode('/',$temp);
			//$result=$data[2].'-'.$data[0].'-'.$data[1];
			//$_POST['diet_day']=$result;
			if($this->form_validation->run() == TRUE)
			{	
			   $fname=$_POST['food_name'];
			   $ds="select * from diets where food_name='$fname'";
			   $de=mysql_query($ds) or die(mysql_error());
			   $dn=mysql_num_rows($de);
			   if($dn<=0)
			   {
				$this->patient->adddiets();
				$this->data['flash']="Diet has been added !";
			   }
			   else
			   {
				 $this->data['flash']="Trying To Enter Same Diet Name Please Enter Anether Name !";  
			   }
			   
			}
			else
			{
				$this->data['flash']="Unsuccesfull, Try again !";

			}	
		
		}
		
			$this->data['caregivers']= $this->user->getCaregiverslist();
			//$this->data['patient'] = $this->patient->get_patient_by_id($this->session->userdata('user_id'));
			
		
		//if($this->session->userdata('user_role')=='admin')
		//{
			$this->load->view('admin/add_diets', $this->data);
		//}
		
		/*else
		{
			$this->load->view('home/add_diets', $this->data);
		}*/

	}
	function list_diets()
	{	   
		$this->data['diets']=$this->patient->getdiets();
		$this->data['menu']="dit";
		$this->load->view('admin/list_diets', $this->data);
	}
	function edit_diets($id=null)
	{
	if($_POST)
		{
		$this->patient->updatediets($id);
			//redirect('/home/list_diets/');
			
			$this->data['flash']=" Updated Successfully ";
			
			
		}
			$this->data['diets']=$this->patient->dietsDetail($id);
			$this->load->view('admin/edit_diets', $this->data);

	}
	
	function details_diets($id=null)
	{
		$this->data['diets']= $this->patient->dietsDetail($id);
		$this->load->view('home/diet_details',$this->data );
	}

	function delete_diets($id)
	{
		$diets= $this->patient->dietsDetail($id);
		$img= "foods/".$diets[0]->food_image;
		//$base=base_url().$img;
		if(unlink($img))
		{
			//echo "ok";
		}
		else
		{
			//echo "Not";
		}
		
		
		$this->patient->deletediets($id);
		redirect('/home/list_diets/');
	}
	
	
/*==============================//////// add Owner,rsa,consortium ////////////================================  */	
	
	function add_rsa_owner()
	{
if($_POST){
		    $this->form_validation->set_rules('owner_name', 'Owner Name', 'trim|required|xss_clean');
			$this->form_validation->set_rules('owner_type', 'Owner Type', 'trim|required|xss_clean');
			$this->form_validation->set_rules('email_address', 'Email Address', 'trim|required|xss_clean');
			//$this->form_validation->set_rules('mobile_no', 'Mobile No', 'trim|required|xss_clean');
			//$this->form_validation->set_rules('address', 'address  ', 'trim|required|xss_clean');
			//$this->form_validation->set_rules('password', 'Password ', 'trim|required|xss_clean');
			$this->form_validation->set_rules('transation', 'transation Number ', 'trim|required|xss_clean');
			$this->form_validation->set_rules('pay', 'pay', 'trim|required|xss_clean');
			$this->form_validation->set_rules('created', 'Date  ', 'trim|required|xss_clean');
			//$temp=$_POST['created'];
			//$data=explode('/',$temp);
			//$result=$data[2].'-'.$data[0].'-'.$data[1];
			//$_POST['created']=$result;
			$type=$_POST['owner_type'];
			if($this->form_validation->run() == TRUE)
				{
					$email=$_POST['email_address'];
					$pn=$this->patient->getprofetion($email);
					$on=$this->patient->getowner($email);
					//$kn=$this->patient->getkoalaower($email);
					
					/*echo "<script>alert('$pn-$on-$kn')</script>";*/
					if($pn==0 and $on==0)
					{
						
						 $otype=$_POST['owner_type'];
						 $email=$_POST['email_address'];
						 
						@$this->session->set_userdata('email_address', $_POST['email_address']); 
						 
						 
						 $trans=$_POST['transation'];
						 $pay=$_POST['pay'];
						 $passwd=md5($_POST['password']);
						 $oname=$_POST['owner_name'];
						 @$lat= substr(constant("lat"),0,9);
                         @$lon= substr(constant("lon"),0,9);
						 $date=$_POST['created'];
						 if($otype=="RSA")
						 {
				$id=$this->patient->add_owner_rsa($otype,$email,$trans,$pay,$passwd,$oname,$lat,$lon,$date);
				$lc=$_POST['licence'];
				$num=count($_POST['licence']);
				
				for($i=0;$i<$num;$i++)
				{
				$mc=$lc[$i];
				@$lids.=$lc[$i]."<br/>";
				$this->patient->add_licence($id,$mc,$email,$type);
				}
						 }
						 
			if($otype=="Consortium")			 
				{
						/*echo "<script>alert('This Licence Id Already prescent')</script>";*/
					     $otype=$_POST['owner_type'];
						 $email=$_POST['email_address'];
						 @$this->session->set_userdata('email_address', $_POST['email_address']); 
						 $trans=$_POST['transation'];
						 $pay=$_POST['pay'];
						 $passwd=md5($_POST['password']);
						 $oname=$_POST['owner_name'];
						 
				$id=$this->patient->add_owner_cons($otype,$email,$trans,$pay,$passwd,$oname);
				$lc=$_POST['licence'];
				$num=count($_POST['licence']);
				
				for($i=0;$i<$num;$i++)
				{
				$mc=$lc[$i];
				$ms="select * from owner_con_licence where licence_no='$mc'";
				$me=mysql_query($ms) or die(mysql_error());
				$mn=mysql_num_rows($me);
				if($mn<=0)
				{
				@$lids.=$lc[$i]."<br/>";
				$this->patient->add_con_licence($id,$mc,$email,$type);
				}
				else
				{
					echo "<script>alert('This Licence Id Already prescent')</script>";
				}
				}
						 
				}
/*------------------------------------------------- the email function is start hear -----------------------------------------------------------*/
    		$this->data['flash']=" Owner  has been added successfully ! with licence id's please verify the email id for licence ids";
			$to = $this->session->userdata('username');
			$subject = "Licence Id's From Koala".$type;
			$message = "	
			<html>
			<head>
			<title>HTML email</title>
			</head>
			<body>
			<table>
			<tr>
			<th> Email: $email; </th>
			<td></td>
			</tr>
			<tr>
			<th> Your Licence ids: </th>
			<td>$lids</td>
			</tr> 
			</table>
			</body>
			</html>
			";
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
			// More headers
			$headers .= 'From: '." Koala Team " . "\r\n";
		if(mail($to,$subject,$message,$headers)){
			 // echo 'Thank you !';
			}
			else{
			  //echo 'Try again !'; 
			}
					 }
					else
					{
			$this->data['flash']=" Owner  Email id Already Present In Owner Records.";
			
			
			
					}
		        }
				
				}
				else
				{
					$this->data['flash']="Unsuccesfull, Try again !";
                    //$this->load->view('admin/add_owner_rsa', $this->data);
				}
		
		    $this->data['message']=$message;
			$this->load->view('admin/add_owner_rsa', $this->data);
		
		}
		
		//$this->data['caregivers']= $this->user->getCaregiverslist();
		//$this->data['patient'] = $this->patient->get_patient_by_id($this->session->userdata('user_id'));
		//if($this->session->userdata('user_role')=='admin'){
		 //$this->load->view('admin/add_parameters', $this->data);
		//}
		//else{
		 //$this->load->view('home/add_parameters', $this->data);
		//}

	
	
	
	
	function add_con_owner()
	{
		if (!($this->session->userdata('ausername')!="")) 
		{
			redirect('/auth/login/');
		}
		/*echo "<script>alert('ok oko ok ok ok ok ok ok ok ok ')</script>"     cname;*/
		if($_POST){

		    $this->form_validation->set_rules('owner_name', 'Owner Name', 'trim|required|xss_clean');
			$this->form_validation->set_rules('owner_lc', 'owner licence', 'trim|required|xss_clean');
			$this->form_validation->set_rules('cname', 'cname', 'trim|required|xss_clean');
			$this->form_validation->set_rules('email_address', 'Email Address', 'trim|required|xss_clean');
			$this->form_validation->set_rules('mobile_no', 'Mobile No', 'trim|required|xss_clean');
			$this->form_validation->set_rules('address', 'address  ', 'trim|required|xss_clean');
			$this->form_validation->set_rules('password', 'Password ', 'trim|required|xss_clean');
			$this->form_validation->set_rules('transation', 'transation Number ', 'trim|required|xss_clean');
			$this->form_validation->set_rules('pay', 'pay', 'trim|required|xss_clean');
			$this->form_validation->set_rules('created', 'Date  ', 'trim|required|xss_clean');
			//$temp=$_POST['created'];
			//$data=explode('/',$temp);
			//$result=$data[2].'-'.$data[0].'-'.$data[1];
			//$_POST['created']=$result;
			$oid=$_POST['owner_name'];
			if($this->form_validation->run() == TRUE)
				{
					$id=$this->patient->add_owner_cons();
					$this->user->updateownersta($oid);
					$this->patient->add_payment($id);
/*------------------------------------------------- the email function is start hear -----------------------------------------------------------*/
				 $this->data['flash']=" Owner  has been added successfully ! with licence id ".$_POST['owner_lc'];
				 //$this->l
			      $this->load->view('admin/add_owner_consortuim', $this->data);	
				
				}
				else
				{
					$this->data['flash']="Unsuccesfull, Try again !";
                    $this->load->view('admin/add_owner_consortuim', $this->data);

				}
		}
		
		//$this->data['caregivers']= $this->user->getCaregiverslist();
		//$this->data['patient'] = $this->patient->get_patient_by_id($this->session->userdata('user_id'));
		//if($this->session->userdata('user_role')=='admin'){
		 //$this->load->view('admin/add_parameters', $this->data);
		//}
		//else{
		 //$this->load->view('home/add_parameters', $this->data);
		//}
	
	
	}
	
	
	
	
	
	function add_parameters_admin()
	{ 
	  
		if($_POST){
			
			//$temp=$_POST['created'];
			//$data=explode('/',$temp);
			//$result=$data[2].'-'.$data[0].'-'.$data[1];
			echo $pname=$_POST['pname'];
			echo $numclu=$_POST['numclu'];
			echo $date=date("Y-m-d");
			$ps="select * from parameter_name where pname='$pname'";
			$pe=mysql_query($ps) or die(mysql_error());
			$pn=mysql_num_rows($pe);
			 if($pn<=0)
			 {
			 $id=$this->patient->addparameters($pname,$numclu,$date);
			
				$lc=$_POST['column'];	
				$num=count($_POST['column']);
				
				for($i=0;$i<$num;$i++)
				{
				$name=$lc[$i];
				$this->patient->add_paracolumns($id,$name);
				
				}	
					
			    $this->data['flash']="Parameters Has Been Added !";		
			 }
			 else
			 {
				$this->data['flash']=" You Are Trying To Enter Same  Parameter Name Again !";	 
			 }
				
		}
		$this->load->view('admin/add_parameters', $this->data);
		//}
		/*else{

		 $this->load->view('home/add_parameters', $this->data);
		}*/
	  
	}
	
	
	
	
	
	
	
	function add_parameters()
	{   
		if($_POST){
		$this->form_validation->set_rules('maximum_pressure', 'Maximum Pressure', 'trim|required|xss_clean');
			$this->form_validation->set_rules('minimum_pressure', 'Minimum Pressure  ', 'trim|required|xss_clean');
			$this->form_validation->set_rules('blood_glucose', 'Blood Glucose ', 'trim|required|xss_clean');
			$this->form_validation->set_rules('heart_rate', 'Heart Rate  ', 'trim|required|xss_clean');
			$this->form_validation->set_rules('weight', 'Weight  ', 'trim|required|xss_clean');
			//$this->form_validation->set_rules('patient_id', 'Patient Name ', 'trim|required|xss_clean');
			$this->form_validation->set_rules('created', 'Date  ', 'trim|required|xss_clean');
			//$temp=$_POST['created'];
			//$data=explode('/',$temp);
			//$result=$data[2].'-'.$data[0].'-'.$data[1];
			$_POST['created'];
			if($this->form_validation->run() == TRUE)
				{
					$this->patient->addparameters();
					$this->data['flash']="Parameters has been added !";
				}
					else
				{
					$this->data['flash']="Unsuccesfull, Try again !";

				}
		}
		$this->data['caregivers']= $this->user->getCaregiverslist();
		//$this->session->userdata('user_id')
		//$this->data['patient'] = $this->patient->get_patient_by_id(90);
		//if($this->session->userdata('user_role')=='admin'){
		 $this->load->view('admin/add_parameters', $this->data);
		//}
		/*else{

		 $this->load->view('home/add_parameters', $this->data);
		}*/
	   }
	
	
	
	
	
	
	

	function list_parameters()
	{	   
		$this->data['parameters']=$this->patient->getparameters();
		$this->data['menu']="pra";
		$this->load->view('admin/list_parameters', $this->data);
	}

	function add_periods()
	{

		if($_POST)
		{
				//$this->form_validation->set_rules('patient_id', 'Patient Name', 'trim|required|xss_clean');
				$this->form_validation->set_rules('sleep_time', 'Sleep Time ', 'trim|required|xss_clean');
				$this->form_validation->set_rules('wake_time', 'Wake Time ', 'trim|required|xss_clean');
				$this->form_validation->set_rules('created', 'Date ', 'trim|required|xss_clean');

				$temp=$_POST['created'];
				$data=explode('/',$temp);
				$result=$data[2].'-'.$data[0].'-'.$data[1];
				$_POST['created']=$result;

			if($this->form_validation->run() == TRUE)
			{
				$this->patient->addperiods();
				$this->data['flash']="Periods has been added !";
			}
				else
				{
					$this->data['flash']="Unsuccesfull, Try again !";
				}
		}
				$this->data['caregivers']= $this->user->getCaregiverslist();
				$this->data['patient'] = $this->patient->get_patient_by_id($this->session->userdata('user_id'));
			if($this->session->userdata('user_role')=='admin')
			{
				$this->load->view('admin/add_periods', $this->data);
			}
		else
		{
			$this->load->view('home/add_periods', $this->data);
		}

	}


	function list_periods()
			{	
				$this->data['periods']=$this->patient->getperiods();
				//print_r($this->data);
				$this->load->view('admin/list_periods', $this->data);
			}

		
    function edit_profile($id=null)
	{
		$temp= $this->user->get_users_by_id($this->session->userdata('user_id'));
		$temp= $temp[0]->id;
	  
		$this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('surname', 'Surname','trim|required|xss_clean');
		$this->form_validation->set_rules('age', 'Age','trim|required|xss_clean');
		$this->form_validation->set_rules('gender', 'Gender','trim|required|xss_clean');
			
		if($this->form_validation->run() == TRUE){       
			if ($_FILES["photo"]["error"] > 0) {
				//echo "Error: " . $_FILES["photo"]["error"] . "<br>";
			}else {
				//echo "Temp file: " . $_FILES["photo"]["tmp_name"] . "<br>";
				if (file_exists("upload/" . $_FILES["photo"]["name"])) {
					//$_FILES["photo"]["name"] . " already exists. ";
				} else {
					$file_name=uniqid().$_FILES["photo"]["name"];
					if(move_uploaded_file($_FILES["photo"]["tmp_name"],
					"uploads/" . $file_name)){
						$this->session->set_userdata('photo', $file_name);
						$this->session->set_userdata('user_photo', $file_name);
					}else{
						$this->session->set_userdata('photo',$this->session->userdata('user_photo'));
						$this->session->set_userdata('user_photo', 'no_profile.jpg');
					}
				}
			}

            //echo $this->session->userdata('photo');
			//die('ok');
			
			$res=$_POST['from'];
			$data=explode('/',$res);
			$result=$data[2].'-'.$data[0].'-'.$data[1];
			$_POST['from']=$result;
			$this->session->set_userdata('from', $_POST['from']);
			$this->user->editProfile($this->session->userdata('user_id'));
			$this->session->set_flashdata('flashMessage', 'Profile Edited Successfully !');
			redirect('/auth/index/');
		}
        $this->data['users'] = $this->user->get_users_by_id($temp);
		$this->data['professionals'] = $this->user->get_professionals_by_id($temp);
		//print_r($this->data['professionals']);
		//$this->data['states']= $this->user->getStates();
		// print_r( $this->data['states']);
		$this->load->view('home/professional_edit', $this->data);
	}
	
	
	function edit_caregivers($id=null)
	{
		  $result= $this->user->get_users_by_id($this->session->userdata('user_id'));
		  $result= $result[0]->id;

            
			$this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');
			$this->form_validation->set_rules('surname', 'Surname','trim|required|xss_clean');
			$this->form_validation->set_rules('age', 'Age','trim|required|xss_clean');
			$this->form_validation->set_rules('gender', 'Gender','trim|required|xss_clean');
			//$this->form_validation->set_rules('city', 'City','trim|required|xss_clean');
			$this->form_validation->set_rules('zipcode', 'Zipcode','trim|required|xss_clean');
			$this->form_validation->set_rules('state', 'State','trim|required|xss_clean');
			$this->form_validation->set_rules('address', 'Address','trim|required|xss_clean');
			
			
			
			if($this->form_validation->run() == TRUE)
			{
                 
				if ($_FILES["photo"]["error"] > 0) {
					//echo "Error: " . $_FILES["photo"]["error"] . "<br>";
				}else {
					//echo "Temp file: " . $_FILES["photo"]["tmp_name"] . "<br>";
					if (file_exists("upload/" . $_FILES["photo"]["name"])) {
						//$_FILES["photo"]["name"] . " already exists. ";
					} else {
					    $file_name=uniqid().$_FILES["photo"]["name"];

						if(move_uploaded_file($_FILES["photo"]["tmp_name"],
						"uploads/" . $file_name)){
						  $this->session->set_userdata('photo', $file_name);
						  $this->session->set_userdata('user_photo', $file_name);
						}else{
						  $this->session->set_userdata('photo',$this->session->userdata('user_photo'));
						  $this->session->set_userdata('user_photo', 'no_profile.jpg');
						};

					}
				}

				 $this->user->editcaregiverProfile($this->session->userdata('user_id'));
   
				 $this->session->set_flashdata('flashMessage', 'Profile Edited Successfully !');
				 
				 redirect('/auth/index/');
			}

			//$this->data['states']= $this->user->getStates();
			$this->data['caregivers'] = $this->user->get_caregivers_by_id($result);
			//print_r($this->data['caregivers']);
			$this->data['users'] = $this->user->get_users_by_id($result);
			$this->load->view('home/caregiver_edit', $this->data);

	   
	}
	
	
	public function fetch_patient_details($id=null){
		$carebyid= $this->patient->get_care_by_id($id);
		$pid=$carebyid[0]->pat_id;
		
		$patient_details= $this->patient->get_patient_by_id($pid);
		echo $patient_details[0]->name.','.$patient_details[0]->id;
	}
	
/*==========================================///////// get the owner information /////////======================================== */	
	public function fetch_owner($id)
	{
		$patient_details= $this->patient->get_owner($id);
		echo $patient_details[0]->own_licence;
	}
	
    public function add_foods(){
	 if($_POST)
	 {
		$this->form_validation->set_rules('food_name', 'Food Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('food_type', 'Food Type ', 'trim|required|xss_clean');
		$this->form_validation->set_rules('created', 'Created ', 'trim|required|xss_clean');
		
			$temp=$_POST['created'];
			$data=explode('/',$temp);
			$result=$data[2].'-'.$data[0].'-'.$data[1];
			$_POST['created']=$result;
			
			if($this->form_validation->run() == TRUE)
			{
				$this->patient->addfoods();
				$this->data['flash']="Diet has been added !";
			}
			else
			{
				$this->data['flash']="Unsuccesfull, Try again !";

			}	
		}

			$this->session->userdata('user_role');

			$this->data['foods']= $this->admin->getfoodsAll();
			if($this->session->userdata('user_role')=='admin')
			{
				$this->load->view('admin/add_foods', $this->data);
			}
			else
			{
				$this->load->view('home/add_foods', $this->data);
			}


    }


	public function list_foods()
	{	   
		$this->data['foods']=$this->patient->getfoods();
		$this->load->view('admin/list_foods', $this->data);
	}

	public function search()
	{
		
		
		if($_POST)
		{
			if($_POST['form_type']== 'patient_search')
			{
			    $this->session->set_userdata('search_keyword1',$_POST['keyword']);
			     
				$this->data['patients']= $this->patient->searchPatients();
				$this->data['professionals']= $this->user->getProfessionals();
				//---------xml write:start---------//
				$_xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
				$_xml .= "<markers>\n";
				foreach ($this->data['patients'] as $temp) {
					$zipcode= $temp->zipcode;
					$url = "http://maps.googleapis.com/maps/api/geocode/json?address=".$zipcode."&sensor=false";
					$details=file_get_contents($url);
					$result = json_decode($details,true);
					if(isset($result['results'][0])){
					$lat=$result['results'][0]['geometry']['location']['lat'];
					$lng=$result['results'][0]['geometry']['location']['lng'];
					}
					else{
					$lat =0;
					$lng =0;
					}
					$name = $temp->name; 
					$role = 'patient'; 
					$_xml .= "<marker>\n";    
					$_xml .= "<name>$name</name>\n";  
					$_xml .= "<role>$role</role>\n";  
					$_xml .= "<lat>$lat</lat>\n";
					$_xml .= "<lng>$lng</lng>\n";
					$_xml .= "</marker>"; 
				}
				$_xml .= "</markers>";
			    $myFile = "markers.xml";
			    $fh = fopen('public/'.$myFile, 'w') or die("can't open file");
			    fwrite($fh, $_xml);
			    fclose($fh);
			    //---------xml write:end---------//
			
			}elseif($_POST['form_type']== 'professional_search')
			{
			    $this->session->set_userdata('search_keyword2',$_POST['keyword']);
				
				$this->data['patients']= $this->patient->getPatients();
				$this->data['professionals']= $this->user->searchProfessionals();
				//---------xml write:start---------//
				$_xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
				$_xml .= "<markers>\n";
				foreach ($this->data['professionals'] as $temp) {
					$zipcode= $temp->zipcode;
					$url = "http://maps.googleapis.com/maps/api/geocode/json?address=".$zipcode."&sensor=false";
					$details=file_get_contents($url);
					$result = json_decode($details,true);
					if(isset($result['results'][0])){
					$lat=$result['results'][0]['geometry']['location']['lat'];
					$lng=$result['results'][0]['geometry']['location']['lng'];
					}
					else{
					$lat =0;
					$lng =0;
					}
					$name = $temp->name; 
					$role = 'professional'; 
					$_xml .= "<marker>\n";    
					$_xml .= "<name>$name</name>\n";  
					$_xml .= "<role>$role</role>\n";  
					$_xml .= "<lat>$lat</lat>\n";
					$_xml .= "<lng>$lng</lng>\n";
					$_xml .= "</marker>"; 
				}
				$_xml .= "</markers>";
				$myFile = "markers.xml";
				$fh = fopen('public/'.$myFile, 'w') or die("can't open file");
				fwrite($fh, $_xml);
				fclose($fh);
				//---------xml write:end---------//
			}
	      
	    }
		else
		{
			$this->data['patients']= $this->patient->getPatients();
			$this->data['professionals']= $this->user->getProfessionals();
			
			//---------xml write:start---------//
			$_xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
			$_xml .= "<markers>\n";
			foreach ($this->data['patients'] as $temp) {
				$zipcode= $temp->zipcode;
				$url = "http://maps.googleapis.com/maps/api/geocode/json?address=".$zipcode."&sensor=false";
				$details=file_get_contents($url);
				$result = json_decode($details,true);
				if(isset($result['results'][0])){
				$lat=$result['results'][0]['geometry']['location']['lat'];
				$lng=$result['results'][0]['geometry']['location']['lng'];
				}
				else{
				$lat =0;
				$lng =0;
				}
				$name = $temp->name; 
				$role = 'patient'; 
				$_xml .= "<marker>\n";    
				$_xml .= "<name>$name</name>\n";  
				$_xml .= "<role>$role</role>\n";  
				$_xml .= "<lat>$lat</lat>\n";
				$_xml .= "<lng>$lng</lng>\n";
				$_xml .= "</marker>"; 
			}
			foreach ($this->data['professionals'] as $temp) {
				$zipcode= $temp->zipcode;
				$url = "http://maps.googleapis.com/maps/api/geocode/json?address=".$zipcode."&sensor=false";
				$details=file_get_contents($url);
				$result = json_decode($details,true);

				if(isset($result['results'][0])){
				$lat=$result['results'][0]['geometry']['location']['lat'];
				$lng=$result['results'][0]['geometry']['location']['lng'];
				}
				else{
				$lat =0;
				$lng =0;
				}
				$name = $temp->name; 
				$role = 'professional'; 
				$_xml .= "<marker>\n";    
				$_xml .= "<name>$name</name>\n";  
				$_xml .= "<role>$role</role>\n";  
				$_xml .= "<lat>$lat</lat>\n";
				$_xml .= "<lng>$lng</lng>\n";
				$_xml .= "</marker>"; 
			}
			$_xml .= "</markers>";
			$myFile = "markers.xml";
			$fh = fopen('public/'.$myFile, 'w') or die("can't open file");
			fwrite($fh, $_xml);
			fclose($fh);
			//---------xml write:end---------//
		}

		$this->load->view('home/search', $this->data);
	}
	
	public function search2()
	{
		if($_POST)
		{
			if($_POST['form_type']== 'patient_search')
			{
			    $this->session->set_userdata('search_keyword1',$_POST['keyword']);
			   	$this->data['patients']= $this->patient->searchPatients();
				$this->data['professionals']= $this->user->getProfessionals();
				
					$str='';
					if(count($this->data['patients'])>0){
					 foreach($this->data['patients'] as $patient){
						 $str .='<div class="profiles">';
						 $str .='<img style="height:60px; width:60px;" src="'.base_url().'uploads/'.$patient->photo.'" alt="" />';
						 $str .='<h4>';
						 $str .='<a style="text-decoration:none;color:#000000" href="'.base_url().'home/caregiverdetails/'.$patient->id.'" >';
						 $str .=$patient->name;
						 $str .='</a>';	 
						 $str .= $patient->surname;
						 $str .=' <span> ';
						 $str .=$patient->age . 'anni';
						 $str .=' </span> ';
						 $str .=' <h6>Donec id elit non</h6>';
						 $str .=' <h6>mi porta</h6>';
						 $str .='</div>';
					 }
					}else{
						 $str .='<div class="profiles">';
						 $str .='<img src="'.base_url("img/no_record.gif").'" alt="" />';
						 $str .='</div>';
					}
	
			
			}elseif($_POST['form_type']== 'professional_search')
			{
			    $this->session->set_userdata('search_keyword2',$_POST['keyword']);
				
				$this->data['patients']= $this->patient->getPatients();
				$this->data['professionals']= $this->user->searchProfessionals();

					$str='';
					if(count($this->data['professionals'])>0){
					foreach($this->data['professionals'] as $professional){
					 $str .='<div class="profiles">';
					 $str .='<img style="height:60px; width:60px;" src="'.base_url().'uploads/'.$professional->photo.'" alt="" />';
					 $str .='<h4>';
					 $str .='<a style="text-decoration:none;color:#000000" href="'.base_url().'home/caregiverdetails/'.$professional->id.'" >';
					 $str .=$professional->name;
					 $str .='</a>';	 
					 $str .= $professional->surname;
					 $str .=' <span> ';
					 $str .=$professional->age . 'anni';
					 $str .=' </span> ';
					 $str .=' <h6>Donec id elit non</h6>';
					 $str .=' <h6>mi porta</h6>';
					 $str .='</div>';
					}
					}else{
					 $str .='<div class="profiles">';
					 $str .='<img src="'.base_url("img/no_record.gif").'" alt="" />';
					 $str .='</div>';
					}
					
			}

					//echo 'welcome !';		  
					 echo $str;		  
		  
		  
	    }

	}
	
	
	function professionaldetails($id=null)
	{
	    if (!($this->session->userdata('user_name')!="")) {
			redirect('/auth/login/');
		} else {
		  $this->data['user'] = $this->user->getProfessionals_by_id($id);
	      $this->load->view('home/professionaldetails', $this->data);
		 
		}
	}
	function caregiverdetails($id=null)
	{
		if (!($this->session->userdata('user_name')!="")) {
		redirect('/auth/login/');
		} else {
		$this->data['user'] = $this->user->getCaregivers_by_id($id);
		$this->load->view('home/caregiverdetails', $this->data);

		}
	}
    
	
}
