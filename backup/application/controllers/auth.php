<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Auth extends CI_Controller
{	
function __construct()
	{
		parent::__construct();
		$this->view_data['error'] = 'false';
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		//$this->load->library('security');
		//$this->load->library('tank_auth');
		//$this->lang->load('tank_auth');
		$this->load->model('user');
		$this->load->library('email');
	    $this->data='';
		
		
	}
	public function index()
	 {
		

		  $lat=$query['lat'];
         $lon=$query['lon'];
		 

		 
	 }
	   
		
			
		
	
	public function login()
		{    
	  $this->data='';
	  $this->form_validation->set_rules('ausername', 'Username', 'trim|required');
	  $this->form_validation->set_rules('apassword', 'Password', 'trim|required|min_length[4]|max_length[15]');
	  if($this->form_validation->run() == FALSE)
	  {
		  //$this->index();
		$this->load->view('auth/login_form');
		
	  }
	  else
	  {
	    echo $username=$this->input->post('ausername');
		$password=md5($this->input->post('apassword'));
     	$result=$this->user->login1($username,$password);
		//print_r();	
		if($result>0)
			{
			$this->session->set_userdata('ausername', $_POST['ausername']);
			/*
              $this->data['flash']='';
			  $username=$this->session->userdata('user_name');
			  $role=$this->session->userdata('user_role');
              if($role=='admin')
			  {
               	redirect('/admin/index/', 'refresh');
              }
			  else
			  {
                redirect('/auth/index/', 'refresh');
              }			  
		   */
		    // echo json_encode(array('statusid' => 1,'message' => "koala Admin Login Successfully",'response' =>true));
		     //$this->load->view('front/test_register', $this->data);
		  	 redirect('/admin/index/', 'refresh');
		   
		   }
		else
			{
			$this->data['flash']='Username or password does not match !';			
			$this->load->view('auth/login_form', $this->data);
			//echo json_encode(array('statusid' => 0,'message' => "Username or password does not match !",'response' =>false));	  
			}
	  }			
		 		
	  }
	  
	  
	public function logout()
	   {
		/*$newdata = array(
		'user_id'   =>'',
		'user_name'  =>'',
		'user_email'     => '',
		'logged_in' => FALSE,
		);*/
		$this->session->unset_userdata("owner_care");
		$this->session->sess_destroy();
		redirect('/', 'refresh');
		
	   }
	/**      Generate reset code (to change password) and send it to user**/
	function forgot_password()
	{
	if ($this->tank_auth->is_logged_in()) {					// logged in
			redirect('');
		} elseif ($this->tank_auth->is_logged_in(FALSE)) {		// logged in, not activated
			redirect('/auth/send_again/');
		} else {
			$this->form_validation->set_rules('login', 'Email or login', 'trim|required|xss_clean');
			$data['errors'] = array();
			if ($this->form_validation->run()) {				// validation ok
				if (!is_null($data = $this->tank_auth->forgot_password(
						$this->form_validation->set_value('login')))) {
					$data['site_name'] = $this->config->item('website_name', 'tank_auth');
					// Send email with password activation link
					$this->_send_email('forgot_password', $data['email'], $data);
				$this->_show_message($this->lang->line('auth_message_new_password_sent'));

				} else 
				{
					$errors = $this->tank_auth->get_error_message();
					foreach ($errors as $k => $v)	$data['errors'][$k] = $this->lang->line($v);
				}
			}
			$this->load->view('auth/forgot_password_form', $data);
		}
		}
		
	/** Replace user password (forgotten) with a new one (set by user)**/
	
	function reset_password()
	{
		
		$user_id		= $this->uri->segment(3);
		$new_pass_key	= $this->uri->segment(4);

		$this->form_validation->set_rules('new_password', 'New Password', 'trim|required|xss_clean|min_length['.$this->config->item('password_min_length', 'tank_auth').']|max_length['.$this->config->item('password_max_length', 'tank_auth').']|alpha_dash');
		$this->form_validation->set_rules('confirm_new_password', 'Confirm new Password', 'trim|required|xss_clean|matches[new_password]');

		$data['errors'] = array();

		if ($this->form_validation->run()) {							 // validation ok
			if (!is_null($data = $this->tank_auth->reset_password(
					$user_id, $new_pass_key,
					$this->form_validation->set_value('new_password')))) {   // success

				$data['site_name'] = $this->config->item('website_name', 'tank_auth');

				// Send email with new password
				$this->_send_email('reset_password', $data['email'], $data);

				$this->_show_message($this->lang->line('auth_message_new_password_activated').' '.anchor('/auth/login/', 'Login'));

			} else {														// fail
				$this->_show_message($this->lang->line('auth_message_new_password_failed'));
			}
		} else {  // Try to activate user by password key (if not activated yet)
			if ($this->config->item('email_activation', 'tank_auth')) {
				$this->tank_auth->activate_user($user_id, $new_pass_key, FALSE);
			}

			if (!$this->tank_auth->can_reset_password($user_id, $new_pass_key)) {
				$this->_show_message($this->lang->line('auth_message_new_password_failed'));
			}
		}
		$this->load->view('auth/reset_password_form', $data);
	
	}
	/** Change user password**/
	function change_password()
	{
		
		if (!($this->session->userdata('user_name')!="")) {	// not logged in or not activated
			//redirect('/auth/login/');

		} else {
			$this->form_validation->set_rules('old_password', 'Old Password', 'trim|required|xss_clean');
			$this->form_validation->set_rules('new_password', 'New Password', 'trim|required|xss_clean|min_length['.$this->config->item('password_min_length', 'tank_auth').']|max_length['.$this->config->item('password_max_length', 'tank_auth').']|alpha_dash');
			$this->form_validation->set_rules('confirm_new_password', 'Confirm new Password', 'trim|required|xss_clean|matches[new_password]');

			$data['errors'] = array();

			if ($this->form_validation->run()) {								// validation ok
				if ($this->tank_auth->change_password(
						$this->form_validation->set_value('old_password'),
						$this->form_validation->set_value('new_password'))) {	// success
					$this->_show_message($this->lang->line('auth_message_password_changed'));

				} else {														// fail
					$errors = $this->tank_auth->get_error_message();
					foreach ($errors as $k => $v)	$data['errors'][$k] = $this->lang->line($v);
				}
			}
			$this->load->view('auth/change_password_form', $data);
		}
		
		$this->load->view('auth/change_password_form', $data);
	
	}
	/**Send email message of given type (activate, forgot_password, etc.)* */
	function _send_email($type, $email, &$data)
	{
		
		$this->load->library('email');
		$this->email->from($this->config->item('webmaster_email', 'tank_auth'), $this->config->item('website_name', 'tank_auth'));
		$this->email->reply_to($this->config->item('webmaster_email', 'tank_auth'), $this->config->item('website_name', 'tank_auth'));
		$this->email->to($email);
		$this->email->subject(sprintf($this->lang->line('auth_subject_'.$type), $this->config->item('website_name', 'tank_auth')));
		$this->email->message($this->load->view('email/'.$type.'-html', $data, TRUE));
		$this->email->set_alt_message($this->load->view('email/'.$type.'-txt', $data, TRUE));
		$this->email->send();
	
	}
	
	
	
	
	
	public function welcome()
	{
		
	 $data['title']= 'Welcome';
	 $this->load->view('header_view',$data);
	 $this->load->view('welcome_view.php', $data);
	 $this->load->view('footer_view',$data);
	
	}
    public function thank()
	{
	$data['title']= 'Thank';
	$this->load->view('header_view',$data);
	$this->load->view('thank_view.php', $data);
	$this->load->view('footer_view',$data);
	}
	public function upload_image() 
	{
	//eruvaka.com
        if (!empty($_FILES['photo']['name'])) {
          //$config['upload_path'] = ''.$_SERVER['DOCUMENT_ROOT'].'/uploads/';
            $config['upload_path'] = base_url().'uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = '5000';
            $config['max_width']  = '2000';
            $config['max_height']  = '2000';
            $config['encrypt_name'] = TRUE;
            $config['remove_spaces'] = TRUE;
            $this->load->library('upload', $config);
		    //$this->upload->initialize($config);
            if ($this->upload->do_upload('photo')) 
			{
                $data = array('upload_data' => $this->upload->data());
                $data = $this->upload->data();
                print_r($data);
            }else{
			  	print_r('caselast');
			}
       }
    }
	//------------   Steps Of Registeration  -------------//
	function sess_destroy()
	{
		$this->session->sess_destroy();
	}
	function generate_qr(){
		$this->load->library('ciqrcode');
		//$params['data'] = 'Name:vikash,surbname: keshri,role:caregiver,address: ranchi, India';
		$params['data'] = 'Name:'.$_POST['name'].',surname:'.$_POST['surname'];
		$params['level'] = 'H';
		$params['size'] = 3;
		//$params['savename'] = FCPATH.'uploads/qrcode/test.png';
		$img_name=uniqid().'.png';
		$params['savename'] = FCPATH.'uploads/qrcode/'.$img_name;
		$this->ciqrcode->generate($params);
		echo $img_name;
		
}
  
  
  function generate_qr1($pid)
  {
		$this->load->library('ciqrcode');
		//$params['data'] = 'Name:vikash,surbname: keshri,role:caregiver,address: ranchi, India';
		$params['data'] = 'Name:'.$_POST['name'].',surname:'.$_POST['surname'];
		$params['level'] = 'H';
		$params['size'] = 3;
		//$params['savename'] = FCPATH.'uploads/qrcode/test.png';
		$img_name=uniqid().'.png';
		$params['savename'] = FCPATH.'uploads/qrcode/'.$img_name;
		$this->ciqrcode->generate($params);
		echo $img_name;
		
		$us=$this->user->update_parient_qr($pid,$img_name);
		
		
		
		
		
}
 
  
  
  
  
  
  /*////////////////////////////////////////////  caregiver activation step /////////////////////////////////////////////////////////////////  */
function caregiver_register($cid)
{
	
	        @$this->session->set_userdata('cname', $_POST['cname']);
			@$this->session->set_userdata('passwordvalue', $_POST['passwordvalue']);
			@$this->session->set_userdata('email', $_POST['username']);
   redirect('/auth/caregiver_register_step2/'.$cid, 'refresh');
   
}
function caregiver_register_step2($cid)
{
	
//$this->data['states']= $this->user->getStates();	
	if($_POST)
	{
	 $cid;
    $password=$this->session->userdata('passwordvalue');
	$email=$this->session->userdata('email');
	$cname=$_POST['cname'];
	$surname=$_POST['surname'];
	$zipcode=$_POST['zipcode'];
	$city=$_POST['city'];
	$address=$_POST['address'];
	$contact=$_POST['contact'];
	$country=$_POST['state'];
	$lat=$_POST['lat'];
	$lon=$_POST['lng'];
	$age=$_POST['age'];
	$gender=$_POST['gender'];
	
	if(!empty($_FILES['photo']['name']))
	{  
	  

				//echo "Temp file: " . $_FILES["photo"]["tmp_name"] . "<br>";
				
					//$file=$_FILES["photo"]["name"];
					//if(move_uploaded_file($_FILES["photo"]["tmp_name"],
					//"uploads/" . $file))
					//$this->session->set_userdata('user_photo', $file);
				
$file=$_FILES['photo']['name'];
 $type=$_FILES['photo']['type'];
move_uploaded_file($_FILES['photo']['tmp_name'],"uploads/".$file);

if($type=='image/jpeg')
{
	$fi="uploads/".$file;
   $newHeight=100;
   $newWidth=100;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;
    list($width,$height) = getimagesize($fi);
    $img = imagecreatefromjpeg($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagejpeg($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }
    if($type=='image/png')
    {
    $fi="uploads/".$file;
   $newHeight=100;
   $newWidth=100;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;

    list($width,$height) = getimagesize($fi);
    $img = imagecreatefrompng($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagepng($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }
    
    if($type=='image/gif')
    {
   $fi="uploads/".$file;
   $newHeight=100;
   $newWidth=100;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;

    list($width,$height) = getimagesize($fi);
    $img = imagecreatefromgif($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagegif($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }

    unlink($fi);  
	}
	else
	{
		$nfile='';
	}
	
	$us=$this->user->getcaregiversbyemail($email);
	
	
   if($us>0)
   {
	   $pwd=md5($password);
 //$lat= constant("lat");
 //$lon= constant("lon");
	   $us1=$this->user->updatecaregiversbycid($cid,$pwd,$lat,$lon,$cname,$surname,$zipcode,$city,$address,$contact,$age,$gender,$country,$nfile); 
	  if($us1)
	  {
		 /* echo "<script>alert('caregiver Activated Successfully please login to continue')</script>";*/
		  
		  
		  
		   redirect('/auth/after_caregiver_register/', 'refresh');
		  
		  
	  }
   }
   else
   {
    echo "<script>alert('Please Enter the correct email id and Try again ')</script>";
	redirect('/auth/careregister/'.$cid, 'refresh');
   
   }
   
	}
	
	  $this->load->view('auth/caregiver_register_step2', $this->data);
}

/* Owner View The caregiver Details */
function view_caredetails($id)
{
	
	
	 $id;
	
	$email=$this->session->userdata('owner_name');
	  	 if(empty($email))
		{ 
			redirect('/');
		}
	

       $this->data['ownerdetails']=$this->user->get_owner_email($email);
	   $oid=$this->data['ownerdetails'][0]->own_id;
			$otype=$this->data['ownerdetails'][0]->l_type;

	   
	   
	   
//$this->data['states']= $this->user->getStates();

	if($_POST)
	{
	 $cid;
    $password=$this->session->userdata('passwordvalue');
	$email=$this->session->userdata('email');
    $oid=$_POST['oid'];
	$cname=$_POST['cname'];
	$surname=$_POST['surname'];
	$zipcode=$_POST['zipcode'];
	$city=$_POST['city'];
	$address=$_POST['address'];
	$contact=$_POST['contact'];
	$country=$_POST['state'];
	$lat=$_POST['lat'];
	$lon=$_POST['lng'];
	$age=$_POST['age'];
	$gender=$_POST['gender'];
	if(!empty($_FILES['photo']['name']))
	{  
	  

				//echo "Temp file: " . $_FILES["photo"]["tmp_name"] . "<br>";
				
					//$file=$_FILES["photo"]["name"];
					//if(move_uploaded_file($_FILES["photo"]["tmp_name"],
					//"uploads/" . $file))
					//$this->session->set_userdata('user_photo', $file);
				
$file=$_FILES['photo']['name'];
 $type=$_FILES['photo']['type'];
move_uploaded_file($_FILES['photo']['tmp_name'],"uploads/".$file);

if($type=='image/jpeg')
{
	$fi="uploads/".$file;
   $newHeight=100;
   $newWidth=100;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;
    list($width,$height) = getimagesize($fi);
    $img = imagecreatefromjpeg($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagejpeg($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }
    if($type=='image/png')
    {
    $fi="uploads/".$file;
   $newHeight=100;
   $newWidth=100;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;

    list($width,$height) = getimagesize($fi);
    $img = imagecreatefrompng($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagepng($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }
    
    if($type=='image/gif')
    {
   $fi="uploads/".$file;
   $newHeight=100;
   $newWidth=100;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;

    list($width,$height) = getimagesize($fi);
    $img = imagecreatefromgif($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagegif($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }

    unlink($fi);  
	}
	else
	{
		$nfile=$_POST['photo'];
	}
	
	$us=$this->user->getcaregiversbyid($id);
	
	
   if($us>0)
   {
 //$lat= constant("lat");
 //$lon= constant("lon");
	   $us1=$this->user->updatecaregiversbycid2($id,$password,$lat,$lon,$cname,$surname,$zipcode,$city,$address,$contact,$age,$gender,$country,$nfile); 
	  if($us1)
	  {
		 /* echo "<script>alert('caregiver Activated Successfully please login to continue')</script>";*/
		   
		if($otype=="RSA" or $otype=="Consortium")  	
			{
			
			redirect('/front/after_consortiam_login/'.$oid);
			}
			else
			{
				redirect('/front/after_owner_login/'.$oid);
			}
		  
		  
		  
		  $this->data['flash']=" Caregier Details Updates Successfully ";
		  
	  }
   }
   else
   {
   /* echo "<script>alert('Please Enter the correct details again ')</script>";
	redirect('/auth/careregister/'.$cid, 'refresh');*/
	$this->data['flash']=" Not Success ";
	
	
   
   }
   
	}
	
	$this->data['caregiver'] = $this->user->get_caregiver($id);
    $this->data['caregiver'][0]->id;
	$this->load->view('auth/view_caregiver', $this->data);
}



 
function after_caregiver_register()
{
$this->load->view('front/after_caregiver_register', $this->data);
}


	function register_step1()
	{
	  $this->form_validation->set_rules('username', 'Email', 'trim|required|xss_clean|valid_email|is_unique[users.username]');
	  if ($this->form_validation->run()==TRUE) {
	  //if (isset($_POST['role'])) {
 	     $role=$this->session->set_userdata('role', $_POST['role']);
         $this->session->set_userdata('username', $_POST['username']);
         $this->session->set_userdata('email', $_POST['email']);
         $this->session->set_userdata('passwordvalue', $_POST['passwordvalue']);
		 redirect('/auth/register_step2/', 'refresh');
	  }				
	  $this->load->view('auth/register_step1', $this->data);		
				
	}
/*  After Admin login    */
	function addpatient($oid)
	{
	$oid;
	 $email=$this->session->userdata('owner_name');
	$this->data['ownerdetails']=$this->user->get_owner_email($email);
	
	
	////$this->data['states']= $this->user->getStates();
	
	$this->load->view('auth/owner_patient_register', $this->data);
	}
	function con_rsa_addpatient()
	{
		$email=$this->session->userdata('owner_name');
	  	 if(empty($email))
		{ 
			redirect('/');
		}
	

       $this->data['ownerdetails']=$this->user->get_owner_email($email);
		
		
		$this->load->view('auth/add_licence_con_rsa', $this->data);
	}
	
	
	
	/*  patient Registration  by Owner      */
	function owner_register_patient($id)
	{
	       //--------uploading--------------//
			if(!empty($_FILES["photo"]["name"]))
			{

				//echo "Temp file: " . $_FILES["photo"]["tmp_name"] . "<br>";
				
					//$file=$_FILES["photo"]["name"];
					//if(move_uploaded_file($_FILES["photo"]["tmp_name"],
					//"uploads/" . $file))
					//$this->session->set_userdata('user_photo', $file);
				
$file=$_FILES['photo']['name'];
 $type=$_FILES['photo']['type'];
move_uploaded_file($_FILES['photo']['tmp_name'],"uploads/".$file);

if($type=='image/jpeg')
{
	$fi="uploads/".$file;
   $newHeight=100;
   $newWidth=100;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;
    list($width,$height) = getimagesize($fi);
    $img = imagecreatefromjpeg($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagejpeg($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }
    if($type=='image/png')
    {
    $fi="uploads/".$file;
   $newHeight=100;
   $newWidth=100;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;

    list($width,$height) = getimagesize($fi);
    $img = imagecreatefrompng($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagepng($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }
    
    if($type=='image/gif')
    {
   $fi="uploads/".$file;
   $newHeight=100;
   $newWidth=100;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;

    list($width,$height) = getimagesize($fi);
    $img = imagecreatefromgif($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagegif($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }

    unlink($fi);
$this->session->set_userdata('photo',$nfile);
			}
			
			
			else{
				$this->session->set_userdata('photo', '');
			}
			
			
			//echo substr($_POST['lng'],0,9);
			
	    	@$this->session->set_userdata('name', $_POST['name']);
			@$this->session->set_userdata('lat', $_POST['lat']);
			@$this->session->set_userdata('lng', $_POST['lng']);
			
			@$this->session->set_userdata('licence', $_POST['licence']);
			@$this->session->set_userdata('surname', $_POST['surname']);
			@$this->session->set_userdata('age', $_POST['age']);
			@$this->session->set_userdata('gender', $_POST['gender']);
			@$this->session->set_userdata('zipcode', $_POST['zipcode']);
			@$this->session->set_userdata('city', $_POST['city']);
			@$this->session->set_userdata('state', $_POST['state']);
			@$this->session->set_userdata('address', $_POST['address']);
			@$this->session->set_userdata('phone', $_POST['phone']);
			@$this->session->set_userdata('phone2', $_POST['phone2']);
			@$this->session->set_userdata('phone3', $_POST['phone3']);
			@$this->session->set_userdata('contact1', $_POST['contact1']);
			@$this->session->set_userdata('contact2', $_POST['contact2']);
			@$this->session->set_userdata('contact3', $_POST['contact3']);
			@$this->session->set_userdata('pathology', $_POST['pathology']);
			@$this->session->set_userdata('allergies', $_POST['allergies']);
			@$this->session->set_userdata('intolerances', $_POST['intolerances']);
			@$this->session->set_userdata('note', $_POST['note']); 
			@$this->session->set_userdata('qr_code', $_POST['qr_code']);
     		redirect('/auth/patient_registerstep2/'.$id, 'refresh');
			
			}
			
	/*  Patient edit by the owner */		
		function edit_owner_patient($pid)
		{
			
			 $email=$this->session->userdata('owner_name');
			$this->data['ownerdetails']=$this->user->get_owner_email($email);
			$id=$this->data['ownerdetails'][0]->own_id;
			$otype=$this->data['ownerdetails'][0]->l_type;
			
			
			//$this->data['states']= $this->user->getStates();
		//$this->data['caregiver'] = $this->user->edit_caregiver($id);
		$this->data['patient']= $this->user->patient_details($pid);
			
		/*echo "<pre>";
		
		print_r($this->data['patient']);
		echo "</pre>";*/
		if($_POST)
		{
		       
	//--------uploading--------------  //
	if(!empty($_FILES["photo"]["name"]))
	{

				//echo "Temp file: " . $_FILES["photo"]["tmp_name"] . "<br>";
				
					//$file=$_FILES["photo"]["name"];
					//if(move_uploaded_file($_FILES["photo"]["tmp_name"],
					//"uploads/" . $file))
					//$this->session->set_userdata('user_photo', $file);
				
$file=$_FILES['photo']['name'];
$type=$_FILES['photo']['type'];
move_uploaded_file($_FILES['photo']['tmp_name'],"uploads/".$file);

if($type=='image/jpeg')
{
	$fi="uploads/".$file;
   $newHeight=100;
   $newWidth=100;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;
    list($width,$height) = getimagesize($fi);
    $img = imagecreatefromjpeg($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagejpeg($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }
    if($type=='image/png')
    {
    $fi="uploads/".$file;
   $newHeight=100;
   $newWidth=100;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;

    list($width,$height) = getimagesize($fi);
    $img = imagecreatefrompng($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagepng($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }
    
    if($type=='image/gif')
    {
   $fi="uploads/".$file;
   $newHeight=100;
   $newWidth=100;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;

    list($width,$height) = getimagesize($fi);
    $img = imagecreatefromgif($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagegif($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }

    unlink($fi);
$this->session->set_userdata('photo', $nfile);
 $nfile;
			}
	else
	{
		$this->session->set_userdata('photo', $_POST['photo']);
	}
	
	
	
	    	$this->session->set_userdata('name', $_POST['name']);
			//$this->session->set_userdata('licenceid', $_POST['licenceid']);
			$this->session->set_userdata('surname', $_POST['surname']);
			$this->session->set_userdata('age', $_POST['age']);
			$this->session->set_userdata('gender', $_POST['gender']);
			$this->session->set_userdata('zipcode', $_POST['zipcode']);
			$this->session->set_userdata('city', $_POST['city']);
			$this->session->set_userdata('state', $_POST['state']);
			$this->session->set_userdata('address', $_POST['address']);
			$this->session->set_userdata('phone', $_POST['phone']);
			$this->session->set_userdata('phone2', $_POST['phone2']);
			$this->session->set_userdata('phone3', $_POST['phone3']);
			$this->session->set_userdata('contact1', $_POST['contact1']);
			$this->session->set_userdata('contact2', $_POST['contact2']);
			$this->session->set_userdata('contact3', $_POST['contact3']);
			$this->session->set_userdata('pathology', $_POST['pathology']);
			$this->session->set_userdata('allergies', $_POST['allergies']);
			$this->session->set_userdata('intolerances', $_POST['intolerances']);
			$this->session->set_userdata('note', $_POST['note']); 
			//$this->session->set_userdata('qr_code', $_POST['qr_code']);
     		//redirect('/auth/patient_registerstep2/'.$id, 'refresh');
			$oid=$this->user->updatepatient($pid);
			
			
			
			
			/*echo "<script>alert('Owner Details Updated Successfully')</script>";*/
			$this->data['flash']="<font color='red'> <b> Patient Details Updated </b> </font> ";
			
			if($otype=="RSA" or $otype=="Consortium")  	
			{
			
			redirect('/front/after_consortiam_login/'.$id);
			}
			else
			{
				redirect('/front/after_owner_login/'.$id);
			}
			
			}
			
		$this->load->view('auth/owner_patient_edit', $this->data);
		
		/*

		
		  */
		
		
			
		}
			
			
			
			
			
			
	
	function patient_registerstep2($id)
	{
		
$oid;
	 $email=$this->session->userdata('owner_name');
	$this->data['ownerdetails']=$this->user->get_owner_email($email);		
		
		
		
		
	$email=$this->session->userdata('owner_name');
		
		if(empty($email))
		{
			redirect('/');
		}
	
	$name=$this->session->userdata('name');
	$gender=$this->session->userdata('gender');
	$licence=$this->session->userdata('licence');
	//$this->data['states']= $this->user->getStates();
	if(!empty($name) and !empty($gender) and !empty($licence))
	{  
	   
		
	//$ono=$this->user->getowner1($email);
	//$eno=$ono['licence_no'];
	//$owid=$ono['own_id'];
    /*echo "<script>alert('$eno')</script>";*/
	
	$patn=$this->user->getpatient1($id,$licence);
	
		if($patn<=0)
		{
	$ms="select * from owner_licence where own_id='$id' and licence_no='$licence'";
	$me=mysql_query($ms) or die(mysql_error());
	$mn=mysql_num_rows($me);
	if($mn>0)
	{
		//$l=session->userdata('lat');
		//$n=session->userdata('lng');
 $lat= $this->session->userdata('lat');
 $lon= $this->session->userdata('lng');

$oid=$this->user->owner_insertpatient($id,$licence,$lat,$lon);
		if($oid)
	{
		$this->data['flash']=" Patient Added Successfully ";
		//redirect('/auth/patient_registerstep3/'.$id, 'refresh');
		echo "<script>alert('Patient Added Successfully')</script>";
		//redirect('/front/addcaregivers/'.$id."?status=Patient Added Successfully Please Add The Caregivers", $this->data);
		
		redirect('/auth/after_patientadd/',$this->data);
		
	}
		
		else
		{
		$this->data['flash']="You have already added Patient with <font color='black'><b> &nbsp; ".$licence." &nbsp; </b></font> Please check again ";	
		
		$this->load->view('auth/owner_patient_register', $this->data);
		//redirect('/auth/addpatient/'.$id, $this->data);
		
		
		}
	}
		else
		{
		$this->data['flash']=" <font color='black'><b> &nbsp; ".$licence." &nbsp; </b></font> This licence id  is not present with this owner  ";	
		$this->load->view('auth/owner_patient_register', $this->data);
		//redirect(' /auth/addpatient/'.$id, $this->data);
		}
		}
		/*$this->session->unset_userdata('licenceid');
		$this->session->unset_userdata('name');
		$this->session->unset_userdata('surname');
		$this->session->unset_userdata('age');
		$this->session->unset_userdata('gender');
		$this->session->unset_userdata('zipcode');
		$this->session->unset_userdata('city');
		$this->session->unset_userdata('state');
		$this->session->unset_userdata('address');
		$this->session->unset_userdata('phone');
		$this->session->unset_userdata('phone2');
		$this->session->unset_userdata('phone3');
		$this->session->unset_userdata('contact1');
		$this->session->unset_userdata('contact2');
		$this->session->unset_userdata('contact3');
		$this->session->unset_userdata('pathology');
		$this->session->unset_userdata('allergies');
		$this->session->unset_userdata('intolerances');is 
		$this->session->unset_userdata('note');
		*/
	//$this->data['flash']=" Patient Added Successfully ";
	
	else
	{
		$this->data['flash']=" <font color='black'><b> &nbsp; ".$licence." &nbsp; </b></font> Patient already registered with this licence id  ";	
		$this->load->view('auth/owner_patient_register', $this->data);
	}
	
	
	
	
	 
	 
	 }
	 
	 else
	 {
		 $this->data['flash']=" Please Fill Required Fields ";
		 $this->load->view('auth/owner_patient_edit', $this->data);
	 }
	 
	 
	}
	
	function after_patientadd()
	{
		$oid;
	 $email=$this->session->userdata('owner_name');
	$this->data['ownerdetails']=$this->user->get_owner_email($email);
		
		
		$this->load->view('front/after_patient_registered', $this->data);
	}
	
	function patient_registerstep3($id)
	{
	$this->data['flash']=" Patient Added Successfully ";
	$this->load->view('auth/patient_registerstep2', $this->data);
	
	
	
	
	
	}
	function update_patient($id,$pid)
	{       
	//--------uploading--------------  //
	if(!empty($_FILES["photo"]["name"]))
	{

				//echo "Temp file: " . $_FILES["photo"]["tmp_name"] . "<br>";
				
					//$file=$_FILES["photo"]["name"];
					//if(move_uploaded_file($_FILES["photo"]["tmp_name"],
					//"uploads/" . $file))
					//$this->session->set_userdata('user_photo', $file);
				
$file=$_FILES['photo']['name'];
$type=$_FILES['photo']['type'];
move_uploaded_file($_FILES['photo']['tmp_name'],"uploads/".$file);

if($type=='image/jpeg')
{
	$fi="uploads/".$file;
   $newHeight=100;
   $newWidth=100;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;
    list($width,$height) = getimagesize($fi);
    $img = imagecreatefromjpeg($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagejpeg($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }
    if($type=='image/png')
    {
    $fi="uploads/".$file;
   $newHeight=100;
   $newWidth=100;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;

    list($width,$height) = getimagesize($fi);
    $img = imagecreatefrompng($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagepng($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }
    
    if($type=='image/gif')
    {
   $fi="uploads/".$file;
   $newHeight=100;
   $newWidth=100;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;

    list($width,$height) = getimagesize($fi);
    $img = imagecreatefromgif($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagegif($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }

    unlink($fi);
$this->session->set_userdata('photo', $nfile);

			}
	else
	{
		$this->session->set_userdata('photo', $_POST['photo']);
	}
	    	$this->session->set_userdata('name', $_POST['name']);
			//$this->session->set_userdata('licenceid', $_POST['licenceid']);
			$this->session->set_userdata('surname', $_POST['surname']);
			$this->session->set_userdata('age', $_POST['age']);
			$this->session->set_userdata('gender', $_POST['gender']);
			$this->session->set_userdata('zipcode', $_POST['zipcode']);
			//$this->session->set_userdata('city', $_POST['city']);
			$this->session->set_userdata('state', $_POST['state']);
			$this->session->set_userdata('address', $_POST['address']);
			$this->session->set_userdata('phone', $_POST['phone']);
			$this->session->set_userdata('phone2', $_POST['phone2']);
			$this->session->set_userdata('phone3', $_POST['phone3']);
			$this->session->set_userdata('contact1', $_POST['contact1']);
			$this->session->set_userdata('contact2', $_POST['contact2']);
			$this->session->set_userdata('contact3', $_POST['contact3']);
			$this->session->set_userdata('pathology', $_POST['pathology']);
			$this->session->set_userdata('allergies', $_POST['allergies']);
			$this->session->set_userdata('intolerances', $_POST['intolerances']);
			$this->session->set_userdata('note', $_POST['note']); 
			//$this->session->set_userdata('qr_code', $_POST['qr_code']);
     		//redirect('/auth/patient_registerstep2/'.$id, 'refresh');
			$oid=$this->user->updatepatient($pid);
			
			/*echo "<script>alert('Owner Details Updated Successfully')</script>";*/
			$this->data['flash']=" Patient Details Updated ";
			redirect('/front/details_updatepatient/'.$pid);
			
			}

	function insertpatient($id)
	{
		$email=$this->session->userdata('owner_name'); 
		if(empty($email))
		{
			redirect('/');
		}
		$owtype=$_POST['owtype'];
		$name=$_POST['cname'];
		$sname=$_POST['sname'];
		$cemail=$_POST['cemail'];
		@$ctype=$_POST['ctype'];
		$patient=$_POST['patient'];
		
		if($owtype=="yes")
		{
		/*echo "<script>alert('yes')</script>";	*/
		
		$us=$this->user->get_own_care($cemail,$id);
		if($us<=0)
		{
		$use=$this->user->insert_own_care($name,$sname,$cemail,$ctype,$patient,$id);
		
	//$this->load->view('auth/patient_registerstep2', $this->data);
	   redirect('/front/addcaregivers/'.$id, $this->data);
	
		}
		else
		{
			$msg=" you have already added as a caregiver please anether caregiver";
		 redirect('/auth/after_patient_register/'.$id."/".$msg);
		}
		}
		if($owtype=="no")
		{
			
		
		redirect('/auth/after_patient_register/'.$id);
		
		}
		//@$this->session->set_userdata('caretype', $_POST['caretype']);
		//if(isset($_POST['caretype']))
		//{
				//@$this->data['owner']=$this->user->updateowntype($id,$ctype);
						
			/*	//echo "<script> alert(print_r('$own')) </script>";*/
		//}
       //$this->session->unset_userdata('caretype');
		@$otype=$_POST['otype'];
		/*echo "<script> alert('$otype') </script>";*/
		//if($otype=="owner")
		//{
		//$this->load->view('auth/after_patient_register', $this->data);
		
		//}
//*===============================///////////////// word cancleing ////////////////// ===================================================  */////	
		/*else { 
			if($otype=="Consortium")
			{
				$this->load->view('auth/after_patient_con_register', $this->data);	
			}
			else
			{
				if($otype=="RSA")
				{
				$this->load->view('auth/after_patient_rsa_register', $this->data);
				}
			}
		} */		
	}
	
	
	function after_patient_register($id,$msg)
	{
		@$this->data['flash']=urldecode($msg);
		$this->load->view('auth/after_patient_register', $this->data);
	}

function delete_patient($id)
	{

		$this->user->deletepatient($id);
		redirect('/front/after_owner_login/');
	}
	
	
	
/*==========================================================>  forget password =====================> */	
	function forgetpassword()
	{
		if($_POST)
		{
		$role=$_POST['role'];	
		if($role=="Owner")
		{
			$licence=$_POST['licence'];
			$email=$_POST['username'];
			
		$os="select * from owner_table where owner_type='$licence' and owner_email='$email'";
		$oe=mysql_query($os) or die(mysql_error());
		$on=mysql_num_rows($oe);
		if($on>0)
		{
			$password=$_POST['passwordvalue'];
			$pass=md5($password);
			$us="updae owner_table set own_passwd='$pass' where owner_email='$email'";
			$ue=mysql_query($us) or die(mysql_error());
			if($ue)
			{
			
			$mail = new PHPMailer();
			$mail->IsSMTP();
			$mail->SMTPAuth   = true; 
			$mail->SMTPSecure = "ssl"; 
            $mail->Port       = 465;
			$mail->Host       = "smtp.yandex.com";
			$mail->Username   = "notification@Koala.Care";
			$mail->Password   = "No1L@S@pp1@mo";
			$mail->SetFrom('notification@Koala.Care', 'Koala Team');
			$mail->Subject = "Forgot Password Details"; //subject      
		    @$to = $email;
			@$subject ="Forgot Password Details";
			@$message ="
			<html>
			<head>
			<title>HTML email</title>
			</head>
			<body>
			<table>
			<th> Subject: </th>
			<td>{Your Login Details }</td>
			</tr> 
			<tr>
			<th> User Name :</th>
			<td>{$email}</td>
			</tr>
			<tr>
			<th> Password :</th>
			<td>{$password}</td>
			</tr>
			</table>
			</body>
			</html>
			";
			
		
			
           @$body = eregi_replace("[\]",'',$message);
           $mail->MsgHTML($body);			
		   $to = $email;
			//$subject = "Caregiver Registration Details";
			$mail->AddAddress($to, "Koala"); 
			
			
			if ($mail->Send()) 
			{
			$this->data['flash']= " Password Change Successfully ";
            }
			
			
			
			
			}
			else
			{
				$this->data['flash']= " Plese Enter The Correct Email Address To Get The Passowrd ";
			}
			
			
			
		}
		
		else
			{
				$this->data['flash']= " Plese Enter The Correct Email Address To Get The Passowrd ";
			}
		
			
		}
		if($role=="professional")
		{
			$email=$_POST['username'];
			$ps="select * from professionals where email='$email'";
			$pe=mysql_query($ps) or die(mysql_error());
			echo $pn=mysql_num_rows($pe);
			if($pn>0)
			{
				
			$password=$_POST['passwordvalue'];
			$pass=md5($password);
			$ups="update professionals set password='$pass' where email='$email'";
			$upe=mysql_query($ups);
			if($upe)
			{
			
			$mail = new PHPMailer();
			$mail->IsSMTP();
			$mail->SMTPAuth   = true; 
			$mail->SMTPSecure = "ssl"; 
            $mail->Port       = 465;
			$mail->Host       = "smtp.yandex.com";
			$mail->Username   = "notification@Koala.Care";
			$mail->Password   = "No1L@S@pp1@mo";
			$mail->SetFrom('notification@Koala.Care', 'Koala Team');
			$mail->Subject = "Forgot Password Details"; //subject      
		    @$to = $email;
			@$subject ="Forgot Password Details";
			@$message ="
			<html>
			<head>
			<title>HTML email</title>
			</head>
			<body>
			<table>
			<th> Subject: </th>
			<td>{Your Login Details }</td>
			</tr> 
			<tr>
			<th> User Name :</th>
			<td>{$email}</td>
			</tr>
			<tr>
			<th> Password :</th>
			<td>{$password}</td>
			</tr>
			</table>
			</body>
			</html>
			";
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
			// More headers
			$headers .= 'From: '." Koala Team " . "\r\n";
		
			
           @$body = eregi_replace("[\]",'',$message);
           $mail->MsgHTML($body);			
		   $to = $email;
			//$subject = "Caregiver Registration Details";
			$mail->AddAddress($to, "Koala"); 
			
			
			if ($mail->Send()) 
			{
			$this->data['flash']= " Password Change Successfully ";
            }
			
			
			
			
			}
			else
			{
				$this->data['flash']= " Plese Enter The Correct Email Address To Get The Passowrd ";
			}
			
			
			
			}
			else
			{
				$this->data['flash']= " Plese Enter The Correct Email Address To Get The Passowrd ";
			}
			
			
			
		}
		if($role=="caregiver")
		{
			$email=$_POST['username'];
			$ps="select * from caregiver where email='$email'";
			$pe=mysql_query($ps) or die(mysql_error());
			$pn=mysql_num_rows($pe);
			
			if($pn>0)
			{
				$password=$_POST['passwordvalue'];
			$pass=md5($password);
			 $ups="update caregiver set passwd='$pass' where email='$email'";
			$upe=mysql_query($ups) or die(mysql_error());
			if($upe)
			{
			
	        $mail = new PHPMailer();
			$mail->IsSMTP();
			$mail->SMTPAuth   = true; 
			$mail->SMTPSecure = "ssl"; 
            $mail->Port       = 465;
			$mail->Host       = "smtp.yandex.com";
			$mail->Username   = "notification@Koala.Care";
			$mail->Password   = "No1L@S@pp1@mo";
			$mail->SetFrom('notification@Koala.Care', 'Koala Team');
			$mail->Subject = "Forgot Password Details"; //subject      
		    @$to = $email;
			@$subject ="Forgot Password Details";
			@$message ="
			<html>
			<head>
			<title>HTML email</title>
			</head>
			<body>
			<table>
			<th> Subject: </th>
			<td>{Your Login Details }</td>
			</tr> 
			<tr>
			<th> User Name :</th>
			<td>{$email}</td>
			</tr>
			<tr>
			<th> Password :</th>
			<td>{$password}</td>
			</tr>
			</table>
			</body>
			</html>
			";
           @$body = eregi_replace("[\]",'',$message);
           $mail->MsgHTML($body);			
		   $to = $email;
			//$subject = "Caregiver Registration Details";
			$mail->AddAddress($to, "Koala"); 
			
			
			if ($mail->Send()) 
			{
			$this->data['flash']= " Password Change Successfully ";
            }
			
			
			
			
			}
			else
			{
				$this->data['flash']= " Plese Enter The Correct Email Address To Get The Passowrd ";
			}
			
			
			}
			
			else
			{
				$this->data['flash']= " Plese Enter The Correct Email Address To Get The Passowrd ";
			}
			
			
			
			
			
		}
		
		
			
		}
		 $this->load->view('auth/forgot_password_form', $this->data); 
		
		
	}
	
	
	
	
	function owner_register_step1()
	{
		//|is_unique[owner_table.owner_email]
	  $this->form_validation->set_rules('username', 'Email', 'trim|required|xss_clean|valid_email');
	  if ($this->form_validation->run()==TRUE) {
	  //if (isset($_POST['role'])) {
 	   @$uname=$this->session->set_userdata('uname', $_POST['uname']);
		@$role=$this->session->set_userdata('role', $_POST['role']);
        @$a= $this->session->set_userdata('username', $_POST['username']);
		 @$this->session->set_userdata('omobile', $_POST['mobile']);
		 @$this->session->set_userdata('oaddress', $_POST['address']);
         $email=@$this->session->set_userdata('email', $_POST['email']);
         @$this->session->set_userdata('passwordvalue', $_POST['passwordvalue']);
		  @$b=$_POST['role'];
		/*	 echo "<script>alert('okoko')</script>";
		 echo "<script>alert('$b')</script>";*/	
		 if($b=="Owner")
		 {
		
			//$use=$this->user->get_owner($email); 
		  redirect('/auth/register_ownstep2/', 'refresh');
		 }
		 else
		 {
		 //owner_register_step1
		redirect('/auth/register_step2/', 'refresh');
		 }
	  }				
	 else
	 {
		 $this->load->view('auth/owner_register_step1', $this->data); 
	 }
	}
	
	
	
	
	
	function getowners($type)
		{   
		   $email=$this->session->userdata('username');
		  $type;
		//echo "asdfasdf";
		$use=$this->user->get_ajax_owner($email,$type);
		//echo $use['id'];
		  echo $use;
	}
//==============================/////// add the busiceness licences ///////////=============================================================//
function addliccenceids()
{
$email=$this->session->userdata('owner_name');
if(empty($email))
{
	redirect("/");
}

$ownid=$_POST['owid'];
$lname=$_POST['lname'];	
$pname=$_POST['pname'];
$psname=$_POST['psname'];

$tot=count($lname);
for($i=0;$i<$tot;$i++)
{
	$lice=$lname[$i];
	$pn=$pname[$i];
	$psn=$psname[$i];
	
if(!empty($lice) and !empty($pn) and !empty($psn))
{
$lnum=$this->user->get_con_rsa_lice($email,$lice);
if($lnum>0)
{
	
$ptnum=$this->user->getpatient_details($ownid,$pn,$psn,$lice);
/*echo "<script>alert('$ptnum')</script>";*/
if($ptnum<=0)
{
$use=$this->user->addmulpat($lice,$pn,$psn,$ownid);
if($use)
{
	echo "<script>alert('Patient Details Added Successfully')</script>";
	//auth/con_rsa_addpatient/70
	//$this->load->view('auth/add_licence_con_rsa', $this->data);	
	redirect('/auth/con_rsa_addpatient/', 'refresh');
	
}
}
else
{
	echo "<script>alert('You are Trying  Entering the  patient Details With same Licence details or same patient name ')</script>";
//$this->load->view('auth/add_licence_con_rsa', $this->data);	
redirect('/auth/con_rsa_addpatient/', 'refresh');
}
}
else
{
echo "<script>alert('Please Enter the Valid Licence')</script>";
	//$this->load->view('auth/add_licence_con_rsa', $this->data);	
	redirect('/auth/con_rsa_addpatient/', 'refresh');
}
}
}
}
function getownerslic($email,$lice)
		{   
		//echo $lice;
		// $email=$this->session->userdata('username');
		//echo "asdfasdf";
		$use=$this->user->get_ajax_owner_licence($email,$lice);
		//echo $use['id'];
		echo $use;
		
	}
	
	
	
	
	
	
	function careregister($email)
	{
		/*echo "<script>alert('$email')</script>";*/
		
 $this->load->view('auth/caregiver_registration', $this->data);
		
		
	}

	function register_ownstep2()
	{	
	//redirect('/auth/register_ownstep2/', 'refresh');		 
	//redirect('/auth/owner_register_step2', 'refresh');
	 $this->load->view('auth/owner_register_step2', $this->data);		
		
	}
	
	
	function ofterrsa_register()
	{
	 @$this->session->set_userdata('licence', $_POST['licence']);
	 @$this->session->set_userdata('ltype', $_POST['ltype']);
	 //$own_licence=$_POST['licence'];
	 $ltype="RSA";
	 $username=$this->session->userdata('username');
	 $mobile=$this->session->userdata('omobile');
	 $address=$this->session->userdata('oaddress');
	 $email=$this->session->userdata('email');
	 $password=md5($this->session->userdata('passwordvalue'));
	 if(!empty($email) and !empty($password))
	 {
	 $ups="update owner_table set own_passwd='$password' where owner_email='$email'"; 
	 $upe=mysql_query($ups) or die(mysql_error());
	 }
	 
	    $this->session->unset_userdata('ltype');
		$this->session->unset_userdata('uname');
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('omobile');
		$this->session->unset_userdata('oaddress');
		$this->session->unset_userdata('email');
		$this->session->unset_userdata('passwordvalue');
	 
	 
	 
	$this->load->view('front/owner_success', $this->data);	
		
		
		
	}
	
	
	// owner payment and registration is done here 
	function register_own_rsa_step3()
	{   
	 $own_licence=$_POST['licence'];
	 $ltype=$this->session->userdata('ltype');
	 $username=$this->session->userdata('username');
	 $mobile=$this->session->userdata('omobile');
	 $address=$this->session->userdata('oaddress');
	 $email=$this->session->userdata('email');
	 $password=$this->session->userdata('passwordvalue');
		
	if(!empty($username) and !empty($password) and !empty($email))
	{
		
		//$ltype=="RSA" or 
		if($ltype=="Consortium")
		{
		 $rs="select * from owner_con_licence where licence_no='$own_licence' and otype='$ltype' ";
		 $re=mysql_query($rs) or die(mysql_error());
		 $rn=mysql_num_rows($re);
		 if($rn>0)
		 {
	   @$lat= substr(constant("lat"),0,9);
       @$lon= substr(constant("lon"),0,9);
	   
  //$ms="select * from owner_table as own, owner_licence as lic  where own.owner_email='$email' and lic.licence_no='$own_licence'";
 $ms="select * from owner_licence where oemail='$email' and licence_no='$own_licence'";
 
 
 
	   $me=mysql_query($ms) or die(mysql_error());
	    $mn=mysql_num_rows($me);
	   
	   
	   
	   if($mn<=0)
	   {
		   
		$id=$this->user->add_session_con_owner($lat,$lon);
		
		$mexe=$this->user->in_owner_lice1($id,$own_licence);
	   
			$mail = new PHPMailer();
			$mail->IsSMTP();
			$mail->SMTPAuth   = true; 
			$mail->SMTPSecure = "ssl"; 
            $mail->Port       = 465;
			$mail->Host       = "smtp.yandex.com";
			$mail->Username   = "notification@Koala.Care";
			$mail->Password   = "No1L@S@pp1@mo";
			$mail->SetFrom('notification@Koala.Care', 'Koala Team');
		 
		 @$message = "
			<html>
			<head>
			<title>HTML email</title>
			</head>
			<body>
			<table>
			<th> Subject: </th>
			<td>{Your Login Details }</td>
			</tr> 
			<tr>
			<th> User Name :</th>
			<td>{$this->session->userdata('email')}</td>
			</tr>
			<tr>
			<th> Password :</th>
			<td>{$this->session->userdata('passwordvalue')}</td>
			</tr>
			<tr>
			<th> Licence  :</th>
			<td>{$own_licence}</td>
			</tr>
			
			
			
			</table>
			</body>
			</html>
			";
		   @$body = eregi_replace("[\]",'',$message);
           $mail->MsgHTML($body);			
			   $to = $username;
			//$subject = "Caregiver Registration Details";
			$mail->AddAddress($to, "Test Recipient"); 
		 
		 if ($mail->Send()) { 
       $this->data['message']="Owner Registered Successfully With Licence id";
	    }
		 
		 
		 
	
		   
  
		   
		   
		  
			
			$this->data['message']="Owner Registered Successfully With Licence id";
			$this->data['licence']=$own_licence;
		 }
		 
		 else
		 {
			 $this->data['message']="<font color='red'>Owner  Already Registered Please Login</font>";
			 $this->data['licence']='';
		 }
		
		$this->load->view('front/owner_success1', $this->data);
		
		
		
		
		}
		
		if($rn<=0)
		{
			$this->data['flash']= "Licence Entered Incorrect please contact to koala ";
			$this->load->view('front/after_rsa_con_licence', $this->data);
		}
		
		
		
		
		}
		else
		{
			$this->data['flash']= "Licence Entered Incorrect please contact to koala ";
			$this->load->view('front/after_rsa_con_licence', $this->data);
		}
		
	}
	else
	{
		
		echo "<script>alert('Un Successfully Please Try Again please enter the all fields ')</script>";
		redirect('/auth/owner_register_step1/', 'refresh');
	}
		
		
	}
	
	
	
	
	function register_ownstep3()
	{	
	    @$this->session->set_userdata('licence', $_POST['licence']);
		@$this->session->set_userdata('ltype', $_POST['ltype']);
		@$ltype=$_POST['ltype'];
		/*echo "<script>alert('$ltype')</script>";*/
		if($ltype=="RSA" or $ltype=="Consortium")
		{
		//$this->load->view('front/test_register', $this->data);	
		/*echo "<script>alert('user Select the rsa type or consortium type')</script>";*/
		//exit();
		 @$semail = $this->session->userdata('username');
		if(!empty($semail))
		{
		   $id=$this->user->update_owner_rsa_con();
		    @$to = $this->session->userdata('username');
			@$subject = "Login Details From Koala";
			@$message = "<html>
			<head>
			<title>HTML email</title>
			</head>
			<body>
			<table>
			<tr>
			<th> Name: </th>
			<td>{$this->session->userdata('uname')}</td>
			</tr>
			<tr>
			<th> Subject: </th>
			<td>{Your Login Details }</td>
			</tr> 
			<tr>
			<th> User Name :</th>
			<td>{$this->session->userdata('email')}</td>
			</tr>
			<tr>
			<th> Password :</th>
			<td>{$this->session->userdata('passwordvalue')}</td>
			</tr>
			</table>
			</body>
			</html>
			";
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
			// More headers
			$headers .= 'From: '." Koala Team " . "\r\n";
		if(mail($to,$subject,$message,$headers)){
			 // echo 'Thank you !';
			}
			else{
			  //echo 'Try again !'; 
			}
		}
       //$this->load->view("front/after_rsa_con_register",$ltype);		
		/*$this->session->unset_userdata('ctype');
		$this->session->unset_userdata('cnumber');
		$this->session->unset_userdata('cvv');
		$this->session->unset_userdata('licence');
		$this->session->unset_userdata('ltype');
		$this->session->unset_userdata('uname');
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('omobile');
		$this->session->unset_userdata('oaddress');
		$this->session->unset_userdata('email');
		$this->session->unset_userdata('passwordvalue');*/
		$this->load->view('front/after_rsa_con_licence', $this->data);
		//$this->load->view('auth/register_ownstep3', $this->data);	
		
		}
		else
		{
		$this->load->view('auth/register_ownstep3', $this->data);	
		}
	}
	
	
	
/*  ====______________________________________________________________ Pay pal Step ___________________________________________________________________________________________  */	
	
	
	function register_ownstep4()
	{	
	@$this->session->set_userdata('ctype', $_POST['ctype']);
	@$this->session->set_userdata('cnumber', $_POST['cnumber']);
	@$this->session->set_userdata('cvv', $_POST['cvv']);
	$this->session->userdata('ctype');
	$this->session->userdata('cnumber');
	$this->session->userdata('cvv');
	$this->session->userdata('licence');
	$ltype=$this->session->userdata('ltype');
	/*echo "<script>alert('$ltype')</script>";*/
	$username=$this->session->userdata('username');
	$mobile=$this->session->userdata('omobile');
	$address=$this->session->userdata('oaddress');
	$email=$this->session->userdata('email');
	$this->session->userdata('passwordvalue');
		
		
		//and !empty($mobile) and !empty($address)
		
	if(!empty($email) and $ltype!="RSA" and $ltype!="Consortium")
	{
		
		$use=$this->user->get_owner1($email); 
		if($use<=0)
		{
       // redirect('/auth/mpaypal', 'refresh');
	   
		$lat="0";
		$lon="0";
		
		
	$id=$this->user->add_session_user123($lat,$lon);
	
	/*echo "<script>alert('$id')</script>";*/
	 //$psts=$this->user->add_payment($id);
	 $licence="koala".$id;
	 $this->session->set_userdata('licence', $licence);
	 $this->session->set_userdata('ownid', $id);
	 
	 
     $this->user->in_owner_lice($id);
	 
	 
	      
	 
	 
	 //$this->user->updateowner($id);
	 //and !empty($psts)
    	if(!empty($id))
		{
		//echo json_encode(array('statusid' => 1,'message' => "Owner Registered Successfully with Payment",'response' => $id));
		   
			
		//$this->load->view('front/test_register', $this->data);
		$this->data['id']=$id;
	   // $this->session->unset_userdata('ctype');
		//$this->session->unset_userdata('cnumber');
		//$this->session->unset_userdata('cvv');
		//$this->session->unset_userdata('licence');
		//$this->session->unset_userdata('ltype');
		//$this->session->unset_userdata('uname');
		//$this->session->unset_userdata('username');
		//$this->session->unset_userdata('omobile');
		//$this->session->unset_userdata('oaddress');
		//$this->session->unset_userdata('email');
		//$this->session->unset_userdata('passwordvalue');
		
		}
		//success_paypal
	
	
	   $this->load->view('auth/paypal', $this->data);
	
	   
		}
		else
		{
			$this->data['status']="Email Id Alredy With Us Please Try Again";
          $this->load->view('front/error_register', $this->data);
		  //$this->load->view('auth/paypal', $this->data);
		}
		
		
		
	}
		else
		{
		$id=0;
		//echo json_encode(array('statusid' => 0,'message' => "Owner Not Registered",'response' => $id));	
		$this->load->view('front/error_register', $this->data);
		// $this->load->view('auth/paypal', $this->data); 
		}
			
	        
			
		/*echo "<script> alert('Registration Successful, Click OK to login') ;</script>";*/
		//redirect('/', 'refresh');
		
		//$this->session->unset_userdata('state');
		
		
		
		
		}
		
		
		
		
		
		
		
		
		
	/*================= with out  paypal ==================>
	
	
	
	function register_ownstep4()
	{	
	
	@$this->session->set_userdata('ctype', $_POST['ctype']);
	@$this->session->set_userdata('cnumber', $_POST['cnumber']);
	@$this->session->set_userdata('cvv', $_POST['cvv']);
	    $this->session->userdata('ctype');
		$this->session->userdata('cnumber');
		$this->session->userdata('cvv');
		$this->session->userdata('licence');
		$ltype=$this->session->userdata('ltype');
		/*echo "<script>alert('$ltype')</script>";
		$username=$this->session->userdata('username');
		$mobile=$this->session->userdata('omobile');
		$address=$this->session->userdata('oaddress');
		$email=$this->session->userdata('email');
		$this->session->userdata('passwordvalue');
		
		
		//and !empty($mobile) and !empty($address)
		
	if(!empty($email) and $ltype!="RSA" and $ltype!="Consortium")
	{
		
		$use=$this->user->get_owner1($email); 
		if($use<=0)
		{
			
			$lat= substr(constant("lat"),0,9);
    $lon= substr(constant("lon"),0,9);
	$id=$this->user->add_session_user123($lat,$lon);
	/*echo "<script>alert('$id')</script>";
	 //$psts=$this->user->add_payment($id);
     $this->user->in_owner_lice($id);
	 //$this->user->updateowner($id);
	 //and !empty($psts)
    	if(!empty($id) )
		{
		//echo json_encode(array('statusid' => 1,'message' => "Owner Registered Successfully with Payment",'response' => $id));
		    @$to = $this->session->userdata('email');
			@$subject = $_POST['subject'];
			@$message = "
			<html>
			<head>
			<title>HTML email</title>
			</head>
			<body>
			<table>
			<tr>
			<th> Name: </th>
			<td>{$this->session->userdata('uname')}</td>
			</tr>
			<tr>
			<th> Subject: </th>
			<td>{Your Login Details }</td>
			</tr> 
			<tr>
			<th> User Name :</th>
			<td>{$this->session->userdata('email')}</td>
			</tr>
			<tr>
			<th> Password :</th>
			<td>{$this->session->userdata('passwordvalue')}</td>
			</tr>
			</table>
			</body>
			</html>
			";
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
			// More headers
			$headers .= 'From: '." Koala Team " . "\r\n";
		
			if(mail($to,$subject,$message,$headers)){
			 // echo 'Thank you !';
			}else{
			  //echo 'Try again !'; 
			}
		
		
		$this->data['licence']="koala".$id;
		
		$this->load->view('front/owner_success', $this->data);
		}
		
		}
		else
		{
			$this->load->view('front/error_register', $this->data);
		}
		
		
		
	}
		else
		{
		$id=0;
		//echo json_encode(array('statusid' => 0,'message' => "Owner Not Registered",'response' => $id));	
		$this->load->view('front/error_register', $this->data);
		}
			
	        
			
		/*echo "<script> alert('Registration Successful, Click OK to login') ;</script>";
		//redirect('/', 'refresh');
		$this->session->unset_userdata('ctype');
		$this->session->unset_userdata('cnumber');
		$this->session->unset_userdata('cvv');
		$this->session->unset_userdata('licence');
		$this->session->unset_userdata('ltype');
		$this->session->unset_userdata('uname');
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('omobile');
		$this->session->unset_userdata('oaddress');
		$this->session->unset_userdata('email');
		$this->session->unset_userdata('passwordvalue');
		//$this->session->unset_userdata('state');
		
		
		
		
		}	*/
		
		
		
		
		
		
		
		
	function paymentcancle()
	{
		//echo "Payment id cancle ";
	 $this->load->view('front/error_paypal', $this->data);	
		
		
	}
	function paymentsuccess()
	{
    @$lat= substr(constant("lat"),0,9);
    @$lon= substr(constant("lon"),0,9);
	$email=$this->session->userdata('username');
	
	 $item_transaction = $_REQUEST['txn_id'];
    $id=$this->session->userdata('ownid');
	$this->data['licence']= $this->session->userdata('licence');
	$lic=$this->session->userdata('licence');
	$uid=$this->user->updatetransation($id,$item_transaction);	
	
	 if(!empty($email))
	 {
		 
	 $mail = new PHPMailer();
			$mail->IsSMTP();
			$mail->SMTPAuth   = true; 
			$mail->SMTPSecure = "ssl"; 
            $mail->Port       = 465;
			$mail->Host       = "smtp.yandex.com";
			$mail->Username   = "notification@Koala.Care";
			$mail->Password   = "No1L@S@pp1@mo";
			$mail->SetFrom('notification@Koala.Care', 'Koala Team');
			$mail->Subject = "Owner Login Details "; //subject 
	
	
	       @$message = "
			<html>
			<head>
			<title>HTML email</title>
			</head>
			<body>
			<table>
			<tr>
			<th> Name: </th>
			<td>{$this->session->userdata('uname')}</td>
			</tr>
			<tr>
			<th> Subject: </th>
			<td>{Your Login Details }</td>
			</tr> 
			<tr>
			<th> User Name :</th>
			<td>{$this->session->userdata('email')}</td>
			</tr>
			<tr>
			<th> Password :</th>
			<td>{$this->session->userdata('passwordvalue')}</td>
			</tr>
			<tr>
			<th> Licence ID </th>
			<td> $lic </td>
			</tr>
			</table>
			</body>
			</html>
			";
			@$body = eregi_replace("[\]",'',$message);
            $mail->MsgHTML($message);			
			$to = $this->session->userdata('username');
			//$subject = "Caregiver Registration Details";
			$mail->AddAddress($to, "Owner Details"); 
			if ($mail->Send()) 
			{ 
				 $msg=" ";
			}
		$this->data['message']="Owner Registered Successfully With Licence id";
		
	 }
	
	
	
	
	

	
	

	
	
	
	
	
//echo 	$item_no = $_GET['item_number'];
 
		
		 $this->load->view('front/payment_success', $this->data);
	}
	/*
	
	
	*/
	
	function mpaypal()
	{
			@$this->load->view('auth/paypal', $this->data);
	}
		
		
	/*
	
	
	
	*/	
		
		
/*===============================================================  Profesional Registration is done hear  =======================================================*/	
function register_step2()
{ 
//$this->load->view('auth/register_step2_caregiver', $this->data);
	  if($this->session->userdata('role')==''){
    	 redirect('/auth/owner_register_step1/', 'refresh');
	   }
	    //$this->data['states']= $this->user->getStates();
		    if (isset($_POST['name'])) {
			//--------uploading--------------//
			
			
			 if(!empty($_FILES["photo"]["name"]))
			{

				//echo "Temp file: " . $_FILES["photo"]["tmp_name"] . "<br>";
				
					//$file=$_FILES["photo"]["name"];
					//if(move_uploaded_file($_FILES["photo"]["tmp_name"],
					//"uploads/" . $file))
					//$this->session->set_userdata('user_photo', $file);
				
$file=$_FILES['photo']['name'];
 $type=$_FILES['photo']['type'];
move_uploaded_file($_FILES['photo']['tmp_name'],"uploads/".$file);

if($type=='image/jpeg')
{
	$fi="uploads/".$file;
   $newHeight=100;
   $newWidth=100;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;
    list($width,$height) = getimagesize($fi);
    $img = imagecreatefromjpeg($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagejpeg($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }
    if($type=='image/png')
    {
    $fi="uploads/".$file;
   $newHeight=100;
   $newWidth=100;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;

    list($width,$height) = getimagesize($fi);
    $img = imagecreatefrompng($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagepng($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }
    
    if($type=='image/gif')
    {
   $fi="uploads/".$file;
   $newHeight=100;
   $newWidth=100;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;

    list($width,$height) = getimagesize($fi);
    $img = imagecreatefromgif($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagegif($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }
    unlink($fi);
			}
			if(!empty($nfile))
			{
			$this->session->set_userdata('photo', $nfile);
			}
			$this->session->set_userdata('name', $_POST['name']);
			$this->session->set_userdata('surname', $_POST['surname']);
			$this->session->set_userdata('age', $_POST['age']);
			$this->session->set_userdata('gender', $_POST['gender']);
			$this->session->set_userdata('zipcode', $_POST['zipcode']);
			$this->session->set_userdata('city', $_POST['city']);
			$this->session->set_userdata('state', $_POST['state']);
			
			
			
			
			
if($this->session->userdata('role')=='caregiver'){ 

 }

else if($this->session->userdata('role')=='professional')
{
//--------uploading cv--------------//
$temp = explode(".", $_FILES["cv"]["name"]);
if ($_FILES["cv"]["error"] > 0) {
//echo "Error: " . $_FILES["cv"]["error"] . "<br>";
}else {
//echo "Temp file: " . $_FILES["cv"]["tmp_name"] . "<br>";
if (file_exists("upload/" . $_FILES["cv"]["name"])) {
echo $_FILES["cv"]["name"] . " already exists. ";
}
else 
{
move_uploaded_file($_FILES["cv"]["tmp_name"],
"uploads/cv/" . $_FILES["cv"]["name"]);
$file_name= $_FILES["cv"]["name"];
}
}
if(isset($file_name)){
$this->session->set_userdata('cv', $file_name);
}else{
$this->session->set_userdata('cv', '');
}
//$res=$_POST['from'];
//$data=explode('/',$res);
//$result=$data[2].'-'.$data[0].'-'.$data[1];
//$_POST['from']=$result;
$this->session->set_userdata('from', $_POST['from']);
@$this->session->set_userdata('phone', $_POST['phone']);
$this->session->set_userdata('address', $_POST['address']);
$this->session->set_userdata('pro_email', $_POST['pro_email']);
$this->session->set_userdata('qualification', $_POST['qualification']);
$this->session->set_userdata('admitted_to', $_POST['admitted_to']);
$this->session->set_userdata('lat', $_POST['lat']);
$this->session->set_userdata('lon', $_POST['lng']);
$this->session->set_userdata('city', $_POST['city']);
$this->session->set_userdata('state', $_POST['state']);



}

    redirect('/auth/register_step3/', 'refresh');
    }
    // print_r($this->data); x
    if($this->session->userdata('role')=='caregiver'){
          $this->load->view('auth/register_step2_caregiver.php', $this->data);
}elseif($this->session->userdata('role')=='professional'){
 $this->load->view('auth/register_step2_professional.php', $this->data);
}
 }

function register_step3() 
{ 
ob_start();
  /* if($this->session->userdata('role')==''){
     redirect('/auth/owner_register_step1/', 'refresh');
   }
   if($this->session->userdata('uname')==''){
     redirect('/auth/register_step2/', 'refresh');
   }*/

  $this->load->view('auth/register_step3.php', $this->data);
}

function buy_extended_licence($oid)
{
 $email=$this->session->userdata('owner_name');
			
			$this->data['ownerdetails']=$this->user->get_owner_email($email);
			$id=$this->data['ownerdetails'][0]->own_id;
			$otype=$this->data['ownerdetails'][0]->l_type;

	
	
	
	
	
	
	
	
	
$this->load->view('auth/owner_buy_extendedlicence', $this->data);

//$this->load->view('auth/owner_buy_extendedlicence', $this->data);

}



function extented_licence($oid)
{
 //redirect('/auth/paypal_anether2/?id='.$oid, 'refresh');	
 
 $email=$this->session->userdata('owner_name');
			
			$this->data['ownerdetails']=$this->user->get_owner_email($email);
			$id=$this->data['ownerdetails'][0]->own_id;
			$otype=$this->data['ownerdetails'][0]->l_type;

 
 
 
 
 //@$this->session->set_userdata('email', $_POST['email']);
 $this->data['oid']=$oid;
 $licence=$_POST['licence'];
 @$this->session->set_userdata('licence', $_POST['licence']);
$this->data['licence']=$licence;
 
 
 
 $num=$this->user->get_owner_by_licence($licence,$id);
 if($num>0)
 {
 $this->load->view('auth/paypal_extented', $this->data);
 }
 else
 {
	 echo "<script>alert('Please Enter The Correct Licence Number')</script>";
	 $this->load->view('auth/owner_buy_extendedlicence', $this->data);
 }
 
 
}
function extentended_success($licence)
{
	$email=$this->session->userdata('email');
	
	$email=$this->session->userdata('email');
	$email1=$this->session->userdata('owner_name');
	
	$this->data['ownerdetails']=$this->user->get_owner_email($email1);
	@$id=$this->data['ownerdetails'][0]->own_id;
	@$otype=$this->data['ownerdetails'][0]->l_type;
	@$licencetype=$this->data['ownerdetails'][0]->l_type;
	$id=$this->user->updateownerlicencestatus1($licence);
	
	@$this->session->set_userdata('email', $_POST['email']);
	$to=$this->session->userdata('owner_name');
$mail = new PHPMailer();
			$mail->IsSMTP();
			$mail->SMTPAuth   = true; 
			$mail->SMTPSecure = "ssl"; 
            $mail->Port       = 465;
			$mail->Host       = "smtp.yandex.com";
			$mail->Username   = "notification@Koala.Care";
			$mail->Password   = "No1L@S@pp1@mo";
			$mail->SetFrom('notification@Koala.Care', 'Koala Team');
$mail->SetFrom('ali.m@bravemount.com', 'Koala'); //from (verified email address)
$mail->Subject = "Owner Licence Is Extented "; //subject
	
	
	 @$to = $email;
			@$subject ="Owner Licence Details ";
			 
			 @$message = "
				<html>
				<head>
				<title>HTML email</title>
				</head>
				<body>
				<table>
				<tr>
				<th> Email: </th>
				<td>{$email}</td>
				</tr>
				<tr>
				<th> Your Licence Is Extented </th>
				<td> </td>
				</tr>
				</table>
				</body>
				</html>
				";
			 
			 
			 
			 
			 
			 $body = $message;
			
		  //$body = eregi_replace("[\]",'',$body);
           $mail->MsgHTML($body);			
			//$subject = "Caregiver Registration Details";
			$mail->AddAddress($to, "Test Recipient"); 	
			
			if ($mail->Send()) { 
	$this->data['flash']= "Licence Extended Successfully ";
			}
	
	
	
	
	
	
	$this->data['flash']= "Licence Extended Successfully ";
	$this->load->view('auth/owner_buy_extendedlicence', $this->data);
	
}

function register_extended_licence($oid)
{
	
	
	
$id=$this->user->updateownerlicencestatus($oid);
$this->data['flash']= "Licence Extended Successfully ";
$this->load->view('auth/owner_buy_extendedlicence', $this->data);

}







function buy_anether_licence($oid)
{
@$this->session->set_userdata('email', $_POST['email']);	
        $email=$this->session->userdata('owner_name');
			
			$this->data['ownerdetails']=$this->user->get_owner_email($email);
			@$id=$this->data['ownerdetails'][0]->own_id;
			@$otype=$this->data['ownerdetails'][0]->l_type;
			if($_POST)	
			{  
				
				
				
				
				
				
				 
				
			}


 




$this->load->view('front/owner_buy_anethe_licence', $this->data);

}
function paypal_anether1($oid)
{
	@$this->session->set_userdata('email', $_POST['email']);
	 redirect('/auth/paypal_anether2/?id='.$oid, 'refresh');
	
}
function paypal_anether2()
{
	$this->load->view('auth/paypal_anether', $this->data);
}



function buy_anether_success($oid)
{
	$email=$this->session->userdata('email');
	$email1=$this->session->userdata('owner_name');
	$this->data['ownerdetails']=$this->user->get_owner_email($email1);
			@$id=$this->data['ownerdetails'][0]->own_id;
			@$otype=$this->data['ownerdetails'][0]->l_type;
	
	
	if(!empty($email))
	{
	//@$this->session->set_userdata('email', $_POST['email']);
	$num=$this->user->get_owner_licence($oid);
	$id=$this->user->insert_anether_olicence($oid,$email,$num);
	$lic="koala".$oid."_".$num;
	
	if($id)
	{
	$mail = new PHPMailer();
			$mail->IsSMTP();
			$mail->SMTPAuth   = true; 
			$mail->SMTPSecure = "ssl"; 
            $mail->Port       = 465;
			$mail->Host       = "smtp.yandex.com";
			$mail->Username   = "notification@Koala.Care";
			$mail->Password   = "No1L@S@pp1@mo";
			$mail->SetFrom('notification@Koala.Care', 'Koala Team');
	$mail->Subject = "Owner  Login Details"; //subject
		
		
		
		
		
		
		
		
		
		
		
		
		
		 @$to = $email;
				
				@$message = "
				<html>
				<head>
				<title>HTML email</title>
				</head>
				<body>
				<table>
				<tr>
				<th> Email: </th>
				<td>{$email}</td>
				</tr>
				<tr>
				<th> Licence Id: </th>
				<td> $lic </td>
				</tr> 
				</table>
				</body>
				</html>
				";
				
				//$body = eregi_replace("[\]",'',$body);
	$mail->MsgHTML($message);
	//$subject = "Caregiver Registration Details";
	$mail->AddAddress($to, "Test Recipient"); 	
	
	if ($mail->Send()) { 
		//echo "Message sent!"; 
		
		$this->data['message']=$message;
		$this->data['success']="Licence Added Successfully With Licence <font color='red'>".$lic."</font>";
	$this->data['flash']= "Licence Added Successfully With Licence <font color='red'>".$lic."</font>";
		
	}
				
				
				
				
				
				
				
				
				
				
			}
	
	
	}
	else
	{
		$this->data['success']= "Owner Licence Is Not Extented Please Try Again ";
		$this->data['flash']="Owner Licence Is Not Extented Please Try Again ";
		
		
	}
	
	$this->session->unset_userdata('email');
	
	
	$this->load->view('front/owner_buy_anethe_licence', $this->data);
	
	
	



	
}








function register_anether_licence($oid)
{
	$email=$_POST['email'];
	
	$num=$this->user->get_owner_licence($oid);
	
  $id=$this->user->insert_anether_olicence($oid,$email,$num);
$lic="koala".$oid."_".$num;
if($id)
{
	 @$to = $email;
			@$subject = "Owner licence details";
			@$message = "
			<html>
			<head>
			<title>HTML email</title>
			</head>
			<body>
			<table>
			<tr>
			<th> Email: </th>
			<td>{$email}</td>
			</tr>
			<tr>
			<th> Licence Id: </th>
			<td>{ $lic }</td>
			</tr> 
			</table>
			</body>
			</html>
			";
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
			// More headers
			$headers .= 'From: '." Koala Team " . "\r\n";
			if(mail($to,$subject,$message,$headers)){
			 // echo 'Thank you !';
			}else{
			  //echo 'Try again !'; 
			}
		}


$this->data['flash']= "Licence Extended Successfully ";
$this->load->view('auth/owner_buy_extendedlicence', $this->data);
}


/**    The Profetional Registration IsStart Hear  **/

function profetional_res()
{  
  
   
 @$lat;
ob_start();
  /* if($this->session->userdata('role')==''){
     redirect('/auth/owner_register_step1/', 'refresh');
   }
   if($this->session->userdata('uname')==''){
     redirect('/auth/register_step2/', 'refresh');
   }*/
   
  //if(isset($_POST['cnumber']))
$name=$this->session->userdata('name');
$this->session->userdata('surname');
$this->session->userdata('age');
$this->session->userdata('gender');
$this->session->userdata('zipcode');
$this->session->userdata('photo');
$this->session->userdata('phone');
$this->session->userdata('address');
 $this->session->userdata('city');
 $this->session->userdata('state');
$this->session->userdata('qualification');
$this->session->userdata('admitted_to');
$this->session->userdata('from');
$this->session->userdata('cv');
$pemail=$this->session->userdata('pro_email');
//$lat=$this->session->userdata('lat');
//$lag=$this->session->userdata('lon');
/*echo "<script>alert('$res')</script>";*/

if(!empty($pemail))
{
$lat= $this->session->userdata('lat');
$lon= $this->session->userdata('lon');
$ps="select * from professionals where email='$pemail'";
$pe=mysql_query($ps) or die(mysql_error());
$pn=mysql_num_rows($pe);
if($pn<=0)
{
	


 $this->load->view('auth/paypal1', $this->data);








}
if($pn>0)
{
	echo "<script>alert('email is already exist')</script>";
}

}
else
{
$res=0;
//echo json_encode(array('statusid' => $res,'message' => "Professional Data Not Sdded",'response' => $res));
}


  

  $this->load->view('auth/register_step3.php', $this->data);
  
  
  
  

}








function profestion_res_success()
{
	
@$lat= $this->session->userdata('lat');
@$lon= $this->session->userdata('lon');
$pro_email=$this->session->userdata('pro_email');
$passwd=$this->session->userdata('passwordvalue');

$ps="select * from professionals where email='$pro_email'";
$pe=mysql_query($ps) or die(mysql_error());
$pn=mysql_num_rows($pe);
if($pn<=0 and !empty($pro_email))
{
	
          $pid=$this->user->add_profetional($lat,$lon);

			$item_transaction = $_REQUEST['txn_id'];
			$uid=$this->user->update_prof_transation($pid,$item_transaction);	
           $mail = new PHPMailer();
			$mail->IsSMTP();
			$mail->SMTPAuth   = true; 
			$mail->SMTPSecure = "ssl"; 
            $mail->Port       = 465;
			$mail->Host       = "smtp.yandex.com";
			$mail->Username   = "notification@Koala.Care";
			$mail->Password   = "No1L@S@pp1@mo";
			$mail->SetFrom('notification@Koala.Care', 'Koala Team');
			$mail->Subject = "Profetional Login Details"; //subject  
           @$message = "
			<html>
			<head>
			<title>HTML email</title>
			</head>
			<body>
			<table>
			<tr>
			<th> Username: </th>
			<td>{$pro_email}</td>
			</tr>
			<tr>
			<th> Password  </th>
			<td>{ $passwd }</td>
			</tr> 
			</table>
			</body>
			</html>
			";
       
 	       @$body = eregi_replace("[\]",'',$message);
           @$mail->MsgHTML($body);			
			   $to = $pro_email;
			//$subject = "Caregiver Registration Details";
			$mail->AddAddress($to, "Test Recipient"); 
			
			
			if ($mail->Send()) { 
 $msg="Mail Send Successfully ";
}
	   
	   
	   
	   

if(!empty($res))
{
	
//echo json_encode(array('statusid' => 1,'message' => "Professional Data Added Successfully",'response' => $res));
}

$this->session->unset_userdata('role');
$this->session->unset_userdata('username');
$this->session->unset_userdata('pro_email');
$this->session->unset_userdata('passwordvalue');
//$this->session->unset_userdata('name');
//$this->session->unset_userdata('surname');
//$this->session->unset_userdata('age');
//$this->session->unset_userdata('gender');
$this->session->unset_userdata('zipcode');
$this->session->unset_userdata('address');
$this->session->unset_userdata('city');
$this->session->unset_userdata('state');
//$this->session->unset_userdata('pro_email');
$this->session->unset_userdata('qualification');
$this->session->unset_userdata('admitted_to');
$this->session->unset_userdata('from');
$this->session->unset_userdata('phone');
$this->session->unset_userdata('phone2');
$this->session->unset_userdata('phone3');
$this->session->unset_userdata('pathology');
$this->session->unset_userdata('allergies');
$this->session->unset_userdata('intolerances');
$this->session->unset_userdata('note'); 
    $this->session->unset_userdata('ctype');
$this->session->unset_userdata('cnumber');
$this->session->unset_userdata('cvv');
$this->session->unset_userdata('licence');
$this->session->unset_userdata('ltype');
$this->session->unset_userdata('uname');
$this->session->unset_userdata('username');
$this->session->unset_userdata('omobile');
$this->session->unset_userdata('oaddress');
$this->session->unset_userdata('email');
//$this->session->unset_userdata('passwordvalue');


echo "<script> alert('Registration Successful, Click OK to login') ;</script>";
//redirect('/', 'refresh');


 //redirect('/auth/profestion_res_success/', 'refresh');

	
$this->load->view('front/profestion_res_success.php', $this->data);	
}
else
{

$this->load->view('front/profestion_res_success.php', $this->data);	

}


}





}

/*============================================ buy extended licence ==========================================================================*/



/* End of file auth.php */
/* Location: ./application/controllers/auth.php */