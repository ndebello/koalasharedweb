<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Caregiver extends CI_Controller
{
function __construct()
	{
 parent::__construct();
 $this->load->library('form_validation');
 $this->load->library('security');
 $this->load->library('tank_auth');
  //$this->load->library('my_router');
 $this->lang->load('tank_auth');
 $this->load->library('email');
 $this->load->helper('url');
 $this->load->model('user');
 $this->load->model('admin');
 $this->load->model('caregiver_model');
 $this->load->model('patient');
 $this->load->library('user_agent');
 //$this->data['patient'] = $this->patient->get_patient_by_id($this->session->userdata('user_id'));
 $this->data='';
 $this->data['flash']='';
 //if(!$this->session->userdata('user_id')){
 //redirect('/');
 //}
 $username = $this->session->userdata('owner_care');
	if(empty($username))
		{
  redirect('/');
		}

if(!empty($username))
{
	
$row = $this->caregiver_model->get_caregiver1($username);
$pid=$row['pat_id'];
$careid=$row['id'];
$this->session->set_userdata('userid', $careid);		
$this->data['carenotifi1']=$this->caregiver_model->get_carenotification($careid);
$this->data['carenotifi2']=$this->caregiver_model->get_carenoti_prof($careid);
$this->data['carenotifi3']=$this->caregiver_model->get_carenoti_msg($careid);
$this->data['carenotifi4']=$this->caregiver_model->get_carenoti_msg_count($careid);
$this->data['carenotifi5']= $this->caregiver_model->get_carenoti_caremsg($careid);
$this->data['carenotifi6']= $this->caregiver_model->get_carenoti_caremsgcout($careid);
 
}
		
		
}
 function carenotification()
{
	 $username = $this->session->userdata('owner_care');
	$row = $this->caregiver_model->get_caregiver1($username);
	$pid=$row['pat_id'];
	$careid=$row['id'];
	$this->session->set_userdata('userid', $careid);		
	$this->data['carenotifi1']=$this->caregiver_model->get_carenotification($careid);
	$this->data['carenotifi2']=$this->caregiver_model->get_carenoti_prof($careid);
	$this->data['carenotifi3']=$this->caregiver_model->get_carenoti_msg($careid);
    $this->data['carenotifi4']=$this->caregiver_model->get_carenoti_msg_count($careid);
	$this->data['carenotifi5']= $this->caregiver_model->get_carenoti_caremsg($careid);
    $this->data['carenotifi6']= $this->caregiver_model->get_carenoti_caremsgcout($careid);
    $this->data['photo']=$row['photo'];
	$this->data['name']=$row['name'];
	
    $this->load->view('home/caregivernotifications', $this->data);


}


function shownotificationcount()
{
	 $username = $this->session->userdata('owner_care');	
	$row = $this->caregiver_model->get_caregiver1($username);
	$pid=$row['pat_id'];
	$careid=$row['id'];
	$this->session->set_userdata('userid', $careid);		
	$this->data['carenotifi1']=$this->caregiver_model->get_carenotification($careid);
	$this->data['carenotifi2']=$this->caregiver_model->get_carenoti_prof($careid);
	$this->data['carenotifi3']=$this->caregiver_model->get_carenoti_msg($careid);
    $this->data['carenotifi4']=$this->caregiver_model->get_carenoti_msg_count($careid);
	$this->data['carenotifi6']= $this->caregiver_model->get_carenoti_caremsgcout($careid);
    $this->load->view('home/caregivernotificationscount', $this->data);
}

function readallnotifications()
{
	 $username = $this->session->userdata('owner_care');	
	 
	$row = $this->caregiver_model->get_caregiver1($username);
	$pid=$row['pat_id'];
	$careid=$row['id'];
	$this->caregiver_model->get_carenotificationupdate($careid);
	$this->caregiver_model->get_carenoti_prof_update($careid);
	$this->caregiver_model->get_carenoti_msg_update($careid);
	$this->caregiver_model->get_carenoti_caremsgupdate($careid);
	
	
	
}

function sendconsultation($prid)
{
$username = $this->session->userdata('owner_care');	

	$row = $this->caregiver_model->get_caregiver1($username);
	 $pid=$row['pat_id'];
	 $careid=$row['id'];
	 $name=$row['name'];
$subject=$_POST['subject'];
$message=$_POST['message'];
$sendid=$careid;
$recid=$prid;
$patid=$pid;
@$lat= constant("lat");
@$lon= constant("lon");
$ip=$_SERVER['REMOTE_ADDR'];
$date=date("Y-m-d");

$cs="insert into cons_request_messages (sender,recipient,message,date,subject,ip,patid,lat,lon) values('$sendid','$recid','$message','$date','$subject','$ip','$patid','$lat','$lon') ";
$ce=mysql_query($cs) or die(mysql_error());
$lid=mysql_insert_id();

$nmsg=$name."  Hab Send The Consultaion Request ";
$ntype="caregiver_consultation";
$nip=$_SERVER['REMOTE_ADDR'];
$nts="insert into popup_notifications(note_message,n_sid,n_rid,nm_id,status,note_type,n_ip) values('$nmsg','$sendid','$recid','$lid','0','$ntype','$nip')";
$nte=mysql_query($nts) or die(mysql_error());


if($ce)
{
echo "<script> alert('Request Sent Successfully ');  </script>";
  redirect('/caregiver/search/');	
}

}
	
	
	
   function index()
	{
	
	}
	function professional_home()
	{
		$this->load->view('home/professional_index', $this->data);
	}
	function search_caregiver()
	{ 
 if($_POST){
   $this->data['caregivers']= $this->user->caregiverSearch();
 }
 else
 {
  $this->data['caregivers']= $this->user->caregiverList();
 }
 $this->load->view('home/caregiver_list', $this->data);
	}
	function search_professional()
	{ 
 if($_POST)
 {
 $this->data['professionals']= $this->user->professionalSearch();
 }else
 {
  $this->data['professionals']= $this->user->professionalList();
 }
 $this->load->view('home/professional', $this->data);
	}

	function professional_details($id=null)
	{
if (!($this->session->userdata('user_name')!="")) {
redirect('/auth/login/');
		} else 
		{
	 $this->data['user'] = $this->user->getProfessionalsAll($id);
     $this->load->view('home/professional_details', $this->data);
	}
}

/*========================================= caregiver is start hear ======================================================*/	
function caregiver_home($pid)
{ 
	$email=$this->session->userdata('owner_care');
    $this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
	//$pid=$this->data['caregiver'][0]->pat_id;
	$this->data['pid']=$pid;
	$this->session->set_userdata('pid', $pid);
	
	$pid=$this->session->userdata('pid');
	
	
	
	
	
	
	$cid1=$this->data['caregiver'][0]->id;
	$this->data['listcaregiver']=$this->caregiver_model->get_caregiver_list($cid1,$pid);
	$ctype=$this->data['listcaregiver'][0]->care_type_id;
	$this->session->set_userdata('ctype', $ctype);
	
	
	if($_SERVER['REMOTE_ADDR']=="127.0.0.1")
{
$ip="183.82.101.75";
}
else
{
$ip = $_SERVER['REMOTE_ADDR']; 
}
	// the IP address to query
	@$query = unserialize(file_get_contents('http://ip-api.com/php/'.$ip));
	//echo @$lat=$query['lat'];
	//@$lon=$query['lon'];
	 $timezone=$query['timezone'];
	 //$ip_data = json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip));
	 date_default_timezone_set("$timezone");
 
      $date=date('Y-m-d');
	$this->data['patient']=$this->caregiver_model->get_patient($pid);
	if(isset($_POST['submit']))
	{
	$date=$_POST['date'];
	$this->data['date']=$date;
		$this->data['event']=$this->caregiver_model->get_event_list($pid,$date);
	}
	else
	{
	$date=date("Y-m-d");
	$this->data['date']=$date;
	$this->data['event']=$this->caregiver_model->get_event_list($pid,$date);	
	}
	
	$ns="select * from notifications where event_type_id='5' and sub_event_id='9' and note_date='$date' and care_id='$cid1' and pat_id='$pid' ORDER BY `note_date` desc";
   $ne=mysql_query($ns) or die(mysql_error());
   $nm=mysql_num_rows($ne);
   $nr=mysql_fetch_array($ne);
   if($nm>0)
   {
	    $this->data['slp']=substr($nr['note_time'],0,2);
   }
   else
   {
	   $this->data['slp']=0;
   }
	
	
	
	$this->data['caregivers']= $this->caregiver_model->getcaregivers_new($pid,$cid1);
	$this->data['profetional']=$this->caregiver_model->getprofetionals($pid,$cid1);
	
	$this->data['menu']="routine";
	
	
	
	
	
	
	
	
	//$eventid=data['event'][0]->event_type_id;
	/*foreach ($this->data['event'] as $temp) {
$eventid=$temp->event_type_id;
	//$temp->medformat;uti
	$this->data['eventdata']=$this->caregiver_model->get_event_data($eventid);
	echo $name= $this->data['eventdata'][0]->evn_name;
	
	$this->data['ename']=$name;
	}
	*/
	
	$this->load->view('home/caregiver_index', $this->data);
}
	
/*  Event Submit    */

function event_update()
{
	$email=$this->session->userdata('owner_care');
    $this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
	$pid=$this->data['caregiver'][0]->pat_id;
	$cid1=$this->data['caregiver'][0]->id;
	$noteid=$_POST['noteid'];
	$careid=$_POST['careid'];
	$date=date("Y-m-d");
	$up="update notifications set status='1',notifications.update='$date',edit_cid='$cid1' where note_id='$noteid'";
	$ue=mysql_query($up) or die(mysql_error());
	redirect('/caregiver/caregiver_home/'.$pid, 'refresh');

}






/* Owner View The caregiver Details */
function view_caredetails($id)
{   
	//$this->data['states']= $this->user->getStates();
$this->data['caregiver'] = $this->user->get_caregiver($id);

	if($_POST)
	{
    $cid;
    $password=$this->session->userdata('passwordvalue');
	$email=$this->session->userdata('email');

	$cname=$_POST['cname'];
	$surname=$_POST['surname'];
	$zipcode=$_POST['zipcode'];
	$city=$_POST['city'];
	$address=$_POST['address'];
	$contact=$_POST['contact'];
	$country=$_POST['state'];
	$lat=$_POST['lat'];
	$lon=$_POST['lng'];
	$age=$_POST['age'];
	$gender=$_POST['gender'];
	if(!empty($_FILES['photo']['name']))
	{  
 

				//echo "Temp file: " . $_FILES["photo"]["tmp_name"] . "<br>";
				
					//$file=$_FILES["photo"]["name"];
					//if(move_uploaded_file($_FILES["photo"]["tmp_name"],
					//"uploads/" . $file))
					//$this->session->set_userdata('user_photo', $file);
				
$file=$_FILES['photo']['name'];
 $type=$_FILES['photo']['type'];
move_uploaded_file($_FILES['photo']['tmp_name'],"uploads/".$file);

if($type=='image/jpeg')
{
	$fi="uploads/".$file;
   $newHeight=100;
   $newWidth=100;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;
    list($width,$height) = getimagesize($fi);
    $img = imagecreatefromjpeg($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagejpeg($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }
    if($type=='image/png')
    {
    $fi="uploads/".$file;
   $newHeight=100;
   $newWidth=100;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;

    list($width,$height) = getimagesize($fi);
    $img = imagecreatefrompng($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagepng($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }
    
    if($type=='image/gif')
    {
   $fi="uploads/".$file;
   $newHeight=100;
   $newWidth=100;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;

    list($width,$height) = getimagesize($fi);
    $img = imagecreatefromgif($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagegif($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }

    unlink($fi);  
	}
	else
	{
		$nfile=$_POST['photo'];
	}
	
	$us=$this->user->getcaregiversbyid($id);
	
	
   if($us>0)
   {
 //$lat= constant("lat");
 //$lon= constant("lon");
  $us1=$this->user->updatecaregiversbycid1($id,$lat,$lon,$cname,$surname,$zipcode,$city,$address,$contact,$age,$gender,$country,$nfile); 
 if($us1)
 {
	/* echo "<script>alert('caregiver Activated Successfully please login to continue')</script>";*/
	 
	 // redirect('/auth/after_caregiver_register/', 'refresh');
	 $this->data['flash']=" Caregier Details Updates Successfully ";
	 redirect('/caregiver/search/', 'refresh');
	 
	 
 }
   }
   else
   {
   /* echo "<script>alert('Please Enter the correct details again ')</script>";
	redirect('/auth/careregister/'.$cid, 'refresh');*/
	$this->data['flash']=" Not Success ";
	
	
   
   }
   
	}
	
 $this->load->view('home/edit_caregiver', $this->data);
}



function careread($a)
{
$this->data['id']=$a;
	$this->load->view('home/final_carenotifi', $this->data);
}

function caregiver_notifications()
{
	$email=$this->session->userdata('owner_care');
    $this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
	$email=$this->session->userdata('owner_care');


$this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
$pid=$this->data['caregiver'][0]->pat_id;
$this->data['patient']=$this->caregiver_model->get_patient($pid);
$db=$this->data['caregiver'];
//$pid
	$cid1=$row['id'];
	$this->data['caregivers']= $this->caregiver_model->getcaregivers_new($pid,$cid1);
	$this->data['profetional']=$this->caregiver_model->getprofetionals($pid,$cid1);
	$this->data['menu']="setnti";
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	$this->load->view('home/caregiver_notification', $this->data);
}	
function caregiver_set_notifications($pid)
{
	$email=$this->session->userdata('owner_care');
    $this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
    //$this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
	//$pid=$this->data['caregiver'][0]->pat_id;
	$patid=$this->session->userdata('pid');
	if(empty($patid))
	{
		$this->session->set_userdata('pid', $pid);
	}
	
	$pid=$this->session->userdata('pid');
	
	$this->data['patient']=$this->caregiver_model->get_patient($pid);
	$db=$this->data['caregiver'];
	$cid1=$this->data['caregiver'][0]->id;
	$this->data['caregivers']= $this->caregiver_model->getcaregivers_new($pid,$cid1);
	$this->data['profetional']=$this->caregiver_model->getprofetionals($pid,$cid1);
	$this->data['noti']=$this->caregiver_model->get_notify($cid1);
	
	
	
	$this->data['menu']="setnti";
	
	$this->load->view('home/caregiver_set_notification', $this->data);
}	
	
/*/////============ medicine is start hear ======================================//////////////////////////////*/	
	function add_medicine()
	{
$email=$this->session->userdata('owner_care');
$this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
// $pid=$this->data['caregiver'][0]->pat_id;
$pid=$this->session->userdata('pid');
$this->data['patient']=$this->caregiver_model->get_patient($pid);
	$cid1=$this->data['caregiver'][0]->id;
	$this->data['caregivers']= $this->caregiver_model->getcaregivers_new($pid,$cid1);
	$this->data['profetional']=$this->caregiver_model->getprofetionals($pid,$cid1);
	$this->data['menu']="drugs";



 
$this->load->view('home/add_medicine', $this->data);

	}
public function show_drugs($pid)
{
$email=$this->session->userdata('owner_care');
$row = $this->caregiver_model->get_caregiver1($email);
$patid=$this->session->userdata('pid');




           //$currentTime = time() + 3600;
                    
             $time2=date('13:51:00'); 


if(empty($patid))
{
$this->session->set_userdata('pid', $pid);
}
$pid=$this->session->userdata('pid');

$cid1=$row['id'];
$this->data['menu']="drugs";
/*echo "<script>alert('$pid')</script>";*/
$this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
$this->data['caregivers']= $this->caregiver_model->getcaregivers_new($pid,$cid1);
$this->data['profetional']=$this->caregiver_model->getprofetionals($pid,$cid1);


  $data['base']=$this->config->item('base_url');
		$total=$this->user->medicine_count();
		$per_pg=10;
		$offset=$this->uri->segment(3);
		$this->load->library('pagination');
		$config['base_url'] = $data['base'].'/caregiver/show_drugs/';
   $config['total_rows'] = $total;
   $config['per_page'] = $per_pg;
		$config["num_links"]=40;
		$config['full_tag_open'] = '<div id="pagination">';
		$config['full_tag_close'] = '</div>';
        $this->pagination->initialize($config);
        $this->data['pagination']=$this->pagination->create_links();
	    //$this->data['foods']=$this->user->activitylist($per_pg,$offset);
		$eventid=2;
		$date=date("Y-m-d");
		//$this->data['foods']=$this->caregiver_model->fetch_medecine($pid,$per_pg,$offset);
		$this->data['foods']=$this->user->fetch_medecine1($pid,$eventid,$date);
		

$pid=$this->session->userdata('pid');
$this->data['patient']=$this->caregiver_model->get_patient($pid);

	


		$this->load->view('home/show_drugs', $this->data);
	}
	function insert_medicine()
	{
	if(isset($_POST))
		{
	//$this->form_validation->set_rules('patient_id', 'Patient Name', 'trim|required|xss_clean');
	$this->form_validation->set_rules('medname', 'Medicine Name ', 'trim|required|xss_clean');
	$this->form_validation->set_rules('med_type', 'med_type ', 'trim|required|xss_clean');
	$this->form_validation->set_rules('med_note', 'med_note ', 'trim|required|xss_clean');
	$this->form_validation->set_rules('med_qua', 'med_qua  ', 'trim|required|xss_clean');
	$this->form_validation->set_rules('wake_time', 'Wake Time ', 'trim|required|xss_clean');
	$this->form_validation->set_rules('pre_date', 'pre_date  ', 'trim|required|xss_clean');
	$this->form_validation->set_rules('exp_date', 'exp_date  ', 'trim|required|xss_clean');
	$this->form_validation->set_rules('med_price', 'med_price  ', 'trim|required|xss_clean');
	$this->form_validation->set_rules('meal_type', 'med_price  ', 'trim|required|xss_clean');
	//$temp=$_POST['created'];
	//$data=explode('/',$temp);
	//$result=$data[2].'-'.$data[0].'-'.$data[1];
	//$_POST['created']=$result;

//if($this->form_validation->run() == TRUE)

//{
	
	
	//$patient_id=$_POST['patient_id'];
	$patient_id=$this->session->userdata('pid');
	$caregiver_id=$_POST['caregiver_id'];
	$med_dose=$_POST['med_dose'];
	$medname=$_POST['medname'];
	$med_type=$_POST['med_type'];
	$med_note=$_POST['med_note'];
	$med_qua=$_POST['med_qua'];
	//$pre_date=explode("-",$_POST['pre_date']);
	//$pre=$pre_date[2]."-".$pre_date[1]."-".$pre_date[0];
	$pre=$_POST['pre_date'];
	//$exp_date=explode("-",$_POST['exp_date']);
	//$exp=$exp_date[2]."-".$exp_date[1]."-".$exp_date[0];
	$exp=$_POST['exp_date'];
	$med_price=$_POST['med_price'];	
	$meal_type=$_POST['meal_type'];
	
	$mn=$this->caregiver_model->get_medlist($medname,$patient_id);
	if($mn<=0)
	{
	$ins=$this->caregiver_model->insert_medicine($patient_id,$caregiver_id,$medname,$med_type,$med_note,$med_qua,$pre,$exp,$med_price,$meal_type,$med_dose);
	
	
      if($ins)	
	{
	//$this->patient->addperiods();
	$data="Medicine has been added !";
	
	//redirect('/caregiver/show_drugs/'.$patient_id,$data);
	redirect('/caregiver/show_drugs/'.$patient_id."?status=".urlencode($data));
	
	
	
	}
	else
	{
$data="Unsuccesfull, Try again !";
//redirect('/caregiver/show_drugs/'.$patient_id,$data);

redirect('/caregiver/show_drugs/'.$patient_id."?status=".urlencode($data));



	}
	
	}
	else
	{
$data="You Have Entered Same Medicine Details Again!";
redirect('/caregiver/show_drugs/'.$patient_id,$data);
	}
		//}
	//else
	//{
//$this->data['flash']="Unsuccesfull, Try again !";
	//}
		}
	}
	function edit_medicine($id)
	{
	$email=$this->session->userdata('owner_care');
	//$data = $this->caregiver_model->get_caregiver1($email);
	$this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
	
	 //$pid=$this->data['caregiver'][0]->pat_id;
	 $pid=$this->session->userdata('pid');
	 
	 $cid1=$this->data['caregiver'][0]->id;
	$this->data['caregivers']= $this->caregiver_model->getcaregivers_new($pid,$cid1);
	$this->data['profetional']=$this->caregiver_model->getprofetionals($pid,$cid1);
	$this->data['menu']="drugs";
	 
	 
	 
	 
	 
$this->data['patient']=$this->caregiver_model->get_patient($pid);
	
	
		// echo $data['id'];
		$this->data['medicine'] = $this->caregiver_model->get_medicine($id);
		$this->load->view('home/edit_medicine', $this->data);
		
	}
	function view_medicine($id)
	{
		$email=$this->session->userdata('owner_care');
	//$data = $this->caregiver_model->get_caregiver1($email);
	$this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
	
	$this->data['medicine'] = $this->caregiver_model->get_medicine($id);
	
	$this->load->view('home/view_medicine', $this->data);
		
	}
	function update_medicine($id)
	{
	$mid=$id;	
	//$patient_id=$_POST['patient_id'];
	$patient_id=$this->session->userdata('pid');
	
	
	$caregiver_id=$_POST['caregiver_id'];
	$med_dose=$_POST['med_dose'];
	$medname=$_POST['medname'];
	$med_type=$_POST['med_type'];
	$med_note=$_POST['med_note'];
	$med_qua=$_POST['med_qua'];
	//$pre_date=explode("-",$_POST['pre_date']);
	//$pre=$pre_date[2]."-".$pre_date[1]."-".$pre_date[0];
	$pre=$_POST['pre_date'];
	//$exp_date=explode("-",$_POST['exp_date']);
	//$exp=$exp_date[2]."-".$exp_date[1]."-".$exp_date[0];
	$exp=$_POST['exp_date'];
	
	
	$med_price=$_POST['med_price'];	
	$meal_type=$_POST['meal_type'];
		$ins=$this->caregiver_model->update_medicine($patient_id,$caregiver_id,$medname,$med_type,$med_note,$med_qua,$pre,$exp,$med_price,$meal_type,$med_dose,$mid);
		
	$data=" Medicine Details Updated Successfully !";
redirect('/caregiver/show_drugs/'.$patient_id.'?status='.urlencode($data),$data);
	}

/*=====================================  The Activity is start hear      =============================================*/
function show_activity($pid)
{   
  
	
$email=$this->session->userdata('owner_care');
$row = $this->caregiver_model->get_caregiver1($email);
//$pid=$row['pat_id'];
$patid=$this->session->userdata('pid');
	if(empty($patid))
{
$this->session->set_userdata('pid', $pid);
}
$pid=$this->session->userdata('pid');
	
$this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
//$pid=$this->data['caregiver'][0]->pat_id;


$this->data['patient']=$this->caregiver_model->get_patient($pid);
$db=$this->data['caregiver'];
//$pid
	$cid1=$row['id'];
	$this->data['caregivers']= $this->caregiver_model->getcaregivers_new($pid,$cid1);
	$this->data['profetional']=$this->caregiver_model->getprofetionals($pid,$cid1);
	$this->data['menu']="activity";

   $data['base']=$this->config->item('base_url');
		$total=$this->user->count_activity();
		$per_pg=1000;
		$offset=$this->uri->segment(3);
		$this->load->library('pagination');
		$config['base_url'] = $data['base'].'/caregiver/show_activity/';
   $config['total_rows'] = $total;
   $config['per_page'] = $per_pg;
		$config["num_links"]=40;
		$config['full_tag_open'] = '<div id="pagination">';
		$config['full_tag_close'] = '</div>';
        $this->pagination->initialize($config);
        $this->data['pagination']=$this->pagination->create_links();
   $eventid=4;
   //$this->data['activity']=$this->caregiver_model->fetch_activity($pid,$per_pg,$offset);
   
   $date=date("Y-m-d");
   $this->data['activity']=$this->user->fetch_activity1($pid,$eventid,$date);
	
	$this->load->view('home/show_activity', $this->data);
	
}


function show_report($pid)
{
  
	
$email=$this->session->userdata('owner_care');
$row = $this->caregiver_model->get_caregiver1($email);
//$pid=$row['pat_id'];
$patid=$this->session->userdata('pid');
	if(empty($patid))
{
	$this->session->set_userdata('pid', $pid);
}

$pid=$this->session->userdata('pid');

$this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
//$pid=$this->data['caregiver'][0]->pat_id;
$pid=$this->session->userdata('pid');

$this->data['patient']=$this->caregiver_model->get_patient($pid);
$db=$this->data['caregiver'];
//$pid
$cid1=$row['id'];
$this->data['caregivers']= $this->caregiver_model->getcaregivers_new($pid,$cid1);
$this->data['profetional']=$this->caregiver_model->getprofetionals($pid,$cid1);
$this->data['menu']="report";

   /*$data['base']=$this->config->item('base_url');
		$total=$this->user->count_activity();
		$per_pg=10;
		$offset=$this->uri->segment(3);
		$this->load->library('pagination');
		$config['base_url'] = $data['base'].'/caregiver/show_activity/';
   $config['total_rows'] = $total;
   $config['per_page'] = $per_pg;
		$config["num_links"]=40;
		$config['full_tag_open'] = '<div id="pagination">';
		$config['full_tag_close'] = '</div>';
        $this->pagination->initialize($config);
        $this->data['pagination']=$this->pagination->create_links();*/
   $eventid=4;
   //$this->data['activity']=$this->caregiver_model->fetch_activity($pid,$per_pg,$offset);
   //$this->data['activity']=$this->user->fetch_activity1($pid,$eventid);
if($_POST)
{
	
$from=$_POST['from'];
$to=$_POST['to'];
$this->data['report']=$this->user->fetch_newreport1($pid,$from,$to);
$this->data['from']=$from;
$this->data['to']=$to;

}
else
{
$this->data['report']=$this->user->fetch_report1($pid);
}
$this->load->view('home/show_reports', $this->data);
	
}

function newreport()
{
$email=$this->session->userdata('owner_care');
$row = $this->caregiver_model->get_caregiver1($email);
//$pid=$row['pat_id'];
$pid=$this->session->userdata('pid');


$this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
//$pid=$this->data['caregiver'][0]->pat_id;
$pid=$this->session->userdata('pid');

$this->data['patient']=$this->caregiver_model->get_patient($pid);
$db=$this->data['caregiver'];
$cid1=$row['id'];
$this->data['caregivers']= $this->caregiver_model->getcaregivers_new($pid,$cid1);
$this->data['profetional']=$this->caregiver_model->getprofetionals($pid,$cid1);
$this->data['menu']="report";

if($_POST)
{
	$from=$_POST['from'];
	$to=$_POST['to'];
	
	
$this->data['report']=$this->user->fetch_newreport2($pid,$from,$to);
$this->data['from']=$from;
$this->data['to']=$to;

}

$this->load->view('home/new_reports', $this->data);

	
}












function view_report($date)
{  
    
  
$email=$this->session->userdata('owner_care');
$row = $this->caregiver_model->get_caregiver1($email);
//$pid=$row['pat_id'];
$pid=$this->session->userdata('pid');

$this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
$pid=$this->data['caregiver'][0]->pat_id;
$this->data['patient']=$this->caregiver_model->get_patient($pid);
$db=$this->data['caregiver'];
//$pid
	$cid1=$row['id'];
	$this->data['caregivers']= $this->caregiver_model->getcaregivers_new($pid,$cid1);
	$this->data['profetional']=$this->caregiver_model->getprofetionals($pid,$cid1);
	$this->data['menu']="report";
	$date;
	
	$this->data['report']=$this->user->fetch_report_details($pid,$date);
	
	$this->data['date']=$date;
	
	
	
	
	
/*foreach($this->data['report'] as $temp ) { 
$careid=$temp->care_id;
$cs="select * from caregiver where id='$careid'";
$ce=mysql_query($cs) or die(mysql_error());
$cr=mysql_fetch_array($ce);
 $this->data['cname']=$cr['name'];
$this->data['evenid']=$temp->event_type_id;
$this->data['subeveid']=$temp->sub_event_id;
if($evenid==1)
{   $this->data['ename']=" Clinical parameters ";
	$ps="select * from parameter_name where id='$subeveid'";
	$pe=mysql_query($ps) or die(mysql_error());
	$pr=mysql_fetch_array($pe);
	$this->data['sname']=$pr['pname'];
}
if($evenid==2)
{   $this->data['ename']=" Drugs ";
    $ps1="select * from medicine where med_id='$subeveid'";
	$pe1=mysql_query($ps1) or die(mysql_error());
	$pr1=mysql_fetch_array($pe1);
	$this->data['sname']=$pr1['med_name'];

}
if($evenid==3)
{  $this->data['ename']=" Diet ";
    $ps2="select * from diets where id='$subeveid'";
	$pe2=mysql_query($ps2) or die(mysql_error());
	$pr2=mysql_fetch_array($pe2);
	$this->data['sname']=$pr2['food_name'];

}
if($evenid==4)
{  $this->data['ename']=" Activities ";
    $ps3="select * from activities where act_id='$subeveid'";
	$pe3=mysql_query($ps3) or die(mysql_error());
	$pr3=mysql_fetch_array($pe3);
	$this->data['sname']=$pr3['act_name'];
}
if($evenid==5)
{  $this->data['ename']=" Sleep / wake ";
    $ps3="select * from sub_event_type where sub_id='$subeveid'";
	$pe3=mysql_query($ps3) or die(mysql_error());
	$pr3=mysql_fetch_array($pe3);
	$this->data['sname']=$pr3['sub_eventname'];
}






$status=$temp->status; 
if($status==0)
{
	$this->data['sta']="Not Done";
}
else
{
	$this->data['sta']="Done";
}
$this->data['message'].="<tr><td>$cname</td><td>$ename</td><td>$sname</td><td> $temp->note_time </td> <td> $sta </td> </tr>";


?>
								    
							<?php } */
							

	
	
	
	
	
	
	
	
	
	
	
	
	
	$this->load->view('home/reports_details', $this->data);
	
	
		
}


function view_report1($date)
{ 
   
    
  
	$email=$this->session->userdata('owner_care');
	$row = $this->caregiver_model->get_caregiver1($email);
	//$pid=$row['pat_id'];
	$pid=$this->session->userdata('pid');
	
	$this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
//$pid=$this->data['caregiver'][0]->pat_id;
$this->data['patient']=$this->caregiver_model->get_patient($pid);
$db=$this->data['caregiver'];
//$pid
	$cid1=$row['id'];
	$this->data['caregivers']= $this->caregiver_model->getcaregivers_new($pid,$cid1);
	$this->data['profetional']=$this->caregiver_model->getprofetionals($pid,$cid1);
	$this->data['menu']="report";
	$date;
	
	$this->data['report']=$this->user->fetch_report_details($pid,$date);
	
	$this->data['date']=$date;
	
	$this->load->view('home/final_report_download', $this->data);
	
	
		
}

function view_report2($date)
{ 
  
   
    
  
	$email=$this->session->userdata('owner_care');
	$row = $this->caregiver_model->get_caregiver1($email);
	//$pid=$row['pat_id'];
	$pid=$this->session->userdata('pid');
	$this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
//$pid=$this->data['caregiver'][0]->pat_id;
$this->data['patient']=$this->caregiver_model->get_patient($pid);
$patname=$this->data['patient'][0]->name;


$db=$this->data['caregiver'];
//$pid
	$cid1=$row['id'];
	$this->data['caregivers']= $this->caregiver_model->getcaregivers_new($pid,$cid1);
	$this->data['profetional']=$this->caregiver_model->getprofetionals($pid,$cid1);
	$this->data['menu']="report";
	$date;
	
	$this->data['report']=$this->user->fetch_report_details($pid,$date);
	
	$this->data['date']=$date;
	$this->data['pname']=$patname;
	
	$this->load->view('home/actionpdf', $this->data);
	
	
		
}







function view_email($date)
{   
  
$email=$this->session->userdata('owner_care');
$row = $this->caregiver_model->get_caregiver1($email);
//$pid=$row['pat_id'];

$pid=$this->session->userdata('pid');

$this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
    $pid=$this->data['caregiver'][0]->pat_id;
	$this->data['patient']=$this->caregiver_model->get_patient($pid);
	$db=$this->data['caregiver'];
	//$pid
	$cid1=$row['id'];
	$this->data['caregivers']= $this->caregiver_model->getcaregivers_new($pid,$cid1);
	$this->data['profetional']=$this->caregiver_model->getprofetionals($pid,$cid1);
	$this->data['menu']="report";
	$date;
	$this->data['report']=$this->user->fetch_report_details($pid,$date);
	$this->data['date']=$date;
	if($_POST)
	{
	
			  $email=$_POST['email'];
			  $message=$_POST['message'];
			  

//SMTP Settingsss

			$mail = new PHPMailer();
			$mail->IsSMTP();
			$mail->SMTPAuth   = true; 
			$mail->SMTPSecure = "ssl"; 
            $mail->Port       = 465;
			$mail->Host       = "smtp.yandex.com";
			$mail->Username   = "notification@Koala.Care";
			$mail->Password   = "No1L@S@pp1@mo";
			$mail->SetFrom('notification@Koala.Care', 'Koala Team');
			$mail->Subject = "Report   Details"; //subject
			@$to = $email;
			//@$subject ="Owner Login Details";
			$body = $message;
     	    //$body = eregi_replace("[\]",'',$body);
           $mail->MsgHTML($body);			
			//$subject = "Caregiver Registration Details";
			$mail->AddAddress($to, "koala"); 	
			
			if ($mail->Send()) 
			{ 
	     echo "<script>alert('Email Send Successfully')</script>";
			}
		  
	
	
		
	}
	
	
	
	
	
	
	
	
	
	$this->load->view('home/reports_mail', $this->data);
		
}



function actionpdf()
{
	
	$_POST['dwd'];	
	$_POST['date'];
	$_POST['pname'];
	
	
	$this->data['dwd']=$_POST['dwd'];
	$this->data['date']=$_POST['date'];
	$this->data['pname']=$_POST['pname'];
	
	
	$this->load->view('home/actionpdf', $this->data);
}






function new_report()
{
	$email=$this->session->userdata('owner_care');
$row = $this->caregiver_model->get_caregiver1($email);
//$pid=$row['pat_id'];
$pid=$this->session->userdata('pid');


$this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
//$pid=$this->data['caregiver'][0]->pat_id;
$this->data['patient']=$this->caregiver_model->get_patient($pid);
$db=$this->data['caregiver'];
//$pid
	$cid1=$row['id'];
	$this->data['caregivers']= $this->caregiver_model->getcaregivers_new($pid,$cid1);
	$this->data['profetional']=$this->caregiver_model->getprofetionals($pid,$cid1);
	$this->data['menu']="report";
	//$date;
	$date="0";
	$this->data['report']=$this->user->fetch_report_details($pid,$date);
	
	//$this->data['date']=$date;
	
	$this->load->view('home/new_reports', $this->data);
}





function add_activity()
{
$email=$this->session->userdata('owner_care');
$this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
//$pid=$this->data['caregiver'][0]->pat_id;


if($_POST)
	{
	//dateofweek,attached,actdetails,acttype,actname,patient_id,caregiver_id
	//$=$_POST['patient_id'];
	$patient_id=$this->session->userdata('pid');
	
	
	$caregiver_id=$_POST['caregiver_id'];
$actdetails=$_POST['actdetails'];
	//$_POST['acttype']
	$acttype=4;
	$actname=$_POST['actname'];
	@$dateofweek=$_POST['dateofweek'];
	//$att=$_FILES[''][''];
	
if(!empty($_FILES['attached']['name']))
{
$file=time().$_FILES['attached']['name'];
$dis="activity/";
$dis=$dis.$file;
move_uploaded_file($_FILES['attached']['tmp_name'],$dis);
}
else
{
	$file='';
}

$ins=$this->caregiver_model->get_activity_name($actname);
if(count($ins)<=0)
{
$ins=$this->caregiver_model->insert_activity($patient_id,$caregiver_id,$actdetails,$acttype,$actname,$dateofweek,$file);
if($ins)
{
$data="Activity has been added !";
redirect('/caregiver/add_activity/?status='.urlencode($data),$data);	
}
}
else
{
	/*echo "<script>alert('Trying To Enter The Same Activity Name')</script>";*/
//echo "Else";
$data="trying to add the same activity again";
redirect('/caregiver/add_activity/?status='.urlencode($data));
//redirect('/caregiver/add_activity/?status='.urlencode($data),$data);

 
}





	
	}



 $pid=$this->session->userdata('pid');
 
	$cid1=$this->data['caregiver'][0]->id;
	$this->data['caregivers']= $this->caregiver_model->getcaregivers_new($pid,$cid1);
	$this->data['profetional']=$this->caregiver_model->getprofetionals($pid,$cid1);
	$this->data['menu']="activity";

 
 
 
$this->data['patient']=$this->caregiver_model->get_patient($pid);


$this->load->view('home/add_activity', $this->data); 



}

function insert_activity()
{
	

	if($_POST)
	{
	//dateofweek,attached,actdetails,acttype,actname,patient_id,caregiver_id
	//$=$_POST['patient_id'];
	$patient_id=$this->session->userdata('pid');
	
	
	$caregiver_id=$_POST['caregiver_id'];
$actdetails=$_POST['actdetails'];
	//$_POST['acttype']
	$acttype=4;
	$actname=$_POST['actname'];
	@$dateofweek=$_POST['dateofweek'];
	//$att=$_FILES[''][''];
	
if(!empty($_FILES['attached']['name']))
{
$file=time().$_FILES['attached']['name'];
$dis="activity/";
$dis=$dis.$file;
move_uploaded_file($_FILES['attached']['tmp_name'],$dis);
}
else
{
	$file='';
}

$ins=$this->caregiver_model->get_activity_name($actname);
if(count($ins)<=0)
{
$ins=$this->caregiver_model->insert_activity($patient_id,$caregiver_id,$actdetails,$acttype,$actname,$dateofweek,$file);
if($ins)
{
$data="Activity has been added !";
redirect('/caregiver/add_activity/?status='.urlencode($data),$data);	
}
}
else
{
	echo "<script>alert('Trying To Enter The Same Activity Name')</script>";
redirect('/caregiver/add_activity/?status='.urlencode($data),$data);
}





	
	}
}
function getevents($id)
{
	
	//echo "okokokokokokoko";
	$this->data['activity'] = $this->caregiver_model->get_event($id);
	$this->load->view('home/final', $this->data);
	
}
function edit_activity($id)
	{
		
		
	$email=$this->session->userdata('owner_care');
	//$data = $this->caregiver_model->get_caregiver1($email);
	$this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
		// echo $data['id'];
		$this->data['activity'] = $this->caregiver_model->get_activity($id);
	 //$pid=$this->data['caregiver'][0]->pat_id;
	 $pid=$this->session->userdata('pid');
		$cid1=$this->data['caregiver'][0]->id;
		$this->data['caregivers']= $this->caregiver_model->getcaregivers_new($pid,$cid1);
		$this->data['profetional']=$this->caregiver_model->getprofetionals($pid,$cid1);
		$this->data['menu']="activity";

	 
	 
	 
$this->data['patient']=$this->caregiver_model->get_patient($pid);
		
		
		
		
		$this->load->view('home/edit_activity', $this->data);
		
	
	
	}

function update_activity($id)
{
	
	if(!empty($_POST['actname']))
	{
	//dateofweek,attached,actdetails,acttype,actname,patient_id,caregiver_id
//$patient_id=$_POST['patient_id'];

$patient_id=$this->session->userdata('pid');

	$caregiver_id=$_POST['caregiver_id'];
	$actdetails=$_POST['actdetails'];
	$acttype=$_POST['acttype'];
	$actname=$_POST['actname'];
	$dateofweek=$_POST['dateofweek'];
	//$att=$_FILES[''][''];
	
if(!empty($_FILES['attached']['name']))
{
$file=time().$_FILES['attached']['name'];
$dis="activity/";
$dis=$dis.$file;
move_uploaded_file($_FILES['attached']['tmp_name'],$dis);
}
else
{
	$file=$_POST['file'];
}

$ins=$this->caregiver_model->update_activity($patient_id,$caregiver_id,$actdetails,$acttype,$actname,$dateofweek,$file,$id);
if($ins)
{
$data="Activity has been Updated !";
redirect('/caregiver/show_activity/'.$patient_id.'?status='.urlencode($data),$data);	
}

	
	}	
}
function view_activity($id)
	{
		$email=$this->session->userdata('owner_care');
	//$data = $this->caregiver_model->get_caregiver1($email);
	$this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
	
		$this->data['activity'] = $this->caregiver_model->get_activity($id);
	
	$this->load->view('home/view_activity', $this->data);
		
	}
	
/* list Consultaion Requests    */	
function consultationrec($cid)	
{
	$email=$this->session->userdata('owner_care');
    //$data = $this->caregiver_model->get_caregiver1($email);
	$this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
	$this->data['const'] = $this->caregiver_model->get_con($cid);
	//print_r($this->data['const']);
	foreach($this->data['const'] as $temp ) 
{
		$id=$temp->recipient;
}
	$this->load->view('home/show_list_consultaion_req', $this->data);

}

/* caregiver get repays from profetional  */
function replay_from_profetional($pid)
{
	$email=$this->session->userdata('owner_care');
	//$data = $this->caregiver_model->get_caregiver1($email);
	$this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
	$this->load->view('home/caregiver_consult_replay_fromprofetionals', $this->data);
}



function consultation_view($cid)
{
		$email=$this->session->userdata('owner_care');
    //$data = $this->caregiver_model->get_caregiver1($email);
   $this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
		$this->load->view('home/caregiver_consult_requests', $this->data);
	
	
	
}


	
	
/*======================================== =================show diets =====================================================================*/	
function show_diets($pid)
{
$email=$this->session->userdata('owner_care');
$row = $this->caregiver_model->get_caregiver1($email);
//$pid=$row['pat_id'];
$patid=$this->session->userdata('pid');
	if(empty($patid))
{
$this->session->set_userdata('pid', $pid);
}
$pid=$this->session->userdata('pid');


$cid1=$row['id'];
 $this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
//$pid=$this->data['caregiver'][0]->pat_id;
$this->data['patient']=$this->caregiver_model->get_patient($pid);
$this->data['caregivers']= $this->caregiver_model->getcaregivers_new($pid,$cid1);
	$this->data['profetional']=$this->caregiver_model->getprofetionals($pid,$cid1);
	$this->data['menu']="diets";

   $data['base']=$this->config->item('base_url');
		$total=$this->user->diets_count();
		$per_pg=400;
		$offset=$this->uri->segment(3);
		$this->load->library('pagination');
		$config['base_url'] = $data['base'].'/caregiver/show_diets/';
   $config['total_rows'] = $total;
   $config['per_page'] = $per_pg;
		$config["num_links"]=40;
		$config['full_tag_open'] = '<div id="pagination">';
		$config['full_tag_close'] = '</div>';
        $this->pagination->initialize($config);
        $this->data['pagination']=$this->pagination->create_links();
	
	$eventid=3;
	//$this->data['foods']=$this->user->activitylist($per_pg,$offset);
	//$this->data['diets']=$this->caregiver_model->fetch_diets($pid,$per_pg,$offset);
	$date=date("Y-m-d");
	$this->data['diets']=$this->user->fetch_diets1($pid,$eventid,$date);
	
	
	
		$this->load->view('home/show_diets', $this->data);
	
}



function list_intolerance($id)
{
	  
$email=$this->session->userdata('owner_care');
$row = $this->caregiver_model->get_caregiver1($email);
$pid=$this->session->userdata('pid');

$cid1=$row['id'];
$this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
//$pid=$this->data['caregiver'][0]->pat_id;
$this->data['patient']=$this->caregiver_model->get_patient($pid);
$this->data['caregivers']= $this->caregiver_model->getcaregivers_new($pid,$cid1);
$this->data['profetional']=$this->caregiver_model->getprofetionals($pid,$cid1);
$this->data['menu']="diets";

/* $data['base']=$this->config->item('base_url');
		$total=$this->user->diets_count();
		$per_pg=4;
		$offset=$this->uri->segment(3);
		$this->load->library('pagination');
		$config['base_url'] = $data['base'].'/caregiver/show_diets/';
   $config['total_rows'] = $total;
   $config['per_page'] = $per_pg;
		$config["num_links"]=40;
		$config['full_tag_open'] = '<div id="pagination">';
		$config['full_tag_close'] = '</div>';
        $this->pagination->initialize($config);
        $this->data['pagination']=$this->pagination->create_links(); */
//$this->data['foods']=$this->user->activitylist($per_pg,$offset);
$this->data['diets']=$this->caregiver_model->list_dies_likes($pid);
$this->load->view('home/show_diets_likes', $this->data);
	
}



function delete_diets($id)
	{
		//$diets= $this->patient->dietsDetail($id);
		//$img= "foods/".$diets[0]->food_image;
		//$base=base_url().$img;
		//if(unlink($img))
		//{
			//echo "ok";
		//}
		//else
		//{
			//echo "Not";
		//}
		
		
		$this->patient->deletediets($id);
			$pid=$this->session->userdata('pid');
		
		redirect('/caregiver/show_diets/'.$pid);
	}



	function delete_activity($id)
	{

		$this->user->deleteactiviy($id);
			$pid=$this->session->userdata('pid');
		redirect('/caregiver/show_activity/'.$pid);
		
		
		
		
	}








function list_favourites($id)
{  
$email=$this->session->userdata('owner_care');
$row = $this->caregiver_model->get_caregiver1($email);
//$pid=$row['pat_id'];
$pid=$this->session->userdata('pid');

$cid1=$row['id'];
 $this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
$pid=$this->data['caregiver'][0]->pat_id;
$this->data['patient']=$this->caregiver_model->get_patient($pid);
$this->data['caregivers']= $this->caregiver_model->getcaregivers_new($pid,$cid1);
$this->data['profetional']=$this->caregiver_model->getprofetionals($pid,$cid1);
$this->data['menu']="diets";
  /* $data['base']=$this->config->item('base_url');
		$total=$this->user->diets_count();
		$per_pg=4;
		$offset=$this->uri->segment(3);
		$this->load->library('pagination');
		$config['base_url'] = $data['base'].'/caregiver/show_diets/';
   $config['total_rows'] = $total;
   $config['per_page'] = $per_pg;
		$config["num_links"]=40;
		$config['full_tag_open'] = '<div id="pagination">';
		$config['full_tag_close'] = '</div>';
        $this->pagination->initialize($config);
        $this->data['pagination']=$this->pagination->create_links();*/
    	//$this->data['foods']=$this->user->activitylist($per_pg,$offset);
   $this->data['diets']=$this->caregiver_model->list_dies_flikes($pid);
		$this->load->view('home/show_diets_favourites', $this->data);
	
}













function add_diet()
{
	
	
$email=$this->session->userdata('owner_care');
$this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
//$pid=$this->data['caregiver'][0]->pat_id;
 $pid=$this->session->userdata('pid');

$this->data['patient']=$this->caregiver_model->get_patient($pid);
$cid1=$this->data['caregiver'][0]->id;
	$this->data['caregivers']= $this->caregiver_model->getcaregivers_new($pid,$cid1);
	$this->data['profetional']=$this->caregiver_model->getprofetionals($pid,$cid1);
	$this->data['menu']="diets";





if(!empty($_POST['foodname']))
	{
	//dateofweek,attached,actdetails,acttype,actname,patient_id,caregiver_id
	//$=$_POST['patient_id'];
	$patient_id=$this->session->userdata('pid');
	
	$caregiver_id=$_POST['caregiver_id'];
	$foodname=$_POST['foodname'];
	$foodtype=$_POST['foodtype'];
	$fooddel=$_POST['fooddel'];
	$dateofweek=$_POST['dateofweek'];
	$foodlike=$_POST['foodlike'];
	//$att=$_FILES[''][''];
/*$file=time().$_FILES['attached']['name'];
$dis="foods/";
$dis=$dis.$file;
move_uploaded_file($_FILES['attached']['tmp_name'],$dis);*/

if(!empty($_FILES['attached']['name']))
	{    


				//echo "Temp file: " . $_FILES["photo"]["tmp_name"] . "<br>";
				
					//$file=$_FILES["photo"]["name"];
					//if(move_uploaded_file($_FILES["photo"]["tmp_name"],
					//"uploads/" . $file))
					//$this->session->set_userdata('user_photo', $file);
				
$file=$_FILES['attached']['name'];
 $type=$_FILES['attached']['type'];
move_uploaded_file($_FILES['attached']['tmp_name'],"foods/".$file);

if($type=='image/jpeg')
{
	$fi="foods/".$file;
   $newHeight=100;
   $newWidth=100;
   $nfile=time().$file;
   $destImage="foods/".time().$file;
    list($width,$height) = getimagesize($fi);
    $img = imagecreatefromjpeg($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagejpeg($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }
    if($type=='image/png')
    {
    $fi="foods/".$file;
   $newHeight=100;
   $newWidth=100;
   $nfile=time().$file;
   $destImage="foods/".time().$file;

    list($width,$height) = getimagesize($fi);
    $img = imagecreatefrompng($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagepng($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }
    
    if($type=='image/gif')
    {
   $fi="foods/".$file;
   $newHeight=100;
   $newWidth=100;
   $nfile=time().$file;
   $destImage="foods/".time().$file;

    list($width,$height) = getimagesize($fi);
    $img = imagecreatefromgif($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagegif($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }

    unlink($fi); 
	}
	else
	{
		//$nfile=$_POST['attached'];
	}








$fs="select * from diets where food_name='$foodname'";
$fe=mysql_query($fs) or die(mysql_error());
 $fn=mysql_num_rows($fe);
if($fn<=0)
{
$ins=$this->caregiver_model->insert_diet($patient_id,$caregiver_id,$foodname,$foodtype,$fooddel,$dateofweek,$nfile,$foodlike);

$is="insert into foodlike(foodid,patid,foodlike) values ('$ins','$patient_id','$foodlike')";
$ie=mysql_query($is) or die(mysql_error());

if($ie)
{
$data="diets has been added !";

//redirect('/caregiver/show_diets/'.$patient_id,$data);	
redirect('/caregiver/show_diets/'.$patient_id."?status=".urlencode($data));	
}
}
	else
	{
		//echo "Not Successfull";
		/*echo "<script>alert('$fn')</script>";*/

		
		$data="Trying To Add Same Food Name please try with anether name";
		
		
redirect('/caregiver/show_diets/'.$patient_id."?status=".urlencode($data));	
		
		
	}
	}


/*




*/






$this->load->view('home/add_diets', $this->data);
}


function insert_diets()
{
	
	if(isset($_POST['addact']))
	{
	//dateofweek,attached,actdetails,acttype,actname,patient_id,caregiver_id
	//$=$_POST['patient_id'];
	$patient_id=$this->session->userdata('pid');
	
	$caregiver_id=$_POST['caregiver_id'];
	$foodname=$_POST['foodname'];
	$foodtype=$_POST['foodtype'];
	$fooddel=$_POST['fooddel'];
	$dateofweek=$_POST['dateofweek'];
	$foodlike=$_POST['foodlike'];
	//$att=$_FILES[''][''];
$file=time().$_FILES['attached']['name'];
$dis="foods/";
$dis=$dis.$file;
move_uploaded_file($_FILES['attached']['tmp_name'],$dis);
$fs="select * from diets where food_name='$foodname'";
$fe=mysql_query($fs) or die(mysql_error());
$fn=mysql_num_rows($fe);
if($fn<=0)
{
$ins=$this->caregiver_model->insert_diet($patient_id,$caregiver_id,$foodname,$foodtype,$fooddel,$dateofweek,$file,$foodlike);
if($ins)
{
$data="diets has been added !";
redirect('/caregiver/show_diets/'.$patient_id,$data);	
}
}
	
	}
}

function edit_diets($id=null)
	{
		$email=$this->session->userdata('owner_care');
		//$data = $this->caregiver_model->get_caregiver1($email);
		$this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
		
	//$pid=$this->data['caregiver'][0]->pat_id;
	$pid=$this->session->userdata('pid');
	
$this->data['patient']=$this->caregiver_model->get_patient($pid);
		$this->data['diets'] = $this->caregiver_model->get_diets($id);
		
		$cid1=$this->data['caregiver'][0]->id;
	$this->data['caregivers']= $this->caregiver_model->getcaregivers_new($pid,$cid1);
	$this->data['profetional']=$this->caregiver_model->getprofetionals($pid,$cid1);
	$this->data['menu']="diets";
		
		
		
		
	if(!empty($_POST['foodname']))
	{
	//dateofweek,attached,actdetails,acttype,actname,patient_id,caregiver_id
	//@$=$_POST['patient_id'];
	$patient_id=$this->session->userdata('pid');
	
	@$caregiver_id=$_POST['caregiver_id'];
	@$foodname=$_POST['foodname'];
	@$foodtype=$_POST['foodtype'];
	@$fooddel=$_POST['fooddel'];
	@$dateofweek=$_POST['dateofweek'];
	@$foodlike=$_POST['foodlike'];
	//$att=$_FILES[''][''];
/*$file=time().$_FILES['attached']['name'];
$dis="foods/";
$dis=$dis.$file;
move_uploaded_file($_FILES['attached']['tmp_name'],$dis);*/

if(!empty($_FILES['attached']['name']))
	{    


				//echo "Temp file: " . $_FILES["photo"]["tmp_name"] . "<br>";
				
					//$file=$_FILES["photo"]["name"];
					//if(move_uploaded_file($_FILES["photo"]["tmp_name"],
					//"uploads/" . $file))
					//$this->session->set_userdata('user_photo', $file);
				
$file=$_FILES['attached']['name'];
 $type=$_FILES['attached']['type'];
move_uploaded_file($_FILES['attached']['tmp_name'],"foods/".$file);

if($type=='image/jpeg')
{
	$fi="foods/".$file;
   $newHeight=100;
   $newWidth=100;
   $nfile=time().$file;
   $destImage="foods/".time().$file;
    list($width,$height) = getimagesize($fi);
    $img = imagecreatefromjpeg($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagejpeg($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }
    if($type=='image/png')
    {
    $fi="foods/".$file;
   $newHeight=100;
   $newWidth=100;
   $nfile=time().$file;
   $destImage="foods/".time().$file;

    list($width,$height) = getimagesize($fi);
    $img = imagecreatefrompng($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagepng($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }
    
    if($type=='image/gif')
    {
   $fi="foods/".$file;
   $newHeight=100;
   $newWidth=100;
   $nfile=time().$file;
   $destImage="foods/".time().$file;

    list($width,$height) = getimagesize($fi);
    $img = imagecreatefromgif($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagegif($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }

    unlink($fi); 
	}
	else
	{
		$nfile=$_POST['attached'];
	}








$fs="select * from diets where food_name='$foodname'";
$fe=mysql_query($fs) or die(mysql_error());
 $fn=mysql_num_rows($fe);

$ins=$this->caregiver_model->update_diet($patient_id,$caregiver_id,$foodname,$foodtype,$fooddel,$dateofweek,$nfile,$foodlike,$id);
$flikeid1=$_POST['flikeid1'];
$flikeid=$_POST['flikeid'];
$fs="select * from foodlike where patid='$patient_id' and foodid='$id'";
$fe=mysql_query($fs) or die(mysql_error());
$fn=mysql_num_rows($fe);
if($fn>0)
{
$is="UPDATE foodlike set foodlike='$foodlike' where id='$flikeid1'";
$ie=mysql_query($is) or die(mysql_error());
}
if($fn<=0)
{
$is="insert into  foodlike (foodid,patid,foodlike) values ('$id','$patient_id','$foodlike') ";
$ie=mysql_query($is) or die(mysql_error());
	
}
if($ie)
{
$data="diets has been Updated !";
redirect('/caregiver/show_diets/'.$patient_id,$data);	
}

	
	}
		
		// echo $data['id'];
		
		$this->load->view('home/edit_diets', $this->data);
	}
function update_diets($id)
{
	
	if(isset($_POST['addact']))
	{
	//dateofweek,attached,actdetails,acttype,actname,patient_id,caregiver_id
	//echo $=$_POST['patient_id'];
	$patient_id=$this->session->userdata('pid');
	
	$caregiver_id=$_POST['caregiver_id'];
	$foodname=$_POST['foodname'];
	$foodtype=$_POST['foodtype'];
	$fooddel=$_POST['fooddel'];
	$dateofweek=$_POST['dateofweek'];
	$foodlike=$_POST['foodlike'];
	//$att=$_FILES[''][''];
	if(!empty($_FILES['attached']['name']))
	{
$file=time().$_FILES['attached']['name'];
$dis="foods/";
$dis=$dis.$file;
move_uploaded_file($_FILES['attached']['tmp_name'],$dis);
	}
	else
	{
		$file=$_POST['attached'];
	}
$ins=$this->caregiver_model->update_diet($patient_id,$caregiver_id,$foodname,$foodtype,$fooddel,$dateofweek,$file,$foodlike,$id);

	if($ins)
{
$data=" diets updated has been added !";
redirect('/caregiver/show_diets/'.$patient_id,$data);	
}

	
	}
}

function edit_notification($nid)
{
	$email=$this->session->userdata('owner_care');
	$this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
	//$this->data['nid']=$nid;
	//$pid=$this->data['caregiver'][0]->pat_id;
	$pid=$this->session->userdata('pid');
	
	$this->data['patient']=$this->caregiver_model->get_patient($pid);
	
	$cid1=$this->data['caregiver'][0]->id;
	$this->data['caregivers']= $this->caregiver_model->getcaregivers_new($pid,$cid1);
	$this->data['profetional']=$this->caregiver_model->getprofetionals($pid,$cid1);
	$this->data['menu']="routine";
	
	
	
	
	$this->data['events']=$this->caregiver_model->eventlist();
	$this->data['editnoti']=$this->caregiver_model->edit_notification($nid);
	
	if($_POST)
	{
		$status=$_POST['status'];
   $time=$_POST['time']; 
		$udate=date("Y-m-d");
		$us="update notifications set status='$status',note_time='$time' where note_id='$nid'";
		$ue=mysql_query($us) or die(mysql_error());
		$this->data['flash']="Event Updated Successfully";
		redirect('/caregiver/caregiver_home/'.$pid,$this->data);
	
	}
	$this->load->view('home/edit_event', $this->data);
	
}
	




	
	
	
function update_event($nid,$cid,$pid)
{
	$nid;
	$status=$_POST['status'];
	$pid=$this->session->userdata('pid');
	if($status==1)
	{
		$udate=date("Y-m-d");
		$us="update care_notification set read1=1 where care_id='$cid' and note_id='$nid'";
		$ue=mysql_query($us) or die(mysql_error());
	/*	$cs="select * from caregiver where pat_id='$pid' and care_type_id=3";
		$ce=mysql_query($cs) or die(msql_error());
	while($cr=mysql_fetch_array($ce))
	{
		$mns="select * from moniter_notification where note_id='$nid'";
		$mne=mysql_query($mns) or die(mysql_error());
		$mnu=mysql_num_rows($mne);
		if($mnu<=0)
		{
		$is="insert into moniter_notification (care_id,note_id,patien_id,moniter_notification.read1) values('$cr[id]','$nid','$pid',0)";
		$ie=mysql_query($is) or die(mysql_error());
		}
	}
	*/
	$this->data['pre']=$this->caregiver_model->update_event($nid,$cid,$status);
	}
	$data=" Status updated successfully !";
	
redirect('/caregiver/caregiver_home/'.$pid.'?status='.urlencode($data),$data);	
	}

function view_diets($id)
	{
		$email=$this->session->userdata('owner_care');
		$this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
		$this->data['diets'] = $this->caregiver_model->get_diets($id);
		$this->load->view('home/view_diets', $this->data);
		
	}
/*================================================================ Event Is Start Hear =========================================================*/
function add_event()
{
$email=$this->session->userdata('owner_care');
$this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
//$pid=$this->data['caregiver'][0]->pat_id;

$pid=$this->session->userdata('pid');

	$this->data['patient']=$this->caregiver_model->get_patient($pid);
$this->data['events']=$this->caregiver_model->eventlist();
	$cid1=$this->data['caregiver'][0]->id;
	$this->data['caregivers']= $this->caregiver_model->getcaregivers_new($pid,$cid1);
	$this->data['profetional']=$this->caregiver_model->getprofetionals($pid,$cid1);
	$this->data['menu']="routine";
$this->load->view('home/add_event', $this->data);
}



function  add_caregiver_parameter()
{
	$email=$this->session->userdata('owner_care');
	$this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
	//$pid=$this->data['caregiver'][0]->pat_id;
	$pid=$pid=$this->session->userdata('pid');
	
	
	$this->data['patient']=$this->caregiver_model->get_patient($pid);
	//$this->data['events']=$this->caregiver_model->eventlist();
$this->data['parameters']=$this->caregiver_model->parameteslist();
	
	$cid1=$this->data['caregiver'][0]->id;
	$this->data['caregivers']= $this->caregiver_model->getcaregivers_new($pid,$cid1);
	$this->data['profetional']=$this->caregiver_model->getprofetionals($pid,$cid1);
	$this->data['menu']="parameter";
	
	
	$this->load->view('home/add_care_parameter', $this->data);
	
	
}





function add_parameter()
{
	
	$email=$this->session->userdata('owner_care');
	$this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
	//$pid=$this->data['caregiver'][0]->pat_id;
	$pid=$this->session->userdata('pid');
	
	
	$this->data['patient']=$this->caregiver_model->get_patient($pid);
	//$this->data['events']=$this->caregiver_model->eventlist();
$this->data['parameters']=$this->caregiver_model->parameteslist();
	
	$cid1=$this->data['caregiver'][0]->id;
	$this->data['caregivers']= $this->caregiver_model->getcaregivers_new($pid,$cid1);
	$this->data['profetional']=$this->caregiver_model->getprofetionals($pid,$cid1);
	$this->data['menu']="parameter";
	
	if($_POST)
	{
		$caregiver_id=$_POST['caregiver_id'];
		$patient_id=$_POST['patient_id'];
		$date=date("Y-m-d");
		$praid=$_POST['acttype'];
		$para=$_POST['para'];
		$pnum=count($_POST['para']);
		/*for($i=0;$i<$pnum;$i++)
		{
			
			@$par.=",".$para[$i];
		}*/
		
		if($pnum==1)
		{
			$opt1=$para[0];
			
		}
	
		
		
		if($pnum==2)
		{
			$opt1=$para[0];
			$opt2=$para[1];
		}
		if($pnum==3)
		{
			$opt1=$para[0];
			$opt2=$para[1];
			$opt3=$para[2];
		}
		if($pnum==4)
		{
			$opt1=$para[0];
			$opt2=$para[1];
			$opt3=$para[2];
			$opt4=$para[3];
		}
		if($pnum==5)
		{
			$opt1=$para[0];
			$opt2=$para[1];
			$opt3=$para[2];
			$opt4=$para[3];
			$opt5=$para[4];
		}

		
		
		
		
		
		
		
		//echo $par;
		//$vra=$caregiver_id.",".$patient_id.",".$date.",".$praid.$par;
		$ins=$this->caregiver_model->parametesinsert($caregiver_id,$patient_id,$praid,@$opt1,@$opt2,@$opt3,@$opt4,@$opt5);
	}
	
	
	
	
	$this->load->view('home/add_parameter', $this->data);
}


function add_parameter1($id,$eid)
{
	
	$email=$this->session->userdata('owner_care');
	$this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
	//$pid=$this->data['caregiver'][0]->pat_id;
	$pid=$this->session->userdata('pid');
	
	$this->data['patient']=$this->caregiver_model->get_patient($pid);
	//$this->data['events']=$this->caregiver_model->eventlist();
$this->data['parameters']=$this->caregiver_model->parameteslist();
	
	$cid1=$this->data['caregiver'][0]->id;
	$this->data['caregivers']= $this->caregiver_model->getcaregivers_new($pid,$cid1);
	$this->data['profetional']=$this->caregiver_model->getprofetionals($pid,$cid1);
	$this->data['menu']="parameter";
	
	if($_POST)
	{
	$email=$this->session->userdata('owner_care');
	$caregiver_id=$_POST['caregiver_id'];
		//$=$_POST['patient_id'];
		$patient_id=$this->session->userdata('pid');
		
		$date=date("Y-m-d");
		$praid=$_POST['acttype'];
		$para=$_POST['para'];
		$pnum=count($_POST['para']);
		$evid=$_POST['evid'];
		/*for($i=0;$i<$pnum;$i++)
		{
			@$par.=",".$para[$i];
		}*/
		
		if($pnum==1)
		{
			$opt1=$para[0];
			
		}
	
		
		
		if($pnum==2)
		{
			$opt1=$para[0];
			$opt2=$para[1];
		}
		if($pnum==3)
		{
			$opt1=$para[0];
			$opt2=$para[1];
			$opt3=$para[2];
		}
		if($pnum==4)
		{
			$opt1=$para[0];
			$opt2=$para[1];
			$opt3=$para[2];
			$opt4=$para[3];
		}
		if($pnum==5)
		{
			$opt1=$para[0];
			$opt2=$para[1];
			$opt3=$para[2];
			$opt4=$para[3];
			$opt5=$para[4];
		}

		
		$ueven=$this->caregiver_model->update_event1($eid);
		
		//echo $par;
		//$vra=$caregiver_id.",".$patient_id.",".$date.",".$praid.$par;
		$ins=$this->caregiver_model->parametesinsert($caregiver_id,$patient_id,$praid,@$opt1,@$opt2,@$opt3,@$opt4,@$opt5);
		
		redirect('/caregiver/showparameter/'.$pid);
		
	}
	
		$ustatus=$this->caregiver_model->num_notification($eid);
		echo $status=$ustatus[0]->status;
		if($status==1)
		{
			redirect('/caregiver/showparameter/'.$pid);
		}
		
		
		
	$this->data['id']=$id;
	$this->data['evid']=$eid;
	
	
	$this->load->view('home/add_parameter1', $this->data);
}













function getmoreevents($id,$pid)
{
	
	//echo "okokokokokokoko";
	$this->data['events'] = $this->caregiver_model->get_moreevent($id);
	$this->data['flash']=$pid;
	$this->load->view('home/final1', $this->data,$this->data);
	
}

function getparameterlist($id)
{
	
	//echo "okokokokokokoko";
	$this->data['parameter'] = $this->caregiver_model->get_moreparameter($id);
	//$this->data['flash']=$id;
	$this->load->view('home/final6_parameter', $this->data);
	
}








function showmedicice($id)
{
	
	//echo "okokokokokokoko";
	$this->data['medi'] = $this->caregiver_model->get_mormedicinies($id);
	//$this->data['flash']=$pid;
	
	$this->load->view('home/medicine_final1', $this->data,$this->data);
	
}

function showactivity($id)
{
	
	//echo "okokokokokokoko";
	//$this->data['medi'] = $this->caregiver_model->get_mormedicinies($id);
	$this->data['activity'] = $this->caregiver_model->get_activity($id);
	
	
	//$this->data['flash']=$pid;
	
	$this->load->view('home/activity_final1', $this->data,$this->data);
	
}

function show_report_details($eid,$sid)
{ 
	
	$email=$this->session->userdata('owner_care');
	$this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
	//$pid=$this->data['caregiver'][0]->pat_id;
	$pid=$this->session->userdata('pid');
	
	
	$this->data['patient']=$this->caregiver_model->get_patient($pid);
	//$this->data['events']=$this->caregiver_model->eventlist();
$this->data['parameters']=$this->caregiver_model->parameteslist();
	
	$cid1=$this->data['caregiver'][0]->id;
	
	
	
	//$this->data['activity'] = $this->caregiver_model->get_activity($eid);
    $this->data['eid']=$eid;
	$this->data['sid']=$sid;
	$this->data['pid']=$pid;
	$this->data['cid']=$cid1;
	
	
	//$this->data['flash']=$pid;
	
	$this->load->view('home/report_final1', $this->data);
	
}







function  insert_event()
{
//$=$_POST['patient_id'];

$patient_id=$this->session->userdata('pid');
$pid=$this->session->userdata('pid');

	$caregiver_id=$_POST['caregiver_id'];
	$acttype=$_POST['acttype'];
	$actdetails=$_POST['actdetails'];
	
	if($acttype==1)
	{
		$ps="select * from parameter_name where id='$actdetails'";
		$pe=mysql_query($ps) or die(mysql_error());
		$pr=mysql_fetch_array($pe);
		$adel=$pr['pname'];
		
	}
	if($acttype==2)
	{
		
		$ps="select * from medicine where med_id='$actdetails'";
		$pe=mysql_query($ps) or die(mysql_error());
		$pr=mysql_fetch_array($pe);
		$adel=$pr['med_name'];
		
		
	}
	if($acttype==3)
	{
		
		$ps="select * from diets where id='$actdetails'";
		$pe=mysql_query($ps) or die(mysql_error());
		$pr=mysql_fetch_array($pe);
		$adel=$pr['food_name'];
		
		
	}
	if($acttype==4)
	{
		
		$ps="select * from activities where act_id='$actdetails'";
		$pe=mysql_query($ps) or die(mysql_error());
		$pr=mysql_fetch_array($pe);
		$adel=$pr['act_name'];
		
	}
	if($acttype==5)
	{
		$ps="select * from sub_event_type where sub_id='$actdetails'";
		$pe=mysql_query($ps) or die(mysql_error());
		$pr=mysql_fetch_array($pe);
		$adel=$pr['sub_eventname'];
		
	}
	
	
	
	$time=$_POST['time'];
	$date=$_POST['date'];
	
$ins11="insert into notifications(pat_id,care_id,event_type_id,sub_event_id,note_time,note_date,note_ipaddress,notifications.status,notifications.read,sub_event_name) values('$patient_id','$caregiver_id','$acttype','$actdetails','$time','$date','','0','0','$adel')";
 	$exe=mysql_query($ins11) or die(mysql_error());
	$ins=mysql_insert_id();
	//$ins=$this->caregiver_model->insert_event($patient_id,$caregiver_id,$acttype,$actdetails,$time,$date);
	//and (care_type_id=1 or care_type_id=2)
	if($ins)
	{
    $ms="select * from caregiver where id!='$caregiver_id' and pat_id='$patient_id' ";
	$me=mysql_query($ms) or die(mysql_error());
	$mn=mysql_num_rows($me);
	if($mn>0)
	{
	while($mr=mysql_fetch_array($me))
	{
		//$is="insert into care_notification (note_id,pati_id,care_id) values('$ins','$patient_id','$mr[id]')";
		//$ie=mysql_query($is) or die(mysql_error());
		redirect('/caregiver/caregiver_home/'.$pid.'?status=event is set successfully');
	}
	if($ie)
	{
		redirect('/caregiver/caregiver_home/'.$pid.'?status=event is set successfully');
	}
	}
	else
	{
		redirect('/caregiver/caregiver_home/'.$pid.'?status=event is set successfully');
	}
	//redirect('/caregiver/add_event/?status=event is set successfully');
	}
	
	
	
}

/*================================================///////// notification is start/////////////////=============================================*/

function add_notification()
{
	
	

$email=$this->session->userdata('owner_care');
$this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
//$pid=$this->data['caregiver'][0]->pat_id;
$pid=$this->session->userdata('pid');



$this->data['patient']=$this->caregiver_model->get_patient($pid);
$cid1=$this->data['caregiver'][0]->id;
	$this->data['caregivers']= $this->caregiver_model->getcaregivers_new($pid,$cid1);
	$this->data['profetional']=$this->caregiver_model->getprofetionals($pid,$cid1);
	$this->data['menu']="setnti";






$this->load->view('home/add_notification', $this->data);
	}
function insert_notif()
{
	
	
	//$=$_POST['patient_id'];
	$patient_id=$this->session->userdata('pid');
	
	
	$caregiver_id=$_POST['caregiver_id'];
	$acttype=$_POST['acttype'];
	$actdetails=$_POST['actdetails'];
	$from_date=explode("-",$_POST['fromd']);
	$fr=$from_date[2]."-".$from_date[1]."-".$from_date[0];
	$to_date=explode("-",$_POST['to']);
	$to=$to_date[2]."-".$to_date[1]."-".$to_date[0];
	$dayweek=$_POST['dayweek'];
	$frequency=$_POST['frequency'];
	$place=$_POST['place'];
	$time=$_POST['time'];
	$ins=$this->caregiver_model->insert_care_notification($patient_id,$caregiver_id,$acttype,$actdetails,$fr,$to,$dayweek,$frequency,$place,$time);
    //and (care_type_id=1 or care_type_id=2)
   $ms="select * from caregiver where id!='$caregiver_id' and pat_id='$patient_id'";
	$me=mysql_query($ms) or die(mysql_error());
	while($mr=mysql_fetch_array($me))
	{
		$is="insert into care_notification (note_id,pati_id,care_id) values('$ins','$patient_id','$mr[id]')";
		//$ie=mysql_query($is) or die(mysql_error());
	}
	if(@$ie)
	{
		//redirect('/caregiver/add_notification/?status=Notification is set successfully');
	}
	
	redirect('/caregiver/add_notification/?status=Notification is set successfully');
	

}
/*===============================================================  Insert Notification ========================================================*/

function insert_notif1()
{
     $withp=$_POST['withp']; 	
//$=$_POST['patient_id'];
$patient_id=$this->session->userdata('pid');


$caregiver_id=$_POST['caregiver_id'];
$parameter=$_POST['parameter'];
$medicine=$_POST['medicine'];
$diet=$_POST['diet'];
 $activity=$_POST['activity'];
	$cdate=date("Y-m-d");
   	//echo "if(!empty($parameter) and !empty($medicine) and !empty($diet) and !empty($activity))";
	if(!empty($parameter) and !empty($medicine) and !empty($diet) and !empty($activity))
	{
	$ins=$this->caregiver_model->insert_set_notification($patient_id,$caregiver_id,$parameter,$medicine,$diet,$activity,$cdate,$withp);
	if($ins)
	{
		redirect('/caregiver/caregiver_set_notifications/'.$patient_id);
	}
   
	}
	else
	{
	//redirect('/caregiver/add_notification/?status=Please Fill The All Fields');
	}
}


function edit_notif($nid)
{
	
	$email=$this->session->userdata('owner_care');
$this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
//$pid=$this->data['caregiver'][0]->pat_id;
$pid=$this->session->userdata('pid');


$this->data['patient']=$this->caregiver_model->get_patient($pid);
$cid1=$this->data['caregiver'][0]->id;
	$this->data['caregivers']= $this->caregiver_model->getcaregivers_new($pid,$cid1);
	$this->data['profetional']=$this->caregiver_model->getprofetionals($pid,$cid1);
	$this->data['menu']="setnti";
	
	
	
	///echo "this is edit notification";
	$this->data['nid']=$nid;
	$this->data['notification']=$this->caregiver_model->editnotification($nid);
	
	
	
	
	if($_POST)
	{
	$withp=$_POST['withp']; 	
 //$=$_POST['patient_id'];
 
 $patient_id=$this->session->userdata('pid');
 
$caregiver_id=$_POST['caregiver_id'];
$parameter=$_POST['parameter'];
$medicine=$_POST['medicine'];
$diet=$_POST['diet'];
$activity=$_POST['activity'];
	$cdate=date("Y-m-d");
   	//echo "if(!empty($parameter) and !empty($medicine) and !empty($diet) and !empty($activity))";
	
	$ins=$this->caregiver_model->update_set_notification($patient_id,$caregiver_id,$parameter,$medicine,$diet,$activity,$cdate,$nid,$withp);
	if($ins)	
		{
			$this->data['status']="Updated Successfully";
			
			redirect('/caregiver/caregiver_set_notifications'.$patient_id);
			
		}
		
		
	}
	
	
	
	
	$this->load->view('home/edit_notification', $this->data);
	
}




/*==================================================== edit caregiver ======================================================================== */
function withpatient()
{
	$pid=$this->session->userdata('pid');
@$this->session->set_userdata('with_patient', "with_p");
redirect('/caregiver/caregiver_home/'.$pid);
}

function with_out_patient()
{
	$this->session->unset_userdata('with_patient');
	$pid=$this->session->userdata('pid');
	redirect('/caregiver/caregiver_home/'.$pid);
	
}


function edit_caregiver()
{
$email=$this->session->userdata('owner_care');
//$data = $this->caregiver_model->get_caregiver1($email);
$this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);	

$this->load->view('home/edit_caregivers', $this->data);	
	
}
function update_caregiver($cid)
{

	
	if(isset($_POST['addact']))
	{
	//dateofweek,attached,actdetails,acttype,actname,patient_id,caregiver_id
	//echo $=$_POST['patient_id'];
	$patient_id=$this->session->userdata('pid');
	
	
	$caregiver_id=$cid;
	$cname=$_POST['cname'];
	$sname=$_POST['sname'];
	$age=$_POST['age'];
	$gender=$_POST['gender'];
	$city=$_POST['city'];
	$state=$_POST['state'];
	$phone=$_POST['phone'];
	$qua=$_POST['qua'];
	$address=$_POST['address'];
	
	
	//$att=$_FILES[''][''];
	if(!empty($_FILES['photo']['name']))
	{

				//echo "Temp file: " . $_FILES["photo"]["tmp_name"] . "<br>";
				
					//$file=$_FILES["photo"]["name"];
					//if(move_uploaded_file($_FILES["photo"]["tmp_name"],
					//"uploads/" . $file))
					//$this->session->set_userdata('user_photo', $file);
				
$file=$_FILES['photo']['name'];
 $type=$_FILES['photo']['type'];
move_uploaded_file($_FILES['photo']['tmp_name'],"uploads/".$file);

if($type=='image/jpeg')
{
	$fi="uploads/".$file;
   $newHeight=100;
   $newWidth=100;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;
    list($width,$height) = getimagesize($fi);
    $img = imagecreatefromjpeg($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagejpeg($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }
    if($type=='image/png')
    {
    $fi="uploads/".$file;
   $newHeight=100;
   $newWidth=100;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;

    list($width,$height) = getimagesize($fi);
    $img = imagecreatefrompng($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagepng($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }
    
    if($type=='image/gif')
    {
   $fi="uploads/".$file;
   $newHeight=100;
   $newWidth=100;
   $nfile=time().$file;
   $destImage="uploads/".time().$file;

    list($width,$height) = getimagesize($fi);
    $img = imagecreatefromgif($fi);
    // create a new temporary image
    $tmp_img = imagecreatetruecolor($newHeight,$newWidth);
    // copy and resize old image into new image
    imagecopyresized( $tmp_img, $img, 0, 0, 0, 0,$newHeight,$newWidth, $width, $height );
    // use output buffering to capture outputted image stream
    ob_start();
    imagegif($tmp_img);
    $i = ob_get_clean();
    // Save file
    $fp = fopen ($destImage,'w');
    fwrite ($fp, $i);
    fclose ($fp);
    }

    unlink($fi);}
	else
	{
		$nfile=$_POST['photo'];
	}
$ins=$this->caregiver_model->update_caregiver($caregiver_id,$cname,$sname,$age,$gender,$city,$state,$phone,$qua,$nfile,$address);

	if($ins)
{
 $data=" details updated successfully !";
 redirect('/caregiver/edit_caregiver/?status='.urlencode($data),$data);	
}

	
	}
	
	
}


function sendfallowrequest($cid,$pid,$pitid)
{
	/*$this->data['events'] = $this->caregiver_model->get_moreevent($id);
	$this->data['flash']=$pid;
	$this->load->view('home/final1', $this->data,$this->data);*/
	$email=$this->session->userdata('owner_care');
    $this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
	$name=$this->data['caregiver'][0]->name;

		$scid=$cid;
		$rpid=$pid;
		$ptid=$patid;
		$date=date("Y-m-d");
		$status=0;
		$ip=$_SERVER['REMOTE_ADDR'];



$is="insert into professional_networks (sid,rid,netstatus,sdate,netip,patid) values('$scid','$rpid','$status','$date','$ip','$ptid')";
$ie=mysql_query($is) or die(mysql_error());
$id=mysql_insert_id();
$nmsg=$name."  Hab Send The Request";
$ntype="caregivernetwork";
$nip=$_SERVER['REMOTE_ADDR'];
$nts="insert into popup_notifications(note_message,n_sid,n_rid,nm_id,status,note_type,n_ip) values('$nmsg','$scid','$rpid','$id','0','$ntype','$nip')";
$nte=mysql_query($nts) or die(mysql_error());
	
	
	
	//$this->load->view('home/final2', $this->data,$this->data);
	
}






function getnote($nid,$rid)
{
 $this->data['nid']=$nid;
 $this->data['rid']=$rid;
 
$this->load->view('home/final3', $this->data,$this->data);
}
/*   Alaram code is start hear   */
function getalarm($cid,$c,$d)
{
 $this->data['cid']=$cid;
 //$this->data['pid']=$pid;
 
 
   $t=$c.":".$d;
 $this->data['time1']=$t;
//echo $t;
$this->load->view('home/final4', $this->data);
}
function vpb_converter()
{
$this->load->view('home/vpb_converter', $this->data);	
}











function getpopup($cid)
{
 $this->data['nid']=$cid;

 
$this->load->view('home/final5', $this->data,$this->data);
}






/*================================= caregiver send messages==========================================================================  */
function caregiver_messages($cid)
{
	$email=$this->session->userdata('owner_care');
//$data = $this->caregiver_model->get_caregiver1($email);
$this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);	
	$this->load->view('home/caregiver_messages', $this->data);
}

	function caregivers_details($id=null)
	{
		if (!($this->session->userdata('user_name')!="")) {
		redirect('/auth/login/');
		} else {
		$this->data['user'] = $this->user->getCaregiversAll($id);
		$this->load->view('home/caregiver_details', $this->data);
		}
	}
   /*==================================================== parameters ======================================================= */	
function showparameter($pid)
{
$email=$this->session->userdata('owner_care');
//$data = $this->caregiver_model->get_caregiver1($email);
$this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
$row = $this->caregiver_model->get_caregiver1($email);
	 $patid=$this->session->userdata('pid');
	if(empty($patid))
	 {
	$this->session->set_userdata('pid', $pid);
	 }


 $pid=$this->session->userdata('pid');
 


$this->data['patient']=$this->caregiver_model->get_patient($pid);

//$this->data['patient']=$this->caregiver_model->get_patient($pid);
$pid=$this->session->userdata('pid');
$cid1=$row['id'];
$this->data['menu']="parameter";
/*echo "<script>alert('$pid')</script>";*/
$this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
$this->data['caregivers']= $this->caregiver_model->getcaregivers_new($pid,$cid1);
	$this->data['profetional']=$this->caregiver_model->getprofetionals($pid,$cid1);


$this->data['foods']=$this->caregiver_model->fetch_parameter($pid);
$data['base']=$this->config->item('base_url');
		$total=$this->user->count_parameter();
		$per_pg=10;
		$offset=$this->uri->segment(3);
		$this->load->library('pagination');
		$config['base_url'] = $data['base'].'/caregiver/showparameter/';
   $config['total_rows'] = $total;
   $config['per_page'] = $per_pg;
		$config["num_links"]=40;
		$config['full_tag_open'] = '<div id="pagination">';
		$config['full_tag_close'] = '</div>';
   $this->pagination->initialize($config);
        $this->data['pagination']=$this->pagination->create_links();
	
   $eventid=1;
	
		//$this->data['parameters']=$this->user->getparameters1($per_pg,$offset);
		$date=date("Y-m-d");
		
		$this->data['parameters']=$this->user->fetch_parameter1($pid,$eventid,$date);








 
$this->load->view('home/show_parameters', $this->data);
}



function showparameter_graph()
{
$email=$this->session->userdata('owner_care');
     //$data = $this->caregiver_model->get_caregiver1($email);
 $this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
$row = $this->caregiver_model->get_caregiver1($email);
$pid=$this->session->userdata('pid');
$this->data['patient']=$this->caregiver_model->get_patient($pid);
$pid=$this->session->userdata('pid');
$cid1=$row['id'];
$this->data['menu']="parameter";
/*echo "<script>alert('$pid')</script>";*/
$this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
$this->data['caregivers']= $this->caregiver_model->getcaregivers_new($pid,$cid1);
	$this->data['profetional']=$this->caregiver_model->getprofetionals($pid,$cid1);
$this->data['parameters']=$this->caregiver_model->get_paremeters();

$this->data['foods']=$this->caregiver_model->fetch_parameter($pid);
$data['base']=$this->config->item('base_url');
		$total=$this->user->count_parameter();
		$per_pg=10;
		$offset=$this->uri->segment(3);
		$this->load->library('pagination');
		$config['base_url'] = $data['base'].'/caregiver/showparameter/';
        $config['total_rows'] = $total;
        $config['per_page'] = $per_pg;
		$config["num_links"]=40;
		$config['full_tag_open'] = '<div id="pagination">';
		$config['full_tag_close'] = '</div>';
        $this->pagination->initialize($config);
        $this->data['pagination']=$this->pagination->create_links();
	
   $eventid=1;
	
	if($_POST)
	{
		$praname=$_POST['praname'];
		$this->data['paraid']=$praname;
		$paraid=$praname;
		$date=$_POST['date'];
		$this->data['date']=$date;
		$this->data['paralist']=$this->caregiver_model->get_paremeters_list($pid,$praname,$date);
		
		if(count($this->data['paralist']))
		{
		 //foreach ($this->data['paralist'] as $temp) {
			
		
		       
					//$arrpush=array();
					//$arr2=array();
			
			$mps="select * from parameter_name where id='$paraid'";
			$mpe=mysql_query($mps) or die(mysql_error());
			$mpr=mysql_fetch_array($mpe);
			$paraname=$mpr['pname'];
					
			$mp="select * from parameter_columns where para_id='$paraid'";
			$me=mysql_query($mp);
			$mn=mysql_num_rows($me);
			while($mr=mysql_fetch_array($me))
			{
				
				 @$n.=$mr['paraname'].",";
				
				 
				
			}
			@$name= explode(",",$n);
			if($mn==2)
			{
			 @$this->data['n1']=$name[0];
			 @$this->data['n2']=$name[1];
			}
			if($mn==3)
			{
			 @$this->data['n1']=$name[0];
			 @$this->data['n2']=$name[1];
			 @$this->data['n3']=$name[2];
			}
			if($mn==4)
			{
			 @$this->data['n1']=$name[0];
			 @$this->data['n2']=$name[1];
			 @$this->data['n3']=$name[2];
			 @$this->data['n4']=$name[3];
			}
			
			if($mn==5)
			{
			 @$this->data['n1']=$name[0];
			 @$this->data['n2']=$name[1];
			 @$this->data['n3']=$name[2];
			 @$this->data['n4']=$name[3];
			 @$this->data['n5']=$name[4];
			}
			
			
			
			
			
			
			
			
								   
			foreach ($this->data['paralist'] as $temp) 
			{
				 @$this->data['arr1'].=$temp->option1.",";
				 @$this->data['arr2'].=$temp->option2.",";
				 @$this->data['arr3'].=$temp->option3.",";
				 @$this->data['arr4'].=$temp->option4.",";
				 @$this->data['arr5'].=$temp->option5.","; 
				 
				 
				 
				 
				 
			}



 	
		     
		
			
			
			
		
		}
		
	}
	else
	{
		 $date=date("Y-m-d");
		  $praname=rand(1,5);
		  $this->data['paraid']=$praname;
		  $this->data['date']=$date;
		$this->data['paralist']=$this->caregiver_model->get_paremeters_list($pid,$praname,$date);
		
		  
		
		
		
		
		
	}
	
		//$this->data['parameters']=$this->user->getparameters1($per_pg,$offset);

   //$this->data['parameters']=$this->user->fetch_parameter1($pid,$eventid);

 
$this->load->view('home/show_parameters_graph', $this->data);
}


















function add_parameters_care()
	{ 
		if($_POST)
		{  
	   
			
			//$temp=$_POST['created'];
			//$data=explode('/',$temp);
			//$result=$data[2].'-'.$data[0].'-'.$data[1];
			 $pname=$_POST['pname'];
			 $numclu=$_POST['numclu'];
			 $date=date("Y-m-d");
			$ps="select * from parameter_name where pname='$pname'";
			$pe=mysql_query($ps) or die(mysql_error());
			$pn=mysql_num_rows($pe);
		if($pn<=0)
		{
		$id=$this->patient->addparameters($pname,$numclu,$date);
			
				$lc=$_POST['column'];	
				$num=count($_POST['column']);
				
				for($i=0;$i<$num;$i++)
				{
				$name=$lc[$i];
				$this->patient->add_paracolumns($id,$name);
				
				}	
					
		   $data="Parameters Has Been Added !";		
		}
		else
		{
				$data=" You Are Trying To Enter Same  Parameter Name Again !";
		}
				
	 
		}
		//$this->load->view('home/add_care_parameter', $this->data);
        $pid=$this->session->userdata('pid');
		//redirect('/caregiver/showparameter/'.$pid);
		redirect('/caregiver/showparameter/'.$pid."?status=".urlencode($data));		
		
		
		
		
	}

function edit_parameters($id=null)
	{     
$email=$this->session->userdata('owner_care');
     //$data = $this->caregiver_model->get_caregiver1($email);
     $this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
$row = $this->caregiver_model->get_caregiver1($email);
//$pid=$row['pat_id'];
$pid=$this->session->userdata('pid');

$this->data['patient']=$this->caregiver_model->get_patient($pid);
$pid=$this->session->userdata('pid');
$cid1=$row['id'];
$this->data['menu']="parameter";
/*echo "<script>alert('$pid')</script>";*/
$this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
$this->data['caregivers']= $this->caregiver_model->getcaregivers_new($pid,$cid1);
	$this->data['profetional']=$this->caregiver_model->getprofetionals($pid,$cid1);
		
		if($_POST)
		{
		$pname=$_POST['pname'];
		
		$lc=$_POST['column1'];	
   $num=count($_POST['column1']);
		
	$this->user->updateparameters($id,$pname);
	$ms="select * from parameter_columns where para_id='$id'";
		$me=mysql_query($ms) or die(mysql_error());
		$i=0;
		while($mr=mysql_fetch_array($me))
		{
		$name=$lc[$i];
		$mid=$mr['id'];	
		$ps="update parameter_columns set paraname='$name' where id='$mid'";
	
	  
	$pe=mysql_query($ps) or die(mysql_error());
		
		$i++ ;
		
		}
		
		/*for($i=0;$i<$num;$i++)
				{
				$name=$lc[$i];
				//$this->user->update_paracolumns($id,$name);
				//$ps="update parameter_columns set paraname='$name' where para_id='$id'";
				//$pe=mysql_query($ps) or die(mysql_error());
				
				
				}*/
				
		@$cn=$_POST['column'];	
   @$cnum=count($_POST['column']);
		if($cnum>0)
		{
			for($i=0;$i<$cnum;$i++)
				{
				$name=$cn[$i];
				$this->patient->add_paracolumns($id,$name);
				
				}
		}
		
		
		//redirect('/admin/list_parameters/');
		
		}
		
		
		
		
	$this->data['menu']="parameter";
		$this->data['parameters']=$this->user->parametersDetail($id);
		$this->load->view('home/edit_care_parameter', $this->data);
 
	}


function showparameterchart()
{
$email=$this->session->userdata('owner_care');
     //$data = $this->caregiver_model->get_caregiver1($email);
     $this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
$row = $this->caregiver_model->get_caregiver1($email);
//$pid=$row['pat_id'];
$pid=$this->session->userdata('pid');
$this->data['patient']=$this->caregiver_model->get_patient($pid);
$pid=$this->session->userdata('pid');
$cid1=$row['id'];
$this->data['menu']="parameter";
/*echo "<script>alert('$pid')</script>";*/
$this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
$this->data['caregivers']= $this->caregiver_model->getcaregivers_new($pid,$cid1);
	$this->data['profetional']=$this->caregiver_model->getprofetionals($pid,$cid1);


$this->data['foods']=$this->caregiver_model->fetch_parameter($pid);
 
$this->load->view('home/showparameterchart', $this->data);
}






function delete_parameters($id)
	{

		$this->user->deleteparameters($id);
			$pid=$this->session->userdata('pid');
		redirect('/caregiver/showparameter/'.$pid);
	}

function view_caregiver($id)
{
	
$email=$this->session->userdata('owner_care');
     //$data = $this->caregiver_model->get_caregiver1($email);
     $this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
	
//$this->data['states']= $this->user->getStates();
$this->data['caregiver1'] = $this->user->get_caregiver($id);

	
 $this->load->view('home/view_caregiver', $this->data);
	
}







	
function add_parameter_old($nid)
	{
	
$nid;
$email=$this->session->userdata('owner_care');
     //$data = $this->caregiver_model->get_caregiver1($email);
     $this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
$row = $this->caregiver_model->get_caregiver1($email);
//$pid=$row['pat_id'];
$pid=$this->session->userdata('pid');

$careid=$row['id'];





	$this->data['nid']=$nid;	
		
		if($_POST){
		$this->form_validation->set_rules('maximum_pressure', 'Maximum Pressure', 'trim|required|xss_clean');
$this->form_validation->set_rules('minimum_pressure', 'Minimum Pressure  ', 'trim|required|xss_clean');
$this->form_validation->set_rules('blood_glucose', 'Blood Glucose ', 'trim|required|xss_clean');
$this->form_validation->set_rules('heart_rate', 'Heart Rate  ', 'trim|required|xss_clean');
$this->form_validation->set_rules('weight', 'Weight  ', 'trim|required|xss_clean');
//$this->form_validation->set_rules('patient_id', 'Patient Name ', 'trim|required|xss_clean');
$this->form_validation->set_rules('created', 'Date  ', 'trim|required|xss_clean');
@$temp=$_POST['created'];
@$data=explode('/',$temp);
echo @$result=$data[2].'-'.$data[0].'-'.$data[1];
$_POST['created']=$result;
$ps="select * from parameters where event_id='$nid'";
$pe=mysql_query($ps) or die(mysql_error());
$pn=mysql_num_rows($pe);
if($pn<=0)
{
	$re=$this->caregiver_model->addparameters();
if($re)
{
$up=$this->caregiver_model->updatenotif($nid,$careid);
$mup=$this->caregiver_model->updatenotifm($nid);

$this->data['flash']="Parameters has been added !";
}
	else
	{

$this->data['flash']="not  successfully !";
	
	}
	
}
else
{
	
	$re=$this->caregiver_model->updateparameters($nid);
	$up=$this->caregiver_model->updatenotif($nid,$careid);
	$mup=$this->caregiver_model->updatenotifm($nid);
	$this->data['flash']="paremeters updated successfully";
}
	
	
$this->load->view('home/add_parameter', $this->data);	
		}
		//$this->data['caregivers']= $this->user->getCaregiverslist();
		//$this->data['patient'] = $this->patient->get_patient_by_id($this->session->userdata('user_id'));
		
		else
		{
	$this->load->view('home/add_parameter', $this->data);
		}
	
	}	
	
	
	
	
	
	
	
	
	
	public function add_diets()
	{
	if($_POST){
$this->form_validation->set_rules('food_type', 'Food Type ', 'trim|required|xss_clean');
$this->form_validation->set_rules('food_name', 'Food Name ', 'trim|required|xss_clean');
$this->form_validation->set_rules('diet_day', 'Diet Day ', 'trim|required|xss_clean');
$temp=$_POST['diet_day'];
$data=explode('/',$temp);
$result=$data[2].'-'.$data[0].'-'.$data[1];
$_POST['diet_day']=$result;
if($this->form_validation->run() == TRUE)
{	
	$this->patient->adddiets();
	$this->data['flash']="Diet has been added !";
}
else
{
	$this->data['flash']="Unsuccesfull, Try again !";

}	
		}
$this->data['caregivers']= $this->user->getCaregiverslist();
$this->data['patient'] = $this->patient->get_patient_by_id($this->session->userdata('user_id'));

		
		if($this->session->userdata('user_role')=='admin')
		{
$this->load->view('admin/add_diets', $this->data);
		}
		else
		{
$this->load->view('home/add_diets', $this->data);
		}

	}
	function list_diets()
	{  
		$this->data['diets']=$this->patient->getdiets();
		$this->load->view('admin/list_diets', $this->data);
	}
	
	
	function details_diets($id=null)
	{
		$this->data['diets']= $this->patient->dietsDetail($id);
		$this->load->view('home/diet_details',$this->data );
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
/*==============================//////// add Owner,rsa,consortium ////////////================================  */	
	
	function add_rsa_owner()
	{}
		
		//$this->data['caregivers']= $this->user->getCaregiverslist();
		//$this->data['patient'] = $this->patient->get_patient_by_id($this->session->userdata('user_id'));
		//if($this->session->userdata('user_role')=='admin'){
	//$this->load->view('admin/add_parameters', $this->data);
		//}
		//else{
	//$this->load->view('home/add_parameters', $this->data);
		//}

	
	
	
	
	function add_con_owner()
	{}
	
	
	
	
	
	
	
	function add_parameters()
	{
		
		if($_POST){
		$this->form_validation->set_rules('maximum_pressure', 'Maximum Pressure', 'trim|required|xss_clean');
$this->form_validation->set_rules('minimum_pressure', 'Minimum Pressure  ', 'trim|required|xss_clean');
$this->form_validation->set_rules('blood_glucose', 'Blood Glucose ', 'trim|required|xss_clean');
$this->form_validation->set_rules('heart_rate', 'Heart Rate  ', 'trim|required|xss_clean');
$this->form_validation->set_rules('weight', 'Weight  ', 'trim|required|xss_clean');
$this->form_validation->set_rules('patient_id', 'Patient Name ', 'trim|required|xss_clean');
$this->form_validation->set_rules('created', 'Date  ', 'trim|required|xss_clean');
$temp=$_POST['created'];
$data=explode('/',$temp);
$result=$data[2].'-'.$data[0].'-'.$data[1];
$_POST['created']=$result;
if($this->form_validation->run() == TRUE)
	{
$this->patient->addparameters();
$this->data['flash']="Parameters has been added !";
	}
else
	{
$this->data['flash']="Unsuccesfull, Try again !";

	}
		}
		$this->data['caregivers']= $this->user->getCaregiverslist();
		$this->data['patient'] = $this->patient->get_patient_by_id($this->session->userdata('user_id'));
		if($this->session->userdata('user_role')=='admin'){
	$this->load->view('admin/add_parameters', $this->data);
		}
		else{

	$this->load->view('home/add_parameters', $this->data);
		}
	
	}

	function list_parameters()
	{  
		$this->data['parameters']=$this->patient->getparameters();
		$this->load->view('admin/list_parameters', $this->data);
	}

	
	
	
	
	
	
	
	
	function add_periods()
	{}
	
	
	
	
	
	


	function list_periods()
{	
	$this->data['periods']=$this->patient->getperiods();
	//print_r($this->data);
	$this->load->view('admin/list_periods', $this->data);
}

		
    function edit_profile($id)
	{
		//$temp= $this->user->get_users_by_id($this->session->userdata('user_id'));
		//$temp= $temp[0]->id;
 
		$this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('surname', 'Surname','trim|required|xss_clean');
		$this->form_validation->set_rules('age', 'Age','trim|required|xss_clean');
		$this->form_validation->set_rules('gender', 'Gender','trim|required|xss_clean');

		if($this->form_validation->run() == TRUE){       
if ($_FILES["photo"]["error"] > 0) {
	//echo "Error: " . $_FILES["photo"]["error"] . "<br>";
}else {
	//echo "Temp file: " . $_FILES["photo"]["tmp_name"] . "<br>";
	if (file_exists("upload/" . $_FILES["photo"]["name"])) {
//$_FILES["photo"]["name"] . " already exists. ";
	} else {
$file_name=uniqid().$_FILES["photo"]["name"];
if(move_uploaded_file($_FILES["photo"]["tmp_name"],
"uploads/" . $file_name)){
	$this->session->set_userdata('photo', $file_name);
	$this->session->set_userdata('user_photo', $file_name);
}else{
	$this->session->set_userdata('photo',$this->session->userdata('user_photo'));
	$this->session->set_userdata('user_photo', 'no_profile.jpg');
}
	}
}

            //echo $this->session->userdata('photo');
//die('ok');

$res=$_POST['from'];
$data=explode('/',$res);
$result=$data[2].'-'.$data[0].'-'.$data[1];
$_POST['from']=$result;
$this->session->set_userdata('from', $_POST['from']);
$exe=$this->user->editProfile($id);
if($exe)
{
$this->session->set_flashdata('flashMessage', 'Profile Edited Successfully !');
redirect('/prof/professional_home/');
}
else
{
$this->session->set_flashdata('flashMessage', 'Profile Edited Is Not Successfully !');
redirect('/prof/professional_home/');
		
}


		}
        //$this->data['users'] = $this->user->get_users_by_id($temp);
		$this->data['professionals'] = $this->user->get_professionals_by_id($id);
		//print_r($this->data['professionals']);
		//$this->data['states']= $this->user->getStates();
		// print_r( $this->data['states']);
		$this->load->view('home/professional_edit', $this->data);
	}
	
	
	function edit_caregivers($id=null)
	{
	 $result= $this->user->get_users_by_id($this->session->userdata('user_id'));
	 $result= $result[0]->id;

            
$this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');
$this->form_validation->set_rules('surname', 'Surname','trim|required|xss_clean');
$this->form_validation->set_rules('age', 'Age','trim|required|xss_clean');
$this->form_validation->set_rules('gender', 'Gender','trim|required|xss_clean');
//$this->form_validation->set_rules('city', 'City','trim|required|xss_clean');
$this->form_validation->set_rules('zipcode', 'Zipcode','trim|required|xss_clean');
$this->form_validation->set_rules('state', 'State','trim|required|xss_clean');
$this->form_validation->set_rules('address', 'Address','trim|required|xss_clean');



if($this->form_validation->run() == TRUE)
{
                 
	if ($_FILES["photo"]["error"] > 0) {
//echo "Error: " . $_FILES["photo"]["error"] . "<br>";
	}else {
//echo "Temp file: " . $_FILES["photo"]["tmp_name"] . "<br>";
if (file_exists("upload/" . $_FILES["photo"]["name"])) {
	//$_FILES["photo"]["name"] . " already exists. ";
} else {
    $file_name=uniqid().$_FILES["photo"]["name"];

	if(move_uploaded_file($_FILES["photo"]["tmp_name"],
	"uploads/" . $file_name)){
 $this->session->set_userdata('photo', $file_name);
 $this->session->set_userdata('user_photo', $file_name);
	}else{
 $this->session->set_userdata('photo',$this->session->userdata('user_photo'));
 $this->session->set_userdata('user_photo', 'no_profile.jpg');
	};

}
	}

$this->user->editcaregiverProfile($this->session->userdata('user_id'));
   
$this->session->set_flashdata('flashMessage', 'Profile Edited Successfully !');

redirect('/auth/index/');
}

//$this->data['states']= $this->user->getStates();
$this->data['caregivers'] = $this->user->get_caregivers_by_id($result);
//print_r($this->data['caregivers']);
$this->data['users'] = $this->user->get_users_by_id($result);
$this->load->view('home/caregiver_edit', $this->data);

  
	}
	
	
	
	
	
	
	
	
	
	
	public function fetch_patient_details($id=null){		
		$patient_details= $this->patient->get_patient_by_id($id);
		echo $patient_details[0]->name.','.$patient_details[0]->id;
	}
	
/*==========================================///////// get the owner information /////////======================================== */	
	public function fetch_owner($id)
	{
		$patient_details= $this->patient->get_owner($id);
		echo $patient_details[0]->own_licence;
	}
	
    public function add_foods(){
if($_POST)
{
		$this->form_validation->set_rules('food_name', 'Food Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('food_type', 'Food Type ', 'trim|required|xss_clean');
		$this->form_validation->set_rules('created', 'Created ', 'trim|required|xss_clean');
		
$temp=$_POST['created'];
$data=explode('/',$temp);
$result=$data[2].'-'.$data[0].'-'.$data[1];
$_POST['created']=$result;

if($this->form_validation->run() == TRUE)
{
	$this->patient->addfoods();
	$this->data['flash']="Diet has been added !";
}
else
{
	$this->data['flash']="Unsuccesfull, Try again !";

}	
		}

$this->session->userdata('user_role');

$this->data['foods']= $this->admin->getfoodsAll();
if($this->session->userdata('user_role')=='admin')
{
	$this->load->view('admin/add_foods', $this->data);
}
else
{
	$this->load->view('home/add_foods', $this->data);
}
    }
	public function list_foods()
	{  
		$this->data['foods']=$this->patient->getfoods();
		$this->load->view('admin/list_foods', $this->data);
	}
/*  caregiver home page start hear        */
public function search()
{

$email=$this->session->userdata('owner_care');
$this->session->set_userdata('usertypeid', '1');
$this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
$row = $this->caregiver_model->get_caregiver1($email);
$pid=$row['pat_id'];
$careid=$row['id'];
$this->session->set_userdata('userid', $careid);		


$this->data['carenotifi1']=$this->caregiver_model->get_carenotification($careid);
$this->data['carenotifi2']=$this->caregiver_model->get_carenoti_prof($careid);
$this->data['carenotifi3']=$this->caregiver_model->get_carenoti_msg($careid);

if($_POST)
		{
if($_POST['form_type']== 'patient_search')
{
	 

    $this->session->set_userdata('search_keyword1',$_POST['keyword']);
     




	$this->data['patients']= $this->patient->searchPatients($pid);
	$this->data['professionals']= $this->user->getProfessionals();


}
elseif($_POST['form_type']== 'professional_search')
{
	
    $this->session->set_userdata('search_keyword2',$_POST['keyword']);
	
	$this->data['patients']= $this->patient->getPatients();
	$this->data['professionals']= $this->user->searchProfessionals();

	
}
     
   }
else
{
$pid;
$this->data['patients']= $this->patient->getPatients11($careid);
//$this->data['professionals1']= $this->user->getProfessionals2($careid);
$this->data['professionals']= $this->user->getProfessionals11($careid);
$this->data['professionals1'] = $this->user->getnewsProfessionals();
}

$this->load->view('home/search1', $this->data);
	
 }
	
	
public function consultation_details($rid)
{ 
//echo $rid;

$email=$this->session->userdata('owner_care');
$this->session->set_userdata('usertypeid', '1');
$this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
$row = $this->caregiver_model->get_caregiver1($email);
$pid=$this->session->userdata('pid');
$careid=$row['id'];
$this->session->set_userdata('userid', $careid);		


$this->data['carenotifi1']=$this->caregiver_model->get_carenotification($careid);
$this->data['carenotifi2']=$this->caregiver_model->get_carenoti_prof($careid);
$this->data['carenotifi3']=$this->caregiver_model->get_carenoti_msg($careid);
		if($_POST)
		{
if($_POST['form_type']== 'patient_search')
{
	 

    $this->session->set_userdata('search_keyword1',$_POST['keyword']);
     




	$this->data['patients']= $this->patient->searchPatients($pid);
	$this->data['professionals']= $this->user->getProfessionals();
	//---------xml write:start---------//
	$_xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
	$_xml .= "<markers>\n";
	foreach ($this->data['patients'] as $temp) {
$zipcode= $temp->zipcode;
$url = "http://maps.googleapis.com/maps/api/geocode/json?address=".$zipcode."&sensor=false";
$details=file_get_contents($url);
$result = json_decode($details,true);
if(isset($result['results'][0])){
$lat=$result['results'][0]['geometry']['location']['lat'];
$lng=$result['results'][0]['geometry']['location']['lng'];
}
else{
$lat =0;
$lng =0;
}
$name = $temp->name; 
$role = 'patient'; 
$_xml .= "<marker>\n";    
$_xml .= "<name>$name</name>\n";  
$_xml .= "<role>$role</role>\n";  
$_xml .= "<lat>$lat</lat>\n";
$_xml .= "<lng>$lng</lng>\n";
$_xml .= "</marker>"; 
	}
	$_xml .= "</markers>";
    $myFile = "markers.xml";
    $fh = fopen('public/'.$myFile, 'w') or die("can't open file");
    fwrite($fh, $_xml);
    fclose($fh);
    //---------xml write:end---------//

}
elseif($_POST['form_type']== 'professional_search')
{
	
    $this->session->set_userdata('search_keyword2',$_POST['keyword']);
	
	$this->data['patients']= $this->patient->getPatients();
	$this->data['professionals']= $this->user->searchProfessionals();

	
}
     
   }
else
{
$this->data['patients']= $this->patient->getPatients11($pid);
//$this->data['professionals1']= $this->user->getProfessionals2($careid);
$this->data['professionals']= $this->user->getProfessionals11($careid);
$this->data['professionals1'] = $this->user->getnewsProfessionals();
}

/*$session_id=$cid;

$iRecipient =  $segs[3];
$iPid = (int)$session_id;

$upms="update cons_request_messages set `read`=1 where sender='$iRecipient' and recipient='$iPid'";
$upme=mysql_query($upms) or die(mysql_error());



$sRecipientSQL = "WHERE ((`sender` = '{$iRecipient}' && stype=1) || (`recipient` = '{$iPid}' && rtype=1) || (`sender` = '{$iRecipient}' && stype=0) || (`recipient` = '{$iPid}' && rtype=1) ) || ((`recipient` = '{$iRecipient}' && stype=1) || (`sender` = '{$iPid}' && rtype=1) || (`recipient` = '{$iRecipient}' && stype=1) && (`sender` = '{$iPid}' && rtype=0) ) ";


//$sRecipientSQL="";


 $us="select * from cons_request_messages {$sRecipientSQL} order by id asc ";*/


$session_id=$careid;

$iRecipient =  $rid;
$iPid = (int)$session_id;


	   $ps1="select * from cons_request_messages as cons,caregiver as cs  where (recipient='$rid' and subject!='') and (cons.sender=cs.id)"; 
       $pe1=mysql_query($ps1) or die(mysql_error());
       $pn1=mysql_num_rows($pe1);
	
	$this->data['pn1']=$pn1;

$upms="update cons_request_messages set `read`=1 where sender='$iRecipient' and recipient='$iPid'";
$upme=mysql_query($upms) or die(mysql_error());



$this->data['consutaiondel'] = $this->user->getnew_consultion($iRecipient,$iPid);










$this->load->view('home/caregiver_replay_from_profetionals', $this->data);
	

	}
	
	
	function consultation_replay_caregiver()
{
	
	
	
 $rid=$_POST['reciver'];
 $sid=$_POST['sender'];
$msg=$_POST['send'];
$when = time();
$date = date('Y-m-d H:i:s');
$uid=$session_id;
$stype=1;
$read=0;

$d="insert into cons_request_messages(sender,recipient,message,`when`,date,stype,`read`) values('$sid','$rid','$msg',UNIX_TIMESTAMP( ),'$date','$stype','$read')";

$e=mysql_query($d) or die(mysql_error());

if($e)
{
	redirect('/caregiver/consultation_details/'.$rid);
}




	
	
}
	
	
	
	
	public function searchdrug()
	{
		
		
$email=$this->session->userdata('owner_care');
$row = $this->caregiver_model->get_caregiver1($email);
$pid=$this->session->userdata('pid');
$cid1=$row['id'];
		
		
		if($_POST)
		{
			
if($_POST['form_type']== 'patient_search')
		{
			$this->session->set_userdata('search_keyword1',$_POST['keyword']);
			$srch=$_POST['keyword'];
			$ctype=$_POST['ctype'];
			//getPatients searchPatients
			$eventid=2;
			//$this->data['patients']= $this->user->serach_drug($srch);
			$this->data['patients']= $this->user->fetch_medecine1_search($pid,$eventid,$srch);
			
			
			
			
			//$this->data['professionals']= $this->user->getProfessionals();
			
		$str='';
		if(count($this->data['patients'])>0)
		{
			$i=0;
	foreach($this->data['patients'] as $temp)
	{
		$i++;
		?> 
			<tr class="lyt">
									<td><img src="<?php echo base_url();  ?>img/<?php echo $temp->medformat ?>.png" alt=""  width="32" height="32"/></td>
<td><a href="javascript:void(0)"  onClick="showdetails('<?php echo $temp->med_id; ?>')" style="color:#015f00;"><?php echo  $temp->med_name;?></a></td>
								<td><?php echo  $temp->med_quantity;?></td>
								<td><?php echo  $temp->med_expirartion_date;?>
                                    </td>
<td> <?php if($ctype!=3) {   ?>
<a href="<?php echo base_url(); ?>caregiver/edit_medicine/<?php echo $temp->med_id;?>"><img src="<?php echo base_url() ?>/img/edit.png" width="20" height="20"></a>
<a href="<?php echo base_url(); ?>caregiver/delete_medicine/<?php echo $temp->note_id;?>/<?php echo $temp->event_type_id; ?>" onclick="return confirm('are you sure to delete')">
<img src="<?php echo base_url() ?>/img/del.gif" width="15" height="15">
</a>
<?php } ?>
</td>  
</tr>
<?php
 } 
 } 
 
 else
		{
			echo "<tr align='center'><td colspan='6' align='center'> No Records Found   </td></tr>";
		}
 
 ?>

</td>
</tr> 



         <?php
         }
		}
		
	}
	
	
	function delete_medicine($id,$eventid)
	{
		
		if($_SERVER['REMOTE_ADDR']=="127.0.0.1")
		{
		$ip="183.82.101.75";
		}
			else
			{
			$ip = $_SERVER['REMOTE_ADDR']; 
			}
	// the IP address to query
	@$query = unserialize(file_get_contents('http://ip-api.com/php/'.$ip));
	//echo @$lat=$query['lat'];
	//@$lon=$query['lon'];
	 $timezone=$query['timezone'];
	 //$ip_data = json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip));
	 date_default_timezone_set("$timezone");
		
		
		
		$time=date('H:i:s');
	    $this->user->deleteMedicines($id,$eventid,$time);
		$pid=$this->session->userdata('pid');
	redirect('/caregiver/show_drugs/'.$pid);
	}
	
/*  Chat code is start heat      */

function ajax_newchat()
{
	
$this->data['rid']=$_POST['Recvierid'];
$this->data['ctitle']=$_POST['chatboxtitle'];
$this->data['ctype']=$_POST['chatwith'];
$this->data['receivertype']=$_POST['receivertype'];
$this->load->view('home/ajax_newchat', $this->data);

}

function ajax_newchat_loadmore()
{
	
$this->data['rid']=$_POST['Recvierid'];
//$this->data['ctitle']=$_POST['chatboxtitle'];
$this->data['ctype']=$_POST['chatwith'];
$this->data['receivertype']=$_POST['receivertype'];
$this->load->view('home/ajax_newchat_load', $this->data);

}






function sendchatinsert($var)
{

	$email=$this->session->userdata('owner_care');
	$row = $this->caregiver_model->get_caregiver1($email);
	$pid=$this->session->userdata('pid');
	$cid1=$row['id'];	
	$this->data['name']=$row['name'];	
	$var;
	$this->data['action']=$var;	
	$this->data['to']=$_POST['to'];
	$this->data['message']=$_POST['message'];
	$this->data['receivertype']=$_POST['receivertype'];
	$this->load->view('home/chat', $this->data);
		
}

function ajax_newchathotbeat()
{
	$this->data['rid']=$_POST['Recvierid'];
	$this->data['ctitle']=$_POST['chatboxtitle'];
	$this->data['ctype']=$_POST['chatwith'];
	$this->data['receivertype']=$_POST['receivertype'];
		
	$this->load->view('home/ajax_chat_hotbeat', $this->data);	
}
function messageread($rid)
{
	$email=$this->session->userdata('owner_care');
	$row = $this->caregiver_model->get_caregiver1($email);
	//$pid=$row['pat_id'];
	$cid1=$row['id'];
	$rid;
	$us="update chat set recd='1' where chat.to='$cid1' and chat.from='$rid'";
	$ue=mysql_query($us) or die(mysql_error());

}

function searchdiets()
	{
		
		if($_POST)
		{
			
if($_POST['form_type']== 'patient_search')
		{
			$this->session->set_userdata('search_keyword1',$_POST['keyword']);
		$srch=$_POST['keyword'];
		 @$ctype=$_POST['ctype'];
			//getPatients searchPatients
			$this->data['patients']= $this->user->serach_deits($srch);
			//$this->data['professionals']= $this->user->getProfessionals();
			
		$str='';
		if(count($this->data['patients'])>0)
		{
			
			$i=0;
	foreach($this->data['patients'] as $temp)
	{
		$i++;
	$sbid=$temp->food_type;
    if(!empty($sbid))
	{
	$s1="select * from sub_event_type where sub_id='$sbid'";
	$e1=mysql_query($s1) or die(mysql_error());
	$r1=mysql_fetch_array($e1);
	}
		
		
		?> 
			<tr>
<td><?php echo  $i;?> </td>
<td><?php echo  $r1['sub_eventname'];?> </td>
<td><?php echo  $temp->food_name; ?> </td>
<td><img src="<?php echo base_url()."foods/".$temp->food_image; ?>"  width="100" height="40"/> </td>
<!--<td><?php //echo  $temp->food_like;?> </td>
<td><?php //echo  $temp->diet_day;?> </td>-->
<td>
<a href="<?php echo base_url(); ?>caregiver/view_diets/<?php echo $temp->id;?>">View</a>/ <a href="<?php echo base_url(); ?>caregiver/edit_diets/<?php echo $temp->id;?>">Edit</a><!-- /<a href="<?php //echo base_url(); ?><?php //echo $temp->id;?>" onclick="return confirma('are you sure to delete')">Delete </a>-->
</td>
</tr> 

         <?php
         }
		}
		else
		{
			echo "<tr align='center'><td colspan='6' align='center'> No Records Found   </td></tr>";
		}
		
		
		
		}
    }
	}
	
	
	
	function searchactivity()
	{
		
		
		if($_POST)
		{ 
			
if($_POST['form_type']== 'patient_search')
		{
			
			$this->session->set_userdata('search_keyword1',$_POST['keyword']);
		$srch=$_POST['keyword'];
		 @$ctype=$_POST['ctype'];
			//getPatients searchPatients
			$this->data['patients']= $this->user->serach_activity($srch);
			//$this->data['professionals']= $this->user->getProfessionals();
			
		$str='';
		if(count($this->data['patients'])>0)
		{
			
			$i=0;
	foreach($this->data['patients'] as $temp)
	{
		$i++;
	$sbid=$temp->act_type;
	$s1="select * from sub_event_type where sub_id='$sbid'";
	$e1=mysql_query($s1) or die(mysql_error());
	$r1=mysql_fetch_array($e1);
	$did=$temp->act_details;
	$d1="select * from sub_even_details where id='$did'";
	$e2=mysql_query($d1) or die(mysql_error());
	$r2=mysql_fetch_array($e2);
		
		
		?> 
			<tr>
<td><?php echo $i;?> </td>
<!--<td><?php //echo  $r1['sub_eventname'];?> </td>-->
<td><?php echo  $temp->act_name; ?> </td>
<td><?php echo  $temp->act_details;   ?> </td>
<td> <a href=""> <?php echo  $temp->act_attached;?> </a> </td>
<td>
<a href="<?php echo base_url(); ?>caregiver/view_activity/<?php echo $temp->act_id;?>">View</a>/ <a href="<?php echo base_url(); ?>caregiver/edit_activity/<?php echo $temp->act_id;?>">Edit</a> /<a href="<?php //echo base_url(); ?><?php //echo $temp->id;?>" onclick="return confirma('are you sure to delete')">Delete </a>
</td>
</tr> 

         <?php
         }
		}
		else
		{
			echo "<tr align='center'><td colspan='6' align='center'> No Records Found   </td></tr>";
		}
		
		
		
		
		}
     }
	
	}
	
	
	function serach_associatte_prof()
	{
$email=$this->session->userdata('owner_care');
$row = $this->caregiver_model->get_caregiver1($email);
$pid=$this->session->userdata('pid');
$careid=$row['id'];
$owid=$row['own_id'];	
 $srch=$_POST['keyword'];
          if($_POST)
		{
if($_POST['form_type']== 'patient_search')
{
	//$this->data['caregivers']= $this->caregiver_model->getcaregivers_new($pid,$cid1);
	$this->data['profetional']=$this->caregiver_model->getprofetionalssearch($pid,$careid,$srch);
	
if(count($profetional)>0){ 
 foreach($profetional as $patient){
 $prid=$patient->rid;
	$mps="select * from professionals where id='$prid'";
	$mpe=mysql_query($mps) or die(mysql_error());
	$mpr=mysql_fetch_array($mpe);
 
 ?>
	<li class="odd">
						<div class="img-thumb">
                        <?php if(!empty($mpr['photo'])) { ?>
                        <img src="<?php echo base_url('uploads/'.$mpr['photo']);?>" alt="pat img"  width="60" height="60"/>
                        <?php } else { ?>
                        <img src="<?php echo base_url(); ?>img/pat-img.jpg" alt="pat img" />
                        <?php } ?>
                        </div>
						<div class="pat-desc-cont">
							<h5><a href="#"><?php  echo $mpr['name'];   ?> </a></h5>
							<p> <a href=""> Message </a> </p>
							<span><b>Gender </b><?php echo $mpr['gender']; ?> <b> Age </b> <?php echo $mpr['age']; ?> </span>
						</div>
					</li>
    
    
    
    <?php
 }
}
	
}
		}
	}
	
	
	
public function search2()
	{
		$email=$this->session->userdata('owner_care');
$row = $this->caregiver_model->get_caregiver1($email);
$pid=$this->session->userdata('pid');
$careid=$row['id'];
$owid=$row['own_id'];	
$name=$row['name'];	
$photo=$row['photo'];	
		if($_POST)
		{
if($_POST['form_type']== 'patient_search')
{
	
    $this->session->set_userdata('search_keyword1',$_POST['keyword']);
	$srch=$_POST['keyword'];
	//getPatients searchPatients
   $this->data['patients']= $this->patient->getPatients12($careid,$srch);
	//$this->data['professionals']= $this->user->getProfessionals();
	
$str='';
if(count($this->data['patients'])>0){
 foreach($this->data['patients'] as $patient){
$pth=$patient->photo;
$noimg="profile_pic.jpg";
?>
<li class="odd">
						<div class="img-thumb">
                        <?php if(!empty($pth)) { ?>
<img src="<?php echo base_url(); ?>uploads/<?php echo $patient->photo; ?>" alt="pat img"  width="60" height="60"/>
                      <?php } else { ?> 
  <img src="<?php echo base_url(); ?>img/img-holder2.jpg" alt="pat img"  width="60" height="60"/>                       
                       <?php } ?>
                        </div>
						<div class="pat-desc-cont">
						<h5><a href="<?php echo base_url() ?>auth/edit_owner_patient/<?php echo $patient->pid; ?>"><?php echo $patient->name; ?> </a></h5>
						<p><?php echo $patient->surname; ?> -  <?php echo $patient->age; ?> </p>
						<span> <?php echo $patient->gender; ?>  </span>
						</div>
					</li>
<?php
 }
}else{
$str .='<li class="odd">';
$str .='<img src="'.base_url("img/no_record.gif").'" alt="" />';
$str .='</li>';
}
	

}

elseif($_POST['form_type']== 'professional_search')
{
    $this->session->set_userdata('search_keyword2',$_POST['keyword']);
	
	 $key= $_POST['keyword'];
	
	//$this->data['patients']= $this->patient->getPatients($pid);
	$this->data['professionals']= $this->user->searchProfessionals($key,$careid);

$str='';
if(count($this->data['professionals'])>0){
foreach($this->data['professionals'] as $professional){ 
  //$mps="select * from professional_networks where sid='$careid' and rid='$pf'";
  /* $mps="select * from professional_networks where  (sid='$careid' or rid='$careid') and  (sid='$pf' or rid='$pf')  ";
  $me=mysql_query($mps) or die(mysql_error());
  $mr=mysql_fetch_array($me);
  $mn1=mysql_num_rows($me);
  $mn2=mysql_num_rows($me);
  $send=$mr['netstatus'];*/
  //if($send!=1)
  //{
  ?>
<li class="odd">
<?php   //echo  $pf=$professional->status;   ?>
<div class="img-thumb">
<?php if(!empty($professional->photo)) { ?>
<img src="<?php echo base_url(); ?>uploads/<?php echo $professional->photo; ?>" alt="pat img" width="64" height="64" />
<?php } else { ?>
<img src="<?php echo base_url(); ?>img/img-holder2.jpg" alt="pat img"  width="60" height="60"/>  
<?php } ?>
</div>
<div class="pat-desc-cont">
<h5><a href="<?php echo base_url() ?>caregiver/professionaldetails/<?php echo $professional->pfid; ?>"><?php echo $professional->name; ?></a></h5>
<p><?php //echo $professional->surname; ?> -  <?php echo $professional->age; ?>
<span><?php echo $professional->gender; ?>
<?php if( $professional->netstatus!=1 and $professional->netstatus!=0 ) {} 
if($professional->netstatus==0 and  $professional->netstatus!=NULL ) { ?> 
<font color="#FF0000"> Pending</font>  
<?php }
else
{
	if($professional->netstatus!=1)
	{
 ?>
 <h6 id="pf<?php echo $professional->id; ?>"><a href="javascript:void(0)" onclick="sendrequest('<?php echo $cid; ?>','<?php echo $professional->id; ?>')"> <img src="<?php echo base_url(); ?>img/add-icn.png" alt="Add"> </a> <?php 
	} }  ?>
 <?php if($professional->netstatus==1 ) { ?>  </h6>
 <div class="icns">
<a href="javascript:void(0)" onclick="javascript:chatWith('<?php echo $professional->pfid; ?>','<?php echo $professional->name; ?>','<?php echo $this->session->userdata('usertypeid'); ?>','<?php echo $name; ?>','<?php echo '2' ?>','<?php echo $photo; ?>')"><img src="<?php echo base_url(); ?>img/msg-icn2.png" alt="MSG" /></a>
<a href="#"><img src="<?php echo base_url(); ?>img/vid-icn.png" alt="MSG" /></a>
</div> <?php } ?></span>
</p>
</div>
</li>
  <?php
//}
  }
}else{
$str .='<li class="odd">';
$str .='<img src="'.base_url("img/no_record.gif").'" alt="" />';
$str .='</li>';
}

}
//echo 'welcome !';	 
echo $str;	 
   }
	}
	
	function professionaldetails($id=null)
	{
	$email=$this->session->userdata('owner_care');
$this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
  
  
  //$pid=$this->data['caregiver'][0]->pat_id;
  $pid=$this->session->userdata('pid');
  
  $cid1=$this->data['caregiver'][0]->id;
  
$this->data['patient']=$this->caregiver_model->get_patient($pid);
$this->data['caregivers']= $this->caregiver_model->getcaregivers_new($pid,$cid1);
	$this->data['profetional']=$this->caregiver_model->getprofetionals($pid,$cid1);
	$this->data['menu']="diets";
  
  // user
	 $this->data['professionals'] = $this->user->getProfessionals_by_id($id);
	 
	 
     $this->load->view('home/professionaldetails1', $this->data);
	 
	 
	 
	 
	}
	function patientdetails($id=null)
	{
	$email=$this->session->userdata('owner_care');
$this->data['caregiver'] = $this->caregiver_model->get_caregiver($email);
		
		$this->data['user'] = $this->user->getCaregivers_by_id($id);
		$this->load->view('home/caregiverdetails', $this->data);

	}
	}
