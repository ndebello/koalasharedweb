<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,  user-scalable=0" />
<title>Koala | Embrace your needs</title>

<!-- Bootstrap -->
<link href="css/bootstrap.css" rel="stylesheet">
<!--<link href="css/bootstrap-theme.css" rel="stylesheet">-->
<link href="css/style.css" rel="stylesheet">
<link href="css/res.css" rel="stylesheet">
<link href="css/bootstrap-select.css" rel="stylesheet">


<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<input type="hidden" id="user_email"  value="<?php echo $_REQUEST['user_email'];?>" >
<input type="hidden" id="user_id"  value="<?php echo $_REQUEST['user_id'];?>" >
<input type="hidden" id="user_disp_name"  value="<?php echo $_REQUEST['user_disp_name'];?>" >
<input type="hidden" id="licence"  value="<?php echo $_REQUEST['licence'];?>" >
<!------Header------>
<div class="header">
  <div class="container">
    <nav class="navbar navbar-default">
      <div class="container-fluid"> 
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header"> 
          <!-- <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span>  <span class="glyphicon"><img src="img/helpicon.png" alt="help"> </span></button>-->
           <div class="row">
            <div class="col-lg-4 col-md-4 col-xs-3">
            <!--<a href="#" class="logo">Koala</a>--></div>
            <!-- <div class="col-lg-4 col-md-4  col-xs-6 text-left no-padding"> </div>
            <div class="col-lg-4 col-md-3  col-xs-2"> <a href="index.html" class="pull-right">
              <div class="nmber"><span class="badge"><span class="glyphicon glyphicon-arrow-right"></span></span> </div>
              </a> </div>--> 
          </div>
        </div>
        
        <!-- Collect the nav links, forms, and other content for toggling --> 
        <!--<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li><a href="howitworks.html">How it works</a></li>
            <li><a href="req-info.html">Request Information</a></li>
          </ul>
        </div>--> 
        <!-- /.navbar-collapse --> 
      </div>
      <!-- /.container-fluid --> 
    </nav>
  </div>
</div>
<div class="clearfix"></div>
<div class="bodywrap">
  <div class="container">
    <div class="success-cont" id="waiting">
      <p><span class="green-txt" id="success_msg">Please Wait For Confirmation.</span><br>
        <br>
      </p>
     
      <div class="clearfix"></div>
    </div>
	
	 <div class="success-cont" id="done" style="display:none;">
      <p><span class="green-txt" id="success_msg">Payment Completed Successfully!!</span><br>
	  <span class="green-txt" id="success_msg">Mail Sent To Registered Mail.</span>
        <br>
      </p>
      <a href="javascript: history.go(-(history.length - 1));" class="btn btn-success">Click here to Continue</a>
      <div class="clearfix"></div>
    </div>
  </div>
</div>
<div class="clearfix"></div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="js/jquery.min.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="js/bootstrap.min.js"></script> 
	<script type="text/javascript" src="local-notification.js"></script>
<script src="js/bootstrap-select.js"></script><script type="text/javascript" charset="utf-8" src="cordova.js"></script> 



<script>
$(document).ready(function() { //window.localStorage.clear(); window.plugin.notification.local.cancelAll();
		
	var user_email = $("#user_email").val();//localStorage.getItem("user_email");
	
	var user_id = $("#user_id").val();//localStorage.getItem("user_id");
	var licence = $("#licence").val();
	var user_disp_name = $("#user_disp_name").val();//localStorage.getItem("user_disp_name");
	//alert(pay_id+"$$"+user_email+"$$"+user_type+"$$"+user_id+"$$"+user_disp_name);
	//alert(globalSiteServices);
	$.post(globalSiteServices+'upgrade_licence',{user_email: user_email, user_id: user_id,user_disp_name:user_disp_name,licence:licence },function( data ){
		//alert(data);
		var obj = jQuery.parseJSON(data);

				
				if(obj.result==="success"){
					$("#waiting").hide();
					$("#done").show();
					
					var my_time = setTimeout(function(){
						history.go(-(history.length - 1));
						},5000);
					
				} else {
					alert("Payment Recived. Problem with mail sending. Contact us.");
					history.go(-(history.length - 1));
				}
				
	
	});
			
	
	
	
	
});
</script> 

</body>
</html>